function cadvice() {
  var name = '#wts-search-box';
  var advice_name = '#wts-search-advice-wrapper';
  var parent_name = '#wts-search-form';
  var suggest_count            = 0;
  var suggest_selected         = 0;
  this.myname = function () {  // Public Method
    return name;
  };
  this.disposition = function(){
    var parent = $(name).offset();
    $(advice_name).offset({top:parent.top + $(name).outerHeight(), left:parent.left});
  }
  this.key_activate = function(n){
    $(advice_name + ' li').eq(suggest_selected - 1).removeClass('active');
   
    if(n == 1 && suggest_selected < suggest_count){suggest_selected++;}
    else if(n == -1 && suggest_selected > 0){suggest_selected--;}

    if(suggest_selected > 0){
      $(advice_name + ' li').eq(suggest_selected-1).addClass('active');
    }
  }
  // определяем какие действия нужно делать при нажатии на клавиатуру
  this.keyup = function(I){
    switch(I.keyCode){
      // игнорируем нажатия на эти клавишы
      case 13:  // enter
      case 27:  // escape
      case 38:  // стрелка вверх
      case 40:  // стрелка вниз
      break;
      default:
        // производим поиск только при вводе более 2х символов
        if($(name).val().length > 2){
          var input_initial_value = $(name).val();
          // производим AJAX запрос к /ajax/ajax.php, передаем ему GET query, в который мы помещаем наш запрос
          $.get('advice/csearch.php', {'q' : input_initial_value}, function(data){
            // перед показом слоя подсказки, его обнуляем
            $(advice_name).html('').show();
            
            //php скрипт возвращает нам строку, ее надо распарсить в массив.
            // возвращаемые данные: ['test','test 1','test 2','test 3']
            var list = eval('(' + data + ')');
            suggest_count = list.length;
            if(suggest_count > 0){
              for(var i in list){
                // добавляем слою позиции
                $(advice_name).append('<li class="wts-advice-variant">'+list[i]+'</li>');
              }
            }
            else{$(advice_name).hide();}
          }, 'html');
        }
        else{$(advice_name).hide().html('');}
      break;
    }
  }
  //считываем нажатие клавиш, уже после вывода подсказки
  this.keydown = function(I){
    switch(I.keyCode){
      
      // по нажатию клавиш прячем подсказку
      case 13: // enter
        if(suggest_selected > 0){
          var str = $(advice_name + ' li').eq(suggest_selected-1).text();
          var ar = str.split('<');
          str = ar[(ar.length - 1)];
          str = str.substr(0, str.length - 1);

          $(name).val(str);
          $(advice_name).hide();
          $(parent_name).submit();
        }
      break;
      case 27: // escape
        $(advice_name).hide();
        suggest_selected = 0;
        return false;
      break;
      // делаем переход по подсказке стрелочками клавиатуры
      case 38: // стрелка вверх
      case 40: // стрелка вниз
        I.preventDefault();
        if(suggest_count){this.key_activate( I.keyCode-39 );}
      break;
    }
  }
  // делаем обработку клика по подсказке
  this.advice_click = function(I){
    if('#' + (I.parent().attr('id')) == advice_name){
      var str = I.text();
      var ar = str.split('<');
      str = ar[(ar.length - 1)];
      str = str.substr(0, str.length - 1);
      
      $(name).val(str);
      // прячем слой подсказки
      $(advice_name).html('');
      suggest_count = suggest_selected = 0;
      $(parent_name).submit();
    }
  }
  // делаем обработку клика вне элемента
  this.click_out = function(){
    $(advice_name + ' li').eq(suggest_selected-1).removeClass('active');
    suggest_selected = 0;
    $(advice_name).hide();
  }
  // если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
  this.click_in = function(){
    if($(advice_name + ' li').length > 0)
      $(advice_name).show();
  }
}

function gadvice() {
  var name = '#wts-search-box-to';
  var advice_name = '#wts-search-advice-wrapper-to';
  var suggest_count            = 0;
  var suggest_selected         = 0;
  this.myname = function () {  // Public Method
    return name;
  };
  this.disposition = function(){
    var parent = $(name).offset();
    $(advice_name).offset({top:parent.top + $(name).outerHeight(), left:parent.left});
  }
  this.key_activate = function(n){
    $(advice_name + ' li').eq(suggest_selected - 1).removeClass('active');
   
    if(n == 1 && suggest_selected < suggest_count){suggest_selected++;}
    else if(n == -1 && suggest_selected > 0){suggest_selected--;}

    if(suggest_selected > 0){
      $(advice_name + ' li').eq(suggest_selected-1).addClass('active');
    }
  }
  // определяем какие действия нужно делать при нажатии на клавиатуру
  this.keyup = function(I){
    switch(I.keyCode){
      // игнорируем нажатия на эти клавишы
      case 13:  // enter
      case 27:  // escape
      case 38:  // стрелка вверх
      case 40:  // стрелка вниз
      break;
      default:
        // производим поиск только при вводе более 2х символов
        if($(name).val().length > 2){
          var input_initial_value = $(name).val();
          // производим AJAX запрос к /ajax/ajax.php, передаем ему GET query, в который мы помещаем наш запрос
          $.get('advice/gsearch.php', {'q' : input_initial_value}, function(data){
            // перед показом слоя подсказки, его обнуляем
            $(advice_name).html('').show();
            
            //php скрипт возвращает нам строку, ее надо распарсить в массив.
            // возвращаемые данные: ['test','test 1','test 2','test 3']
            var list = eval('(' + data + ')');
            suggest_count = list.length;
            if(suggest_count > 0){
              for(var i in list){
                // добавляем слою позиции
                $(advice_name).append('<li class="wts-advice-variant">'+list[i]+'</li>');
              }
            }
            else{$(advice_name).hide();}
          }, 'html');
        }
        else{$(advice_name).hide().html('');}
      break;
    }
  }
  //считываем нажатие клавиш, уже после вывода подсказки
  this.keydown = function(I){
    switch(I.keyCode){
      
      // по нажатию клавиш прячем подсказку
      case 13: // enter
        if(suggest_selected > 0){
          var str = $(advice_name + ' li').eq(suggest_selected-1).text();
          $(name).val(str);
          $(advice_name).hide().html('');
          suggest_count = suggest_selected = 0;
        }
        return false;
      break;
      case 27: // escape
        $(advice_name).hide();
        suggest_selected = 0;
        return false;
      break;
      // делаем переход по подсказке стрелочками клавиатуры
      case 38: // стрелка вверх
      case 40: // стрелка вниз
        I.preventDefault();
        if(suggest_count){this.key_activate( I.keyCode-39 );}
      break;
    }
  }
  // делаем обработку клика по подсказке
  this.advice_click = function(I){
    if('#' + (I.parent().attr('id')) == advice_name){
      $(name).val(I.text());
      // прячем слой подсказки
      $(advice_name).html('');
      suggest_count = suggest_selected = 0;
    }
  }
  // делаем обработку клика вне элемента
  this.click_out = function(){
    $(advice_name + ' li').eq(suggest_selected-1).removeClass('active');
    suggest_selected = 0;
    $(advice_name).hide();
  }
  // если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
  this.click_in = function(){
    if($(advice_name + ' li').length > 0)
      $(advice_name).show();
  }
}

function qadvice() {
  var name = '#wts-search-box-cc';
  var advice_name = '#wts-search-advice-wrapper-cc';
  var suggest_count            = 0;
  var suggest_selected         = 0;
  this.myname = function () {  // Public Method
    return name;
  };
  this.disposition = function(){
    var parent = $(name).offset();
    $(advice_name).offset({top:parent.top + $(name).outerHeight(), left:parent.left});
  }
  this.key_activate = function(n){
    $(advice_name + ' li').eq(suggest_selected - 1).removeClass('active');
   
    if(n == 1 && suggest_selected < suggest_count){suggest_selected++;}
    else if(n == -1 && suggest_selected > 0){suggest_selected--;}

    if(suggest_selected > 0){
      $(advice_name + ' li').eq(suggest_selected-1).addClass('active');
    }
  }
  // определяем какие действия нужно делать при нажатии на клавиатуру
  this.keyup = function(I){
    switch(I.keyCode){
      // игнорируем нажатия на эти клавишы
      case 13:  // enter
      case 27:  // escape
      case 38:  // стрелка вверх
      case 40:  // стрелка вниз
      break;
      default:
        // производим поиск только при вводе более 2х символов
        if($(name).val().length > 2){
          var input_initial_value = $(name).val();
          // производим AJAX запрос к /ajax/ajax.php, передаем ему GET query, в который мы помещаем наш запрос
          $.get('advice/qsearch.php', {'q' : input_initial_value}, function(data){
            // перед показом слоя подсказки, его обнуляем
            $(advice_name).html('').show();
            
            //php скрипт возвращает нам строку, ее надо распарсить в массив.
            // возвращаемые данные: ['test','test 1','test 2','test 3']
            var list = eval('(' + data + ')');
            suggest_count = list.length;
            if(suggest_count > 0){
              for(var i in list){
                // добавляем слою позиции
                $(advice_name).append('<li class="wts-advice-variant">'+list[i]+'</li>');
              }
            }
            else{$(advice_name).hide();}
          }, 'html');
        }
        else{$(advice_name).hide().html('');}
      break;
    }
  }
  //считываем нажатие клавиш, уже после вывода подсказки
  this.keydown = function(I){
    switch(I.keyCode){
      
      // по нажатию клавиш прячем подсказку
      case 13: // enter
        if(suggest_selected > 0){
          var str = $(advice_name + ' li').eq(suggest_selected-1).text();
          $(name).val(str);
          $(advice_name).hide().html('');
          suggest_count = suggest_selected = 0;
        }
        return false;
      break;
      case 27: // escape
        $(advice_name).hide();
        suggest_selected = 0;
        return false;
      break;
      // делаем переход по подсказке стрелочками клавиатуры
      case 38: // стрелка вверх
      case 40: // стрелка вниз
        I.preventDefault();
        if(suggest_count){this.key_activate( I.keyCode-39 );}
      break;
    }
  }
  // делаем обработку клика по подсказке
  this.advice_click = function(I){
    if('#' + (I.parent().attr('id')) == advice_name){
      $(name).val(I.text());
      // прячем слой подсказки
      $(advice_name).html('');
      suggest_count = suggest_selected = 0;
    }
  }
  // делаем обработку клика вне элемента
  this.click_out = function(){
    $(advice_name + ' li').eq(suggest_selected-1).removeClass('active');
    suggest_selected = 0;
    $(advice_name).hide();
  }
  // если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
  this.click_in = function(){
    if($(advice_name + ' li').length > 0)
      $(advice_name).show();
  }
}

//работаем только после загрузки
$(window).load(function(){
  var c = new cadvice();
  c.disposition();
  $(c.myname()).keyup(function(I){c.keyup(I);});
  $(c.myname()).keydown(function(I){return c.keydown(I);});
  /*$(c.myname()).focusout(function(I){return c.click_out();});*/

  var g;
  var q;
  
  if($('#wts-search-box-to').length){
    g = new gadvice();
  }
  if($('#wts-search-box-cc').length){
    q = new qadvice();
  }
  
  if(g !== undefined){
    g.disposition();
    $(g.myname()).keyup(function(I){g.keyup(I);});
    $(g.myname()).keydown(function(I){return g.keydown(I);});
    /*$(g.myname()).focusout(function(I){return g.click_out();});*/
    $(g.myname()).click(function(event){
      g.click_in();
      c.click_out();
      if(q !== undefined)q.click_out();
      event.stopPropagation();
    });
  }
  
  if(q !== undefined){
    q.disposition();
    $(q.myname()).keyup(function(I){q.keyup(I);});
    $(q.myname()).keydown(function(I){return q.keydown(I);});
    /*$(q.myname()).focusout(function(I){return q.click_out();});*/
    $(q.myname()).click(function(event){
      q.click_in();
      c.click_out();
      g.click_out();
      event.stopPropagation();
    });
  }

  $(c.myname()).click(function(event){
    c.click_in();
    if(g !== undefined)g.click_out();
    if(q !== undefined)q.click_out();
    event.stopPropagation();
  });
  $(function(){
    $(document).on('click', '.wts-advice-variant', function(){
      c.advice_click($(this));
      if(g !== undefined)g.advice_click($(this));
      if(q !== undefined)q.advice_click($(this));
    })
  });
  $('html').click(function(){
    c.click_out();
    if(g !== undefined)g.click_out();
    if(q !== undefined)q.click_out();
  });
});
