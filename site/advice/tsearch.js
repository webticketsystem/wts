function advice(name_, getter_){
  var getter = 'advice/' + getter_;
  var name = '#wts-search-box-' + name_;
  var advice_name = '#wts-search-advice-wrapper-' + name_;
  var suggest_count            = 0;
  var suggest_selected         = 0;
  var input_initial_value      = '';
  this.myname = function () {  // Public Method
    return name;
  }
  this.disposition = function(){
    var parent = $(name).offset();
    $(advice_name).offset({top:parent.top + $(name).outerHeight(), left:parent.left});
  }
  this.key_activate = function(n){
    $(advice_name + ' li').eq(suggest_selected - 1).removeClass('active');
   
    if(n == 1 && suggest_selected < suggest_count){suggest_selected++;}
    else if(n == -1 && suggest_selected > 0){suggest_selected--;}

    if(suggest_selected > 0){
      $(advice_name + ' li').eq(suggest_selected-1).addClass('active');
    }
  }
  // определяем какие действия нужно делать при нажатии на клавиатуру
  this.keyup = function(I){
    switch(I.keyCode){
      // игнорируем нажатия на эти клавишы
      case 13:  // enter
      case 27:  // escape
      case 38:  // стрелка вверх
      case 40:  // стрелка вниз
      break;
      default:
        // производим поиск только при вводе более 2х символов
        if($(name).val().length > 2){
          var input_initial_value = $(name).val();
          // производим AJAX запрос к /ajax/ajax.php, передаем ему GET query, в который мы помещаем наш запрос
          $.get(getter, {'q' : input_initial_value}, function(data){
            // перед показом слоя подсказки, его обнуляем
            $(advice_name).html('').show();
            
            //php скрипт возвращает нам строку, ее надо распарсить в массив.
            // возвращаемые данные: ['test','test 1','test 2','test 3']
            var list = eval('(' + data + ')');
            suggest_count = list.length;
            if(suggest_count > 0){
              for(var i in list){
                // добавляем слою позиции
                $(advice_name).append('<li class="wts-advice-variant">'+list[i]+'</li>');
              }
            }
            else{$(advice_name).hide();}
          }, 'html');
        }
        else{$(advice_name).hide().html('');}
      break;
    }
  }
  //считываем нажатие клавиш, уже после вывода подсказки
  this.keydown = function(I){
    switch(I.keyCode){
      
      // по нажатию клавиш прячем подсказку
      case 13: // enter
        if(suggest_selected > 0){
          var str = $(advice_name + ' li').eq(suggest_selected-1).text();
          var ar = str.split('<');
          str = ar[(ar.length - 1)];
          str = str.substr(0, str.length - 1);

          $(name).val(str);
          
          $(advice_name).hide().html('');
          suggest_count = suggest_selected = 0;
        }
        return false;
      break;
      case 27: // escape
        $(advice_name).hide();
        suggest_selected = 0;
        return false;
      break;
      // делаем переход по подсказке стрелочками клавиатуры
      case 38: // стрелка вверх
      case 40: // стрелка вниз
        I.preventDefault();
        if(suggest_count){this.key_activate( I.keyCode-39 );}
      break;
    }
  }
  // делаем обработку клика по подсказке
  this.advice_click = function(I){
    if('#' + (I.parent().attr('id')) == advice_name){
      var str = I.text();
      var ar = str.split('<');
      str = ar[(ar.length - 1)];
      str = str.substr(0, str.length - 1);

      $(name).val(str);

      // прячем слой подсказки
      $(advice_name).html('');
      suggest_count = suggest_selected = 0;
    }
  }
  // делаем обработку клика вне элемента
  this.click_out = function(){
    $(advice_name + ' li').eq(suggest_selected-1).removeClass('active');
    suggest_selected = 0;
    $(advice_name).hide();
  }
  // если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
  this.click_in = function(){
    if($(advice_name + ' li').length > 0)
      $(advice_name).show();
  }
}

function gadvice(name_, getter_){
  var getter = 'advice/' + getter_;
  var name = '#wts-search-box-' + name_;
  var advice_name = '#wts-search-advice-wrapper-' + name_;
  var suggest_count            = 0;
  var suggest_selected         = 0;
  var input_initial_value      = '';
  this.myname = function () {  // Public Method
    return name;
  };
  this.disposition = function(){
    var parent = $(name).offset();
    $(advice_name).offset({top:parent.top + $(name).outerHeight(), left:parent.left});
  }
  this.key_activate = function(n){
    $(advice_name + ' li').eq(suggest_selected - 1).removeClass('active');
   
    if(n == 1 && suggest_selected < suggest_count){suggest_selected++;}
    else if(n == -1 && suggest_selected > 0){suggest_selected--;}

    if(suggest_selected > 0){
      $(advice_name + ' li').eq(suggest_selected-1).addClass('active');
    }
  }
  // определяем какие действия нужно делать при нажатии на клавиатуру
  this.keyup = function(I){
    switch(I.keyCode){
      // игнорируем нажатия на эти клавишы
      case 13:  // enter
      case 27:  // escape
      case 38:  // стрелка вверх
      case 40:  // стрелка вниз
      break;
      default:
        // производим поиск только при вводе более 2х символов
        if($(name).val().length > 2){
          var input_initial_value = $(name).val();
          // производим AJAX запрос к /ajax/ajax.php, передаем ему GET query, в который мы помещаем наш запрос
          $.get(getter, {'q' : input_initial_value}, function(data){
            // перед показом слоя подсказки, его обнуляем
            $(advice_name).html('').show();
            
            //php скрипт возвращает нам строку, ее надо распарсить в массив.
            // возвращаемые данные: ['test','test 1','test 2','test 3']
            var list = eval('(' + data + ')');
            suggest_count = list.length;
            if(suggest_count > 0){
              for(var i in list){
                // добавляем слою позиции
                $(advice_name).append('<li class="wts-advice-variant">'+list[i]+'</li>');
              }
            }
            else{$(advice_name).hide();}
          }, 'html');
        }
        else{$(advice_name).hide().html('');}
      break;
    }
  }
  //считываем нажатие клавиш, уже после вывода подсказки
  this.keydown = function(I){
    switch(I.keyCode){
      
      // по нажатию клавиш прячем подсказку
      case 13: // enter
        if(suggest_selected > 0){
          var str = $(advice_name + ' li').eq(suggest_selected-1).text();
          $(name).val(str);
          $(advice_name).hide().html('');
          suggest_count = suggest_selected = 0;
        }
        return false;
      break;
      case 27: // escape
        $(advice_name).hide();
        suggest_selected = 0;
        return false;
      break;
      // делаем переход по подсказке стрелочками клавиатуры
      case 38: // стрелка вверх
      case 40: // стрелка вниз
        I.preventDefault();
        if(suggest_count){this.key_activate( I.keyCode-39 );}
      break;
    }
  }
  // делаем обработку клика по подсказке
  this.advice_click = function(I){
    if('#' + (I.parent().attr('id')) == advice_name){
      $(name).val(I.text());
      // прячем слой подсказки
      $(advice_name).html('');
      suggest_count = suggest_selected = 0;
    }
  }
  // делаем обработку клика вне элемента
  this.click_out = function(){
    $(advice_name + ' li').eq(suggest_selected-1).removeClass('active');
    suggest_selected = 0;
    $(advice_name).hide();
  }
  // если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
  this.click_in = function(){
    if($(advice_name + ' li').length > 0)
      $(advice_name).show();
  }
}

//работаем только после загрузки
$(window).load(function(){
  var cnt1;
  var cnt2;
  var grp1;
  var grp2;
  if($('#wts-search-box-cnt1').length){
    cnt1 = new advice('cnt1', 'csearch.php');
  }
  if($('#wts-search-box-cnt2').length){
    cnt2 = new advice('cnt2', 'csearch.php');
  }
  if($('#wts-search-box-grp1').length){
    grp1 = new gadvice('grp1', 'gsearch.php', false);
  }
  if($('#wts-search-box-grp2').length){
    grp2 = new gadvice('grp2', 'gsearch.php', false);
  }

  if(cnt1 !== undefined){
    cnt1.disposition();
    $(cnt1.myname()).keyup(function(I){cnt1.keyup(I);});
    $(cnt1.myname()).keydown(function(I){return cnt1.keydown(I);});
    $(cnt1.myname()).click(function(event){
      cnt1.click_in();
      if(cnt2 !== undefined)cnt2.click_out();
      if(grp2 !== undefined)grp2.click_out();
      event.stopPropagation();
    });
  }
  if(cnt2 !== undefined){
    cnt2.disposition();
    $(cnt2.myname()).keyup(function(I){cnt2.keyup(I);});
    $(cnt2.myname()).keydown(function(I){return cnt2.keydown(I);});
    $(cnt2.myname()).click(function(event){
      cnt2.click_in();
      if(cnt1 !== undefined)cnt1.click_out();
      if(grp1 !== undefined)grp1.click_out();
      event.stopPropagation();
    });
  }
  if(grp1 !== undefined){
    grp1.disposition();
    $(grp1.myname()).keyup(function(I){grp1.keyup(I);});
    $(grp1.myname()).keydown(function(I){return grp1.keydown(I);});
    $(grp1.myname()).click(function(event){
      grp1.click_in();
      if(cnt2 !== undefined)cnt2.click_out();
      if(grp2 !== undefined)grp2.click_out();
      event.stopPropagation();
    });
  }
  if(grp2 !== undefined){
    grp2.disposition();
    $(grp2.myname()).keyup(function(I){grp2.keyup(I);});
    $(grp2.myname()).keydown(function(I){return grp2.keydown(I);});
    $(grp2.myname()).click(function(event){
      grp2.click_in();
      if(cnt1 !== undefined)cnt1.click_out();
      if(grp1 !== undefined)grp1.click_out();
      event.stopPropagation();
    });
  }
  
  // читаем ввод с клавиатуры
  // делаем обработку клика по подсказке
  $(function(){
    $(document).on('click', '.wts-advice-variant', function(){
      if(cnt1 !== undefined)cnt1.advice_click($(this));
      if(cnt2 !== undefined)cnt2.advice_click($(this));
      if(grp1 !== undefined)grp1.advice_click($(this));
      if(grp2 !== undefined)grp2.advice_click($(this));
    })
  });
  // если кликаем в любом месте сайта, нужно спрятать подсказку
  $('html').click(function(){
    if(cnt1 !== undefined)cnt1.click_out();
    if(cnt2 !== undefined)cnt2.click_out();
    if(grp1 !== undefined)grp1.click_out();
    if(grp2 !== undefined)grp2.click_out();
  });

});
