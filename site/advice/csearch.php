<?php
/*
 * csearch.php (part of WTS) - ajax search for contacts
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('../include/defs.php');
  require_once('../include/res.php');
  require_once('../include/core/utils/db.php');
  require_once('../include/core/cont.php');

  try{
    if(isset($_GET[WTS_PARAM_SEARCH])
    && mb_strlen($_GET[WTS_PARAM_SEARCH], WTS_ENC) > 2){
      if(!session_id())session_start();
      
      $cDb = new cDb(WTS_DB_NAME, WTS_DB_USER, WTS_DB_PASS, WTS_DB_TABLE_PREFIX);
      
      //validate session by guid and timeout
      if(isset($_SESSION[WTS_SESSION_PARAM_LOGIN]) 
      && isset($_SESSION[WTS_SESSION_PARAM_HASH])
      && ($cCont = cCont::mCheckSsn($cDb, $_SESSION[WTS_SESSION_PARAM_LOGIN], $_SESSION[WTS_SESSION_PARAM_HASH], WTS_SESSION_TIMEOUT))
      && $cCont->iType() !== cCont::CSTMR){
        $aConts = false;
        /*cCont::bAddFilterDisabled(false);*/
        if(isset($_GET[WTS_PARAM_TYPE]) && (int)$_GET[WTS_PARAM_TYPE] === cCont::AGENT){
          cCont::bAddFilterQueue(-1);
        }
        
        
        cCont::bAddFilterPartValue($cDb, $_GET[WTS_PARAM_SEARCH]);
        if(cCont::iCount($cDb) > 0
        && cCont::iUnserialize($cDb, 1, WTS_ROW_PER_ADVICE, $aConts) > 0){
          $sAdvise = false;
          foreach($aConts as &$c){
            if($sAdvise){$sAdvise .= '\',\'';}
            else{$sAdvise = '[\'';}
            $sAdvise .= htmlentities(($c->sName() ? $c->sName() . ' <' : '<') . $c->sLogin() . '>');
          }
          echo $sAdvise . '\']';
          //echo str_replace('/', '\/', $sAdvise) . '\']';
          unset($aConts);
        }
        else{echo '[]';}
        unset($cCont);
      }
      else{header(WTS_HTTP_401);}
      unset($cDb);
    }
  }
  catch (\Exception $e){
    echo 'Exception ' . $e->GetCode() . ': ' . $e->GetMessage() . cString::EOL . 'In file: ' . basename($e->GetFile()) . cString::EOL . 'line: ' . $e->GetLine() . cString::EOL;
    exit;
  }

}
?>
