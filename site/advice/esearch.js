function advice(name_, getter_, single_){
  if (single_ === undefined) {
    single_ = true;
  }
  var getter = 'advice/' + getter_;
  var single = single_;
  var name = '#wts-search-box-' + name_;
  var advice_name = '#wts-search-advice-wrapper-' + name_;
  var suggest_count            = 0;
  var suggest_selected         = 0;
  var input_initial_value      = '';
  this.myname = function () {  // Public Method
    return name;
  };
  this.disposition = function(){
    var parent = $(name).offset();
    $(advice_name).offset({top:parent.top + $(name).outerHeight(), left:parent.left});
  }
  this.key_activate = function(n){
    $(advice_name + ' li').eq(suggest_selected - 1).removeClass('active');
   
    if(n == 1 && suggest_selected < suggest_count){suggest_selected++;}
    else if(n == -1 && suggest_selected > 0){suggest_selected--;}

    if(suggest_selected > 0){
      $(advice_name + ' li').eq(suggest_selected-1).addClass('active');
    }
  }
  // определяем какие действия нужно делать при нажатии на клавиатуру
  this.keyup = function(I){
    switch(I.keyCode){
      // игнорируем нажатия на эти клавишы
      case 13:  // enter
      case 27:  // escape
      case 38:  // стрелка вверх
      case 40:  // стрелка вниз
      break;
      default:
        //проверим строку на наличие или отсутсвие адреса (разделители ; или , )
        var full_str = $(name).val();
        if(full_str.lastIndexOf(',') > -1 || full_str.lastIndexOf(';') > -1){
          full_str = full_str.replace(';',','); //(mail1@mail.com,mail2@mail.com,mail3@mail.com)
          var ar = full_str.split(',');
          full_str = ar[(ar.length - 1)];
          full_str = $.trim(full_str);
        }
        // производим поиск только при вводе более 2х символов
        if(full_str.length > 2){
          this.input_initial_value = $(name).val();
          // производим AJAX запрос к /ajax/ajax.php, передаем ему GET query, в который мы помещаем наш запрос
          $.get(getter, {'q' : full_str}, function(data){
            // перед показом слоя подсказки, его обнуляем
            $(advice_name).html('').show();
            
            //php скрипт возвращает нам строку, ее надо распарсить в массив.
            // возвращаемые данные: ['test','test 1','test 2','test 3']
            var list = eval('(' + data + ')');
            suggest_count = list.length;
            if(suggest_count > 0){
              for(var i in list){
                // добавляем слою позиции
                $(advice_name).append('<li class="wts-advice-variant">'+list[i]+'</li>');
              }
            }
            else{$(advice_name).hide();}
          }, 'html');
        }
        else{$(advice_name).hide().html('');}
      break;
    }
  }
  //считываем нажатие клавиш, уже после вывода подсказки
  this.keydown = function(I){
    switch(I.keyCode){
      
      // по нажатию клавиш прячем подсказку
      case 13: // enter
        if(suggest_selected > 0){
          var str = $(advice_name + ' li').eq(suggest_selected-1).text();
          var ar = str.split('<');
          str = ar[(ar.length - 1)];
          str = str.substr(0, str.length - 1);
          ///
          var full_str = this.input_initial_value;
          
          if(full_str.lastIndexOf(',') > -1 || full_str.lastIndexOf(';') > -1){
            full_str = full_str.replace(';',',');
            full_str = full_str.substr(0, full_str.lastIndexOf(',') + 1) + ' ';
          }
          else full_str = '';

          if(this.single){$(name).val(str);}
          else{$(name).val(full_str + str);}
          $(advice_name).hide().html('');
          suggest_count = suggest_selected = 0;
        }
        return false;
      break;
      case 27: // escape
        $(advice_name).hide();
        suggest_selected = 0;
        return false;
      break;
      // делаем переход по подсказке стрелочками клавиатуры
      case 38: // стрелка вверх
      case 40: // стрелка вниз
        I.preventDefault();
        if(suggest_count){this.key_activate( I.keyCode-39 );}
      break;
    }
  }
  // делаем обработку клика по подсказке
  this.advice_click = function(I){
    if('#' + (I.parent().attr('id')) == advice_name){
      var str = I.text();
      var ar = str.split('<');
      str = ar[(ar.length - 1)];
      str = str.substr(0, str.length - 1);
      
      var full_str = this.input_initial_value;
      if(full_str.lastIndexOf(',') > -1 || full_str.lastIndexOf(';') > -1){
        full_str = full_str.replace(';',',');
        full_str = full_str.substr(0, full_str.lastIndexOf(',') + 1) + ' ';
      }
      else full_str = '';

      if(this.single){$(name).val(str);}
      else{$(name).val(full_str + str);}
      /*$(name).val(full_str + str);*/
      // прячем слой подсказки
      $(advice_name).html('');
      suggest_count = suggest_selected = 0;
    }
  }
  // делаем обработку клика вне элемента
  this.click_out = function(){
    $(advice_name + ' li').eq(suggest_selected-1).removeClass('active');
    suggest_selected = 0;
    $(advice_name).hide();
  }
  // если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
  this.click_in = function(){
    if($(advice_name + ' li').length > 0)
      $(advice_name).show();
  }
}

//работаем только после загрузки
$(window).load(function(){
  var cnt;
  var agt;
  var to;
  var cc;
  var bcc;
  if($('#wts-search-box-cnt').length){
    cnt = new advice('cnt', 'csearch.php');
  }
  if($('#wts-search-box-agt').length){
    agt = new advice('agt', 'csearch.php?t=20');
  }
  if($('#wts-search-box-to').length){
    to = new advice('to', 'csearch.php', false);
  }
  if($('#wts-search-box-cc').length){
    cc = new advice('cc', 'csearch.php', false);
  }
  if($('#wts-search-box-bcc').length){
    bcc = new advice('bcc', 'csearch.php', false);
  }
  
  if(cnt !== undefined){
    cnt.disposition();
    $(cnt.myname()).keyup(function(I){cnt.keyup(I);});
    $(cnt.myname()).keydown(function(I){return cnt.keydown(I);});
    $(cnt.myname()).click(function(event){
      cnt.click_in();
      if(agt !== undefined)agt.click_out();
      if(to  !== undefined)to.click_out();
      if(cc  !== undefined)cc.click_out();
      if(bcc !== undefined)bcc.click_out();
      event.stopPropagation();
    });
  }
  if(agt !== undefined){
    agt.disposition();
    $(agt.myname()).keyup(function(I){agt.keyup(I);});
    $(agt.myname()).keydown(function(I){return agt.keydown(I);});
    $(agt.myname()).click(function(event){
      agt.click_in();
      if(cnt !== undefined)cnt.click_out();
      if(to  !== undefined)to.click_out();
      if(cc  !== undefined)cc.click_out();
      if(bcc !== undefined)bcc.click_out();
      event.stopPropagation();
    });
  }
  if(to !== undefined){
    to.disposition();
    $(to.myname()).keyup(function(I){to.keyup(I);});
    $(to.myname()).keydown(function(I){return to.keydown(I);});
    $(to.myname()).click(function(event){
      to.click_in();
      if(cc  !== undefined)cc.click_out();
      if(bcc !== undefined)bcc.click_out();
      if(cnt !== undefined)cnt.click_out();
      if(agt !== undefined)agt.click_out();
      event.stopPropagation();
    });
  }
  if(cc !== undefined){
    cc.disposition();
    $(cc.myname()).keyup(function(I){cc.keyup(I);});
    $(cc.myname()).keydown(function(I){return cc.keydown(I);});
    $(cc.myname()).click(function(event){
      cc.click_in();
      if(to  !== undefined)to.click_out();
      if(bcc !== undefined)bcc.click_out();
      if(cnt !== undefined)cnt.click_out();
      if(agt !== undefined)agt.click_out();
      event.stopPropagation();
    });
  }
  if(bcc !== undefined){
    bcc.disposition();
    $(bcc.myname()).keyup(function(I){bcc.keyup(I);});
    $(bcc.myname()).keydown(function(I){return bcc.keydown(I);});
    $(bcc.myname()).click(function(event){
      bcc.click_in();
      if(to  !== undefined)to.click_out();
      if(cc  !== undefined)cc.click_out();
      if(cnt !== undefined)cnt.click_out();
      if(agt !== undefined)agt.click_out();
      event.stopPropagation();
    });
  }

  // читаем ввод с клавиатуры
  // делаем обработку клика по подсказке
  $(function(){
    $(document).on('click', '.wts-advice-variant', function(){
      if(to  !== undefined)to.advice_click($(this));
      if(cc  !== undefined)cc.advice_click($(this));
      if(bcc !== undefined)bcc.advice_click($(this));
      if(cnt !== undefined)cnt.advice_click($(this));
      if(agt !== undefined)agt.advice_click($(this));
    })
  });
  // если кликаем в любом месте сайта, нужно спрятать подсказку
  $('html').click(function(){
    if(to  !== undefined)to.click_out();
    if(cc  !== undefined)cc.click_out();
    if(bcc !== undefined)bcc.click_out();
    if(cnt !== undefined)cnt.click_out();
    if(agt !== undefined)agt.click_out();
  });

});
