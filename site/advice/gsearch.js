function gadvice() {
  var name = '#wts-search-box';
  var advice_name = '#wts-search-advice-wrapper';
  var parent_name = '#wts-search-form';
  var suggest_count            = 0;
  var suggest_selected         = 0;
  this.myname = function () {  // Public Method
    return name;
  };
  this.disposition = function(){
    var parent = $(name).offset();
    $(advice_name).offset({top:parent.top + $(name).outerHeight(), left:parent.left});
  }
  this.key_activate = function(n){
    $(advice_name + ' li').eq(suggest_selected - 1).removeClass('active');
   
    if(n == 1 && suggest_selected < suggest_count){suggest_selected++;}
    else if(n == -1 && suggest_selected > 0){suggest_selected--;}

    if(suggest_selected > 0){
      $(advice_name + ' li').eq(suggest_selected-1).addClass('active');
    }
  }
  // определяем какие действия нужно делать при нажатии на клавиатуру
  this.keyup = function(I){
    switch(I.keyCode){
      // игнорируем нажатия на эти клавишы
      case 13:  // enter
      case 27:  // escape
      case 38:  // стрелка вверх
      case 40:  // стрелка вниз
      break;
      default:
        // производим поиск только при вводе более 2х символов
        if($(name).val().length > 2){
          var input_initial_value = $(name).val();
          // производим AJAX запрос к /ajax/ajax.php, передаем ему GET query, в который мы помещаем наш запрос
          $.get('advice/gsearch.php', {'q' : input_initial_value}, function(data){
            // перед показом слоя подсказки, его обнуляем
            $(advice_name).html('').show();
            
            //php скрипт возвращает нам строку, ее надо распарсить в массив.
            // возвращаемые данные: ['test','test 1','test 2','test 3']
            var list = eval('(' + data + ')');
            suggest_count = list.length;
            if(suggest_count > 0){
              for(var i in list){
                // добавляем слою позиции
                $(advice_name).append('<li class="wts-advice-variant">'+list[i]+'</li>');
              }
            }
            else{$(advice_name).hide();}
          }, 'html');
        }
        else{$(advice_name).hide().html('');}
      break;
    }
  }
  //считываем нажатие клавиш, уже после вывода подсказки
  this.keydown = function(I){
    switch(I.keyCode){
      
      // по нажатию клавиш прячем подсказку
      case 13: // enter
        if(suggest_selected > 0){
          var str = $(advice_name + ' li').eq(suggest_selected-1).text();
          $(name).val(str);
          $(advice_name).hide();
          $(parent_name).submit();
        }
      break;
      case 27: // escape
        $(advice_name).hide();
        suggest_selected = 0;
        return false;
      break;
      // делаем переход по подсказке стрелочками клавиатуры
      case 38: // стрелка вверх
      case 40: // стрелка вниз
        I.preventDefault();
        if(suggest_count){this.key_activate( I.keyCode-39 );}
      break;
    }
  }
  // делаем обработку клика по подсказке
  this.advice_click = function(I){
    
    if('#' + (I.parent().attr('id')) == advice_name){
      $(name).val(I.text());
      // прячем слой подсказки
      $(advice_name).html('');
      suggest_count = suggest_selected = 0;
      $(parent_name).submit();
    }
  }
  // делаем обработку клика вне элемента
  this.click_out = function(){
    $(advice_name + ' li').eq(suggest_selected-1).removeClass('active');
    suggest_selected = 0;
    $(advice_name).hide();
  }
  // если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
  this.click_in = function(){
    if($(advice_name + ' li').length > 0)
      $(advice_name).show();
  }
}


//работаем только после загрузки
$(window).load(function(){
  var g = new gadvice();

  g.disposition();
  
  // читаем ввод с клавиатуры
  $(g.myname()).keyup(function(I){g.keyup(I);});

  //считываем нажатие клавиш, уже после вывода подсказки
  $(g.myname()).keydown(function(I){return g.keydown(I);});
 
  // делаем обработку клика по подсказке
  $(function(){
    $(document).on('click', '.wts-advice-variant', function(){
      g.advice_click($(this));
    })
  });
  // если кликаем в любом месте сайта, нужно спрятать подсказку
  $('html').click(function(){
    g.click_out();
  });
  // если кликаем на поле input и есть пункты подсказки, то показываем скрытый слой
  $(g.myname()).click(function(event){
    g.click_in();
    event.stopPropagation();
  });
});

