<?php
/*
 * imgupload.php (part of WTS) - upload images for events
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('include/defs.php');
  require_once('include/res.php');
  require_once('include/core/utils/db.php');
  require_once('include/core/utils/string.php');
  require_once('include/core/cont.php');
  require_once('include/core/upload.php');


  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    
    $sPostPointer = 'image';
    
    $aFile = $_FILES[$sPostPointer];

    if(!$aFile) {
      $sError = 'Must be less than ' . cString::sBytesToString(WTS_MAX_PKG_SIZE + 1024);
      //in json format
      echo '{"error":"' . str_replace('/', '\/', $sError) . '"}';
      exit;
    }
    
    $aSize   = false;
    $sSrc    = false;
    $aImgExt = array('.jpg', '.jpeg', '.png', '.gif', '.bmp');
    $sName   = strtolower($aFile['name']);
    $sExt    = strrchr($sName, '.');
    if($sExt
    && in_array($sExt, $aImgExt)
    && ($aSize = getimagesize($aFile['tmp_name']))
    && ($cStream = fopen($aFile['tmp_name'], 'r'))){
      $sSrc = stream_get_contents($cStream);
      $sName = $aFile['name'];
      fclose($cStream);
    }
    if(!$sSrc
    || strlen($sSrc) == 0){
      header(WTS_HTTP_404);
      $sError = _('Invalid image file.');
      //in json format
      echo '{"error":"' . str_replace('/', '\/', $sError) . '"}';
      exit;
    }

    try{
      if(!session_id())session_start();
      
      $cDb = new cDb(WTS_DB_NAME, WTS_DB_USER, WTS_DB_PASS, WTS_DB_TABLE_PREFIX);
      
      //validate session by guid and timeout
      if(isset($_SESSION[WTS_SESSION_PARAM_LOGIN]) 
      && isset($_SESSION[WTS_SESSION_PARAM_HASH])
      && ($cCont = cCont::mCheckSsn($cDb, $_SESSION[WTS_SESSION_PARAM_LOGIN], $_SESSION[WTS_SESSION_PARAM_HASH], WTS_SESSION_TIMEOUT))
      && $cCont->iType() !== cCont::CSTMR){
        //если вход выполнен - сложем картинку и вернём ссылку....
        $cUpload = new cUpload();
        $cUpload->Name($sName);
        $cUpload->Value($sSrc);
        if($cUpload->bSerialize($cDb)){
          header(WTS_HTTP_200);
          $sHref = WTS_PATH . 'gimgupload.php?uid=' . $cUpload->sUID();
          echo '{"upload":{"' . $sPostPointer . '":{"width":' . $aSize[0] . ',"height":' . $aSize[1] . ',"size":'. $aFile['size'] . '},"links":{"original":"' . str_replace('/', '\/', $sHref) . '"}}}';
        }
        unset($cCont);
      }
      else{
        header(WTS_HTTP_401);
        echo '{"error":"Unauthorized user"}';
      }
      unset($cDb);
    }
    catch (\Exception $e){
      echo 'Exception ' . $e->GetCode() . ': ' . $e->GetMessage() . PHP_EOL . 'In file: ' . basename($e->GetFile()) . PHP_EOL . 'line: ' . $e->GetLine() . PHP_EOL;
      exit;
    }
  }
  
}
?>
