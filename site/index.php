<?php
/*
 * index.php (part of WTS) - run app
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
require_once('include/app.php');

use wts\app as ap;

try{
  $app = new ap();
  $app->run();
}
catch (\Exception $e){
  
  echo 'Exception ' . $e->GetCode() . ': ' . $e->GetMessage() . '<p>' . 'In file: ' . basename($e->GetFile()) . '<p>' . 'line: ' . $e->GetLine() . '<p>';
  exit;
}
?>
