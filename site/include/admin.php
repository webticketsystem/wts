<?php
/*
 * admin.php (part of WTS) - class for Admin level UI
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

namespace wts{

  require_once('customerbase.php');
  require_once('withdashboardpage.php');
  require_once('withcontactpage.php');
  require_once('withgrouppage.php');
  require_once('withproductpage.php');
  require_once('withcontractpage.php');

  /*-- admin --**********************************************************/
  class cAdmin extends cCustomerBase{
    use tWithDashboardPage;
    use tWithContactPage;
    use tWithGroupPage;
    use tWithProductPage;
    use tWithContractPage;
    
    public function __construct(cDb &$cDb, cCont &$cCont){
      parent::__construct($cDb, $cCont);
    }
    
    protected function AddRplExp(cPage &$cPage){
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Content(new cElementBase(_('Replaceable Expressions')));
      
      $cElement->Content($cElementCapt);
      
      $cToc = new cToc();
      
      foreach(cResponse::RPL_EXP as $sKey => $sExpr){
        $cToc->Child(new cElementBase(cResponse::mReplExpUiName($sKey)), new cElementBase($sExpr));
      }

      $cElement->Content($cToc->cRoot());
      
      $cPage->Aside($cElement); 
    }

    public function Run(){
      
      $cPage = new cPage();
      
      $sDefAction = $sAction = WTS_PARAM_ACT_DASHBOARD;
      
      if(isset($_GET[WTS_PARAM_ACTION])){
        $sAction = $_GET[WTS_PARAM_ACTION];
      }
      
      switch($sAction){
        case WTS_PARAM_ACT_CHAIN:
          $this->DoChainPage($cPage);
          break;
        case WTS_PARAM_ACT_EVENT:
          $this->DoEventPage($cPage);
          break;
        case WTS_PARAM_ACT_TAG:
          $this->DoTagPage($cPage);
          break;
        case WTS_PARAM_ACT_QUEUE:
          $this->DoQueuePage($cPage);
          break;
        case WTS_PARAM_ACT_REGEXP:
          $this->DoRegExpPage($cPage);
          break;
        case WTS_PARAM_ACT_RESPONSE:
          $this->DoResponsePage($cPage);
          break;
        case WTS_PARAM_ACT_SALUTATION:
          $this->DoSalutationPage($cPage);
          break;
        case WTS_PARAM_ACT_SIGNATURE:
          $this->DoSignaturePage($cPage);
          break;
        case WTS_PARAM_ACT_CONT:
          $this->DoContactPage($cPage);
          break;
        case WTS_PARAM_ACT_CONT_ATTR:
          $this->DoContactAttrPage($cPage);
          break;
        case WTS_PARAM_ACT_GROUP:
          $this->DoGroupPage($cPage);
          break;
        case WTS_PARAM_ACT_GROUP_ATTR:
          $this->DoGroupAttrPage($cPage);
          break;
        case WTS_PARAM_ACT_PRODUCT:
          $this->DoProductPage($cPage);
          break;
        case WTS_PARAM_ACT_PROD_ATTR:
          $this->DoProdAttrPage($cPage);
          break;
        case WTS_PARAM_ACT_CATEGORY:
          $this->DoCategoryPage($cPage);
          break;
        case WTS_PARAM_ACT_CONTR:
          $this->DoContractPage($cPage);
          break;
        case WTS_PARAM_ACT_REPORT:
          $this->DoReportPage($cPage);
          break;
        case WTS_PARAM_ACT_ABOUT:
          $this->DoAboutPage($cPage);
          break;
        case WTS_PARAM_ACT_LOG:
          $this->DoLogPage($cPage);
          break;
        case WTS_PARAM_ACT_PROFILE:
          $this->DoProfilePage($cPage);
          break;
        case WTS_PARAM_ACT_LOGOUT:
          $this->cCont->CloseSsn($this->cDb);
          header('Location: ' . WTS_HREF);
          exit;
        default:
          if($sAction != $sDefAction){$sAction = $sDefAction;}
          $this->DoDashboardPage($cPage);
          break;
      }

      //--login info
      $cPage->Header($this->cAddAccount());
      $cPage->AddTitle();
      
      //--message - if exist
      $iHeaderHeight = 100;
      if(($i = $this->iAddMessage($cPage)) > 0){
        $iHeaderHeight += ($i * 35);
      }
      
      //--menu
      $cMenu = new cMenu();
      
      $cMenu->Child($this->cAddDashboardMenuItem($sAction));
      $cParentMenu = $this->cAddChainMenuItem($sAction);
      $cMenu->Child($cParentMenu);
      $cParentMenu->Child($this->cAddTagMenuItem($sAction));
      
      $cParentMenu = $this->cAddQueueMenuItem($sAction);
      $cMenu->Child($cParentMenu);
      $cParentMenu->Child($this->cAddRegExpMenuItem($sAction));
      $cParentMenu->Child($this->cAddResponseMenuItem($sAction));
      $cParentMenu->Child($this->cAddSalutationMenuItem($sAction));
      $cParentMenu->Child($this->cAddSignatureMenuItem($sAction));
      
      $cParentMenu = $this->cAddContMenuItem($sAction);
      $cMenu->Child($cParentMenu);
      $cParentMenu->Child($this->cAddContAttrMenuItem($sAction));
      
      $cParentMenu = $this->cAddGroupMenuItem($sAction);
      $cMenu->Child($cParentMenu);
      $cParentMenu->Child($this->cAddGroupAttrMenuItem($sAction));
      
      $cParentMenu = $this->cAddProdMenuItem($sAction);
      $cMenu->Child($cParentMenu);
      $cParentMenu->Child($this->cAddProdAttrMenuItem($sAction));
      $cParentMenu->Child($this->cAddCategoryMenuItem($sAction));
     
      $cParentMenu = $this->cAddContractMenuItem($sAction);
      $cMenu->Child($cParentMenu);
      $cParentMenu->Child($this->cAddTagMenuItem($sAction));
      
      $cMenu->Child($this->cAddReportMenuItem($sAction));
      $cParentMenu = $this->cAddAboutMenuItem($sAction);
      $cMenu->Child($cParentMenu);
      $cParentMenu->Child($this->cAddLogMenuItem($sAction));
      
      $cPage->Header($cMenu->cRoot());

      echo $cPage->sSerialize($iHeaderHeight);
      unset($cPage);
    }
    
    
    
    protected function &cAddTagMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Tags')
                         , _('Tags management')
                         , ($sCurAct !== WTS_PARAM_ACT_TAG ? sAction(WTS_PARAM_ACT_TAG) : null));
      return $cMenuItem;
    }
    
    protected function &cAddLogMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('SysLog')
                         , _('View system log')
                         , ($sCurAct !== WTS_PARAM_ACT_LOG ? sAction(WTS_PARAM_ACT_LOG) : null));
      return $cMenuItem;
    }
    
    protected function &cAddQueueMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Queues')
                         , _('Queue management')
                         , ($sCurAct !== WTS_PARAM_ACT_QUEUE ? sAction(WTS_PARAM_ACT_QUEUE) : null));
      return $cMenuItem;
    }
    
    protected function &cAddRegExpMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('RegExps')
                         , _('Regular Expressions management')
                         , ($sCurAct !== WTS_PARAM_ACT_REGEXP ? sAction(WTS_PARAM_ACT_REGEXP) : null));
      return $cMenuItem;
    }
    
    protected function &cAddSalutationMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Salutations')
                         , _('Salutations management')
                         , ($sCurAct !== WTS_PARAM_ACT_SALUTATION ? sAction(WTS_PARAM_ACT_SALUTATION) : null));
      return $cMenuItem;
    }
    
    protected function &cAddSignatureMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Signatures')
                         , _('Signatures management')
                         , ($sCurAct !== WTS_PARAM_ACT_SIGNATURE ? sAction(WTS_PARAM_ACT_SIGNATURE) : null));
      return $cMenuItem;
    }
    
    protected function &cAddResponseMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Responses')
                         , _('Responses management')
                         , ($sCurAct !== WTS_PARAM_ACT_RESPONSE ? sAction(WTS_PARAM_ACT_RESPONSE) : null));
      return $cMenuItem;
    }
    
    protected function &cAddContAttrMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Attributes')
                         , _('Contact Attributes management')
                         , ($sCurAct !== WTS_PARAM_ACT_CONT_ATTR ? sAction(WTS_PARAM_ACT_CONT_ATTR) : null));
      return $cMenuItem;
    }
    
    protected function &cAddGroupAttrMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Attributes')
                         , _('Group Attributes management')
                         , ($sCurAct !== WTS_PARAM_ACT_GROUP_ATTR ? sAction(WTS_PARAM_ACT_GROUP_ATTR) : null));
      return $cMenuItem;
    }
    
    protected function &cAddProdAttrMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Attributes')
                         , _('Product Attributes management')
                         , ($sCurAct !== WTS_PARAM_ACT_PROD_ATTR ? sAction(WTS_PARAM_ACT_PROD_ATTR) : null));
      return $cMenuItem;
    }
    
    protected function &cAddCategoryMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Categories')
                         , _('Product Categories management')
                         , ($sCurAct !== WTS_PARAM_ACT_CATEGORY ? sAction(WTS_PARAM_ACT_CATEGORY) : null));
      return $cMenuItem;
    }
    
    public function DoReportPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref      = sAction(WTS_PARAM_ACT_REPORT);

      $aReports   = false;
      $cCurReport = false;
      $iRows = cReport::iUnserialize($this->cDb, $aReports);
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurReport = &cReport::mInArray($aReports, $_GET[WTS_PARAM_ID]);
      }
      
      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $cObj = new cReport(isset($_POST[WTS_PARAM_TO]) ? $_POST[WTS_PARAM_TO] : 0);
          if(isset($_POST[WTS_PARAM_NAME])){
            $cObj->Name($_POST[WTS_PARAM_NAME]);
            $cObj->Value('select count(*) from ' . $this->cDb->sTablePrefix() . cReport::TABLE);
          }
          if($cObj->bSerialize($this->cDb)){$cCurReport = &$cObj;}
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Report using Name \'') . $cObj->sName() . '\'.');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_DELETE:
          if($cCurReport){
            $cCurReport->bDelete($this->cDb);
            unset($cCurReport);
            $cCurReport = false;
          }
          break; 
        case WTS_PARAM_EACT_UPDATE:
          if($cCurReport){
            if(isset($_POST[WTS_PARAM_VALUE])){$cCurReport->Value($_POST[WTS_PARAM_VALUE]);}
            $cCurReport->bSerialize($this->cDb);
          }
          break;
        default: break;
        }
        if($cCurReport !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurReport->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_REPORT);
      $cElement->Content($cButton->cRoot());
      
      //button delete
      if($cCurReport){
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete Report'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_REPORT);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        $cButton->Hidden(WTS_PARAM_ID, $cCurReport->iID());
        $cElement->Content($cButton->cRoot());
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Report Name'));
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }
        
        $iID = 0;
        if($cCurReport){$iID = $cCurReport->iID();}
        
        foreach($aReports as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          
          if($c->iType() == cCont::CSTMR){
            $cElementImg->Attribute('src', IMG_CONTACT);
            $cElementImg->Attribute('title', _('For Customer'));
          }
          else{
            $cElementImg->Attribute('src', IMG_AGENT);
            $cElementImg->Attribute('title', _('For Agent'));
          }
          
          $cElementTd->Content($cElementImg);

          $cElementTd->Content(new cElementBase($c->sName()));
          $cElementTr->Content($cElementTd);
         
          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //050. edit element
      if($cCurReport){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-main-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Attribute('style', 'text-align: center;');
        $cElementCapt->Content(new cElementBase(_('Report Query')));
        
        $cElement->Content($cElementCapt);

        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
                                         . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_UPDATE)
                                         . sSubAction(WTS_PARAM_ID, $cCurReport->iID()));
        $cElementForm->Attribute('method', 'post');
        
        $cField = new cField();
        $cField->Input(cField::cElementTextArea(WTS_PARAM_VALUE, $cCurReport->sValue()));
        
        $cElementForm->Content($cField->cBuild());
        
        $cField = new cField(cField::CENTER);
        $cElementUpd = cField::cElementSubmit(_('Update'));
        $cElementUpd->Attribute('id', 'id_' . WTS_PARAM_VALUE);
        $cElementUpd->Attribute('onclick', 'window.content_changed = false;');
        $cField->Input($cElementUpd);
        
        $cElementForm->Content($cField->cBuild());

        $cElement->Content($cElementForm);
        
        $cPage->Main($cElement);
      }

      //060. side bar create
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Content(new cElementBase(_('Add New Report')));
      
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
                                       . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_CREATE));
      $cElementForm->Attribute('method', 'post');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
      
      $cElementInp = cField::cElementInput(WTS_PARAM_NAME);
      $cElementInp->Attribute('placeholder', _('My Report'));
      $cElementInp->Attribute('title', _('Name of Report must be unique.'));
      
      $cField->Input($cElementInp);
      
      $cElementForm->Content($cField->cBuild());

      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Report For'), WTS_PARAM_TO));
      
      $cSelect = new cSelect(WTS_PARAM_TO, WTS_PFX . '-input');
      
      $cSelect->Option(false, cCont::CSTMR, _('Customer'));
      $cSelect->Option(true, cCont::AGENT, _('Agent'));
      
      $cField->Input($cSelect->cRoot());
      
      $cElementForm->Content($cField->cBuild());
      
      $cField = new cField(cField::RIGHT);
      $cField->Input(cField::cElementSubmit(_('Create')));
      
      $cElementForm->Content($cField->cBuild());

      $cElement->Content($cElementForm);
      
      $cPage->Aside($cElement);
      
      //070. side bar replaceable expressions
      
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Content(new cElementBase(_('Replaceable Expressions')));
      
      $cElement->Content($cElementCapt);
      
      $cToc = new cToc();
      
      foreach(cReport::RPL_EXP as $sKey => $sExpr){
        $cToc->Child(new cElementBase(cReport::mReplExpUiName($sKey)), new cElementBase($sExpr));
      }

      $cElement->Content($cToc->cRoot());
      
      $cPage->Aside($cElement); 
    }
    
    public function DoLogPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref    = sAction(WTS_PARAM_ACT_LOG);
      $iPage    = ((isset($_GET[WTS_PARAM_PAGE]) && $_GET[WTS_PARAM_PAGE] > 0) ? $_GET[WTS_PARAM_PAGE] : 1);
      
      $iCurType = false;
      if(isset($_GET[WTS_PARAM_TYPE]) && cLog::bAddFilterType($_GET[WTS_PARAM_TYPE])){
        $iCurType = $_GET[WTS_PARAM_TYPE];
      }
      
      $sCurSearch = false;
      if(isset($_GET[WTS_PARAM_SEARCH]) && cLog::bAddFilterPartValue($this->cDb, $_GET[WTS_PARAM_SEARCH])){
        $sCurSearch = $_GET[WTS_PARAM_SEARCH];
      }
      
      $iRows  = cLog::iCount($this->cDb);
      $iPages = 1;

      $aLogs = false;
      if($iRows > 0){
        //normalize page
        if($iRows > WTS_NARROW_ROW_PER_PAGE){
          $iPages = ceil($iRows / WTS_NARROW_ROW_PER_PAGE);
        }
        if($iPage > $iPages){$iPage = $iPages;}

        $iRows = cLog::iUnserialize($this->cDb, $iPage, WTS_NARROW_ROW_PER_PAGE, $aLogs);
      }

      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      if($sAction !== false && $sAction == WTS_PARAM_EACT_DELETE && $iRows > 0){
        cLog::DeleteAllLogs($this->cDb);
        
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_LOG);
      $cElement->Content($cButton->cRoot());
      
      //select log type
      $cButton = new cButton();
        
      $cSelect = new cSelect(WTS_PARAM_TYPE, WTS_PFX . '-toolbar');
      $cSelect->Option($iCurType === false, 0, _('All'));
      $cSelect->Option($iCurType == cLog::MSG, cLog::MSG, _('Messages'));
      $cSelect->Option($iCurType == cLog::WARN, cLog::WARN, _('Warnings'));
      $cSelect->Option($iCurType == cLog::ERR, cLog::ERR, _('Errors'));
      
      $cElementS = $cSelect->cRoot();
      $cElementS->Attribute('onchange', 'this.form.submit();');
      $cButton->Content($cElementS);
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_LOG);
      $cElement->Content($cButton->cRoot());
      
      
      //search for logs
      $cButton = new cButton();
      $cElementI = new cElement('input', false);
      $cElementI->Attribute('type', 'search');
      $cElementI->Attribute('class', WTS_PFX . '-toolbar-search');
      $cElementI->Attribute('name', WTS_PARAM_SEARCH);
      $cElementI->Attribute('placeholder', '&#128270; ' . _('Search for Logs'));
      $cElementI->Attribute('title', _('Enter Word for Search'));
      if($sCurSearch !== false){
        $cElementI->Attribute('value', htmlentities($sCurSearch));
      }
      $cButton->Content($cElementI);
      
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_LOG);
      
      $cElement->Content($cButton->cRoot());
      
      //button delete
      if($iRows > 0){
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete All Logs'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_LOG);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        $cElement->Content($cButton->cRoot());
      }
      
      //page navbar
      if($iPages > 1){
        $aButtons = array();
        $this->CreateButtonsNav(WTS_PARAM_ACT_LOG, $iPage, $iPages, $aButtons);
        foreach($aButtons as &$cButton){
          if($iCurType !== false){$cButton->Hidden(WTS_PARAM_TYPE, $iCurType);}
          if($sCurSearch !== false){$cButton->Hidden(WTS_PARAM_SEARCH, $sCurSearch);}
          $cElement->Content($cButton->cRoot());
        }
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Date'), '145px');
      $cTable->Column(_('Message'));
      
       if($iRows > 0){
         foreach($aLogs as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          
          //column 1
          $cElementTd = new cElement('td', false);
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          
          if($c->iType() == cLog::MSG){
            $cElementImg->Attribute('src', IMG_ENABLED);
            $cElementImg->Attribute('title', _('Message'));
          }
          else{
            if($c->iType() == cLog::WARN){
              $cElementImg->Attribute('src', IMG_SUSPENDED);
              $cElementImg->Attribute('title', _('Warning'));
            }
            else{
              $cElementImg->Attribute('src', IMG_DISABLED);
              $cElementImg->Attribute('title', _('Error'));
            }
          }
          
          $cElementTd->Content($cElementImg);
          $cElementTd->Content($this->cDoTime($c->iDt()));
          $cElementTd->Content($this->cDoDate($c->iDt()));
          $cElementTr->Content($cElementTd);
          //column 2
          $cElementTd = new cElement('td', false);
          $cElementTd->Content(new cElementBase($c->sValue()));
          
          $cElementTr->Content($cElementTd);
          
          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
    }
    
    public function DoSalutationPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref          = sAction(WTS_PARAM_ACT_SALUTATION);
      $aSalutations   = false;
      $cCurSalutation = false;
      $iRows = cSalutation::iUnserialize($this->cDb, $aSalutations);
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurSalutation = &cSalutation::mInArray($aSalutations, $_GET[WTS_PARAM_ID]);
      }
      
      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $cObj = new cSalutation();
          if(isset($_POST[WTS_PARAM_NAME])){
            $cObj->Name($_POST[WTS_PARAM_NAME]);
            $cObj->Value(_('Type content here.'));
          }
          if($cObj->bSerialize($this->cDb)){$cCurSalutation = &$cObj;}
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Salutation using Name \'') . $cObj->sName() . '\'.');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_DELETE:
          if($cCurSalutation){
            cResponse::ClearSalutation($this->cDb, $cCurSalutation->iID());
            $cCurSalutation->bDelete($this->cDb);
            unset($cCurSalutation);
            $cCurSalutation = false;
          }
          break; 
        case WTS_PARAM_EACT_UPDATE:
          if($cCurSalutation){
            if(isset($_POST[WTS_PARAM_VALUE])){$cCurSalutation->Value($_POST[WTS_PARAM_VALUE]);}
            $cCurSalutation->bSerialize($this->cDb);
          }
          break;
        default: break;
        }
        if($cCurSalutation !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurSalutation->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_SALUTATION);
      $cElement->Content($cButton->cRoot());
      
      if($cCurSalutation){
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete Salutation'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_SALUTATION);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        $cButton->Hidden(WTS_PARAM_ID, $cCurSalutation->iID());
        $cElement->Content($cButton->cRoot());
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Salutation Name'));
      
      if($iRows > 0){

        $iID = 0;
        if($cCurSalutation){$iID = $cCurSalutation->iID();}
        
        foreach($aSalutations as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);

          $cElementTd->Content(new cElementBase($c->sName()));
          $cElementTr->Content($cElementTd);

          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //050. html-edit element
      if($cCurSalutation){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-main-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Attribute('style', 'text-align: center;');
        $cElementCapt->Content(new cElementBase(_('Salutation Content')));
        
        $cElement->Content($cElementCapt);

        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
                                         . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_UPDATE)
                                         . sSubAction(WTS_PARAM_ID, $cCurSalutation->iID()));
        $cElementForm->Attribute('method', 'post');
        
        $cField = new cField();
        $cField->Input(cField::cElementTextArea(WTS_PARAM_VALUE, $cCurSalutation->sValue()));
        
        $cElementForm->Content($cField->cBuild());
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Attribute('src', WTS_PATH . 'nicedit/nicEdit.js');
        $cPage->Head($cElementS);
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $sScript = cField::sNicEditInitScript(cField::sNicEditCreateScript(WTS_PARAM_VALUE, 250));
        $cElementS->Content(new cElementBase($sScript, true));

        $cPage->Head($cElementS);
        
        
        $cField = new cField(cField::CENTER);
        $cElementUpd = cField::cElementSubmit(_('Update'));
        $cElementUpd->Attribute('id', 'id_' . WTS_PARAM_VALUE);
        $cElementUpd->Attribute('onclick', 'window.content_changed = false;');
        $cField->Input($cElementUpd);
        
        $cElementForm->Content($cField->cBuild());

        $cElement->Content($cElementForm);
        
        $cPage->Main($cElement);
      }
      
      //060. side bar create
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Content(new cElementBase(_('Add New Salutation')));
      
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
                                       . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_CREATE));
      $cElementForm->Attribute('method', 'post');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
      $cField->Input(cField::cElementInput(WTS_PARAM_NAME));
      
      $cElementForm->Content($cField->cBuild());
      
      $cField = new cField(cField::RIGHT);
      $cField->Input(cField::cElementSubmit(_('Create')));
      
      $cElementForm->Content($cField->cBuild());

      $cElement->Content($cElementForm);
      
      $cPage->Aside($cElement);
      
      //070. replaceable expressions
      $this->AddRplExp($cPage);
      
    }
    
    public function DoSignaturePage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref         = sAction(WTS_PARAM_ACT_SIGNATURE);
      $aSignatures   = false;
      $cCurSignature = false;
      $iRows = cSignature::iUnserialize($this->cDb, $aSignatures);
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurSignature = &cSignature::mInArray($aSignatures, $_GET[WTS_PARAM_ID]);
      }
      
      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $cObj = new cSignature();
          if(isset($_POST[WTS_PARAM_NAME])){
            $cObj->Name($_POST[WTS_PARAM_NAME]);
            $cObj->Value(_('Type content here.'));
          }
          if($cObj->bSerialize($this->cDb)){$cCurSignature = &$cObj;}
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Signature using Name \'') . $cObj->sName() . '\'.');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_DELETE:
          if($cCurSignature){
            cResponse::ClearSignature($this->cDb, $cCurSignature->iID());
            $cCurSignature->bDelete($this->cDb);
            unset($cCurSignature);
            $cCurSignature = false;
          }
          break; 
        case WTS_PARAM_EACT_UPDATE:
          if($cCurSignature){
            if(isset($_POST[WTS_PARAM_VALUE])){$cCurSignature->Value($_POST[WTS_PARAM_VALUE]);}
            $cCurSignature->bSerialize($this->cDb);
          }
          break;
        default: break;
        }
        if($cCurSignature !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurSignature->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_SIGNATURE);
      $cElement->Content($cButton->cRoot());
      
      if($cCurSignature){
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete Signature'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_SIGNATURE);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        $cButton->Hidden(WTS_PARAM_ID, $cCurSignature->iID());
        $cElement->Content($cButton->cRoot());
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Signature Name'));
      
      if($iRows > 0){
        
        $iID = 0;
        if($cCurSignature){$iID = $cCurSignature->iID();}
        
        foreach($aSignatures as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);

          $cElementTd->Content(new cElementBase($c->sName()));
          $cElementTr->Content($cElementTd);

          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //050. html-edit element
      if($cCurSignature){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-main-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Attribute('style', 'text-align: center;');
        $cElementCapt->Content(new cElementBase(_('Signature Content')));
        
        $cElement->Content($cElementCapt);

        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
                                         . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_UPDATE)
                                         . sSubAction(WTS_PARAM_ID, $cCurSignature->iID()));
        $cElementForm->Attribute('method', 'post');
        
        $cField = new cField();
        $cField->Input(cField::cElementTextArea(WTS_PARAM_VALUE, $cCurSignature->sValue()));
        
        $cElementForm->Content($cField->cBuild());
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Attribute('src', WTS_PATH . 'nicedit/nicEdit.js');
        $cPage->Head($cElementS);
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $sScript = cField::sNicEditInitScript(cField::sNicEditCreateScript(WTS_PARAM_VALUE, 250));
        $cElementS->Content(new cElementBase($sScript, true));

        $cPage->Head($cElementS);
        
        
        $cField = new cField(cField::CENTER);
        $cElementUpd = cField::cElementSubmit(_('Update'));
        $cElementUpd->Attribute('id', 'id_' . WTS_PARAM_VALUE);
        $cElementUpd->Attribute('onclick', 'window.content_changed = false;');
        $cField->Input($cElementUpd);
        
        $cElementForm->Content($cField->cBuild());

        $cElement->Content($cElementForm);
        
        $cPage->Main($cElement);
      }
      
      //060. side bar create
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Content(new cElementBase(_('Add New Signature')));
      
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
                                       . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_CREATE));
      $cElementForm->Attribute('method', 'post');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
      $cField->Input(cField::cElementInput(WTS_PARAM_NAME));
      
      $cElementForm->Content($cField->cBuild());
      
      $cField = new cField(cField::RIGHT);
      $cField->Input(cField::cElementSubmit(_('Create')));
      
      $cElementForm->Content($cField->cBuild());

      $cElement->Content($cElementForm);
      
      $cPage->Aside($cElement);
      
      //070. replaceable expressions
      $this->AddRplExp($cPage);
    }
    
    public function DoResponsePage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref        = sAction(WTS_PARAM_ACT_RESPONSE);
      $aResponses   = false;
      $cCurResponse = false;
      $iRows = cResponse::iUnserialize($this->cDb, $aResponses);
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurResponse = &cResponse::mInArray($aResponses, $_GET[WTS_PARAM_ID]);
      }
      
      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $cObj = new cResponse();
          if(isset($_POST[WTS_PARAM_NAME])){
            $cObj->Name($_POST[WTS_PARAM_NAME]);
            $cObj->Value(_('Type content here.'));
          }
          if($cObj->bSerialize($this->cDb)){$cCurResponse = &$cObj;}
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Response using Name \'') . $cObj->sName() . '\'.');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_DELETE:
          if($cCurResponse){
            cQueue::DeleteResp($this->cDb, $cCurResponse->iID());
            $cCurResponse->bDelete($this->cDb);
            unset($cCurResponse);
            $cCurResponse = false;
          }
          break; 
        case WTS_PARAM_EACT_UPDATE:
          if($cCurResponse){
            if(isset($_POST[WTS_PARAM_VALUE])){
              $cCurResponse->Value($_POST[WTS_PARAM_VALUE]);
            }
            if(isset($_POST[WTS_PARAM_ACT_SALUTATION])){
              $cCurResponse->Salutation($_POST[WTS_PARAM_ACT_SALUTATION]);
            }
            if(isset($_POST[WTS_PARAM_ACT_SIGNATURE])){
              $cCurResponse->Signature($_POST[WTS_PARAM_ACT_SIGNATURE]);
            }
            $cCurResponse->bSerialize($this->cDb);
          }
          break;
        default: break;
        }
        if($cCurResponse !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurResponse->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_RESPONSE);
      $cElement->Content($cButton->cRoot());
      
      if($cCurResponse){
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete Response'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_RESPONSE);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        $cButton->Hidden(WTS_PARAM_ID, $cCurResponse->iID());
        $cElement->Content($cButton->cRoot());
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Response Name'));
      
      if($iRows > 0){

        $iID = 0;
        if($cCurResponse){$iID = $cCurResponse->iID();}
        
        foreach($aResponses as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);

          $cElementTd->Content(new cElementBase($c->sName()));
          $cElementTr->Content($cElementTd);

          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);

      //050. html-edit element
      if($cCurResponse){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-main-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Attribute('style', 'text-align: center;');
        $cElementCapt->Content(new cElementBase(_('Response Content')));
        
        $cElement->Content($cElementCapt);

        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
                                         . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_UPDATE)
                                         . sSubAction(WTS_PARAM_ID, $cCurResponse->iID()));
        $cElementForm->Attribute('method', 'post');
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Salutation'), WTS_PARAM_ACT_SALUTATION));
        
        $cSelect = new cSelect(WTS_PARAM_ACT_SALUTATION, WTS_PFX . '-input');
        $cSelect->Option($cCurResponse->iSalutation() === 0, 0, _('Not use'));
        
        $aSalutations = false;
        if(cSalutation::iUnserialize($this->cDb, $aSalutations) > 0){
          foreach($aSalutations as $key => &$c){
            $cSelect->Option($cCurResponse->iSalutation() === $c->iID(), $c->iID(), $c->sName());
            unset($aSalutations[$key]);
          }
        }
        $cField->Input($cSelect->cRoot());
        
        $cElementForm->Content($cField->cBuild(true));
        
        
        $cField = new cField();
        $cField->Input(cField::cElementTextArea(WTS_PARAM_VALUE, $cCurResponse->sValue()));
        
        $cElementForm->Content($cField->cBuild());
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Attribute('src', WTS_PATH . 'nicedit/nicEdit.js');
        $cPage->Head($cElementS);
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $sScript = cField::sNicEditInitScript(cField::sNicEditCreateScript(WTS_PARAM_VALUE, 250));
        $cElementS->Content(new cElementBase($sScript, true));

        $cPage->Head($cElementS);
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Signature'), WTS_PARAM_ACT_SIGNATURE));
        
        $cSelect = new cSelect(WTS_PARAM_ACT_SIGNATURE, WTS_PFX . '-input');
        $cSelect->Option($cCurResponse->iSignature() === 0, 0, _('Not use'));
        
        $aSignatures = false;
        if(cSignature::iUnserialize($this->cDb, $aSignatures) > 0){
          foreach($aSignatures as $key => &$c){
            $cSelect->Option($cCurResponse->iSignature() === $c->iID(), $c->iID(), $c->sName());
            unset($aSignatures[$key]);
          }
        }
        $cField->Input($cSelect->cRoot());
        
        $cElementForm->Content($cField->cBuild(true));
        
        
        $cField = new cField(cField::CENTER);
        $cElementUpd = cField::cElementSubmit(_('Update'));
        $cElementUpd->Attribute('id', 'id_' . WTS_PARAM_VALUE);
        $cElementUpd->Attribute('onclick', 'window.content_changed = false;');
        $cField->Input($cElementUpd);
        
        $cElementForm->Content($cField->cBuild());

        $cElement->Content($cElementForm);
        
        $cPage->Main($cElement);
      }
      
      //060. side bar create
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Content(new cElementBase(_('Add New Response')));
      
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
                                       . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_CREATE));
      $cElementForm->Attribute('method', 'post');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
      $cField->Input(cField::cElementInput(WTS_PARAM_NAME));
      
      $cElementForm->Content($cField->cBuild());
      
      $cField = new cField(cField::RIGHT);
      $cField->Input(cField::cElementSubmit(_('Create')));
      
      $cElementForm->Content($cField->cBuild());

      $cElement->Content($cElementForm);
      
      $cPage->Aside($cElement);
      
      //070. replaceable expressions
      $this->AddRplExp($cPage);
    }
    
    public function DoQueuePage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref     = sAction(WTS_PARAM_ACT_QUEUE);
      $aQueues   = false;
      $cCurQueue = false;
      $iRows = cQueue::iUnserialize($this->cDb, $aQueues);
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurQueue = &cQueue::mInArray($aQueues, $_GET[WTS_PARAM_ID]);
      }
      
      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $cObj = new cQueue();
          if(isset($_POST[WTS_PARAM_LOGIN])){
            $cObj->Login($_POST[WTS_PARAM_LOGIN]);
          }
          if($cObj->bSerialize($this->cDb)){$cCurQueue = &$cObj;}
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Queue using Login \'') . $cObj->sLogin() . '\'.');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_UPDATE:
          if($cCurQueue){
            if(isset($_POST[WTS_PARAM_NAME])){$cCurQueue->Name($_POST[WTS_PARAM_NAME]);}
            if(isset($_POST[WTS_PARAM_LOGIN])){$cCurQueue->Login($_POST[WTS_PARAM_LOGIN]);}
            if(isset($_POST[WTS_PARAM_PWD])){$cCurQueue->Passwd($_POST[WTS_PARAM_PWD]);}
            if(isset($_POST[WTS_PARAM_IMAP . 1])){$cCurQueue->Host(cQueue::IMAP, $_POST[WTS_PARAM_IMAP . 1]);}
            if(isset($_POST[WTS_PARAM_IMAP . 2])){$cCurQueue->Login_(cQueue::IMAP, $_POST[WTS_PARAM_IMAP . 2]);}
            if(isset($_POST[WTS_PARAM_IMAP . 3])){$cCurQueue->Encryption(cQueue::IMAP, $_POST[WTS_PARAM_IMAP . 3]);}
            if(isset($_POST[WTS_PARAM_IMAP . 4])){$cCurQueue->Port(cQueue::IMAP, $_POST[WTS_PARAM_IMAP . 4]);}
            if(isset($_POST[WTS_PARAM_SMTP . 1])){$cCurQueue->Host(cQueue::SMTP, $_POST[WTS_PARAM_SMTP . 1]);}
            if(isset($_POST[WTS_PARAM_SMTP . 2])){$cCurQueue->Login_(cQueue::SMTP, $_POST[WTS_PARAM_SMTP . 2]);}
            if(isset($_POST[WTS_PARAM_SMTP . 3])){$cCurQueue->Encryption(cQueue::SMTP, $_POST[WTS_PARAM_SMTP . 3]);}
            if(isset($_POST[WTS_PARAM_SMTP . 4])){$cCurQueue->Port(cQueue::SMTP, $_POST[WTS_PARAM_SMTP . 4]);}
            
            if(isset($_POST[WTS_PARAM_DISABLED])){
              if($_POST[WTS_PARAM_DISABLED] === 'true'){$cCurQueue->Disable();}
              else{$cCurQueue->Enable();}
            }
            $cCurQueue->bSerialize($this->cDb);

            $aResponsesID = false;
            $cCurQueue->iUnserializeResps($this->cDb, $aResponsesID);
            if($aResponsesID !== false){
              foreach($aResponsesID as $k => $v){
                if(isset($_POST[WTS_PARAM_TEMPLATE . $k])){
                  $cCurQueue->bSerializeResp($this->cDb, $k, $_POST[WTS_PARAM_TEMPLATE . $k]);
                }
              }
            }
          }
          break;
        default: break;
        }
        if($cCurQueue !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurQueue->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_QUEUE);
      $cElement->Content($cButton->cRoot());
      
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Queue Name'));
      $cTable->Column(_('Login'));
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }

        $iID = 0;
        if($cCurQueue){$iID = $cCurQueue->iID();}
        
        foreach($aQueues as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          
          if($c->bDisabled() === true){
            $cElementImg->Attribute('src', IMG_DISABLED);
            $cElementImg->Attribute('title', _('Disabled'));
          }
          else{
            $cElementImg->Attribute('src', IMG_ENABLED);
            $cElementImg->Attribute('title', _('Enabled'));
          }
          
          $cElementTd->Content($cElementImg);

          $cElementTd->Content(new cElementBase($c->sName()));
          $cElementTr->Content($cElementTd);
          
          //column 2
          $cElementTd = new cElement('td', false);

          $cElementTd->Content(new cElementBase($c->sLogin()));
          $cElementTr->Content($cElementTd);

          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //041. imap settings element
      if($cCurQueue){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-main-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Attribute('style', 'text-align: center;');
        $cElementCapt->Content(new cElementBase(_('IMAP Settings')));
        
        $cElement->Content($cElementCapt);

        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
                                         . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_UPDATE)
                                         . sSubAction(WTS_PARAM_ID, $cCurQueue->iID()));
        $cElementForm->Attribute('method', 'post');
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Host'), WTS_PARAM_IMAP . 1));      
        $cField->Input(cField::cElementInput(WTS_PARAM_IMAP . 1, $cCurQueue->sHost(cQueue::IMAP)));
        
        $cElementForm->Content($cField->cBuild(true));
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Login'), WTS_PARAM_IMAP . 2));      
        $cField->Input(cField::cElementInput(WTS_PARAM_IMAP . 2, $cCurQueue->sLogin_(cQueue::IMAP)));
        
        $cElementForm->Content($cField->cBuild(true));
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Encryption'), WTS_PARAM_IMAP . 3));
        
        $cSelect = new cSelect(WTS_PARAM_IMAP . 3, WTS_PFX . '-input');
        $iEnc = $cCurQueue->iEncryption(cQueue::IMAP);
        $cSelect->Option(($iEnc === 0), 0, _('None'));
        $cSelect->Option(($iEnc === cQueue::ENC_SSL), cQueue::ENC_SSL, 'SSL');
        $cSelect->Option(($iEnc === cQueue::ENC_TLS), cQueue::ENC_TLS, 'TLS');
        
        $cField->Input($cSelect->cRoot());
        
        $cElementForm->Content($cField->cBuild(true));
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Port'), WTS_PARAM_IMAP . 4));      
        $cField->Input(cField::cElementInput(WTS_PARAM_IMAP . 4, $cCurQueue->iPort(cQueue::IMAP)));
        
        $cElementForm->Content($cField->cBuild(true));
        

        $cField = new cField(cField::CENTER);
        $cField->Input(cField::cElementSubmit(_('Update')));
        
        $cElementForm->Content($cField->cBuild());

        $cElement->Content($cElementForm);
        
        $cPage->Main($cElement);
      }
      
      //042. smtp settings element
      if($cCurQueue){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-main-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Attribute('style', 'text-align: center;');
        $cElementCapt->Content(new cElementBase(_('SMTP Settings')));
        
        $cElement->Content($cElementCapt);

        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
                                         . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_UPDATE)
                                         . sSubAction(WTS_PARAM_ID, $cCurQueue->iID()));
        $cElementForm->Attribute('method', 'post');
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Host'), WTS_PARAM_SMTP . 1));      
        $cField->Input(cField::cElementInput(WTS_PARAM_SMTP . 1, $cCurQueue->sHost(cQueue::SMTP)));
        
        $cElementForm->Content($cField->cBuild(true));
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Login'), WTS_PARAM_SMTP . 2));      
        $cField->Input(cField::cElementInput(WTS_PARAM_SMTP . 2, $cCurQueue->sLogin_(cQueue::SMTP)));
        
        $cElementForm->Content($cField->cBuild(true));
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Encryption'), WTS_PARAM_SMTP . 3));
        
        $cSelect = new cSelect(WTS_PARAM_SMTP . 3, WTS_PFX . '-input');
        $iEnc = $cCurQueue->iEncryption(cQueue::SMTP);
        $cSelect->Option(($iEnc === 0), 0, _('None'));
        $cSelect->Option(($iEnc === cQueue::ENC_SSL), cQueue::ENC_SSL, 'SSL');
        $cSelect->Option(($iEnc === cQueue::ENC_TLS), cQueue::ENC_TLS, 'TLS');
        
        $cField->Input($cSelect->cRoot());
        
        $cElementForm->Content($cField->cBuild(true));
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Port'), WTS_PARAM_SMTP . 4));      
        $cField->Input(cField::cElementInput(WTS_PARAM_SMTP . 4, $cCurQueue->iPort(cQueue::SMTP)));
        
        $cElementForm->Content($cField->cBuild(true));
        
        $cField = new cField(cField::CENTER);
        $cField->Input(cField::cElementSubmit(_('Update')));
        
        $cElementForm->Content($cField->cBuild());

        $cElement->Content($cElementForm);
        
        $cPage->Main($cElement);
      }
      
      //043. template settings element
      if($cCurQueue){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-main-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Attribute('style', 'text-align: center;');
        $cElementCapt->Content(new cElementBase(_('Template Settings')));
        
        $cElement->Content($cElementCapt);

        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
                                         . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_UPDATE)
                                         . sSubAction(WTS_PARAM_ID, $cCurQueue->iID()));
        $cElementForm->Attribute('method', 'post');
        
        //загрузим все шаблоны
        $aResponsesID = $aResponses = false;
        $cCurQueue->iUnserializeResps($this->cDb, $aResponsesID);
        if($aResponsesID !== false){

          cResponse::iUnserialize($this->cDb, $aResponses);
          
          foreach($aResponsesID as $k => $v){
            
            $cField = new cField();
            $cField->Label(cField::cElementLabel($cCurQueue->mRespsUiName($k), WTS_PARAM_TEMPLATE . $k));
            
            $cSelect = new cSelect(WTS_PARAM_TEMPLATE . $k, WTS_PFX . '-input');
            $cSelect->Option($v === 0, 0, _('Not use'));
            
            if($aResponses !== false){
              foreach($aResponses as &$c){
                $cSelect->Option($v === $c->iID(), $c->iID(), $c->sName());
              }
            }
            $cField->Input($cSelect->cRoot());
            
            $cElementForm->Content($cField->cBuild(true));
          }
        
        }

        $cField = new cField(cField::CENTER);
        $cField->Input(cField::cElementSubmit(_('Update')));
        
        $cElementForm->Content($cField->cBuild());
        $cElement->Content($cElementForm);
        
        $cPage->Main($cElement);
      }
      
      //050. side bar create or update
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      
      $c = ($cCurQueue ? $cCurQueue : false);
      
      if($c){
        $cElementCapt->Content(new cElementBase(_('Queue Properties')));
      }
      else{
        $cElementCapt->Content(new cElementBase(_('Add New Queue')));
      }
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
          . sSubAction(WTS_PARAM_EDIT, ($c ? WTS_PARAM_EACT_UPDATE : WTS_PARAM_EACT_CREATE))
          . ($c ? sSubAction(WTS_PARAM_ID, $c->iID()) : ''));
      $cElementForm->Attribute('method', 'post');
      $cElementForm->Attribute('name', WTS_PARAM_ACT_QUEUE);
      $cElementForm->Attribute('onsubmit', 'return validate()');
      
      $cField = new cField();

      $cField->Label(cField::cElementLabel(_('Login'), WTS_PARAM_LOGIN));
      $cElementInp = cField::cElementInput(WTS_PARAM_LOGIN, ($c ? $c->sLogin() : null));
      $cElementInp->Attribute('placeholder', 'my.queue@example.com');
      $cElementInp->Attribute('title', _('Login for Queue must be unique. Login must be real email address.'));
      $cField->Input($cElementInp);
      
      $cElementSpan = new cElement('span', false);
      $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
      $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_LOGIN);
      $cField->Label($cElementSpan);

      $sScript = '
function validate(){
  if(document.forms[\'' . WTS_PARAM_ACT_QUEUE . '\'][\'' . WTS_PARAM_LOGIN . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_LOGIN . '\').innerHTML=\'&#9998;\';
    return false;
  }
  return true;
}';
        
      $cElementS = new cElement('script', false);
      $cElementS->Attribute('type', 'text/javascript');
      $cElementS->Content(new cElementBase($sScript, false));
      $cPage->Head($cElementS);
      
      $cElementForm->Content($cField->cBuild());
      
      if($c){
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
        $cField->Input(cField::cElementInput(WTS_PARAM_NAME, $c->sName()));
        
        $cElementForm->Content($cField->cBuild());
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Password'), WTS_PARAM_PWD));      
        $cField->Input(cField::cElementInput(WTS_PARAM_PWD, $c->sPasswd(), 'password'));
      
        $cElementForm->Content($cField->cBuild());
        
        //$cElementHR = new cElement('div', false);
        //$cElementHR->Attribute('class', 'wts-hr');
        
        //$cElementForm->Content($cElementHR);
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Is Disabled'), WTS_PARAM_DISABLED));
        
        $cSelect = new cSelect(WTS_PARAM_DISABLED, WTS_PFX . '-input');
        $cSelect->Option($c->bDisabled(), 'true', _('Yes'));
        $cSelect->Option(!$c->bDisabled(), 'false', _('No'));
        
        $cField->Input($cSelect->cRoot());
        
        $cElementForm->Content($cField->cBuild());
      }
      
      //ok
      $cField = new cField(cField::RIGHT);
      if($c){
        $cField->Input(cField::cElementSubmit(_('Reset'), true));
        $cField->Input(cField::cElementSubmit(_('Apply')));
      }
      else{
        $cField->Input(cField::cElementSubmit(_('Create')));
      }
      $cElementForm->Content($cField->cBuild());
      
      $cElement->Content($cElementForm);
      
      $cPage->Aside($cElement);

      //060. queue status
      if($c && $c->iID() > 1){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Queue Info')));
        
        $cElement->Content($cElementCapt);
        
        $cToc = new cToc();
        
        //agents in queue
        cCont::bAddFilterQueue($c->iID());
        $iConts = cCont::iCount($this->cDb);

        $cElementInfo = new cElement('a', false);
        $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CONT)
        . sSubAction(WTS_PARAM_ACT_QUEUE, $c->iID()));
        $cElementInfo->Content(new cElementBase($iConts > 0 ? $iConts : _('Not set')));
        $cToc->Child(new cElementBase(_('Agents')), $cElementInfo);
       
        
        //open chains
        cChain::bAddFilterQueue($c->iID());
        cChain::bAddFilterClosed(false);
        $iChains = cChain::iCount($this->cDb);
        if($iChains > 0){
          $cElementInfo = cSE::cCreate('a', $iChains, WTS_PFX . '-alert ' . WTS_PFX . '-info2');
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CHAIN)
          . sSubAction(WTS_PARAM_ACT_QUEUE, $c->iID())
          . sSubAction(WTS_PARAM_CLOSED, 'false'));
          $cToc->Child(new cElementBase(_('Open Chains')), $cElementInfo);
        }
        
        //closed chains
        cChain::ClearFilters();
        cChain::bAddFilterQueue($c->iID());
        cChain::bAddFilterClosed(true);
        $iChains = cChain::iCount($this->cDb);
        if($iChains > 0){
          $cElementInfo = cSE::cCreate('a', $iChains, WTS_PFX . '-notify ' . WTS_PFX . '-info2');
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CHAIN)
          . sSubAction(WTS_PARAM_ACT_QUEUE, $c->iID())
          . sSubAction(WTS_PARAM_CLOSED, 'true'));
          $cToc->Child(new cElementBase(_('Closed Chains')), $cElementInfo);
        }
        
        $cElement->Content($cToc->cRoot());

        $cPage->Aside($cElement);
      }
    }
    
    public function DoRegExpPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref      = sAction(WTS_PARAM_ACT_REGEXP);
      
      $iCurQueue = 0;
      if(isset($_GET[WTS_PARAM_SEARCH]) && (int)$_GET[WTS_PARAM_SEARCH] > 1){
        $iCurQueue = (int)$_GET[WTS_PARAM_SEARCH];
      }
      
      $aQueues   = false;
      $cCurQueue = false;
      if(cQueue::iUnserialize($this->cDb, $aQueues) > 0
      && isset($_GET[WTS_PARAM_SEARCH])
      && (int)$_GET[WTS_PARAM_SEARCH] > 1){
        $cCurQueue = &cQueue::mInArray($aQueues, $_GET[WTS_PARAM_SEARCH]);
      }
      
      $aRegExps   = false;
      $cCurRegExp = false;
      $iRows      = 0;
      if($cCurQueue){
        $iRows = cRegExp::iUnserialize($this->cDb, $cCurQueue->iID(), $aRegExps);
        if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
          $cCurRegExp = &cRegExp::mInArray($aRegExps, $_GET[WTS_PARAM_ID]);
        }
      }
      
      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false && $cCurQueue !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $cObj = new cRegExp($cCurQueue->iID());
          if(isset($_POST[WTS_PARAM_NAME])){
            $cObj->Name($_POST[WTS_PARAM_NAME]);
            $cObj->Value(_('Type content here.'));
          }
          if($cObj->bSerialize($this->cDb)){$cCurRegExp = &$cObj;}
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create RegExp using Name \'') . $cObj->sName() . '\'.');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_DELETE:
          if($cCurRegExp){
            $cCurRegExp->bDelete($this->cDb);
            unset($cCurRegExp);
            $cCurRegExp = false;
          }
          break; 
        case WTS_PARAM_EACT_UPDATE:
          if($cCurRegExp){
            if(isset($_POST[WTS_PARAM_VALUE])){$cCurRegExp->Value($_POST[WTS_PARAM_VALUE]);}
            $cCurRegExp->bSerialize($this->cDb);
          }
          break;
        default: break;
        }
        $sHref .= sSubAction(WTS_PARAM_SEARCH, $cCurQueue->iID());
        if($cCurRegExp !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurRegExp->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_REGEXP);
      if($cCurQueue)$cButton->Hidden(WTS_PARAM_SEARCH, $cCurQueue->iID());
      $cElement->Content($cButton->cRoot());
      
      //select cur queue
      $cButton = new cButton();
        
      $cSelect = new cSelect(WTS_PARAM_SEARCH, WTS_PFX . '-toolbar');
      if($cCurQueue === false){
        if(count($aQueues) > 1){$cSelect->Option(true, 0, _('Please select Queue...'));}
        else{$cSelect->Option(true, 0, _('Please create work Queue'));}
      }
      
      if($aQueues){
        foreach($aQueues as $key => &$c){
          if($c->iID() === 1)continue;
          $cSelect->Option(($cCurQueue ? $cCurQueue->iID() === $c->iID() : false), $c->iID(), $c->sName());
          unset($aQueues[$key]);
        }
      }
      
      $cElementS = $cSelect->cRoot();
      $cElementS->Attribute('onchange', 'this.form.submit();');
      $cButton->Content($cElementS);
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_REGEXP);
      $cElement->Content($cButton->cRoot());
      
      //button delete
      if($cCurRegExp){
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete RegExp'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_REGEXP);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        $cButton->Hidden(WTS_PARAM_SEARCH, $cCurQueue->iID());
        $cButton->Hidden(WTS_PARAM_ID, $cCurRegExp->iID());
        $cElement->Content($cButton->cRoot());
      }
      $cPage->Main($cElement);
      
      //040. table
      if($cCurQueue){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-main-section');
        
        $cTable = new cTable(WTS_PFX . '-table');
        $cTable->Column(_('RegExp Name'));
        
        if($iRows > 0){
          
          $iID = 0;
          if($cCurRegExp){$iID = $cCurRegExp->iID();}
          
          foreach($aRegExps as &$c){
            //row
            $cElementTr = new cElement('tr', false);
            if($c->iID() == $iID){
              $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
            }
            else{
              $cElementTr->Attribute('onclick', 'window.open(\''
              . $sHref
              . sSubAction(WTS_PARAM_SEARCH, $cCurQueue->iID())
              . sSubAction(WTS_PARAM_ID, $c->iID())
              . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
            }

            //column 1
            $cElementTd = new cElement('td', false);

            $cElementTd->Content(new cElementBase($c->sName()));
            $cElementTr->Content($cElementTd);

            $cTable->Row($cElementTr);
          }
        }
        else{$cTable->ColSpan(_('No data found'));}
        
        $cElement->Content($cTable->cRoot());

        $cPage->Main($cElement);
      }
      
      //050. html-edit element
      if($cCurRegExp){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-main-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Attribute('style', 'text-align: center;');
        $cElementCapt->Content(new cElementBase(_('RegExp Content')));
        
        $cElement->Content($cElementCapt);

        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
                                         . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_UPDATE)
                                         . sSubAction(WTS_PARAM_SEARCH, $cCurQueue->iID())
                                         . sSubAction(WTS_PARAM_ID, $cCurRegExp->iID()));
        $cElementForm->Attribute('method', 'post');
        
        $cField = new cField();
        $cField->Input(cField::cElementTextArea(WTS_PARAM_VALUE, $cCurRegExp->sValue()));
        
        $cElementForm->Content($cField->cBuild());
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Attribute('src', WTS_PATH . 'nicedit/nicEdit.js');
        $cPage->Head($cElementS);
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $sScript = cField::sNicEditInitScript(cField::sNicEditCreateScript(WTS_PARAM_VALUE, 250));
        $cElementS->Content(new cElementBase($sScript, true));

        $cPage->Head($cElementS);
        
        
        $cField = new cField(cField::CENTER);
        $cElementUpd = cField::cElementSubmit(_('Update'));
        $cElementUpd->Attribute('id', 'id_' . WTS_PARAM_VALUE);
        $cElementUpd->Attribute('onclick', 'window.content_changed = false;');
        $cField->Input($cElementUpd);
        
        $cElementForm->Content($cField->cBuild());

        $cElement->Content($cElementForm);
        
        $cPage->Main($cElement);
      }
      
      //060. side bar create
      if($cCurQueue){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Add New Regexp')));
        
        $cElement->Content($cElementCapt);
        
        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
                                         . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_CREATE)
                                         . sSubAction(WTS_PARAM_SEARCH, $cCurQueue->iID()));
        $cElementForm->Attribute('method', 'post');
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
        $cField->Input(cField::cElementInput(WTS_PARAM_NAME));
        
        $cElementForm->Content($cField->cBuild());
        
        $cField = new cField(cField::RIGHT);
        $cField->Input(cField::cElementSubmit(_('Create')));
        
        $cElementForm->Content($cField->cBuild());

        $cElement->Content($cElementForm);
        
        $cPage->Aside($cElement);
      }
    }
    
    public function DoTagPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref   = sAction(WTS_PARAM_ACT_TAG);
      $aTags   = false;
      $cCurTag = false;
      $iRows = cTag::iUnserialize($this->cDb, $aTags);
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurTag = &cTag::mInArray($aTags, $_GET[WTS_PARAM_ID]);
      }
      
      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $cObj = new cTag();
          if(isset($_POST[WTS_PARAM_NAME])){$cObj->Name($_POST[WTS_PARAM_NAME]);}
          if(isset($_POST[WTS_PARAM_SLUG])){$cObj->Slug($_POST[WTS_PARAM_SLUG]);}

          if($cObj->bSerialize($this->cDb)){$cCurTag = &$cObj;}
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Tag using Name \'') . $cObj->sName() . '\' and Slug \'' . $cObj->sSlug() . '\'.');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_DELETE:
          if($cCurTag){
            if(cChain::iTagUsage($this->cDb, $cCurTag->iID()) > 0
            || cContract::iTagUsage($this->cDb, $cCurTag->iID()) > 0){
              $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
              $cMsg->Value(_('Tag \'') . $cCurTag->sName() . '\' in use. Can\'t delete this Tag.');
              $cMsg->bSerialize($this->cDb);
              unset($cMsg);
            }
            else{
              $cCurTag->bDelete($this->cDb);
              unset($cCurTag);
              $cCurTag = false;
            }
          }
          break; 
        case WTS_PARAM_EACT_UPDATE:
          if($cCurTag){
            if(isset($_POST[WTS_PARAM_NAME])){$cCurTag->Name($_POST[WTS_PARAM_NAME]);}
            if(isset($_POST[WTS_PARAM_COLOR])){$cCurTag->Color($_POST[WTS_PARAM_COLOR]);}
            $cCurTag->bSerialize($this->cDb);
          }
          break;
        default: break;
        }
        if($cCurTag !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurTag->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_TAG);
      $cElement->Content($cButton->cRoot());
      
      if($cCurTag){
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete Tag'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_TAG);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        $cButton->Hidden(WTS_PARAM_ID, $cCurTag->iID());
        $cElement->Content($cButton->cRoot());
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Tag Name'));
      $cTable->Column(_('Slug'));
      $cTable->Column(_('Color'), '15%');
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }

        $iID = 0;
        if($cCurTag){$iID = $cCurTag->iID();}
        
        foreach($aTags as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);

          $cElementTd->Content(new cElementBase($c->sName()));
          $cElementTr->Content($cElementTd);
          //column 2
          $cElementTd = new cElement('td', false);

          $cElementTd->Content(new cElementBase($c->sSlug()));
          $cElementTr->Content($cElementTd);
          //column 3
          $cElementTd = new cElement('td', false);
          
          if($c->sColor()){
            $cElementDiv = new cElement('div', false);
            $cElementDiv->Attribute('style', 'background-color:#' . $c->sColor() . ';');
            $cElementDiv->Attribute('class', WTS_PFX . '-table-color');
            $cElementDiv->Content(new cElementBase('#' . $c->sColor()));

            $cElementTd->Content($cElementDiv);
          }
          $cElementTr->Content($cElementTd);

          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //050. side bar create or update
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      
      $c = ($cCurTag ? $cCurTag : false);
      
      if($c){
        $cElementCapt->Content(new cElementBase(_('Tag Properties')));
      }
      else{
        $cElementCapt->Content(new cElementBase(_('Add New Tag')));
      }
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
          . sSubAction(WTS_PARAM_EDIT, ($c ? WTS_PARAM_EACT_UPDATE : WTS_PARAM_EACT_CREATE))
          . ($c ? sSubAction(WTS_PARAM_ID, $c->iID()) : ''));
      $cElementForm->Attribute('method', 'post');
      $cElementForm->Attribute('name', WTS_PARAM_ACT_TAG);
      $cElementForm->Attribute('onsubmit', 'return validate()');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
      
      $cElementInp = cField::cElementInput(WTS_PARAM_NAME, ($c ? $c->sName() : null));
      $cElementInp->Attribute('placeholder', _('My Tag'));
      $cElementInp->Attribute('title', _('Name of Tag must be unique.'));
      
      $cField->Input($cElementInp);
      
      $cElementSpan = new cElement('span', false);
      $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
      $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_NAME);
      $cField->Label($cElementSpan);
      $cElementForm->Content($cField->cBuild());
      
      if($c){
        
        $sScript = '
function validate(){
  if(document.forms[\'' . WTS_PARAM_ACT_TAG . '\'][\'' . WTS_PARAM_NAME . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_NAME . '\').innerHTML=\'&#9998;\';
    return false;
  }
  return true;
}';
          
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Content(new cElementBase($sScript, false));
        $cPage->Head($cElementS);
        
        //color
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Color'), WTS_PARAM_COLOR));
        $cElementInput = cField::cElementInput(WTS_PARAM_COLOR, $c->sColor());
        $cElementInput->Attribute('class', WTS_PFX . '-input ' . WTS_PARAM_COLOR);
        $cField->Input($cElementInput);
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Attribute('src', WTS_PATH . 'jscolor/jscolor.js');
        $cPage->Head($cElementS);
        
        $cElementForm->Content($cField->cBuild());
      }
      else{
      
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Slug'), WTS_PARAM_SLUG));
        $cElementInp = cField::cElementInput(WTS_PARAM_SLUG);
        $cElementInp->Attribute('placeholder', _('my-tag'));
        $cElementInp->Attribute('title', _('Slug must be unique.'));
        
        $cField->Input($cElementInp);
        
        $cElementSpan = new cElement('span', false);
        $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
        $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_SLUG);
        $cField->Label($cElementSpan);
        $cElementForm->Content($cField->cBuild());

        $sScript = '
function validate(){
  var ret=true;
  if(document.forms[\'' . WTS_PARAM_ACT_TAG . '\'][\'' . WTS_PARAM_NAME . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_NAME . '\').innerHTML=\'&#9998;\';
    ret=false;
  }
  if(document.forms[\'' . WTS_PARAM_ACT_TAG . '\'][\'' . WTS_PARAM_SLUG . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_SLUG . '\').innerHTML=\'&#9998;\';
    ret=false;
  }
  return ret;
}';
          
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Content(new cElementBase($sScript, false));
        $cPage->Head($cElementS);
      }
      
      //ok
      $cField = new cField(cField::RIGHT);
      if($c){
        $cField->Input(cField::cElementSubmit(_('Reset'), true));
        $cField->Input(cField::cElementSubmit(_('Apply')));
      }
      else{
        $cField->Input(cField::cElementSubmit(_('Create')));
      }
      $cElementForm->Content($cField->cBuild());
      
      $cElement->Content($cElementForm);
      
      $cPage->Aside($cElement);
      
      //060. tag status
      if($cCurTag){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Tag Usage')));
        
        $cElement->Content($cElementCapt);
        
        $cToc = new cToc();

        //chains
        $aSlugs = array($cCurTag->sSlug());
        cChain::bAddFilterTag($this->cDb, $aSlugs);
        $iChains = cChain::iCount($this->cDb);
        if($iChains > 0){
          $cElementInfo = new cElement('a', false);
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CHAIN)
          . sSubAction(WTS_PARAM_TYPE, cChActivity::ALL)
          . sSubAction(WTS_PARAM_ACT_TAG, '{' . $cCurTag->sSlug() . '}'));
          $cElementInfo->Content(new cElementBase($iChains));
          $cToc->Child(new cElementBase(_('Chains')), $cElementInfo);
        }
        else{
          $cToc->Child(new cElementBase(_('Chains')), new cElementBase('Not use'));
        }
        
        //contracts
        cContract::bAddFilterTag($this->cDb, $aSlugs);
        $iContracts = cContract::iCount($this->cDb);
        if($iContracts > 0){
          $cElementInfo = new cElement('a', false);
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CONTR)
          . sSubAction(WTS_PARAM_ACT_TAG, '{' . $cCurTag->sSlug() . '}'));
          $cElementInfo->Content(new cElementBase($iContracts));
          $cToc->Child(new cElementBase(_('Contracts')), $cElementInfo);
        }
        else{
          $cToc->Child(new cElementBase(_('Contracts')), new cElementBase('Not use'));
        }

        $cElement->Content($cToc->cRoot());
        
        $cPage->Aside($cElement);
      }
    }
    
    public function DoContactAttrPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref    = sAction(WTS_PARAM_ACT_CONT_ATTR);
      $aAttrTypes   = false;
      $cCurAttrType = false;
      $iRows = cContAttrType::iUnserialize($this->cDb, $aAttrTypes);
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurAttrType = &cContAttrType::mInArray($aAttrTypes, $_GET[WTS_PARAM_ID]);
      }
      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        $bRebuildNames = false;
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $bMultiple = (isset($_POST[WTS_PARAM_MULTIPLE]) && $_POST[WTS_PARAM_MULTIPLE] === 'true') ? true : false;
          $cObj = new cContAttrType($bMultiple);
          if(isset($_POST[WTS_PARAM_NAME])){$cObj->Name($_POST[WTS_PARAM_NAME]);}
          
          if($cObj->bSerialize($this->cDb)){$cCurAttrType = &$cObj;}
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Attribute using Name \'') . $cObj->sName() . '\'.');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_MOVE_UP:
          if($cCurAttrType
          && $cCurAttrType->bMoveUp($this->cDb)
          && $cCurAttrType->bInName()){
            $bRebuildNames = true;
          }
          break;
        case WTS_PARAM_EACT_MOVE_DOWN:
          if($cCurAttrType
          && $cCurAttrType->bMoveDown($this->cDb)
          && $cCurAttrType->bInName()){
            $bRebuildNames = true;
          }
          break;
        case WTS_PARAM_EACT_DELETE://remove - all attributes in group
          if($cCurAttrType){
            if($cCurAttrType->bInName()){$bRebuildNames = true;}
            cCont::DeleteAttrByType($this->cDb, $cCurAttrType->iID());
            $cCurAttrType->bDelete($this->cDb);
            unset($cCurAttrType);
            $cCurAttrType = false;
          }
          break; 
        case WTS_PARAM_EACT_UPDATE:
          if($cCurAttrType){
            if(isset($_POST[WTS_PARAM_NAME])){$cCurAttrType->Name($_POST[WTS_PARAM_NAME]);}
            if(isset($_POST[WTS_PARAM_COLOR])){$cCurAttrType->Color($_POST[WTS_PARAM_COLOR]);}
            if(isset($_POST[WTS_PARAM_IN_NAME])){
              $b = ($_POST[WTS_PARAM_IN_NAME] === 'true') ? true : false;
              if($cCurAttrType->bInName() !== $b){
                $cCurAttrType->InName($b);
                $bRebuildNames = true;
              }
            }
            $cCurAttrType->bSerialize($this->cDb);
          }
          break;
        default: break;
        }
        if($bRebuildNames){cCont::RebuildAllNames($this->cDb);}

        if($cCurAttrType !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurAttrType->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CONT_ATTR);
      $cElement->Content($cButton->cRoot());
      
      if($cCurAttrType){
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete Attribute Type'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CONT_ATTR);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        $cButton->Hidden(WTS_PARAM_ID, $cCurAttrType->iID());
        $cElement->Content($cButton->cRoot());
        
        if($iRows > 1){
          $cButton = new cButton();
          $cButton->Image(IMG_EDIT_MOVE_UP, _('Move Up'), WTS_PFX . '-toolbar-button');
          $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CONT_ATTR);
          $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_MOVE_UP);
          $cButton->Hidden(WTS_PARAM_ID, $cCurAttrType->iID());
          $cElement->Content($cButton->cRoot());
          
          $cButton = new cButton();
          $cButton->Image(IMG_EDIT_MOVE_DOWN, _('Move Down'), WTS_PFX . '-toolbar-button');
          $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CONT_ATTR);
          $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_MOVE_DOWN);
          $cButton->Hidden(WTS_PARAM_ID, $cCurAttrType->iID());
          $cElement->Content($cButton->cRoot());
        }
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Contact Attribute Name'));
      $cTable->Column(_('Color'), '15%');
      $cTable->Column(_('In Name'), '15%');
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }

        $iID = 0;
        if($cCurAttrType){$iID = $cCurAttrType->iID();}
        
        foreach($aAttrTypes as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          if($c->bMultiple()){
            $cElementImg->Attribute('src', IMG_ATTR_MULTIPLE);
            $cElementImg->Attribute('title', _('Multiple Attribute'));
          }
          else{
            $cElementImg->Attribute('src', IMG_ATTR_SIMPLE);
            $cElementImg->Attribute('title', _('Simple Attribute'));
          }
          
          $cElementTd->Content($cElementImg);

          $cElementTd->Content(new cElementBase($c->sName()));
          $cElementTr->Content($cElementTd);
          //column 2
          $cElementTd = new cElement('td', false);
          
          if($c->sColor()){
            $cElementDiv = new cElement('div', false);
            $cElementDiv->Attribute('style', 'background-color:#' . $c->sColor() . ';');
            $cElementDiv->Attribute('class', WTS_PFX . '-table-color');
            $cElementDiv->Content(new cElementBase('#' . $c->sColor()));

            $cElementTd->Content($cElementDiv);
          }
          $cElementTr->Content($cElementTd);
          //column 3
          $cElementTd = new cElement('td', false);

          if($c->bInName()){
            $cElementImg = new cElement('img', false);
            $cElementImg->Attribute('class', WTS_PFX . '-table-image');
            $cElementImg->Attribute('src', IMG_ACPT);
            $cElementTd->Content($cElementImg);
          }
          $cElementTr->Content($cElementTd);

          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //050. side bar create or update
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      
      $c = ($cCurAttrType ? $cCurAttrType : false);
      
      if($c){
        $cElementCapt->Content(new cElementBase(_('Attribute Type Properties')));
      }
      else{
        $cElementCapt->Content(new cElementBase(_('Add New Attribute Type')));
      }
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
          . sSubAction(WTS_PARAM_EDIT, ($c ? WTS_PARAM_EACT_UPDATE : WTS_PARAM_EACT_CREATE))
          . ($c ? sSubAction(WTS_PARAM_ID, $c->iID()) : ''));
      $cElementForm->Attribute('method', 'post');
      $cElementForm->Attribute('name', WTS_PARAM_ACT_CONT_ATTR);
      $cElementForm->Attribute('onsubmit', 'return validate()');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
      
      $cElementInp = cField::cElementInput(WTS_PARAM_NAME, ($c ? $c->sName() : null));
      $cElementInp->Attribute('placeholder', _('My Attribute Type'));
      $cElementInp->Attribute('title', _('Name of Attribute Type must be unique.'));
      
      $cField->Input($cElementInp);
      
      $cElementSpan = new cElement('span', false);
      $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
      $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_NAME);
      $cField->Label($cElementSpan);

      $sScript = '
function validate(){
  if(document.forms[\'' . WTS_PARAM_ACT_CONT_ATTR . '\'][\'' . WTS_PARAM_NAME . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_NAME . '\').innerHTML=\'&#9998;\';
    return false;
  }
  return true;
}';
        
      $cElementS = new cElement('script', false);
      $cElementS->Attribute('type', 'text/javascript');
      $cElementS->Content(new cElementBase($sScript, false));
      $cPage->Head($cElementS);
      
      $cElementForm->Content($cField->cBuild());
      
      if($c){//color
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Color'), WTS_PARAM_COLOR));
        $cElementInput = cField::cElementInput(WTS_PARAM_COLOR, $c->sColor());
        $cElementInput->Attribute('class', WTS_PFX . '-input ' . WTS_PARAM_COLOR);
        $cField->Input($cElementInput);
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Attribute('src', WTS_PATH . 'jscolor/jscolor.js');
        $cPage->Head($cElementS);
        
        $cElementForm->Content($cField->cBuild());
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('In Name'), WTS_PARAM_IN_NAME));
        
        $cSelect = new cSelect(WTS_PARAM_IN_NAME, WTS_PFX . '-input');
        $cSelect->Option($c->bInName(), 'true', _('Yes'));
        $cSelect->Option(!$c->bInName(), 'false', _('No'));
        
        $cField->Input($cSelect->cRoot());
        
        $cElementForm->Content($cField->cBuild());
      }
      else{//simple || multi
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Is Multiple'), WTS_PARAM_MULTIPLE));
        
        $cSelect = new cSelect(WTS_PARAM_MULTIPLE, WTS_PFX . '-input');
        
        $cSelect->Option(true, 'false', _('No'));
        $cSelect->Option(false, 'true', _('Yes'));
        
        $cField->Input($cSelect->cRoot());
        
        $cElementForm->Content($cField->cBuild());
      }
      
      //ok
      $cField = new cField(cField::RIGHT);
      if($c){
        $cField->Input(cField::cElementSubmit(_('Reset'), true));
        $cField->Input(cField::cElementSubmit(_('Apply')));
      }
      else{
        $cField->Input(cField::cElementSubmit(_('Create')));
      }
      $cElementForm->Content($cField->cBuild());
      
      $cElement->Content($cElementForm);
      
      $cPage->Aside($cElement);
    }
    
    public function DoGroupAttrPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref    = sAction(WTS_PARAM_ACT_GROUP_ATTR);
      $aAttrTypes   = false;
      $cCurAttrType = false;
      $iRows = cGroupAttrType::iUnserialize($this->cDb, $aAttrTypes);
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurAttrType = &cGroupAttrType::mInArray($aAttrTypes, $_GET[WTS_PARAM_ID]);
      }
      
      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $bMultiple = (isset($_POST[WTS_PARAM_MULTIPLE]) && $_POST[WTS_PARAM_MULTIPLE] === 'true') ? true : false;
          $cObj = new cGroupAttrType($bMultiple);
          if(isset($_POST[WTS_PARAM_NAME])){$cObj->Name($_POST[WTS_PARAM_NAME]);}
          
          if($cObj->bSerialize($this->cDb)){$cCurAttrType = &$cObj;}
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Attribute using Name \'') . $cObj->sName() . '\'.');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_MOVE_UP:
          if($cCurAttrType){$cCurAttrType->bMoveUp($this->cDb);}
          break;
        case WTS_PARAM_EACT_MOVE_DOWN:
          if($cCurAttrType){$cCurAttrType->bMoveDown($this->cDb);}
          break;
        case WTS_PARAM_EACT_DELETE://remove - all attributes in group
          if($cCurAttrType){
            cGroup::DeleteAttrByType($this->cDb, $cCurAttrType->iID());
            $cCurAttrType->bDelete($this->cDb);
            unset($cCurAttrType);
            $cCurAttrType = false;
          }
          break; 
        case WTS_PARAM_EACT_UPDATE:
          if($cCurAttrType){
            if(isset($_POST[WTS_PARAM_NAME])){$cCurAttrType->Name($_POST[WTS_PARAM_NAME]);}
            if(isset($_POST[WTS_PARAM_COLOR])){$cCurAttrType->Color($_POST[WTS_PARAM_COLOR]);}
            $cCurAttrType->bSerialize($this->cDb);
          }
          break;
        default: break;
        }
        if($cCurAttrType !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurAttrType->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_GROUP_ATTR);
      $cElement->Content($cButton->cRoot());
      
      if($cCurAttrType){
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete Attribute Type'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_GROUP_ATTR);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        $cButton->Hidden(WTS_PARAM_ID, $cCurAttrType->iID());
        $cElement->Content($cButton->cRoot());
        
        if($iRows > 1){
          $cButton = new cButton();
          $cButton->Image(IMG_EDIT_MOVE_UP, _('Move Up'), WTS_PFX . '-toolbar-button');
          $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_GROUP_ATTR);
          $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_MOVE_UP);
          $cButton->Hidden(WTS_PARAM_ID, $cCurAttrType->iID());
          $cElement->Content($cButton->cRoot());
          
          $cButton = new cButton();
          $cButton->Image(IMG_EDIT_MOVE_DOWN, _('Move Down'), WTS_PFX . '-toolbar-button');
          $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_GROUP_ATTR);
          $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_MOVE_DOWN);
          $cButton->Hidden(WTS_PARAM_ID, $cCurAttrType->iID());
          $cElement->Content($cButton->cRoot());
        }
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Group Attribute Name'));
      $cTable->Column(_('Color'), '15%');
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }

        $iID = 0;
        if($cCurAttrType){$iID = $cCurAttrType->iID();}
        
        foreach($aAttrTypes as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          if($c->bMultiple()){
            $cElementImg->Attribute('src', IMG_ATTR_MULTIPLE);
            $cElementImg->Attribute('title', _('Multiple Attribute'));
          }
          else{
            $cElementImg->Attribute('src', IMG_ATTR_SIMPLE);
            $cElementImg->Attribute('title', _('Simple Attribute'));
          }
          
          $cElementTd->Content($cElementImg);

          $cElementTd->Content(new cElementBase($c->sName()));
          $cElementTr->Content($cElementTd);
          //column 2
          $cElementTd = new cElement('td', false);
          
          if($c->sColor()){
            $cElementDiv = new cElement('div', false);
            $cElementDiv->Attribute('style', 'background-color:#' . $c->sColor() . ';');
            $cElementDiv->Attribute('class', WTS_PFX . '-table-color');
            $cElementDiv->Content(new cElementBase('#' . $c->sColor()));

            $cElementTd->Content($cElementDiv);
          }
          $cElementTr->Content($cElementTd);

          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //050. side bar create or update
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      
      $c = ($cCurAttrType ? $cCurAttrType : false);
      
      if($c){
        $cElementCapt->Content(new cElementBase(_('Attribute Type Properties')));
      }
      else{
        $cElementCapt->Content(new cElementBase(_('Add New Attribute Type')));
      }
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
          . sSubAction(WTS_PARAM_EDIT, ($c ? WTS_PARAM_EACT_UPDATE : WTS_PARAM_EACT_CREATE))
          . ($c ? sSubAction(WTS_PARAM_ID, $c->iID()) : ''));
      $cElementForm->Attribute('method', 'post');
      $cElementForm->Attribute('name', WTS_PARAM_ACT_GROUP_ATTR);
      $cElementForm->Attribute('onsubmit', 'return validate()');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
      
      $cElementInp = cField::cElementInput(WTS_PARAM_NAME, ($c ? $c->sName() : null));
      $cElementInp->Attribute('placeholder', _('My Attribute Type'));
      $cElementInp->Attribute('title', _('Name of Attribute Type must be unique.'));
      
      $cField->Input($cElementInp);
      
      $cElementSpan = new cElement('span', false);
      $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
      $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_NAME);
      $cField->Label($cElementSpan);

      $sScript = '
function validate(){
  if(document.forms[\'' . WTS_PARAM_ACT_GROUP_ATTR . '\'][\'' . WTS_PARAM_NAME . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_NAME . '\').innerHTML=\'&#9998;\';
    return false;
  }
  return true;
}';
        
      $cElementS = new cElement('script', false);
      $cElementS->Attribute('type', 'text/javascript');
      $cElementS->Content(new cElementBase($sScript, false));
      $cPage->Head($cElementS);
      
      $cElementForm->Content($cField->cBuild());
      
      if($c){//color
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Color'), WTS_PARAM_COLOR));
        $cElementInput = cField::cElementInput(WTS_PARAM_COLOR, $c->sColor());
        $cElementInput->Attribute('class', WTS_PFX . '-input ' . WTS_PARAM_COLOR);
        $cField->Input($cElementInput);
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Attribute('src', WTS_PATH . 'jscolor/jscolor.js');
        $cPage->Head($cElementS);
        
        $cElementForm->Content($cField->cBuild());
      }
      else{//simple || multi
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Is Multiple'), WTS_PARAM_MULTIPLE));
        
        $cSelect = new cSelect(WTS_PARAM_MULTIPLE, WTS_PFX . '-input');
        
        $cSelect->Option(true, 'false', _('No'));
        $cSelect->Option(false, 'true', _('Yes'));
        
        $cField->Input($cSelect->cRoot());
        
        $cElementForm->Content($cField->cBuild());
      }
      
      //ok
      $cField = new cField(cField::RIGHT);
      if($c){
        $cField->Input(cField::cElementSubmit(_('Reset'), true));
        $cField->Input(cField::cElementSubmit(_('Apply')));
      }
      else{
        $cField->Input(cField::cElementSubmit(_('Create')));
      }
      $cElementForm->Content($cField->cBuild());
      
      $cElement->Content($cElementForm);
      
      $cPage->Aside($cElement);
    }
    
    public function DoProdAttrPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref    = sAction(WTS_PARAM_ACT_PROD_ATTR);
      $aAttrTypes   = false;
      $cCurAttrType = false;
      $iRows = cProdAttrType::iUnserialize($this->cDb, $aAttrTypes);
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurAttrType = &cProdAttrType::mInArray($aAttrTypes, $_GET[WTS_PARAM_ID]);
      }
      
      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $bMultiple = (isset($_POST[WTS_PARAM_MULTIPLE]) && $_POST[WTS_PARAM_MULTIPLE] === 'true') ? true : false;
          $cObj = new cProdAttrType($bMultiple);
          if(isset($_POST[WTS_PARAM_NAME])){$cObj->Name($_POST[WTS_PARAM_NAME]);}
          
          if($cObj->bSerialize($this->cDb)){$cCurAttrType = &$cObj;}
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Attribute using Name \'') . $cObj->sName() . '\'.');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_MOVE_UP:
          if($cCurAttrType){$cCurAttrType->bMoveUp($this->cDb);}
          break;
        case WTS_PARAM_EACT_MOVE_DOWN:
          if($cCurAttrType){$cCurAttrType->bMoveDown($this->cDb);}
          break;
        case WTS_PARAM_EACT_DELETE://remove - all attributes in group
          if($cCurAttrType){
            cProduct::DeleteAttrByType($this->cDb, $cCurAttrType->iID());
            $cCurAttrType->bDelete($this->cDb);
            unset($cCurAttrType);
            $cCurAttrType = false;
          }
          break; 
        case WTS_PARAM_EACT_UPDATE:
          if($cCurAttrType){
            if(isset($_POST[WTS_PARAM_NAME])){$cCurAttrType->Name($_POST[WTS_PARAM_NAME]);}
            if(isset($_POST[WTS_PARAM_COLOR])){$cCurAttrType->Color($_POST[WTS_PARAM_COLOR]);}
            $cCurAttrType->bSerialize($this->cDb);
          }
          break;
        default: break;
        }
        if($cCurAttrType !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurAttrType->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_PROD_ATTR);
      $cElement->Content($cButton->cRoot());
      
      if($cCurAttrType){
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete Attribute Type'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_PROD_ATTR);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        $cButton->Hidden(WTS_PARAM_ID, $cCurAttrType->iID());
        $cElement->Content($cButton->cRoot());
        
        if($iRows > 1){
          $cButton = new cButton();
          $cButton->Image(IMG_EDIT_MOVE_UP, _('Move Up'), WTS_PFX . '-toolbar-button');
          $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_PROD_ATTR);
          $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_MOVE_UP);
          $cButton->Hidden(WTS_PARAM_ID, $cCurAttrType->iID());
          $cElement->Content($cButton->cRoot());
          
          $cButton = new cButton();
          $cButton->Image(IMG_EDIT_MOVE_DOWN, _('Move Down'), WTS_PFX . '-toolbar-button');
          $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_PROD_ATTR);
          $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_MOVE_DOWN);
          $cButton->Hidden(WTS_PARAM_ID, $cCurAttrType->iID());
          $cElement->Content($cButton->cRoot());
        }
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Product Attribute Name'));
      $cTable->Column(_('Color'), '15%');
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }

        $iID = 0;
        if($cCurAttrType){$iID = $cCurAttrType->iID();}
        
        foreach($aAttrTypes as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          if($c->bMultiple()){
            $cElementImg->Attribute('src', IMG_ATTR_MULTIPLE);
            $cElementImg->Attribute('title', _('Multiple Attribute'));
          }
          else{
            $cElementImg->Attribute('src', IMG_ATTR_SIMPLE);
            $cElementImg->Attribute('title', _('Simple Attribute'));
          }
          
          $cElementTd->Content($cElementImg);

          $cElementTd->Content(new cElementBase($c->sName()));
          $cElementTr->Content($cElementTd);
          //column 2
          $cElementTd = new cElement('td', false);
          
          if($c->sColor()){
            $cElementDiv = new cElement('div', false);
            $cElementDiv->Attribute('style', 'background-color:#' . $c->sColor() . ';');
            $cElementDiv->Attribute('class', WTS_PFX . '-table-color');
            $cElementDiv->Content(new cElementBase('#' . $c->sColor()));

            $cElementTd->Content($cElementDiv);
          }
          $cElementTr->Content($cElementTd);

          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //050. side bar create or update
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      
      $c = ($cCurAttrType ? $cCurAttrType : false);
      
      if($c){
        $cElementCapt->Content(new cElementBase(_('Attribute Type Properties')));
      }
      else{
        $cElementCapt->Content(new cElementBase(_('Add New Attribute Type')));
      }
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
          . sSubAction(WTS_PARAM_EDIT, ($c ? WTS_PARAM_EACT_UPDATE : WTS_PARAM_EACT_CREATE))
          . ($c ? sSubAction(WTS_PARAM_ID, $c->iID()) : ''));
      $cElementForm->Attribute('method', 'post');
      $cElementForm->Attribute('name', WTS_PARAM_ACT_PROD_ATTR);
      $cElementForm->Attribute('onsubmit', 'return validate()');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
      
      $cElementInp = cField::cElementInput(WTS_PARAM_NAME, ($c ? $c->sName() : null));
      $cElementInp->Attribute('placeholder', _('My Attribute Type'));
      $cElementInp->Attribute('title', _('Name of Attribute Type must be unique.'));
      
      $cField->Input($cElementInp);
      
      $cElementSpan = new cElement('span', false);
      $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
      $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_NAME);
      $cField->Label($cElementSpan);

      $sScript = '
function validate(){
  if(document.forms[\'' . WTS_PARAM_ACT_PROD_ATTR . '\'][\'' . WTS_PARAM_NAME . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_NAME . '\').innerHTML=\'&#9998;\';
    return false;
  }
  return true;
}';
        
      $cElementS = new cElement('script', false);
      $cElementS->Attribute('type', 'text/javascript');
      $cElementS->Content(new cElementBase($sScript, false));
      $cPage->Head($cElementS);
      
      $cElementForm->Content($cField->cBuild());
      
      if($c){//color
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Color'), WTS_PARAM_COLOR));
        $cElementInput = cField::cElementInput(WTS_PARAM_COLOR, $c->sColor());
        $cElementInput->Attribute('class', WTS_PFX . '-input ' . WTS_PARAM_COLOR);
        $cField->Input($cElementInput);
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Attribute('src', WTS_PATH . 'jscolor/jscolor.js');
        $cPage->Head($cElementS);
        
        $cElementForm->Content($cField->cBuild());
      }
      else{//simple || multi
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Is Multiple'), WTS_PARAM_MULTIPLE));
        
        $cSelect = new cSelect(WTS_PARAM_MULTIPLE, WTS_PFX . '-input');
        
        $cSelect->Option(true, 'false', _('No'));
        $cSelect->Option(false, 'true', _('Yes'));
        
        $cField->Input($cSelect->cRoot());
        
        $cElementForm->Content($cField->cBuild());
      }
      
      //ok
      $cField = new cField(cField::RIGHT);
      if($c){
        $cField->Input(cField::cElementSubmit(_('Reset'), true));
        $cField->Input(cField::cElementSubmit(_('Apply')));
      }
      else{
        $cField->Input(cField::cElementSubmit(_('Create')));
      }
      $cElementForm->Content($cField->cBuild());
      
      $cElement->Content($cElementForm);
      
      $cPage->Aside($cElement);
    }
    
    public function DoCategoryPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref        = sAction(WTS_PARAM_ACT_CATEGORY);
      $aCategories  = false;
      $cCurCategory = false;
      $iRows = cCategory::iUnserialize($this->cDb, $aCategories);
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurCategory = &cCategory::mInArray($aCategories, $_GET[WTS_PARAM_ID]);
      }

      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $bNode = (isset($_POST[WTS_PARAM_NODE]) && $_POST[WTS_PARAM_NODE] === 'true') ? true : false;
          $cObj = new cCategory($bNode);
          if(isset($_POST[WTS_PARAM_NAME])){$cObj->Name($_POST[WTS_PARAM_NAME]);}
          if(isset($_POST[WTS_PARAM_PARENT])){$cObj->Parent($_POST[WTS_PARAM_PARENT]);}
          
          if($cObj->bSerialize($this->cDb)){$cCurCategory = &$cObj;}
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Category using Name \'') . $cObj->sName() . '\'.');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_DELETE:
          if($cCurCategory){
            cProduct::bAddFilterCategory($cCurCategory->iID());
            if(cProduct::iCount($this->cDb) > 0){
              $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
              $cMsg->Value(_('Category \'') . $cCurCategory->sName() . _('\' use in Products. Can\'t delete this Category.'));
              $cMsg->bSerialize($this->cDb);
              unset($cMsg);
            }
            else{
              $cCurCategory->bDelete($this->cDb);
              unset($cCurCategory);
              $cCurCategory = false;
            }
          }
          break; 
        case WTS_PARAM_EACT_UPDATE:
          if($cCurCategory){
            if(isset($_POST[WTS_PARAM_NAME])){$cCurCategory->Name($_POST[WTS_PARAM_NAME]);}
            if(isset($_POST[WTS_PARAM_COLOR])){$cCurCategory->Color($_POST[WTS_PARAM_COLOR]);}
            if(isset($_POST[WTS_PARAM_PARENT])){$cCurCategory->Parent($_POST[WTS_PARAM_PARENT]);}
            $cCurCategory->bSerialize($this->cDb);
          }
          break;
        default: break;
        }
        if($cCurCategory !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurCategory->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CATEGORY);
      $cElement->Content($cButton->cRoot());
      
      if($cCurCategory){
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete Category'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CATEGORY);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        $cButton->Hidden(WTS_PARAM_ID, $cCurCategory->iID());
        $cElement->Content($cButton->cRoot());

      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Category Name'));
      $cTable->Column(_('Color'), '15%');
      $cTable->Column(_('Is Node'), '15%');
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }

        $iID = 0;
        if($cCurCategory){$iID = $cCurCategory->iID();}
        
        foreach($aCategories as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);

          //$cElementTd->Content(new cElementBase(($c->iLevel() > 0 ? '○ ' : '● ') . $c->sName()));
          $cElementTd->Content(new cElementBase($c->sName()));
          
          $i = 7 + $c->iLevel()* 15;

          $cElementTd->Attribute('style', 'padding-left:' . $i . 'px;');
          $cElementTr->Content($cElementTd);
          
          //column 2
          $cElementTd = new cElement('td', false);
          
          if($c->sColor()){
            $cElementDiv = new cElement('div', false);
            $cElementDiv->Attribute('style', 'background-color:#' . $c->sColor() . ';');
            $cElementDiv->Attribute('class', WTS_PFX . '-table-color');
            $cElementDiv->Content(new cElementBase('#' . $c->sColor()));

            $cElementTd->Content($cElementDiv);
          }
          $cElementTr->Content($cElementTd);
          
          //column 3
          $cElementTd = new cElement('td', false);

          if($c->bIsNode()){
            $cElementImg = new cElement('img', false);
            $cElementImg->Attribute('class', WTS_PFX . '-table-image');
            $cElementImg->Attribute('src', IMG_ACPT);
            $cElementTd->Content($cElementImg);
          }
          $cElementTr->Content($cElementTd);

          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //050. side bar create or update
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      
      if($cCurCategory){
        $cElementCapt->Content(new cElementBase(_('Category Properties')));
      }
      else{
        $cElementCapt->Content(new cElementBase(_('Add New Category')));
      }
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
          . sSubAction(WTS_PARAM_EDIT, ($cCurCategory ? WTS_PARAM_EACT_UPDATE : WTS_PARAM_EACT_CREATE))
          . ($cCurCategory ? sSubAction(WTS_PARAM_ID, $cCurCategory->iID()) : ''));
      $cElementForm->Attribute('method', 'post');
      $cElementForm->Attribute('name', WTS_PARAM_ACT_CATEGORY);
      $cElementForm->Attribute('onsubmit', 'return validate()');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
      
      $cElementInp = cField::cElementInput(WTS_PARAM_NAME, ($cCurCategory ? $cCurCategory->sName() : null));
      $cElementInp->Attribute('placeholder', _('My Category'));
      $cElementInp->Attribute('title', _('Name of Category must be unique.'));
      
      $cField->Input($cElementInp);
      
      $cElementSpan = new cElement('span', false);
      $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
      $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_NAME);
      $cField->Label($cElementSpan);

      $sScript = '
function validate(){
  if(document.forms[\'' . WTS_PARAM_ACT_CATEGORY . '\'][\'' . WTS_PARAM_NAME . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_NAME . '\').innerHTML=\'&#9998;\';
    return false;
  }
  return true;
}';
        
      $cElementS = new cElement('script', false);
      $cElementS->Attribute('type', 'text/javascript');
      $cElementS->Content(new cElementBase($sScript, false));
      $cPage->Head($cElementS);
      
      $cElementForm->Content($cField->cBuild());
      
      if($cCurCategory){//color
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Color'), WTS_PARAM_COLOR));
        $cElementInput = cField::cElementInput(WTS_PARAM_COLOR, $cCurCategory->sColor());
        $cElementInput->Attribute('class', WTS_PFX . '-input ' . WTS_PARAM_COLOR);
        $cField->Input($cElementInput);
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Attribute('src', WTS_PATH . 'jscolor/jscolor.js');
        $cPage->Head($cElementS);
        
        $cElementForm->Content($cField->cBuild());
      }
      else{//is node
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Is Node'), WTS_PARAM_NODE));
        
        $cSelect = new cSelect(WTS_PARAM_NODE, WTS_PFX . '-input');
        
        $cSelect->Option(true, 'false', _('No'));
        $cSelect->Option(false, 'true', _('Yes'));
        
        $cElementS = $cSelect->cRoot();
        $cElementS->Attribute('title'
                          , _('Use Nodes for main sections. Unable to create a product from Node.'));
        
        $cField->Input($cElementS);
        
        $cElementForm->Content($cField->cBuild());
      }
      //parent
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Parent'), WTS_PARAM_PARENT));
      
      $cSelect = new cSelect(WTS_PARAM_PARENT, WTS_PFX . '-input');
      
      $cSelect->Option(($cCurCategory === false  || $cCurCategory->iParent() === 0), 0, _('Root'));
      
      if($iRows > 0){

        foreach($aCategories as $key => &$cCat){
          $sLabel = '';
          for($i = 0; $i < $cCat->iLevel(); $i++){
            $sLabel .= ' ';
          }
          //if($cCat->iLevel() === 0){$sLabel .= '● ';}
          //else{$sLabel .= '○ ';}
          
          //$sLabel .= $cCat->sName();
          //if($cCat->bIsNode()){$sLabel .= ' ✓';}
          if($cCat->bIsNode()){$sLabel .= '[' . $cCat->sName(). ']';}
          else{$sLabel .= $cCat->sName();}

          $cSelect->Option(($cCurCategory && $cCurCategory->iParent() === $cCat->iID()), $cCat->iID(), $sLabel, $cCat->sColor(), ($cCurCategory && $cCurCategory->iID() === $cCat->iID()));
          unset($aCategories[$key]);
        }
      }
      
      $cField->Input($cSelect->cRoot());
      
      $cElementForm->Content($cField->cBuild());
      
      //ok
      $cField = new cField(cField::RIGHT);
      if($cCurCategory){
        $cField->Input(cField::cElementSubmit(_('Reset'), true));
        $cField->Input(cField::cElementSubmit(_('Apply')));
      }
      else{
        $cField->Input(cField::cElementSubmit(_('Create')));
      }
      $cElementForm->Content($cField->cBuild());
      
      $cElement->Content($cElementForm);
      
      $cPage->Aside($cElement);

    }
  }
  
}
?>
