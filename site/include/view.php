<?php
/*
 * view.php (part of WTS) - classes for generate Web UI
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('core/utils/hypertext.php');
  
  /**
   * cSE - Simple Element
   */
  class cSE{
    //for span, b, a...
    public static function &cCreate($sName, $sContent, $sClass = null){
      $cElement = new cElement($sName, false);
      $cElement->Content(new cElementBase(htmlentities($sContent)));
      if(isset($sClass)){$cElement->Attribute('class', $sClass);}
      return $cElement;
    }
  }
  
  /**
   * cPage - page container
   *
   * @var sHead - string for scripts
   * @var sBody - string for content
   */
  class cPage{
    protected $aHead_;   //between tag <head>
    protected $aHeader_; //between tag <header>
    protected $aMain_;
    protected $aAside_; //between tag <aside> if exist - $aContent_ = $aAside_ ? wts-content : wts-content-wide 
    protected $aFooter_; //between tag <footer>
    
    /*

    | header (+ nav)      |
    |---------------------|
    | content     | aside |
    |             |       |
    |             |       |
    |             |       |
    |             |       |
    |             |       |
    |---------------------|
    | footer              |

    */
    
    public function Head(cElementBase &$cElementBase){
      $this->aHead_[] = $cElementBase;
    }
    
    public function Header(cElementBase &$cElementBase){
      $this->aHeader_[] = $cElementBase;
    }
    
    public function AddTitle(){
      $cElement = new cElement('a', false);
      $cElement->Attribute('href', WTS_LOGO_HREF);
      //$cElement->Attribute('target', '_blank');
      $cElement->Attribute('title', WTS_LOGO_TITLE);
     
      $cElementImg = new cElement('img', false);
      $cElementImg->Attribute('id', WTS_PFX . '-logo-img');
      $cElementImg->Attribute('src', IMG_MAIN_LOGO);
      $cElement->Content($cElementImg);
      
      $this->Header($cElement);
      
      $cElement = new cElement('hgroup', false);
      
      $cElementH1 = new cElement('div', false);
      $cElementH1->Attribute('id', WTS_PFX . '-logo-title');
      $cElementH1->Content(new cElementBase(WTS_TITLE));
      $cElement->Content($cElementH1);
      
      $cElementH2 = new cElement('div', false);
      $cElementH2->Attribute('id', WTS_PFX . '-logo-description');
      $cElementH2->Content(new cElementBase(WTS_TITLE_DESC));
      $cElement->Content($cElementH2);
      
      $this->Header($cElement);
    }
    
    public function Main(cElementBase &$cElementBase){
      $this->aMain_[] = $cElementBase;
    }
    
    public function Aside(cElementBase &$cElementBase){
      $this->aAside_[] = $cElementBase;
    }
    
    public function Footer(cElementBase &$cElementBase){
      $this->aFooter_[] = $cElementBase;
    }
    
    public function sSerialize($iHeaderHeight = 70){
      $cHead = new cElement('head', false);
      
      $cPageWrap = new cElement('div', false);
      $cPageWrap->Attribute('id', WTS_PFX . '-pagewrap');
      
      $cElement = new cElement('body', false);
      $cElement->Content($cPageWrap);
      
      $cHtml = new cElement('html', false);
      $cHtml->Content($cHead);
      $cHtml->Content($cElement);
      
      //--HEAD
      
      //WTS_CHARSET
      $cElement = new cElement('meta', false);
      $cElement->Attribute('charset', WTS_CHARSET);
      $cHead->Content($cElement);
      //WTS_TITLE
      $cHead->Content(cSE::cCreate('title', WTS_TITLE));
      //ICON
      $cElement = new cElement('link', false);
      $cElement->Attribute('rel', 'shortcut icon');
      $cElement->Attribute('href', IMG_MAIN_ICO);
      $cHead->Content($cElement);
      //CSS
      $cElement = $cElement->cCopy();
      $cElement->Attribute('rel', 'stylesheet');
      $cElement->Attribute('type', 'text/css');
      
      $sCSS = WTS_STYLES[0];
      if(isset($_COOKIE[WTS_COOKIE_PARAM_CSS])){
        $iCSS = (int)$_COOKIE[WTS_COOKIE_PARAM_CSS];
        if($iCSS > 0
        || !array_key_exists($iCSS, WTS_STYLES)){
          $sCSS = WTS_STYLES[$iCSS];
        }
      }
      
      $cElement->Attribute('href', $sCSS);
      $cHead->Content($cElement);
      
      $cElement = $cElement->cCopy();
      $cElement->Attribute('href', WTS_CSS_PATH . 'media-queries.css');
      $cHead->Content($cElement);
      
      //OVER SCRIPTS
      if(isset($this->aHead_)){
        foreach($this->aHead_ as &$cElementBase){
          $cHead->Content($cElementBase);
        }
      }
      
      //--PAGEWRAP (BODY)
      
      //header
      $cElement = new cElement('header', false);
      $cElement->Attribute('id', WTS_PFX . '-header');
      
      $iHeaderHeight = (int)$iHeaderHeight;
      if($iHeaderHeight < 70 ){$iHeaderHeight = 70;}
      
      $cElement->Attribute('style', 'background-image: url(\'' . IMG_MAIN_BKGRND
                                    . '\'); height: ' . $iHeaderHeight . 'px;');
      
      
      if(!isset($this->aHeader_)){$this->AddTitle();}
      
      foreach($this->aHeader_ as &$cElementBase){
        $cElement->Content($cElementBase);
      }
      
      $cPageWrap->Content($cElement);
      
      //content
      $cElement = new cElement('main', false);

      if(isset($this->aAside_)){$cElement->Attribute('id', WTS_PFX . '-main');}
      else{$cElement->Attribute('id', WTS_PFX . '-main-wide');}
      
      if(isset($this->aMain_)){
        foreach($this->aMain_ as &$cElementBase){
          $cElement->Content($cElementBase);
        }
      }
      
      $cPageWrap->Content($cElement);
      
      //sidebar
      if(isset($this->aAside_)){
        $cElement = new cElement('aside', false);
        $cElement->Attribute('id', WTS_PFX . '-sidebar');
        foreach($this->aAside_ as &$cElementBase){
          $cElement->Content($cElementBase);
        }
        $cPageWrap->Content($cElement);
      }
      
      //footer © WebTicketSystem, 2016
      $cElement = new cElement('footer', false);
      $cElement->Attribute('id', WTS_PFX . '-footer');
      
      $cElementP = new cElement('p', false);
      $cElementP->Content(new cElementBase('&copy; Web Ticket System, 2014-2018<br>Build: ' . WTS_BUILD));
      $cElement->Content($cElementP);

      $cPageWrap->Content($cElement);

      $s = '';
      
      echo '<!DOCTYPE html>' . $cHtml->sSerialize($s) . cString::EOL;
    }
    
  }
  
  class cField{
    //align
    const LEFT   = 10; 
    const CENTER = 20;
    const RIGHT  = 30;
    
    protected $cElementRoot_;
    protected $aElementsLabel_;
    protected $aElementsInput_;
    protected $bBuilded_;
    
    public function __construct($iAlign = self::LEFT){
      $this->cElementRoot_ = new cElement('div', false);
      $this->cElementRoot_->Attribute('class', WTS_PFX . '-input-bar');
      $iAlign = (int)$iAlign;
      switch($iAlign){
        case self::CENTER:
          $this->cElementRoot_->Attribute('style', 'text-align: center;');
          break;
        case self::RIGHT:
          $this->cElementRoot_->Attribute('style', 'text-align: right;');
          break;
        default: break;
      }
      $this->bBuilded_ = false;
    }
    
    public function Label(cElementBase &$cElementBase){
      $this->aElementsLabel_[] = $cElementBase;
    }
    public function Input(cElementBase &$cElementBase){
      $this->aElementsInput_[] = $cElementBase;
    }
    
    public function &cBuild($bForMain = false){
      if(!$this->bBuilded_){
        $this->bBuilded_ = true;
        
        if($bForMain){
          $cElement = new cElement('div', false);
          $cElement->Attribute('class', WTS_PFX . '-input-bar-left');
          
          if(isset($this->aElementsLabel_)){
            foreach($this->aElementsLabel_ as &$cElementBase){
              $cElement->Content($cElementBase);
            }
          }
          $this->cElementRoot_->Content($cElement);
          
        }
        else{
          if(isset($this->aElementsLabel_)){
            foreach($this->aElementsLabel_ as &$cElementBase){
              $this->cElementRoot_->Content($cElementBase);
            }
          }
        }
        
        if($bForMain){
          $cElement = new cElement('div', false);
          $cElement->Attribute('class', WTS_PFX . '-input-bar-right');
          
          if(isset($this->aElementsInput_)){
            foreach($this->aElementsInput_ as &$cElementBase){
              $cElement->Content($cElementBase);
            }
          }
          $this->cElementRoot_->Content($cElement);
        }
        else{
          if(isset($this->aElementsInput_)){
            foreach($this->aElementsInput_ as &$cElementBase){
              $this->cElementRoot_->Content($cElementBase);
            }
          }
        }
      }
      return $this->cElementRoot_;
    }
    
    public static function &cElementLabel($sLabel, $sName = null){
      $cElement = new cElement('label', false);
      $cElement->Attribute('class', WTS_PFX . '-label');
      if(isset($sName)){$cElement->Attribute('for', $sName);}
      $cElement->Content(new cElementBase($sLabel));
      return $cElement;
    }
    
    public static function &cElementInput($sName = null, $sValue = null, $sType = 'text'){
      $cElement = new cElement('input', false);
      $cElement->Attribute('class', WTS_PFX . '-input');
      $cElement->Attribute('type', $sType);
      if(isset($sName)){$cElement->Attribute('name', $sName);}
      if(isset($sValue)){$cElement->Attribute('value', htmlentities($sValue));}
      return $cElement;
    }
    
    public static function &cElementCalImg($sID = false){
      $cElement = new cElement('img');
      $cElement->Attribute('class', WTS_PFX . '-calendar-image');
      $cElement->Attribute('src', WTS_PATH . 'datetimepick/cal.gif');
      if($sID !== false){
        $cElement->Attribute('title', _('Pick a Date'));
        $cElementA = new cElement('a');
        $cElementA->Attribute('href'
                            , 'javascript:NewCssCal(\''
                            . $sID
                            . '\', \'ddmmyyyy\', \'arrow\', true, 24, true)');
        $cElementA->Content($cElement);
        return $cElementA;
      }
      return $cElement;
    }
    
    public static function &cElementSubmit($sLabel, $bReset = false){
      $cElement = new cElement('input', false);
      $cElement->Attribute('class', WTS_PFX . '-button');
      $cElement->Attribute('type', ($bReset ? 'reset' : 'submit'));
      $cElement->Attribute('value', htmlentities($sLabel));
      return $cElement;
    }
    
    public static function &cElementTextArea($sName, $sValue = null){
      $cElement = new cElement('textarea', false);
      $cElement->Attribute('rows', '10');
      $cElement->Attribute('cols', '20');
      $cElement->Attribute('style', 'width: 100%; resize: vertical; box-sizing: border-box;-moz-box-sizing: border-box;-webkit-box-sizing: border-box;');
      $cElement->Attribute('name', $sName);
      $cElement->Attribute('id', $sName);
      if(isset($sValue)){
        $cElement->Content(new cElementBase($sValue, true));
      }
      return $cElement;
    }

    public static function sNicEditCreateScript($sName, $iMaxHeight, $bUploadImg = false){
      $sScript = '
  new nicEditor({
    buttonList:[\'fontFamily\'
              , \'fontSize\'
              , \'bold\'
              , \'italic\'
              , \'underline\'
              , \'strikethrough\'
              , \'forecolor\'
              , \'bgcolor\'
              , \'ol\'
              , \'ul\'
              , \'hr\'
              , \'link\'
              , \'unlink\'
              , \'';
      if((bool)$bUploadImg){
        $sScript .= 'upload\', \'removeformat\', \'xhtml\'], uploadURI:\'' . WTS_PATH . 'imgupload.php\',';
      }
      else{$sScript .= 'removeformat\', \'xhtml\'],';}
      $sScript .= ' maxHeight:' . $iMaxHeight . '
  }).panelInstance(\'' . $sName . '\');';
      return $sScript;
    }
    public static function sNicEditInitScript($sNicEditCreateScript){
      $sScript  = '
bkLib.onDomLoaded(function(){';
      $sScript .= $sNicEditCreateScript;
      $sScript .= '
});

window.onload=function(){

  ' . ((isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0) ? 'window.scroll(0,' . $_GET[WTS_PARAM_SCROLL] . ');' : '') . '

  var nicdiv = document.getElementsByClassName(\'nicEdit-main\')[0];
  nicdiv.style.width=\'100%\';
  nicdiv.style.boxSizing=\'border-box\';
  nicdiv.onkeypress =  function(){window.content_changed = true;};
  nicdiv.onmousedown = function(){window.content_changed = true;};
  
  var nicpanel = document.getElementsByClassName(\'nicEdit-panelContain\')[0];
  nicpanel.style.boxSizing=\'border-box\';
  nicpanel.parentElement.style.width=\'100%\';
  nicpanel.parentElement.style.boxSizing=\'border-box\';
  nicpanel.parentElement.nextSibling.style.width=\'100%\';
  nicpanel.parentElement.nextSibling.style.boxSizing=\'border-box\';
  
  /*session timeout*/
  var nicsubmit = document.getElementById(\'id_' . WTS_PARAM_VALUE . '\');
  if(nicsubmit){
    setTimeout(function(){nicsubmit.disabled = true;}, ' . (WTS_SESSION_TIMEOUT * 1000 - 5000) . ');
  }
}

window.onbeforeunload=function(){
  if(window.content_changed){
    return \'You have unsaved changes on this page.\';
  }
}';
      return $sScript;
    }
    
    public static function sAddAttachmentInitScript(){
      $sScript  = '
function calc_files_size(nBytes){
  var sOutput = nBytes + \' b\';
  // optional code for multiples approximation
  for (var aMultiples = [\'Kb\', \'Mb\', \'Gb\', \'Tb\', \'Pb\', \'Eb\', \'Zb\', \'Yb\'], nMultiple = 0, nApprox = nBytes / 1024; nApprox > 1; nApprox /= 1024, nMultiple++) {
    sOutput = nApprox.toFixed(2) + \' \' + aMultiples[nMultiple];
  }
  return sOutput;
}

function update_files_size(){
  var sz = 0;
  var c = document.getElementById(\'' . WTS_PFX . '-attachment-info\').children;
  for(var i = 0; i < c.length; i++){
    if(\'files\' in c[i]){
      if(c[i].files.length > 0){
        var file = c[i].files[0];
        if(\'size\' in file){sz += file.size;}
      }
    }
  }
  if(sz > 0){
    document.getElementById (\'' . WTS_PFX . '-attachment-size\').innerHTML = calc_files_size(sz);
    if(sz > ' . (ceil(WTS_MAX_PKG_SIZE/4)*3) . '){
      document.getElementById (\'' . WTS_PFX . '-attachment-size\').className = \'' . WTS_PFX . '-alert\';
    }
    else{
      document.getElementById (\'' . WTS_PFX . '-attachment-size\').className = \'' . WTS_PFX . '-notify\';
    }
  }
}

function add_file(){
  div=document.getElementById(\'' . WTS_PFX . '-attachment-info\');
  input = document.createElement(\'input\');
  input.type=\'file\';
  input.name=\'' . WTS_PARAM_FILE . '[]\';
  input.setAttribute(\'onchange\', \'update_files_size()\');
  div.appendChild(input);
  /*div.appendChild(document.createElement(\'br\'));*/
}

function clear_file(){
  div = document.getElementById(\'' . WTS_PFX . '-attachment-info\');
  while(div.firstChild){
    div.removeChild(div.firstChild);
  }
  document.getElementById (\'' . WTS_PFX . '-attachment-size\').innerHTML = \'\';
}';
      return $sScript;
    }
    
    //--mesagges before main menu
    public static function &cElementMessage($sLabel, $bWarn = false){
      $cElement    = new cElement('div', false);
      $cElementImg = new cElement('img');
      
      if($bWarn){
        $cElementImg->Attribute('src', IMG_WARN);
        $cElement->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info' );
      }
      else{
        $cElementImg->Attribute('src', IMG_ACPT);
        $cElement->Attribute('class', WTS_PFX . '-notify ' . WTS_PFX . '-info' );
      }
      
      $cElement->Content($cElementImg);
      $cElement->Content(new cElementBase(htmlentities($sLabel), true));
  
      return $cElement;
    }
    
  }
  
  /**
   * Toc class - Table of content
   */
  class cToc{
    protected $cElement_;
    protected $cElementLastChild_;
    
    public function __construct(){
      $this->cElement_ = new cElement('ul', false);
      $this->cElement_->Attribute('class', WTS_PFX . '-toc');
      $this->cElementLastChild_ = false;
    }
    
    public function Child(cElementBase &$cElementTitle, cElementBase &$cElementLink){
      $cElement = new cElement('li', false);
      $this->cElementLastChild_ = new cElement('div', false);
      $this->cElementLastChild_->Attribute('style', 'text-align: right;');
      
      
      $cElementDiv = new cElement('div', false);
      $cElementDiv->Attribute('style', 'float: left;  margin-right: 5px;');
      $cElementDiv->Content($cElementTitle);
      
      $this->cElementLastChild_->Content($cElementDiv);
      $this->cElementLastChild_->Content($cElementLink);
      
      $cElement->Content($this->cElementLastChild_);
      $this->cElement_->Content($cElement);
    }
    
    public function AddLinkForLastChild(cElementBase &$cElement){
      if($this->cElementLastChild_  !== false){
        $this->cElementLastChild_->Content($cElement);
      }
    }
    
    public function &cRoot(){
      return $this->cElement_;
    }
  }

  /**
   * Menu classes
   */
  class cMenuBase{
    protected $cElement_;
    protected $cChildElement_;
    
    public function Child(cMenuItem &$cMenuItem){
      if(!isset($this->cChildElement_)){
        $this->cChildElement_ = new cElement('ul', false);
        $this->cElement_->Content($this->cChildElement_);
      }
      $this->cChildElement_->Content($cMenuItem->cElement_);
    }
  }
  
  
  class cMenuItem extends cMenuBase{

    public function __construct($sName, $sTitle, $sHref = null){
      $cElement = false;
      if(isset($sHref)){
        $cElement = new cElement('a');
        $cElement->Attribute('href', $sHref);
      }
      else{$cElement = new cElement('span');}
      $cElement->Attribute('title', $sTitle);
      $cElement->Content(new cElementBase($sName));
      $this->cElement_ = new cElement('li', false);
      $this->cElement_->Content($cElement);
    }
  }
  
  class cMenu extends cMenuBase{

    public function __construct(){
      $this->cChildElement_ = new cElement('ul', false);
      $this->cChildElement_->Attribute('id', WTS_PFX . '-main-nav');
      $this->cElement_ = new cElement('nav', false);
      $this->cElement_->Content($this->cChildElement_);
    }
    
    public function &cRoot(){
      return $this->cElement_;
    }
  }
  
  /**
   * cAttachmentView
   */
  class cAttachmentView{
    protected $cElement_;
    
    public function __construct(){
      $this->cElement_ = new cElement('ul', false);
      $this->cElement_->Attribute('class', WTS_PFX . '-thumb');
    }
    
    public function Attachment($sLink, $sName){
      $sName = htmlentities($sName);
      //a
      $cElement = new cElement('a', false);
      $cElement->Attribute('href', $sLink);
      $cElement->Attribute('title', _('Download file \'') . $sName . '\'');
      //img in a
      $cElementImg = new cElement('img', false);
      $cElementImg->Attribute('src', $sLink . '&'. WTS_PARAM_THUMB .'=true');
      $cElement->Content($cElementImg);
      //figcaption in a
      $cElementFig = new cElement('figcaption', false);
      $cElementFig->Content(new cElementBase($sName));
      $cElement->Content($cElementFig);
      
      $cElementLi = new cElement('li', false);
      $cElementLi->Content($cElement);
      $this->cElement_->Content($cElementLi);
    }
    
    public function &cRoot(){
      return $this->cElement_;
    }
  }
  
  /**
   * cSelect
   */
  class cSelect{
    protected $cElement_;
    
    public function __construct($sName, $sClassPrefix){
      $this->cElement_ = new cElement('select', false);
      $this->cElement_->Attribute('class', $sClassPrefix . '-select');
      $this->cElement_->Attribute('size', '1');
      $this->cElement_->Attribute('name', $sName);
    }
    
    public function Option($bSelected, $sValue = null, $sName = null, $sColor = null, $bDisabled = false){
      $cElement = new cElement('option', false);
      if(isset($sValue)){
        $cElement->Attribute('value', $sValue);
        if((bool)$bSelected){$cElement->Attribute('selected');}
      }
      
      if($bDisabled && !$bSelected){
        $cElement->Attribute('disabled');
      }
      
      if(isset($sColor)){
        $cElement->Attribute('style', 'background-color:#' . $sColor . ';');
      }

      if(isset($sName)){
        $cElement->Content(new cElementBase(htmlentities($sName)));
      }
      
      
      $this->cElement_->Content($cElement);
    }
    
    public function &cRoot(){
      return $this->cElement_;
    }
  }
  
  /**
   * cButton - class for toolbar content
   */
  class cButton{
    protected $cElement_;
    
    public function __construct($sAction = WTS_HREF, $sMethod = 'get'){
      $this->cElement_ = new cElement('form', false);
      $this->cElement_->Attribute('action', $sAction);
      $this->cElement_->Attribute('method', $sMethod);
    }

    public function Image($sSrc, $sTitle, $sClass){
      $cElement = new cElement('input', false);
      $cElement->Attribute('type', 'image');
      $cElement->Attribute('class', $sClass);
      $cElement->Attribute('src', $sSrc);
      $cElement->Attribute('title', $sTitle);

      $this->cElement_->Content($cElement);
    }
    
    public function Hidden($sName, $sValue){
      $cElement = new cElement('input', false);
      $cElement->Attribute('type', 'hidden');
      $cElement->Attribute('name', $sName);
      $cElement->Attribute('value', htmlentities($sValue));
      
      $this->cElement_->Content($cElement);
    }
    
    public function Content(cElementBase &$cElementBase){
      $this->cElement_->Content($cElementBase);
    }
    
    public function &cRoot(){
      return $this->cElement_;
    }
  }
  
  class cTable{
    protected $aElements_;
    protected $iColumns_;
    
    public function __construct($sClass){
      $this->aElements_[0] = new cElement('table', false);
      $this->aElements_[0]->Attribute('class', $sClass);
      $this->iColumns_ = 0;
    }
    
    public function Column($sName, $sWidth = null){
      if(!isset($this->aElements_[2])){
        $this->aElements_[1] = new cElement('thead', false);
        $this->aElements_[0]->Content($this->aElements_[1]);
        
        $this->aElements_[2] = new cElement('tr', false);
        $this->aElements_[1]->Content($this->aElements_[2]);
      }
      
      $cElement = new cElement('td', false);
      if(isset($sWidth))$cElement->Attribute('width', $sWidth);
      $cElement->Content(new cElementBase($sName));
      
      $this->aElements_[2]->Content($cElement);
      $this->iColumns_++;
    }
    
    public function Row(cElementBase &$cElementBase){
      if(!isset($this->aElements_[3])){
        $this->aElements_[3] = new cElement('tbody', false);
        $this->aElements_[0]->Content($this->aElements_[3]);
      }
      $this->aElements_[3]->Content($cElementBase);
    }
    
    public function ColSpan($sName){
      if($this->iColumns_ > 0){
        $cElementTd = new cElement('td', false);
        $cElementTd->Attribute('colspan', $this->iColumns_);
        $cElementTd->Content(new cElementBase($sName));
        
        $cElementTr = new cElement('tr', false);
        $cElementTr->Content($cElementTd);
        $this->Row($cElementTr);
      }
    }
    
    public function &cRoot(){
      return $this->aElements_[0];
    }
  }


}

?>
