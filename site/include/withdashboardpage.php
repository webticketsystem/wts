<?php
/*
 * withdashboardpage.php (part of WTS) - trait UI tWithDashboardPage
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  trait tWithDashboardPage{
    
    protected function &cAddDashboardMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Dashboard')
                         , _('Event management')
                         , ($sCurAct !== WTS_PARAM_ACT_DASHBOARD ? sAction(WTS_PARAM_ACT_DASHBOARD) : null));
      return $cMenuItem;
    }
    
    protected function &cDoSvgChainChart($iDays){
      
      $cElement = new cElement('svg', false);
      $iWidth = 240;
      $iHeight = 150;

      $cElement->Attribute('class', WTS_PFX . '-svg');
      $cElement->Attribute('viewBox', '0 0 ' . $iWidth . ' ' . $iHeight);
      
      if($this->cCont->iType() === cCont::AGENT){
        $iQueue = $this->cCont->iQueue();
      }
      else{$iQueue = 0;}
    
      $a = false;
      $iMaxChains = cChain::iGetStatistics($this->cDb, $iQueue, $iDays, $a);
      $iDays = count($a);

      $iHalf = 150; //max 300 chains
      if($iMaxChains <= 150){$iHalf = 75;}
      if($iMaxChains <= 100){$iHalf = 50;}
      if($iMaxChains <= 50){$iHalf = 25;}
      if($iMaxChains <= 30){$iHalf = 15;}
      if($iMaxChains <= 20){$iHalf = 10;}
      if($iMaxChains <= 10){$iHalf = 5;}
      
      $iFontSize = 12;//in px
      $iGuide = 4;//in pix
      $aRect = array('l' => $iGuide + 3 + (int)(($iFontSize / 2) * strlen((string)($iHalf * 2)))
                   , 'r' => $iWidth - (int)($iFontSize / 2) - 3
                   , 't' => (int)($iFontSize / 2) + $iFontSize //for legend
                   , 'b' => $iHeight - $iFontSize - $iGuide);
                   
      $fXStep = ($aRect['r'] - $aRect['l']) / ($iDays - 1);
      
      $fYStep = ($aRect['b'] - $aRect['t']) / ($iHalf * 2);
      
      $sLineStyle = 'stroke: #999;';
      $sFontStyle = 'font-size: ' . $iFontSize . 'px; cursor: default;';
      
      //подложка
      $cElementRect = new cElement('rect', false);
      $cElementRect->Attribute('style', 'fill:rgba(255, 255, 255, .5); stroke:none;');
      $cElementRect->Attribute('x', $aRect['l']);
      $cElementRect->Attribute('y', $aRect['t']);
      $cElementRect->Attribute('width', $aRect['r'] - $aRect['l']);
      $cElementRect->Attribute('height', $aRect['b'] - $aRect['t']);
      $cElement->Content($cElementRect);
      
      //шкала дней
      for($i = 0; $i < $iDays; $i++){
        $cElementG = new cElement('g', false);
        $cElementG->Attribute('transform', 'translate(' . ($i * $fXStep + $aRect['l']) . ')');
        
        $cElementLine = new cElement('line', false);
        $cElementLine->Attribute('style', $sLineStyle);
        $cElementLine->Attribute('y1', $aRect['t']);
        $cElementLine->Attribute('y2', $aRect['b'] + $iGuide);
        
        $cElementG->Content($cElementLine);
        
        $cElementText = new cElement('text', false);
        $cElementText->Attribute('style', $sFontStyle);
        $cElementText->Attribute('y', $iHeight);
        $cElementText->Attribute('x', -(int)($iFontSize / 2));
        
        $cElementTitle = new cElement('title');
        $cElementTitle->Content(new cElementBase(date(WTS_D_FMT, $a[$i]['tmstamp'])));

        $cElementText->Content($cElementTitle);
        $cElementText->Content(new cElementBase($a[$i]['day']));
        $cElementG->Content($cElementText);
        
        $cElement->Content($cElementG);
      }
      
      //шкала заявок
      for($i = 0; $i < 3; $i++){
        $cElementG = new cElement('g', false);
        $cElementG->Attribute('transform', 'translate(0, ' . ($i * (($aRect['b'] - $aRect['t']) / 2.0) + $aRect['t']) . ')');
        
        $cElementLine = new cElement('line', false);
        $cElementLine->Attribute('style', $sLineStyle);
        $cElementLine->Attribute('x1', $aRect['l'] - $iGuide);
        $cElementLine->Attribute('x2', $aRect['r']);
        
        $cElementG->Content($cElementLine);
        
        $cElement->Content($cElementG);
        
        if($i < 2){
          $cElementText = new cElement('text', false);
          $cElementText->Attribute('style', $sFontStyle);
          $cElementText->Attribute('y', (int)($iFontSize / 2));
          
          if($i === 1 && strlen((string)($iHalf * 2)) > strlen((string)($iHalf))){
            $cElementText->Attribute('x', (int)($iFontSize / 2));
          }
          
          $cElementTitle = new cElement('title');
          $cElementTitle->Content(new cElementBase(_('Count of Chains')));
          
          $cElementText->Content($cElementTitle);
          
          if($i === 0){$cElementText->Content(new cElementBase($iHalf * 2));}
          else{$cElementText->Content(new cElementBase($iHalf));}
          
          $cElementG->Content($cElementText);
        }
      }
      
      //polylines
      $sClosed = '';
      $sCreated = '';
      $b = false;
      for($i = 0; $i < $iDays; $i++){
        if($b === false){$b = true;}
        else{
          $sCreated .= ' ';
          $sClosed  .= ' ';
        }
        $sCreated .= ($aRect['l'] + $i * $fXStep) . ',' . ($aRect['b'] - $fYStep * $a[$i]['created']);
        $sClosed  .= ($aRect['l'] + $i * $fXStep) . ',' . ($aRect['b'] - $fYStep * $a[$i]['closed']);

      }
      
      $sClosedStyleS  = 'stroke:rgb(0, 139, 91);';
      $sClosedStyleF  = 'fill:rgba(0, 139, 91, .3);';
      $sCreatedStyleS = 'stroke:rgb(139, 0, 0);';
      $sCreatedStyleF = 'fill:rgba(139, 0, 0, .3);';
      
      $cElementPLine = new cElement('polyline', false);
      $cElementPLine->Attribute('points', $sClosed);
      $cElementPLine->Attribute('style', $sClosedStyleS . 'fill:none;');
      $cElement->Content($cElementPLine);
      
      $cElementPLine = new cElement('polyline', false);
      $cElementPLine->Attribute('points', $sCreated);
      $cElementPLine->Attribute('style', $sCreatedStyleS . 'fill:none;');
      $cElement->Content($cElementPLine);
      
      if($a[0]['created'] > 0){$sCreated = $aRect['l'] . ',' . $aRect['b'] . ' ' . $sCreated;}
      if($a[0]['closed'] > 0){$sClosed  = $aRect['l'] . ',' . $aRect['b'] . ' ' . $sClosed;}
      
      if($a[$iDays - 1]['created'] > 0){$sCreated .= ' ' . $aRect['r'] . ',' . $aRect['b'];}
      if($a[$iDays - 1]['closed']  > 0){$sClosed  .= ' ' . $aRect['r'] . ',' . $aRect['b'];}

      
      $cElementPLine = new cElement('polyline', false);
      $cElementPLine->Attribute('points', $sClosed);
      $cElementPLine->Attribute('style', $sClosedStyleF . 'stroke:none;');
      $cElement->Content($cElementPLine);
      
      $cElementPLine = new cElement('polyline', false);
      $cElementPLine->Attribute('points', $sCreated);
      $cElementPLine->Attribute('style', $sCreatedStyleF . 'stroke:none;');
      $cElement->Content($cElementPLine);
      
      //точки графика с подсказками
      for($i = 0; $i < $iDays; $i++){
        
        if($a[$i]['created'] > 0 || $a[$i]['closed'] > 0){
          if($a[$i]['created'] === $a[$i]['closed']){
            
            $cElementCir = new cElement('circle', false);
            $cElementCir->Attribute('style', 'stroke:none;fill:rgb(139, 0, 0)');
            $cElementCir->Attribute('cx', ($aRect['l'] + $i * $fXStep));
            $cElementCir->Attribute('cy', ($aRect['b'] - $fYStep * $a[$i]['created']));
            $cElementCir->Attribute('r', 3);
            
            $cElementTitle = new cElement('title');
            $cElementTitle->Content(new cElementBase($a[$i]['created']));
            $cElementCir->Content($cElementTitle);
            
            $cElement->Content($cElementCir);

          }
          else{
            if($a[$i]['created'] > 0){
              $cElementCir = new cElement('circle', false);
              $cElementCir->Attribute('style', 'stroke:none;fill:rgb(139, 0, 0)');
              $cElementCir->Attribute('cx', ($aRect['l'] + $i * $fXStep));
              $cElementCir->Attribute('cy', ($aRect['b'] - $fYStep * $a[$i]['created']));
              $cElementCir->Attribute('r', 3);
              
              $cElementTitle = new cElement('title');
              $cElementTitle->Content(new cElementBase($a[$i]['created']));
              $cElementCir->Content($cElementTitle);
              
              $cElement->Content($cElementCir);
            }
            
            if($a[$i]['closed'] > 0){
              $cElementCir = new cElement('circle', false);
              $cElementCir->Attribute('style', 'stroke:none;fill:rgb(0, 139, 91)');
              $cElementCir->Attribute('cx', ($aRect['l'] + $i * $fXStep));
              $cElementCir->Attribute('cy', ($aRect['b'] - $fYStep * $a[$i]['closed']));
              $cElementCir->Attribute('r', 3);
              
              $cElementTitle = new cElement('title');
              $cElementTitle->Content(new cElementBase($a[$i]['closed']));
              $cElementCir->Content($cElementTitle);
              
              $cElement->Content($cElementCir);
            }
          }
        }
      }
      
      //легенда
      //created
      $cElementCir = new cElement('circle', false);
      $cElementCir->Attribute('style', $sCreatedStyleS . $sCreatedStyleF);
      $cElementCir->Attribute('cx', $aRect['l'] + 6);
      $cElementCir->Attribute('cy', 6);
      $cElementCir->Attribute('r', 5);
      
      $cElement->Content($cElementCir);
      
      $cElementText = new cElement('text', false);
      $cElementText->Attribute('style', $sFontStyle);
      $cElementText->Attribute('x', $aRect['l'] + 14);
      $cElementText->Attribute('y', $iFontSize - 1);
      $cElementText->Content(new cElementBase(_('Created')));
      
      $cElement->Content($cElementText);
      
      //closed
      $cElementCir = new cElement('circle', false);
      $cElementCir->Attribute('style', $sClosedStyleS . $sClosedStyleF);
      $cElementCir->Attribute('cx', $iWidth/2  + 6);
      $cElementCir->Attribute('cy', 6);
      $cElementCir->Attribute('r', 5);
      
      $cElement->Content($cElementCir);
      
      $cElementText = new cElement('text', false);
      $cElementText->Attribute('style', $sFontStyle);
      $cElementText->Attribute('x', ($iWidth/2) + 14);
      $cElementText->Attribute('y', $iFontSize - 1);
      $cElementText->Content(new cElementBase(_('Closed')));
      
      $cElement->Content($cElementText);
      
      return $cElement;
    }
    
    protected function DoDashboardPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref     = sAction(WTS_PARAM_ACT_DASHBOARD);
      $aEvents   = false;
      $cCurEvent = false;
      $iPage     = ((isset($_GET[WTS_PARAM_PAGE]) && $_GET[WTS_PARAM_PAGE] > 0) ? $_GET[WTS_PARAM_PAGE] : 1);
      
      cEvent::AddFilterChain(0);//not attached to chain
      cEvent::bAddFilterProcessed(false);

      if($this->cCont->iType() === cCont::AGENT){
        cEvent::bAddFilterQueue($this->cCont->iQueue());
      }
      
      $iRows  = cEvent::iCount($this->cDb);
      $iPages = 1;

      if($iRows > 0){
        //normalize page
        if($iRows > WTS_WIDE_ROW_PER_PAGE){
          $iPages = ceil($iRows / WTS_WIDE_ROW_PER_PAGE);
        }
        if($iPage > $iPages){$iPage = $iPages;}

        $iRows = cEvent::iUnserialize($this->cDb, $iPage, WTS_WIDE_ROW_PER_PAGE, $aEvents);
      }
      
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurEvent = &cEvent::mInArray($aEvents, $_GET[WTS_PARAM_ID]);
      }

      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        
        //проверим блокировку
        if($cCurEvent
        && $cCurEvent->bUnserializeLock($this->cDb)
        && $cCurEvent->iLockOwner() !== $this->cCont->iID()){
          
          $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
          
          if($cCont = cCont::mUnserialize($this->cDb, $cCurEvent->iLockOwner())){
            $cMsg->Value(_('Event from \'') . $cCurEvent->sFrom()
                     . _('\' locked by ') . ($cCont->sName() ? $cCont->sName() : '\'' . $cCont->sLogin() . '\''));
            unset($cCont);
          }
          else{
            $cMsg->Value(_('Event from \'') . $cCurEvent->sFrom() . _('\' locked by ') . _('Unknown'));
          }

          $cMsg->bSerialize($this->cDb);
          unset($cMsg);

          header('Location: ' . $sHref
            . ($iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
            . sSubAction(WTS_PARAM_ID, $cCurEvent->iID()));
          exit;
        }
        
        switch($sAction){
        case WTS_PARAM_EACT_MERGE:
          if($cCurEvent
          && !$cCurEvent->iChain()
          && isset($_GET[WTS_PARAM_ACT_CHAIN])
          && ($cChain = cChain::mUnserialize($this->cDb, $_GET[WTS_PARAM_ACT_CHAIN]))){
            
            $cCurEvent->Chain($cChain->iID());
            $cCurEvent->bSerialize($this->cDb);
            unset($cCurEvent);
            $cCurEvent = false;
            
            if($cChain->bClosed()){
              $cChain->Open();
              $cChain->bSerialize($this->cDb);
            }
            unset($cChain);
          }
          break;
        case WTS_PARAM_EACT_DELETE:
          if($cCurEvent
          && !$cCurEvent->iChain()
          && !$cCurEvent->bProcessed()
          && $cCurEvent->bDelete($this->cDb)){
            unset($cCurEvent);
            $cCurEvent = false;
          }
          break; 
        default: break;
        }
        if($iPage > 1){$sHref .= sSubAction(WTS_PARAM_PAGE, $iPage);}
        if($cCurEvent !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurEvent->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_DASHBOARD);
      $cElement->Content($cButton->cRoot());
      
      if($cCurEvent){
        //delete
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete Event'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_DASHBOARD);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        $cButton->Hidden(WTS_PARAM_ID, $cCurEvent->iID());
        if($iPage > 1){$cButton->Hidden(WTS_PARAM_PAGE, $iPage);}
        
        $cElement->Content($cButton->cRoot());
        
        if($this->cCont->iType() === cCont::AGENT && $cCurEvent->iType() === cEvent::IN_EMAIL){
          //forward
          $cButton = new cButton();
          $cButton->Image(IMG_EDIT_FORWARD, _('Forward Event'), WTS_PFX . '-toolbar-button');
          $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_EVENT);
          $cButton->Hidden(WTS_PARAM_ID, $cCurEvent->iID());
          $cButton->Hidden(WTS_PARAM_FORWARD, 'true');
          $cElement->Content($cButton->cRoot());
        }
        
        //to chain
        $cButton = new cButton();
        
        $cElementI = new cElement('input', false);
        $cElementI->Attribute('type', 'search');
        $cElementI->Attribute('class', WTS_PFX . '-toolbar-search');
        $cElementI->Attribute('style', 'min-width: 68px; width: 68px;');
        $cElementI->Attribute('placeholder', _('Chain ID'));
        $cElementI->Attribute('name', WTS_PARAM_ACT_CHAIN);
        $cElementI->Attribute('title', _('Enter Chain ID to attach Event'));
        
        if($iChainID = $cCurEvent->iExpectedChain(WTS_SBJ_PREFIX)){
          $cElementI->Attribute('value', $iChainID);
        }

        $cButton->Content($cElementI);
        $cButton->Image(IMG_EDIT_MERGE, _('Attach To Chain'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_DASHBOARD);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_MERGE);
        $cButton->Hidden(WTS_PARAM_ID, $cCurEvent->iID());
        if($iPage > 1){$cButton->Hidden(WTS_PARAM_PAGE, $iPage);}
        $cElement->Content($cButton->cRoot());
        
        //new chain
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_NEW_CHAIN, _('New Chain'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_EVENT);
        $cButton->Hidden(WTS_PARAM_ID, $cCurEvent->iID());
        $cElement->Content($cButton->cRoot());
        
      }
      
      if($this->cCont->iType() === cCont::AGENT){
        //add new
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_NEW_EVENT, _('New Event'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_EVENT);
        $cElement->Content($cButton->cRoot());
      }
      
      //page navbar
      if($iPages > 1){
        $aButtons = array();
        $this->CreateButtonsNav(WTS_PARAM_ACT_DASHBOARD, $iPage, $iPages, $aButtons);
        foreach($aButtons as &$cButton){
          $cElement->Content($cButton->cRoot());
        }
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Date'), '165px');
      $cTable->Column(_('Subject & Preview'));
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }

        $iID = 0;
        if($cCurEvent){$iID = $cCurEvent->iID();}
        
        foreach($aEvents as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . ($iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);
          
          $cElementTd->Content($this->cElementEventImg($c->iType()));
          
          //fwd in list
          if(!$c->bProcessed() && $c->iType() === cEvent::OUT_EMAIL){
            $cElementImg = new cElement('img', false);
            $cElementImg->Attribute('class', WTS_PFX . '-table-narrow-image');
            $cElementImg->Attribute('src', IMG_EVENT_NEW);
            $cElementImg->Attribute('title', _('New Event'));
            
            $cElementTd->Content($cElementImg);
          }
          else{
            //locks
            if($c->bUnserializeLock($this->cDb)){
              $cElementImg = new cElement('img', false);
              $cElementImg->Attribute('class', WTS_PFX . '-table-narrow-image');
              $cElementImg->Attribute('src', IMG_EVENT_LOCK);
              if($c->iLockOwner() === $this->cCont->iID()){
                $cElementImg->Attribute('title', _('Locked by ') . ($this->cCont->sName() ? $this->cCont->sName() : '\'' . $this->cCont->sLogin() . '\''));
              }
              elseif($cCont = cCont::mUnserialize($this->cDb, $c->iLockOwner())){
                $cElementImg->Attribute('title', _('Locked by ') . ($cCont->sName() ? $cCont->sName() : '\'' . $cCont->sLogin() . '\''));
                unset($cCont);
              }else{$cElementImg->Attribute('title', _('Locked by ')  . _('Unknown'));}
              
              $cElementTd->Content($cElementImg);
            }
          }
          
          $cElementTd->Content($this->cDoTime($c->iDtEnd()));
          $cElementTd->Content($this->cDoDate($c->iDtEnd()));

          $cElementTr->Content($cElementTd);
          
          //column 2
          $cElementTd = new cElement('td', false);
          
          if($c->sSubject()){
            $cElementTd->Content(cSE::cCreate('b', $c->sSubject()));
            $cElementTd->Content(new cElement('br', false));
          }
          
          if($c->sPreview()){
            //$cElementTd->Content(cSE::cCreate('small', $c->sPreview()));
            $cElementTd->Content(new cElementBase(htmlentities($c->sPreview())));
          }
          $cElementTr->Content($cElementTd);
         
          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //050. Event Body
      if($cCurEvent){
        $cPage->Main($this->cDoEvent($cCurEvent));
      }
      
      //060. Side bar
      
      //--chart of chains
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Content(new cElementBase(_('Chart of Chains')));
      
      $cElement->Content($cElementCapt);
      
      $cField = new cField(cField::CENTER);
      $cField->Label($this->cDoSvgChainChart(10));
      
      $cElement->Content($cField->cBuild());
        
      $cPage->Aside($cElement);
      
      //--event info
      if($cCurEvent){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Event Info')));
        
        $cElement->Content($cElementCapt);
        
        $cToc = new cToc();

        if($cCurEvent->iType() === cEvent::IN_EMAIL){ //info from
          
          $aConts = $cElementCont = $cElementGroup = false;
          cCont::bAddFilterPartValue($this->cDb, $cCurEvent->sFrom());
          if(cCont::iCount($this->cDb) > 0
          && cCont::iUnserialize($this->cDb, 1, 1, $aConts) > 0){
            $cCont = $aConts[0];
            
            $cElementCont = $this->cDoContInfo($cCont);
            if($cCont->bDisabled()){
              $cElementCont->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
            }
            else{
              $cElementCont->Attribute('class', WTS_PFX . '-notify ' . WTS_PFX . '-info2');
            }
            
            if($cCont->iGroup() > 0){
              if($cGroup = cGroup::mUnserialize($this->cDb, $cCont->iGroup())){
                $cElementGroup = new cElement('a', false);
                $cElementGroup->Attribute('href', sAction(WTS_PARAM_ACT_GROUP)
                . sSubAction(WTS_PARAM_SEARCH, urlencode($cGroup->sName()))
                . sSubAction(WTS_PARAM_ID, $cGroup->iID()));
                $cElementGroup->Content(new cElementBase(htmlentities($cGroup->sName())));
                
                unset($cGroup);
              }
            }
            unset($cCont);
          }
          else{
            $cElementCont = new cElementBase(_('Unknown'));
          }
          $cToc->Child(new cElementBase(_('Contact')), $cElementCont);
          
          if($cElementGroup){
            $cToc->Child(new cElementBase(_('Group')), $cElementGroup);
          }
            
        }
        else{
          $cContOwn = $bUnsetOwn = false;
          
          if($cCurEvent->iOwner() > 1){
            if($cCurEvent->iOwner() !== $this->cCont->iID()){
              $cContOwn = cCont::mUnserialize($this->cDb, $cCurEvent->iOwner());
              $bUnsetOwn = true;
            }
            else{$cContOwn = $this->cCont;}
          }
          
          $cToc->Child(new cElementBase(_('Owner'))
                     , ($cContOwn === false ? new cElementBase(_('System')) : $this->cDoContInfo($cContOwn)));
                     
          if($bUnsetOwn){unset($cContOwn);}
        }

        $cElement->Content($cToc->cRoot());
        
        $cPage->Aside($cElement);
      }
    }
  }
  
  
}
