<?php
/*
 * withproductpage.php (part of WTS) - trait UI tWithProductPage
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('core/product.php');

  trait tWithProductPage{
    
    protected function &cAddProdMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Products')
                         , _('Products management')
                         , ($sCurAct !== WTS_PARAM_ACT_PRODUCT ? sAction(WTS_PARAM_ACT_PRODUCT) : null));
      return $cMenuItem;
    }
    
    protected function &cDoSvgProdChart($iType, $iID){
      $cElement = new cElement('svg', false);
      $iWidth = 240;
      $iHeight = 160;
      $iCX = $iWidth/2;
      $iCY = $iHeight/2;
      $iR = $iHeight/2 - 5;

      $cElement->Attribute('class', WTS_PFX . '-svg');
      $cElement->Attribute('viewBox', '0 0 ' . $iWidth . ' ' . $iHeight);
      
      $a = false;
      $iProdCount = cProduct::iGetStatistics($this->cDb, $iType, $iID, $a);
      
      if($iProdCount > 0 && $a){
        //названия категорий, цвет и угол в радианах
        $keyMaxAngle = $aPie = false;//[count, category]
        $iMaxAngle = $iFullAngle = 0; //360
        $i = 0;
        foreach($a as $b){//)))
          
          if($cCategory = cCategory::mUnserialize($this->cDb, $b['category'])){
            $iAngle = ceil(360 * $b['count'] / $iProdCount);
            if($iAngle > 0){
            
              if($iAngle > $iMaxAngle){
                $iMaxAngle = $iAngle;
                $keyMaxAngle = $i;
              }
              $iFullAngle += $iAngle;
              
              $aPie[$i] = array(
                'color' => ($cCategory->sColor() ? $cCategory->sColor() : (cString::sRandColorPart() . cString::sRandColorPart() . cString::sRandColorPart()))
              , 'name' => htmlentities($cCategory->sName())
              , 'count' => $b['count']
              , 'angle' => $iAngle);
              $i++;
            }
            unset($cCategory);
            
          }
        }
        if($aPie){
          //уменьшить/увеличить самый большой сектор
          if($iFullAngle > 360 && $iMaxAngle > ($iFullAngle - 360)){
            $aPie[$keyMaxAngle]['angle'] -= ($iFullAngle - 360);
          }
          elseif($iFullAngle < 360){
            $aPie[$keyMaxAngle]['angle'] += (360 - $iFullAngle);
          }
          //
          $iBA = $iEA = 270;
          $sM = 'M ' . $iCX . ' ' . $iCY . ' L ';
          $sA = ' A ' . $iR . ' ' . $iR . ' 0 ';
          for($i = 0; $i < count($aPie); $i++){ 
            $iBA = $iEA;
            $iEA = $iBA + $aPie[$i]['angle'];
            
            $x1 = ceil($iCX + $iR * cos(M_PI * $iBA / 180.0));
            $y1 = ceil($iCY + $iR * sin(M_PI * $iBA / 180.0));

            $x2 = ceil($iCX + $iR * cos(M_PI * $iEA / 180.0));
            $y2 = ceil($iCY + $iR * sin(M_PI * $iEA / 180.0));
            
            $sStyle = 'stroke: none; fill: #' . $aPie[$i]['color'] . '; fill-opacity: .7;';
            if($x1 === $x2 && $y1 === $y2){
              $cElementP = new cElement('circle', false);
              $cElementP->Attribute('cx', $iCX);
              $cElementP->Attribute('cy', $iCY);
              $cElementP->Attribute('r', $iR);
            }
            else{
              $cElementP = new cElement('path', false);
              $cElementP->Attribute('d', $sM . $x1 . ' ' . $y1 . $sA  . ($aPie[$i]['angle'] > 180 ? 1 : 0) . ' 1 ' . $x2 . ' ' . $y2 . ' Z');
            }
            $cElementP->Attribute('style', $sStyle);
            
            $cElementTitle = new cElement('title');
            $cElementTitle->Content(new cElementBase($aPie[$i]['name'] . ' [' . $aPie[$i]['count'] . ']'));
            $cElementP->Content($cElementTitle);
            
            $cElement->Content($cElementP);
            
          }
        }
        
      }
      else{
        $cElementGrad = new cElement('radialGradient', false);
        $cElementGrad->Attribute('id', 'rg-1');
        $cElementGrad->Attribute('cx', '50%');
        $cElementGrad->Attribute('cy', '50%');
        $cElementGrad->Attribute('r', '50%');
        
        $cElementStop = new cElement('stop', false);
        $cElementStop->Attribute('offset', '0%');
        $cElementStop->Attribute('style', 'stop-color: rgba(0, 0, 0, .7);');
        $cElementGrad->Content($cElementStop);
        
        $cElementStop = new cElement('stop', false);
        $cElementStop->Attribute('offset', '100%');
        $cElementStop->Attribute('style', 'stop-color: rgba(255, 255, 255, .7);');
        $cElementGrad->Content($cElementStop);
        
        $cElementDef = new cElement('defs', false);
        $cElementDef->Content($cElementGrad);
        
        $cElement->Content($cElementDef);
        
        $cElementCircle = new cElement('circle', false);
        $cElementCircle->Attribute('style', 'fill:url(#rg-1); stroke:none;');
        $cElementCircle->Attribute('cx', $iCX);
        $cElementCircle->Attribute('cy', $iCY);
        $cElementCircle->Attribute('r', $iR);
        
        $cElementTitle = new cElement('title');
        $cElementTitle->Content(new cElementBase(_('No Products found.')));
        $cElementCircle->Content($cElementTitle);
        
        $cElement->Content($cElementCircle);
      }

      return $cElement;
    }
    
    public function DoProductPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref       = sAction(WTS_PARAM_ACT_PRODUCT);
      $aProducts   = false;
      $cCurProduct = false;
      $iPage       = ((isset($_GET[WTS_PARAM_PAGE]) && $_GET[WTS_PARAM_PAGE] > 0) ? $_GET[WTS_PARAM_PAGE] : 1);
      
      $sCurSearch = $mCurSearch = false;
      
      if(isset($_GET[WTS_PARAM_SEARCH])){
        $sCurSearch = WTS_PARAM_SEARCH;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_CONTR])){
        $sCurSearch = WTS_PARAM_ACT_CONTR;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_PRODUCT])){
        $sCurSearch = WTS_PARAM_ACT_PRODUCT;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_CONT])){
        $sCurSearch = WTS_PARAM_ACT_CONT;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_GROUP])){
        $sCurSearch = WTS_PARAM_ACT_GROUP;
      }
      
      switch($sCurSearch){
        case WTS_PARAM_SEARCH:
          if(cProduct::bAddFilterPartValue($this->cDb, $_GET[WTS_PARAM_SEARCH])){
            $mCurSearch = $_GET[WTS_PARAM_SEARCH];
          }
          break;
        case WTS_PARAM_ACT_CONTR:
          if(cProduct::bAddFilterContract($this->cDb, $_GET[WTS_PARAM_ACT_CONTR])){
            $mCurSearch = $_GET[WTS_PARAM_ACT_CONTR];
          }
          break;
        case WTS_PARAM_ACT_PRODUCT:
          if(cProduct::bAddFilterProd($_GET[WTS_PARAM_ACT_PRODUCT])){
            $mCurSearch = $_GET[WTS_PARAM_ACT_PRODUCT];
          }
          break;
        case WTS_PARAM_ACT_CONT:
          if(cProduct::bAddFilterCont($_GET[WTS_PARAM_ACT_CONT])){
            $mCurSearch = $_GET[WTS_PARAM_ACT_CONT];
          }
          break;
        case WTS_PARAM_ACT_GROUP:
          if(cProduct::bAddFilterGroup($_GET[WTS_PARAM_ACT_GROUP])){
            $mCurSearch = $_GET[WTS_PARAM_ACT_GROUP];
          }
          break;
        default: break;
      }
      if($mCurSearch === false && $sCurSearch !== false){$sCurSearch = false;}
      
      //show only main product
      if($sCurSearch === false){
        cProduct::bAddFilterProd(-1);
      }
      
      $iRows  = cProduct::iCount($this->cDb);
      $iPages = 1;

      if($iRows > 0){
        //normalize page
        if($iRows > WTS_NARROW_ROW_PER_PAGE){
          $iPages = ceil($iRows / WTS_NARROW_ROW_PER_PAGE);
        }
        if($iPage > $iPages){$iPage = $iPages;}

        $iRows = cProduct::iUnserialize($this->cDb, $iPage, WTS_NARROW_ROW_PER_PAGE, $aProducts);
      }
      
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurProduct = &cProduct::mInArray($aProducts, $_GET[WTS_PARAM_ID]);
      }

      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $iCount = 1;
          if(isset($_POST[WTS_PARAM_COUNT])
          && (int)$_POST[WTS_PARAM_COUNT] > 1){
            $iCount = (int)$_POST[WTS_PARAM_COUNT];
          }
          $cObj = false;
          if($sCurSearch === WTS_PARAM_ACT_CONTR
          && ($cContract = cContract::mUnserialize($this->cDb, $mCurSearch))){
            
            if(isset($_POST[WTS_PARAM_TYPE])
            && ($cCategory = cCategory::mUnserialize($this->cDb, $_POST[WTS_PARAM_TYPE]))
            && $cCategory->bIsNode() === false){
              
              for($i = 0; $i < $iCount; $i++){
                $cObj = new cProduct($cCategory->iID());
                $cObj->Creator($this->cCont->iID());
                $cObj->Holder($cContract->iBuyer(), $cContract->iBuyerType());
                //dates
                $cObj->DtBegin($cContract->iDtBegin());
                $cObj->DtEnd($cContract->iDtEnd());
                
                if($cObj->bSerialize($this->cDb)){
                  if(!$cContract->bAttach($this->cDb, $cObj)){
                    $cObj->bDelete(cDb &$cDb);
                    unset($cObj);
                    $cObj = false;
                    break;
                  }
                }
                else{
                  unset($cObj);
                  $cObj = false;
                  break;
                }
              }
              
              if($cObj === false){
                $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
                $cMsg->Value(_('Can\'t create Product(s) in Category \'') . $cCategory->sName() . _('\' for Contract ') . $cContract->iID());
                $cMsg->bSerialize($this->cDb);
                unset($cMsg);
              }
              unset($cCategory);
              
            }
            unset($cContract);
          }
          break;
        case WTS_PARAM_EACT_UPDATE:
          if($cCurProduct){
            $cCurProduct->Updater($this->cCont->iID());
            //update attributes: attr-0-1
            $aAttrView = false;
            foreach($_POST as $key => &$s){
              if(strlen($key) > 7 && (WTS_PARAM_ATTRIBUTE . '-' === substr($key, 0, 5))){
                $a = explode('-', $key);
                if(count($a) === 3 && strlen($s) > 0){//[0] - attr, [1] - id, [2] - type
                  $aAttrView[] = new cAttrView($a[1], $a[2], $s);
                }
              }
            }
            if($cCurProduct->bSerializeAttr($this->cDb, $aAttrView)){
              $cLog = new cLog();
              $cLog->Value('Attributes for Product ' . $cCurProduct->iID()
              . ' updated by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);
            }
          }
          break;
        case WTS_PARAM_EACT_DETACH:
          if($cCurProduct){
            //delete or transfer product back to seller
            if($sCurSearch === WTS_PARAM_ACT_CONTR){
              /*
               * ищем контракт, по которому попали, если он последний для продукта
               * пытаемся вернуть старому владельцу, если он есть
               * куст продукта будет разобран, чужеродных детей оставим владельцу
               */
              if($cCurProduct->iHolderType() !== cProduct::PRODUCT
              && ($cContract = cContract::mUnserialize($this->cDb, $mCurSearch))
              && $cContract->iBuyer() === $cCurProduct->iHolder()
              && $cContract->iBuyerType() === $cCurProduct->iHolderType()
              //если даже владелец тот же, сам контракт будет не последним - отделять нельзя
              && cContract::iLast($this->cDb, $cCurProduct) === $cContract->iID()){
                
                $a = $cCurProduct->mChilds($this->cDb);
                //дополним себя для обработки в цикле
                $a['root'] = $cCurProduct->iID();
                
                foreach($a as $key => $iID){
                  $cProduct = false;
                  if($key !== 'root'){
                    $cProduct = cProduct::mUnserialize($this->cDb, $iID);
                  }
                  else{$cProduct = $cCurProduct;}
                  if($cProduct !== false){
                    $cProduct->Updater($this->cCont->iID());

                    //из нашего контракта - отделим
                    if(cContract::iLast($this->cDb, $cProduct) === $cContract->iID()){
                      
                      $cContract->bDetach($this->cDb, $cProduct);

                      if(($iPrev = cContract::iLast($this->cDb, $cProduct))
                      && ($cPrevContract = cContract::mUnserialize($this->cDb, $iPrev))){

                        //отметим нарушение топологии
                        if($cPrevContract->iBuyer() !== $cContract->iSeller()
                        || $cPrevContract->iBuyerType() !== $cContract->iSellerType()){
                          $cLog = new cLog(cLog::ERR);
                          $cLog->Value('Violation topology of Contracts ' . $cPrevContract->iID()
                          . ' and ' . $cContract->iID() . ' by Product ' . $cProduct->iID());
                          $cLog->bSerialize($this->cDb);
                          unset($cLog);
                          unset($cPrevContract);
                          unset($cProduct);
                          break;
                        }
                        //вернём продавцу
                        $cProduct->Holder($cContract->iSeller(), $cContract->iSellerType());
                        
                        //dates
                        $cProduct->DtBegin($cPrevContract->iDtBegin());
                        $cProduct->DtEnd($cPrevContract->iDtEnd());
                        $cProduct->bSerialize($this->cDb);
                        unset($cPrevContract);
                      }
                      else{
                        //так как больше нет записей по этому продукту, удаляем
                        $cProduct->bDelete($this->cDb);
                      }
                    }
                    else{//это не из нашего контракта - сбросим корень
                      $cProduct->Holder($cContract->iBuyer(), $cContract->iBuyerType());
                      $cProduct->bSerialize($this->cDb);
                    }
                    if($key !== 'root'){unset($cProduct);}
                  }
                }//foreach
              }
              else{
                $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
                $cMsg->Value(_('Can\'t detach Product from Contract \'') . $mCurSearch . _('\'. Product already use in other Contract.'));
                $cMsg->bSerialize($this->cDb);
                unset($cMsg);
              }

            }
            //detach from parent product
            elseif($sCurSearch === WTS_PARAM_ACT_PRODUCT){
              
              /*
               * если продукт попал к владельцу - он попал туда с конрактом,
               * в который вошли все дочерние продукты.
               * даже если дочерние продукты попали по другому контракту
               * владелец продукта от этого не меняется
               */
              if($cCurProduct->iHolderType() === cProduct::PRODUCT
              && ($i = $cCurProduct->iRoot($this->cDb)) > 0
              && $i !== $cCurProduct->iID()
              && ($cRootProduct = cProduct::mUnserialize($this->cDb, $i))){

                $cCurProduct->Updater($this->cCont->iID());
                $cCurProduct->Holder($cRootProduct->iHolder(), $cRootProduct->iHolderType());
                $cCurProduct->bSerialize($this->cDb);
                switch($cRootProduct->iHolderType()){
                  case cProduct::PRODUCT:
                    $sCurSearch = WTS_PARAM_ACT_PRODUCT;
                    break;
                  case cProduct::CONTACT:
                    $sCurSearch = WTS_PARAM_ACT_CONT;
                    break;
                  case cProduct::GROUP:
                    $sCurSearch = WTS_PARAM_ACT_GROUP;
                    break;
                  default: break;
                }
                $mCurSearch = $cRootProduct->iHolder();
                
                unset($cRootProduct);
              }
            }
          }
          break;
        case WTS_PARAM_EACT_MERGE: //attach product and all childs to contract 
          if($cCurProduct
          && isset($_GET[WTS_PARAM_ACT_CHAIN])
          && ($cContract = cContract::mUnserialize($this->cDb, $_GET[WTS_PARAM_ACT_CHAIN]))){
            //передаем только если он до сих пор у владельца
            if($cContract->iSellerType() === $cCurProduct->iHolderType()
            && $cContract->iSeller() === $cCurProduct->iHolder()){
              
              $a = $cCurProduct->mChilds($this->cDb);
              //дополним себя для обработки в цикле
              $a['root'] = $cCurProduct->iID();
              
              foreach($a as $key => $iID){
                $cProduct = false;
                if($key !== 'root'){
                  $cProduct = cProduct::mUnserialize($this->cDb, $iID);
                }
                else{
                  $cProduct = $cCurProduct;
                  $cProduct->Holder($cContract->iBuyer(), $cContract->iBuyerType());
                }
                if($cProduct !== false){
                  $cProduct->Updater($this->cCont->iID());
                  //dates
                  $cProduct->DtBegin($cContract->iDtBegin());
                  $cProduct->DtEnd($cContract->iDtEnd());
                  
                  $cProduct->bSerialize($this->cDb);
                  
                  if(!$cContract->bAttach($this->cDb, $cProduct)){
                    $cLog = new cLog(cLog::ERR);
                    $cLog->Value('Fail transfer Pdoduct ' . $cProduct->iID() . ' to Contract ' . $cContract->iID() . ' by Login ' . $this->cCont->sLogin());
                    $cLog->bSerialize($this->cDb);
                    unset($cLog);
                  }
                  if($key !== 'root'){unset($cProduct);}
                }
              }//foreach

              $sCurSearch = WTS_PARAM_ACT_CONTR;
              $mCurSearch = $cContract->iID();
            }
            else{
              $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
              $cMsg->Value(_('Can\'t attach Product to Contract ') . $cContract->iID());
              $cMsg->bSerialize($this->cDb);
              unset($cMsg);
            }
            unset($cContract);
          }
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t find Contract by ID ') . $_GET[WTS_PARAM_ACT_CHAIN]);
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_CHOWN: //set parent product
          $a = false;
          cProduct::ClearFilters();
          
          if($cCurProduct
          //найдём будующего предка
          && isset($_POST[WTS_PARAM_VALUE])
          && cProduct::bAddFilterPartValue($this->cDb, $_POST[WTS_PARAM_VALUE])
          && cProduct::iCount($this->cDb) === 1
          && cProduct::iUnserialize($this->cDb, 1, 1, $a) === 1
          //предок должен быть один
          && $a[0]->iHolder() === $cCurProduct->iHolder()
          && $a[0]->iHolderType() === $cCurProduct->iHolderType()
          //запретить делать родителей тойже категории, хотя бы через
          && $a[0]->iCategory() !== $cCurProduct->iCategory()){
            $cCurProduct->Updater($this->cCont->iID());
            $cCurProduct->Holder($a[0]->iID(), cProduct::PRODUCT);

            if($cCurProduct->bSerialize($this->cDb)){
              $sCurSearch = WTS_PARAM_ACT_PRODUCT;
              $mCurSearch = $a[0]->iID();
            }
            else{
              $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
              $cMsg->Value(_('Can\'t set Parent Product by Attribute \'') . $_POST[WTS_PARAM_VALUE] . '\'');
              $cMsg->bSerialize($this->cDb);
              unset($cMsg);
            }
          }
          else{
            if(isset($_POST[WTS_PARAM_VALUE])){
              $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
              $cMsg->Value(_('Can\'t find unique Parent Product by Attribute \'') . $_POST[WTS_PARAM_VALUE] . '\'');
              $cMsg->bSerialize($this->cDb);
              unset($cMsg);
            }
          }
          if($a){unset($a[0]);}
          break;
        default: break;
        }
        if($sCurSearch !== false){$sHref .= sSubAction($sCurSearch, urlencode($mCurSearch));}
        if($iPage > 1){$sHref .= sSubAction(WTS_PARAM_PAGE, $iPage);}
        if($cCurProduct !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurProduct->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_PRODUCT);
      $cElement->Content($cButton->cRoot());
      
      //search for products
      $cButton = new cButton();
      $cElementI = new cElement('input', false);
      $cElementI->Attribute('type', 'search');
      $cElementI->Attribute('class', WTS_PFX . '-toolbar-search');
      $cElementI->Attribute('name', WTS_PARAM_SEARCH);
      $cElementI->Attribute('id', WTS_PFX . '-search-box');
      $cElementI->Attribute('placeholder', '&#128270; ' . _('Search for Products'));
      $cElementI->Attribute('title', _('Enter Word for Search'));
      if($sCurSearch === WTS_PARAM_SEARCH){
        $cElementI->Attribute('value', htmlentities($mCurSearch));
      }
      $cButton->Content($cElementI);
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_PRODUCT);
      $cElement->Content($cButton->cRoot());
      
      //--btn detach
      if($cCurProduct
      && $sCurSearch){
        if(($sCurSearch === WTS_PARAM_ACT_CONTR && $cCurProduct->iHolderType() !== cProduct::PRODUCT)
        || $sCurSearch === WTS_PARAM_ACT_PRODUCT){
          $cButton = new cButton();
          $cButton->Image(IMG_EDIT_SPLIT, _('Detach Product'), WTS_PFX . '-toolbar-button');
          $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_PRODUCT);
          $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DETACH);
          $cButton->Hidden($sCurSearch, $mCurSearch);
          $cButton->Hidden(WTS_PARAM_ID, $cCurProduct->iID());
          if($iPage > 1){$cButton->Hidden(WTS_PARAM_PAGE, $iPage);}
          $cElement->Content($cButton->cRoot());
        }
        elseif($sCurSearch === WTS_PARAM_ACT_CONT || $sCurSearch === WTS_PARAM_ACT_GROUP){
          //to contract
          $cButton = new cButton();
          
          $cElementI = new cElement('input', false);
          $cElementI->Attribute('type', 'search');
          $cElementI->Attribute('class', WTS_PFX . '-toolbar-search');
          $cElementI->Attribute('style', 'min-width: 82px; width: 82px;');
          $cElementI->Attribute('placeholder', _('Contract ID'));
          //это не ошибка - параметр WTS_PARAM_ACT_CONTR уже используется для поиска
          $cElementI->Attribute('name', WTS_PARAM_ACT_CHAIN);
          
          $cElementI->Attribute('title', _('Enter Contract ID where current ') . ($sCurSearch === WTS_PARAM_ACT_CONT ? _('Contact') : _('Group')) . _(' as Buyer to attach Product'));


          $cButton->Content($cElementI);
          $cButton->Image(IMG_EDIT_MERGE, _('Attach To Contract'), WTS_PFX . '-toolbar-button');
          $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_PRODUCT);
          $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_MERGE);
          $cButton->Hidden($sCurSearch, $mCurSearch);
          $cButton->Hidden(WTS_PARAM_ID, $cCurProduct->iID());
          if($iPage > 1){$cButton->Hidden(WTS_PARAM_PAGE, $iPage);}
          $cElement->Content($cButton->cRoot());
        }
      }

      //page navbar
      if($iPages > 1){
        $aButtons = array();
        $this->CreateButtonsNav(WTS_PARAM_ACT_PRODUCT, $iPage, $iPages, $aButtons);
        
        foreach($aButtons as &$cButton){
          if($sCurSearch !== false){$cButton->Hidden($sCurSearch, $mCurSearch);}
          $cElement->Content($cButton->cRoot());
        }
      }
      $cPage->Main($cElement);
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Product Name'));
      $cTable->Column(_('Holder'));
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }
        
        $iNow = strtotime('now');
        $aConts = $aGroups = $aCats = false;

        $iID = 0;
        if($cCurProduct){$iID = $cCurProduct->iID();}
        
        foreach($aProducts as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . ($iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
            . ($sCurSearch === false ? '' : sSubAction($sCurSearch, urlencode($mCurSearch)))
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          
          if($c->iDtBegin() <= $iNow && $c->iDtEnd() >= $iNow){
            $cElementImg->Attribute('src', IMG_ENABLED);
            $cElementImg->Attribute('title', _('Valid'));
          }
          else{
            $cElementImg->Attribute('src', IMG_DISABLED);
            $cElementImg->Attribute('title', _('Invalid'));
          }
          $cElementTd->Content($cElementImg);
          
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          
          
          $cElementImg->Attribute('src', IMG_PRODUCT);
          $cElementImg->Attribute('title', _('Product'));

          $cElementTd->Content($cElementImg);

          if($c->iHolderType() === cProduct::PRODUCT){
            $cElementImg2 = new cElement('img', false);
            $cElementImg2->Attribute('class', WTS_PFX . '-table-narrow-image');
            $cElementImg2->Attribute('src', IMG_CHILD_PRODUCTS);
            $cElementImg2->Attribute('title', _('Has parent Product'));
            $cElementTd->Content($cElementImg2);
            //$cElementTd->Content(new cElementBase(_('Other Product')));
          }

          //I lost my name in battle! I implore you, let me search my name here, please )))
          if(!isset($aCats[$c->iCategory()])){
            $aCats[$c->iCategory()] = cCategory::mUnserialize($this->cDb, $c->iCategory());
          }
          
          if($aCats[$c->iCategory()]){
            
            if($aCats[$c->iCategory()]->sColor()){
              
              $cElementImg->Attribute('style', 'background-color:#'
              . $aCats[$c->iCategory()]->sColor() . ';');
            }
            $cElementTd->Content(new cElementBase(htmlentities($aCats[$c->iCategory()]->sName())));
          }
          
          //child products
          cProduct::ClearFilters();
          cProduct::bAddFilterProd($c->iID());
          if(cProduct::iCount($this->cDb) > 0){
            $cElementA = cSE::cCreate('a', _('Childs'), WTS_PFX . '-link');
            $cElementA->Attribute('href', sAction(WTS_PARAM_ACT_PRODUCT)
            . sSubAction(WTS_PARAM_ACT_PRODUCT, $c->iID()));
            $cElementA->Attribute('title', _('View child Products'));

            $cElementTd->Content($cElementA);
          }
            
          $cElementTr->Content($cElementTd);
          
          //column 2 - holder
          $cElementTd = new cElement('td', false);
          
          if($c->iHolderType() === cProduct::CONTACT){
            if(!isset($aConts[$c->iHolder()])){
              $aConts[$c->iHolder()] = cCont::mUnserialize($this->cDb, $c->iHolder());
            }
            if($aConts[$c->iHolder()]){
              $cElementTd->Content($this->cDoContInfo($aConts[$c->iHolder()]));
            }
          }
          elseif($c->iHolderType() === cProduct::GROUP){
            if(!isset($aGroups[$c->iHolder()])){
              $aGroups[$c->iHolder()] = cGroup::mUnserialize($this->cDb, $c->iHolder());
            }
            if($aGroups[$c->iHolder()]){
              $cElementTd->Content($this->cDoGroupInfo($aGroups[$c->iHolder()]));
            }
          }
          elseif($c->iHolderType() === cProduct::PRODUCT){
            
            if(($i = $c->iRoot($this->cDb)) > 0
            && $i !== $c->iID()
            && ($cRootProduct = cProduct::mUnserialize($this->cDb, $i))){
              if($cRootProduct->iHolderType() === cProduct::CONTACT){
                if(!isset($aConts[$cRootProduct->iHolder()])){
                  $aConts[$cRootProduct->iHolder()] = cCont::mUnserialize($this->cDb, $cRootProduct->iHolder());
                }
                if($aConts[$cRootProduct->iHolder()]){
                  $cElementTd->Content($this->cDoContInfo($aConts[$cRootProduct->iHolder()]));
                }
              }
              elseif($cRootProduct->iHolderType() === cProduct::GROUP){
                if(!isset($aGroups[$cRootProduct->iHolder()])){
                  $aGroups[$cRootProduct->iHolder()] = cGroup::mUnserialize($this->cDb, $cRootProduct->iHolder());
                }
                if($aGroups[$cRootProduct->iHolder()]){
                  $cElementTd->Content($this->cDoGroupInfo($aGroups[$cRootProduct->iHolder()]));
                }
              }
              unset($cRootProduct);
            }
          }
          
          $cElementTr->Content($cElementTd);
         
          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //049. view info
      $cElementCapt = $cToc = false;
      
      if($sCurSearch === WTS_PARAM_ACT_PRODUCT){
        if(($cProd = cProduct::mUnserialize($this->cDb, (int)$mCurSearch))
        && ($cCat = cCategory::mUnserialize($this->cDb, $cProd->iCategory()))){
          
          $cElementCapt = new cElement('div', false);
          $cElementCapt->Content(new cElementBase(_('View Childs')));

          $cToc = new cToc();
          $cToc->Child(new cElementBase(_('For Product')), new cElementBase($cCat->sName()));

          unset($cCat);
          unset($cProd);
        }
      }
      elseif($sCurSearch === WTS_PARAM_ACT_CONT){
        if($cCont = cCont::mUnserialize($this->cDb, (int)$mCurSearch)){
          $cToc = new cToc();
          $cToc->Child(new cElementBase(_('For Contact')), $this->cDoContInfo($cCont));
          
          unset($cCont);
        }
      }
      elseif($sCurSearch === WTS_PARAM_ACT_GROUP){
        if($cGroup = cGroup::mUnserialize($this->cDb, (int)$mCurSearch)){
          $cToc = new cToc();
          $cToc->Child(new cElementBase(_('For Group')), $this->cDoGroupInfo($cGroup));

          unset($cGroup);
        }
      }
      elseif($sCurSearch === WTS_PARAM_ACT_CONTR){
        if($cContract = cContract::mUnserialize($this->cDb, (int)$mCurSearch)){
          $cToc = new cToc();
          $cElementA = new cElement('a', false);
          $cElementA->Attribute('href', sAction(WTS_PARAM_ACT_CONTR)
          . sSubAction(WTS_PARAM_SEARCH, urlencode($cContract->iID()))
          . sSubAction(WTS_PARAM_ID, $cContract->iID()));
          $cElementA->Content(new cElementBase($cContract->iID()));
          
          $cToc->Child(new cElementBase(_('For Contract')), $cElementA);

          unset($cContract);
        }
      }
      if($cToc){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        if($cElementCapt === false){
          $cElementCapt = new cElement('div', false);
          $cElementCapt->Content(new cElementBase(_('View Products')));
        }
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElement->Content($cElementCapt);
        $cElement->Content($cToc->cRoot());
        $cPage->Aside($cElement);
      }

      //050. chart of products
      if($cCurProduct === false){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');

        $cElementCapt->Content(new cElementBase(_('Chart of Products')));

        $cElement->Content($cElementCapt);

        $iType = $iID = false;
        if($sCurSearch === WTS_PARAM_ACT_PRODUCT){
          $iType = cProduct::PRODUCT;
        }
        elseif($sCurSearch === WTS_PARAM_ACT_CONT){
          $iType = cProduct::CONTACT;
        }
        elseif($sCurSearch === WTS_PARAM_ACT_GROUP){
          $iType = cProduct::GROUP;
        }
        elseif($sCurSearch === WTS_PARAM_ACT_CONTR){
          $iType = 0;
        }
        
        $cField = new cField(cField::CENTER);
        $cField->Label($this->cDoSvgProdChart($iType, $mCurSearch));
        
        $cElement->Content($cField->cBuild());
          
        $cPage->Aside($cElement);
      }
      
      //060. side bar update
      if($cCurProduct){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      
        $cElementCapt->Content(new cElementBase(_('Product Properties')));
        $cElement->Content($cElementCapt);
        
        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
            . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_UPDATE)
            . ($iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
            . sSubAction(WTS_PARAM_ID, $cCurProduct->iID())
            . ($sCurSearch !== false ? sSubAction($sCurSearch, urlencode($mCurSearch)) : ''));
        $cElementForm->Attribute('method', 'post');
        
        //attr
        $aAttrView = false;
        if($cCurProduct->iUnserializeAttr($this->cDb, $aAttrView) > 0){
          foreach($aAttrView as $key => &$cAttrView){
            $sAttr = WTS_PARAM_ATTRIBUTE . '-' . $cAttrView->iID() . '-' . $cAttrView->iType();
            
            $cField = new cField();
            $cField->Label(cField::cElementLabel($cAttrView->sName(), $sAttr));

            $cElementInput = cField::cElementInput($sAttr, ($cAttrView->iID() === 0 ? null : $cAttrView->sValue()));
            if($cAttrView->sColor()){
              $cElementInput->Attribute('style', 'background-color:#' . $cAttrView->sColor());
            }
            $cField->Input($cElementInput);
            $cElementForm->Content($cField->cBuild());
            unset($aAttrView[$key]);
          }
        }
        
        //ok
        $cField = new cField(cField::RIGHT);

        $cField->Input(cField::cElementSubmit(_('Reset'), true));
        $cField->Input(cField::cElementSubmit(_('Apply')));
        
        $cElementForm->Content($cField->cBuild());
        
        $cElement->Content($cElementForm);
        
        $cPage->Aside($cElement);
        
        //070. side bar status
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Product Info')));
        
        $cElement->Content($cElementCapt);
        
        $cToc = new cToc();
        //owner???
        
        $cToc->Child(new cElementBase(_('Created')), $this->cDoTime($cCurProduct->iDtCreated()));
        $cToc->AddLinkForLastChild($this->cDoDate($cCurProduct->iDtCreated()));
        
        $cContCr = $bUnsetCr = false;
        if($cCurProduct->iCreator() !== $this->cCont->iID()){
          $cContCr = cCont::mUnserialize($this->cDb, $cCurProduct->iCreator());
          $bUnsetCr = true;
        }
        else{$cContCr = $this->cCont;}
        
        $cToc->Child(new cElementBase(_('Created by'))
                   , ($cContCr === false ? new cElementBase(_('Unknown')) : $this->cDoContInfo($cContCr)));
                   
        if($cCurProduct->iDtUpdated() > 0){
          $cToc->Child(new cElementBase(_('Updated')), $this->cDoTime($cCurProduct->iDtUpdated()));
          $cToc->AddLinkForLastChild($this->cDoDate($cCurProduct->iDtUpdated()));
          
          $cContUp = $bUnsetUp = false;
          
          if($cCurProduct->iUpdater() != $cContCr->iID()){
            if($cCurProduct->iUpdater() != $this->cCont->iID()){
              $cContUp = cCont::mUnserialize($this->cDb, $cCurProduct->iUpdater());
              $bUnsetUp = true;
            }
            else{$cContUp = $this->cCont;}
          }
          else{$cContUp = $cContCr;}
          
          $cToc->Child(new cElementBase(_('Updated by'))
                     , ($cContUp === false ? new cElementBase(_('Unknown')) : $this->cDoContInfo($cContUp)));
          if($bUnsetUp){unset($cContUp);}
        }
        
        if($bUnsetCr){unset($cContCr);}
        
        //life time
        $iDiff = 0;
        $iDiff = $cCurProduct->iDtEnd() - $cCurProduct->iDtBegin();
        $cToc->Child(new cElementBase(_('Life Time')), new cElementBase(cString::sTimediffToString($iDiff)));
        //begin/end
        $cToc->Child(new cElementBase(_('Begin')), $this->cDoTime($cCurProduct->iDtBegin()));
        $cToc->AddLinkForLastChild($this->cDoDate($cCurProduct->iDtBegin()));
        $cToc->Child(new cElementBase(_('End')), $this->cDoTime($cCurProduct->iDtEnd()));
        $cToc->AddLinkForLastChild($this->cDoDate($cCurProduct->iDtEnd()));
        
        //contracts
        cContract::bAddFilterProduct($this->cDb, $cCurProduct->iID());
        $iContrs = cContract::iCount($this->cDb);
        if($iContrs > 0){
          $cElementInfo = new cElement('a', false);
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CONTR)
          . sSubAction(WTS_PARAM_ACT_PRODUCT, $cCurProduct->iID()));
          $cElementInfo->Content(new cElementBase($iContrs));
          $cToc->Child(new cElementBase(_('Contracts')), $cElementInfo);
        }
        
        //child products //cCurProduct
        cProduct::ClearFilters();
        cProduct::bAddFilterProd($cCurProduct->iID());
        if(($iChilds = cProduct::iCount($this->cDb)) > 0){

          $cElementInfo = new cElement('a', false);
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_PRODUCT)
          . sSubAction(WTS_PARAM_ACT_PRODUCT, $cCurProduct->iID()));
          $cElementInfo->Attribute('title', _('View child Products'));
          $cElementInfo->Content(new cElementBase($iChilds));
          $cToc->Child(new cElementBase(_('Childs')), $cElementInfo);
        }
        
        $cElement->Content($cToc->cRoot());
        
        $cPage->Aside($cElement);
     
      }
      //080. side bar create
      $aCategories = false;
      if($sCurSearch === WTS_PARAM_ACT_CONTR
      && cCategory::iUnserialize($this->cDb, $aCategories) > 0){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Add New Product')));
        
        $cElement->Content($cElementCapt);
        
        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
            . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_CREATE)
            . ($iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
            . sSubAction($sCurSearch, $mCurSearch));
        $cElementForm->Attribute('method', 'post');
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Category'), WTS_PARAM_TYPE));
        
        $cSelect = new cSelect(WTS_PARAM_TYPE, WTS_PFX . '-input');

        foreach($aCategories as $key => &$cCat){
          $sLabel = '';
          for($i = 0; $i < $cCat->iLevel(); $i++){

            $sLabel .= ' ';
          }
          //if($cCat->iLevel() === 0){$sLabel .= '● ';}
          //else{$sLabel .= '○ ';}
          
          //$sLabel .= $cCat->sName();
          
          if($cCat->bIsNode()){$sLabel .= '[' . $cCat->sName(). ']';}
          else{$sLabel .= $cCat->sName();}

          $cSelect->Option(false, $cCat->iID(), $sLabel, $cCat->sColor(), $cCat->bIsNode());
          unset($aCategories[$key]);
        }
        
        $cField->Input($cSelect->cRoot());
        
        $cElementForm->Content($cField->cBuild());
        
        //count
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Count'), WTS_PARAM_COUNT));
        
        $cField->Input(cField::cElementInput(WTS_PARAM_COUNT, 1));
        
        $cElementForm->Content($cField->cBuild());
        //ok
        $cField = new cField(cField::RIGHT);
        $cField->Input(cField::cElementSubmit(_('Create')));

        $cElementForm->Content($cField->cBuild());
        
        $cElement->Content($cElementForm);
        
        $cPage->Aside($cElement);
        
        
      }
      
      //090. side bar set parent (search by attribute in same product holder: if found only one - link as parent)
      if($iRows > 1
      && $cCurProduct
      && ($sCurSearch === WTS_PARAM_ACT_CONT
       || $sCurSearch === WTS_PARAM_ACT_GROUP
       || $sCurSearch === WTS_PARAM_ACT_PRODUCT)){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Set Parent for Selected')));
        
        $cElement->Content($cElementCapt);
        
        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
            . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_CHOWN)
            . ($iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
            . sSubAction(WTS_PARAM_ID, $cCurProduct->iID())
            . sSubAction($sCurSearch, urlencode($mCurSearch)));
        $cElementForm->Attribute('method', 'post');
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Attribute'), WTS_PARAM_VALUE));
        
        $cElementInp = cField::cElementInput(WTS_PARAM_VALUE, null);
        $cElementInp->Attribute('placeholder', _('Parent Attribute'));
        $cElementInp->Attribute('title', _('Enter unique Attribute of Parent Product from other Category and from same Holder.'));
        
        $cField->Input($cElementInp);
        
        $cElementForm->Content($cField->cBuild());
        
        //ok
        $cField = new cField(cField::RIGHT);
        $cField->Input(cField::cElementSubmit(_('Link')));

        $cElementForm->Content($cField->cBuild());
        
        $cElement->Content($cElementForm);
        
        $cPage->Aside($cElement);
      }

    }
  }

}
?>
