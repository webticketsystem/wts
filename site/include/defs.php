<?php
/*
 * defs.php (part of WTS) - main difinitions file, you can redifine some values in conf.php 
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  /**
   * for debug mode
   */
  //ini_set('error_reporting', E_ALL);
  //ini_set('display_errors', 1);
  //ini_set('display_startup_errors', 1);
  
  /**
   * if defined all system messages were logged
   */
  //define('WTS_DEBUG_LOG',               'true');

  /**
   * main system settings file - created during installation
   * put your determination in this file
   */
  if(file_exists(dirname(__FILE__) .    '/conf.php')){
    require_once('conf.php');
  }
  
  define('WTS_PHP_VERSION_MIN',         '5.6.0');
  //define('WTS_MYSQL_VERSION_MIN',       '5.1');
  define('WTS_PFX',                     'wts');

  //Name.Year.Month.Day.Revision [dev|alpha|beta||rc(release-canditate)|r(release)]
  define('WTS_BUILD_NAME',              'Demetria');
  define('WTS_BUILD',                   WTS_BUILD_NAME . '.2018.10.09.r');

  /**
   * default encoding
   */
  if(!defined('WTS_ENC')){
    define('WTS_ENC',                   'UTF-8');
  }
  
  if(!defined('WTS_CHARSET')){
    define('WTS_CHARSET',               'utf-8');
  }
  
  if(!defined('WTS_PATH')){
    define('WTS_PATH',                  './');
  }
  define('WTS_HREF',                    WTS_PATH . 'index.php');
  
  /*-- header settings --**********************************************/
  if(!defined('WTS_TITLE_DESC')){
    define('WTS_TITLE_DESC',            'Powered by WTS');
  }
  
  if(!defined('WTS_LOGO_TITLE')){
    define('WTS_LOGO_TITLE',            WTS_TITLE_DESC);
  }
  
  if(!defined('WTS_LOGO_HREF')){
    define('WTS_LOGO_HREF',             WTS_HREF);
  }
  
  /*-- security --*****************************************************/
  if(!defined('WTS_PASSWD_LENGHT')){
    define('WTS_PASSWD_LENGHT',         '6');
  }
  /**
   * timeout of session
   * 
   * logof then timeout > timeout of session
   * default 1h = 60*60 sec
   */
  if(!defined('WTS_SESSION_TIMEOUT')){
    define('WTS_SESSION_TIMEOUT',       '3600');
  }
  /**
   * timeout event lock
   *
   * default 30m = 30*60 sec
   */
  if(!defined('WTS_LOCK_TIMEOUT')){
    define('WTS_LOCK_TIMEOUT',          '1800');
  }
  /**
   * life cicle of backups before autocleaning in days
   *
   * default 2 week
   */
  if(!defined('WTS_BACKUP_LC')){
    define('WTS_BACKUP_LC',         '14');
  }
  
  /*-- sizes --********************************************************/
  /**
   * max email size
   * 
   * 1(byte)*1024(kb)*1024(mb)*15 = 15728640 - 1024(buffer) = 15727616
   */
  if(!defined('WTS_MAX_PKG_SIZE')){
    define('WTS_MAX_PKG_SIZE',          '15727616');
  }
  
  /*-- db --***********************************************************/
  /**
   * database scheme version
   * a.d.c
   * 
   * a - change if rename/delete exisitng tables
   * b - change if add new tables
   * c - change if add collumns in exisitng tables
   */
  define('WTS_DB_SCHEME',               '3.0.0');

  if(!defined('WTS_DB_TABLE_PREFIX')){
    define('WTS_DB_TABLE_PREFIX',       WTS_PFX);
  }
  
  if(!defined('WTS_DB_CHARSET')){
    define('WTS_DB_CHARSET',            'utf8');
  }
  
  if(!defined('WTS_DB_COLLATE')){
    define('WTS_DB_COLLATE',            'utf8_unicode_ci');
  }
  /**
   * max table size in bytes
   * 
   * change data table then table is more
   * default 512m
   */
  if(!defined('WTS_DB_MAX_TBL_SIZE')){
    define('WTS_DB_MAX_TBL_SIZE',     '536870912');
  }
  
  /**
   * date format
   */
  if(!defined('WTS_D_FMT')){
    define('WTS_D_FMT',                 'd.m.Y');
  }
  /**
   * time short format
   */
  if(!defined('WTS_ST_FMT')){
    define('WTS_ST_FMT',                'H:i');
  }
  /**
   * time format
   */
  if(!defined('WTS_T_FMT')){
    define('WTS_T_FMT',                 WTS_ST_FMT . ':s');
  }
  /**
   * date & time format
   */
    define('WTS_DT_FMT',                WTS_D_FMT . ' ' . WTS_T_FMT);
  /**
   * date & time format for backup
   */
  if(!defined('WTS_DT_BACKUP_FMT')){
    define('WTS_DT_BACKUP_FMT',         'Ymd_His');
  }
  /**
   * first day of week: 0 - sun, 1 - mon, 2 - tue
   */
  if(!defined('WTS_DT_FD')){
    define('WTS_DT_FD',                 1);
  }
  /**
   * currency - one off: &pound;, &#8381;, &yen;, &euro;, &#36;, &#22291;, ...
   */
  if(!defined('WTS_CURRENCY')){
    define('WTS_CURRENCY',              '&#8381;');
  }
  
  /**
   * prefix for subject
   */
  if(!defined('WTS_SBJ_PREFIX')){
    define('WTS_SBJ_PREFIX',            'Request #');
  }
  /**
   * autobind with chain by subject
   * 
   * values: true, false
   */
    define('WTS_SBJ_AUTOBIND',          'true');
  /**
   * event delay before send
   * 
   * default: 3m  = 3*60 sec
   */
    define('WTS_EVENT_DELAY',           '180');
  /**
   * text for empty body
   */
  if(!defined('WTS_EVENT_NOTEXT')){
    define('WTS_EVENT_NOTEXT',          '- No text message -');
  }
  /**
   * size for body preview
   */
  if(!defined('WTS_EVENT_PREVIEW_LENGHT')){
    define('WTS_EVENT_PREVIEW_LENGHT',  128);
  }
  /**
   * size for message attachment thumbnails
   */
  if(!defined('WTS_EVENT_THUMB_SIDE')){
    define('WTS_EVENT_THUMB_SIDE',      160);
  }
  
  /**
   * count records per page
   */
  if(!defined('WTS_NARROW_ROW_PER_PAGE')){//chain, dashboard
    define('WTS_NARROW_ROW_PER_PAGE',   25);
  }
  
  if(!defined('WTS_WIDE_ROW_PER_PAGE')){//contacts, groups, reports...
    define('WTS_WIDE_ROW_PER_PAGE',     15);
  }
  /**
   * count records per advice
   */
  if(!defined('WTS_ROW_PER_ADVICE')){
    define('WTS_ROW_PER_ADVICE',        15);
  }
  
  /*-- HTTP headers --*************************************************/
  define('WTS_HTTP_200',                'HTTP/1.1 200 OK');
  define('WTS_HTTP_202',                'HTTP/1.1 202 Accepted');//for add
  define('WTS_HTTP_204',                'HTTP/1.1 204 No Content');//successful delete request
  define('WTS_HTTP_400',                'HTTP/1.1 400 Bad Request');
  define('WTS_HTTP_401',                'HTTP/1.1 401 Unauthorized');
  define('WTS_HTTP_403',                'HTTP/1.1 403 Forbidden');
  define('WTS_HTTP_404',                'HTTP/1.1 404 Not Found');

}
?>
