<?php
/*
 * agent.php (part of WTS) - class for Agent level UI
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
namespace wts{

  require_once('customer.php');
  require_once('withdashboardpage.php');
  require_once('withcontactpage.php');
  require_once('withgrouppage.php');
  require_once('withproductpage.php');
  require_once('withcontractpage.php');


  /*-- agent --**********************************************************/
  class cAgent extends cCustomer{
    use tWithDashboardPage;
    use tWithContactPage;
    use tWithGroupPage;
    use tWithProductPage;
    use tWithContractPage;
    

    public function __construct(cDb &$cDb, cCont &$cCont){
      parent::__construct($cDb, $cCont);
    }

    public function Run(){
      
      $cPage = new cPage();
      
      $sDefAction = $sAction = WTS_PARAM_ACT_DASHBOARD;
      
      if(isset($_GET[WTS_PARAM_ACTION])){
        $sAction = $_GET[WTS_PARAM_ACTION];
      }
      
      switch($sAction){
        case WTS_PARAM_ACT_CHAIN:
          $this->DoChainPage($cPage);
          break;
        case WTS_PARAM_ACT_EVENT:
          $this->DoEventPage($cPage);
          break;
        case WTS_PARAM_ACT_CONT:
          $this->DoContactPage($cPage);
          break;
        case WTS_PARAM_ACT_GROUP:
          $this->DoGroupPage($cPage);
          break;
        case WTS_PARAM_ACT_PRODUCT:
          $this->DoProductPage($cPage);
          break;
        case WTS_PARAM_ACT_CONTR:
          $this->DoContractPage($cPage);
          break;
        case WTS_PARAM_ACT_CALENDAR:
          $this->DoCalendarPage($cPage);
          break;
        case WTS_PARAM_ACT_REPORT:
          $this->DoReportPage($cPage);
          break;
        case WTS_PARAM_ACT_ABOUT:
          $this->DoAboutPage($cPage);
          break;
        case WTS_PARAM_ACT_PROFILE:
          $this->DoProfilePage($cPage);
          break;
        case WTS_PARAM_ACT_LOGOUT:
          $this->cCont->CloseSsn($this->cDb);
          header('Location: ' . WTS_HREF);
          exit;
        default:
          if($sAction != $sDefAction){$sAction = $sDefAction;}
          $this->DoDashboardPage($cPage);
          break;
      }

      //--login info
      $cPage->Header($this->cAddAccount());
      $cPage->AddTitle();
      
      //--message - if exist
      $iHeaderHeight = 100;
      if(($i = $this->iAddMessage($cPage)) > 0){
        $iHeaderHeight += ($i * 35);
      }
      
      //--menu
      $cMenu = new cMenu();
      
      $cMenu->Child($this->cAddDashboardMenuItem($sAction));
      $cMenu->Child($this->cAddChainMenuItem($sAction));
      $cMenu->Child($this->cAddContMenuItem($sAction));
      $cMenu->Child($this->cAddGroupMenuItem($sAction));
      $cMenu->Child($this->cAddProdMenuItem($sAction));
      $cMenu->Child($this->cAddContractMenuItem($sAction));
      $cMenu->Child($this->cAddCalendarMenuItem($sAction));
      $cMenu->Child($this->cAddReportMenuItem($sAction));
      $cMenu->Child($this->cAddAboutMenuItem($sAction));
      
      $cPage->Header($cMenu->cRoot());

      echo $cPage->sSerialize($iHeaderHeight);
      unset($cPage);
    }
  }

}
?>
