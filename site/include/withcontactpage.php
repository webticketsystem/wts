<?php
/*
 * withcontactpage.php (part of WTS) - trait UI tWithContactPage
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  trait tWithContactPage{
    
    protected function &cAddContMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Contacts')
                         , _('Contacts management')
                         , ($sCurAct !== WTS_PARAM_ACT_CONT ? sAction(WTS_PARAM_ACT_CONT) : null));
      return $cMenuItem;
    }
    
    public function DoContactPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref    = sAction(WTS_PARAM_ACT_CONT);
      $aConts   = false;
      $cCurCont = false;
      $iPage    = ((isset($_GET[WTS_PARAM_PAGE]) && $_GET[WTS_PARAM_PAGE] > 0) ? $_GET[WTS_PARAM_PAGE] : 1);
      
      $sCurSearch = $mCurSearch = false;
      
      if(isset($_GET[WTS_PARAM_SEARCH])){
        $sCurSearch = WTS_PARAM_SEARCH;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_GROUP])){
        $sCurSearch = WTS_PARAM_ACT_GROUP;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_QUEUE])){
        $sCurSearch = WTS_PARAM_ACT_QUEUE;
      }
      
      switch($sCurSearch){
        case WTS_PARAM_SEARCH:
          if(cCont::bAddFilterPartValue($this->cDb, $_GET[WTS_PARAM_SEARCH])){
            $mCurSearch = $_GET[WTS_PARAM_SEARCH];
          }
          break;
        case WTS_PARAM_ACT_GROUP:
          if(cCont::bAddFilterGroup($_GET[WTS_PARAM_ACT_GROUP])){
            $mCurSearch = $_GET[WTS_PARAM_ACT_GROUP];
          }
          break;
        case WTS_PARAM_ACT_QUEUE:
          if(cCont::bAddFilterQueue($_GET[WTS_PARAM_ACT_QUEUE])){
            $mCurSearch = $_GET[WTS_PARAM_ACT_QUEUE];
          }
          break;
        default: break;
      }
      if($mCurSearch === false && $sCurSearch !== false){$sCurSearch = false;}

      $iRows  = cCont::iCount($this->cDb);
      $iPages = 1;

      if($iRows > 0){
        //normalize page
        if($iRows > WTS_NARROW_ROW_PER_PAGE){
          $iPages = ceil($iRows / WTS_NARROW_ROW_PER_PAGE);
        }
        if($iPage > $iPages){$iPage = $iPages;}

        $iRows = cCont::iUnserialize($this->cDb, $iPage, WTS_NARROW_ROW_PER_PAGE, $aConts);
      }
      
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurCont = &cCont::mInArray($aConts, $_GET[WTS_PARAM_ID]);
      }
      
      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE://разрешаем создавать всем
          $cObj = new cCont();
          if(isset($_POST[WTS_PARAM_LOGIN])){
            $cObj->Login($_POST[WTS_PARAM_LOGIN]);
            $cObj->Creator($this->cCont->iID());
            
            if(isset($_POST[WTS_PARAM_ACT_GROUP])
            && ($cGroup = cGroup::mUnserialize($this->cDb, $_POST[WTS_PARAM_ACT_GROUP]))){
              $cObj->Group($cGroup->iID());
              unset($cGroup);
            }
            
            if(isset($_POST[WTS_PARAM_ACT_QUEUE])
            && $this->cCont->iType() === cCont::ADMIN //только админ может назначать очередь
            && ($cQueue = cQueue::mUnserialize($this->cDb, $_POST[WTS_PARAM_ACT_QUEUE]))){
              $cObj->Queue($cQueue->iID());
              unset($cQueue);
            }
          }
          
          if($cObj->bSerialize($this->cDb)){

            $cCurCont = &$cObj;
            $iPage = 1;
            $sCurSearch = WTS_PARAM_SEARCH;
            $mCurSearch = $cObj->sLogin();
            $cLog = new cLog();
            $cLog->Value('Contact ' . $cCurCont->sLogin() . ' created by ' . $this->cCont->sLogin());
            $cLog->bSerialize($this->cDb);
            unset($cLog);
          }
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Contact using Login \'') . $_POST[WTS_PARAM_LOGIN] . '\'');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_UPDATE://при обновлении смотрим, чтобы агенты не редактировали агентов
          if($cCurCont
          && ($cCurCont->iType() === cCont::CSTMR || $this->cCont->iType() === cCont::ADMIN)){
            $cCurCont->Updater($this->cCont->iID());
            if(isset($_POST[WTS_PARAM_LOGIN])){
              $cCurCont->Login($_POST[WTS_PARAM_LOGIN]);
            }
            
            if(isset($_POST[WTS_PARAM_ACT_GROUP])){
              $cGroup = cGroup::mUnserialize($this->cDb, $_POST[WTS_PARAM_ACT_GROUP]);
              $cCurCont->Group($cGroup === false ? 0 : $cGroup->iID());
            }

            if(isset($_POST[WTS_PARAM_ACT_QUEUE])
            && $this->cCont->iType() === cCont::ADMIN){//только админ может менять очередь
              
              $cQueue = cQueue::mUnserialize($this->cDb, $_POST[WTS_PARAM_ACT_QUEUE]);
              
              if($cCurCont->iQueue() > 0
              && ($cQueue === false || $cQueue->iID() !== $cCurCont->iQueue())){
                //не переводим агента в другую очередь, пока  у него есть открытые заявки!
                cChain::bAddFilterOwner($cCurCont->iID());
                cChain::bAddFilterClosed(false);
                if(cChain::iCount($this->cDb) > 0){
                  $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
                  $cMsg->Value(_('Can\'t change Queue for Contact \'') . $cCurCont->sLogin()
                  . _('\' that has Child objects: Open Chains'));
                  $cMsg->bSerialize($this ->cDb);
                  unset($cMsg);
                }
                else{$cCurCont->Queue($cQueue === false ? 0 : $cQueue->iID());}
              }
              else{$cCurCont->Queue($cQueue === false ? 0 : $cQueue->iID());}
            }
            
            //disabled/enabled
            if(isset($_POST[WTS_PARAM_DISABLED])){
              if($_POST[WTS_PARAM_DISABLED] === 'true'){$cCurCont->Disable();}
              else{$cCurCont->Enable();}
            }

            if($cCurCont->bSerialize($this->cDb)){
              $cLog = new cLog();
              $cLog->Value('Contact ' . $cCurCont->sLogin() . ' updated by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);
            }
            //update attributes: attr-0-1
            $aAttrView = false;
            foreach($_POST as $key => &$s){
              if(strlen($key) > 7 && (WTS_PARAM_ATTRIBUTE . '-' === substr($key, 0, 5))){
                $a = explode('-', $key);
                if(count($a) === 3 && strlen($s) > 0){//[0] - attr, [1] - id, [2] - type
                  $aAttrView[] = new cAttrView($a[1], $a[2], $s);
                }
              }
            }
            if($cCurCont->bSerializeAttr($this->cDb, $aAttrView)){
              $cLog = new cLog();
              $cLog->Value('Attributes for Contact ' . $cCurCont->sLogin()
              . ' updated by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);
            }
          }
          break;
        case WTS_PARAM_EACT_MERGE:
          if($cCurCont
          && isset($_POST[WTS_PARAM_LOGIN])
          && ($cCont = cCont::mUnserialize($this->cDb, $_POST[WTS_PARAM_LOGIN]))
          && $cCont->iID() !== $cCurCont->iID()
          && $cCont->iID() > 1
          && $cCurCont->iID() > 1
          && ($cCont->iType() === cCont::CSTMR || $this->cCont->iType() === cCont::ADMIN)
          && ($cCurCont->iType() === cCont::CSTMR || $this->cCont->iType() === cCont::ADMIN)){
            
            //создавал/правил ли заявки
            cChain::bAddFilterCreatorOwner($cCont->iID());
            //создавал/правил ли контакты
            cCont::ClearFilters();
            cCont::bAddFilterCreatorOwner($cCont->iID());
            //создавал/правил ли группы
            cGroup::bAddFilterCreatorOwner($cCont->iID());
            //создавал/правил ли контракты
            cContract::bAddFilterCreatorOwner($cCont->iID());
            //создавал/правил ли продукты
            cProduct::bAddFilterCreatorOwner($cCont->iID());
            
            if(cChain::iCount($this->cDb) > 0
            || cCont::iCount($this->cDb) > 0
            || cGroup::iCount($this->cDb) > 0
            || cContract::iCount($this->cDb) > 0
            || cProduct::iCount($this->cDb) > 0){
              $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
              $cMsg->Value(_('Can\'t merge Contact \'') . $cCont->sLogin()
             . _('\' that has been modifier of objects like: Contacts, Groups, Chains, Contracts, Products'));
              $cMsg->bSerialize($this ->cDb);
              unset($cMsg);
            }
            else{//он не оставил след в системе - можно сливать
              $cLog = new cLog(cLog::WARN);
              $cLog->Value('Merge Contact ' . $cCont->sLogin()
              . ' into Contact ' . $cCurCont->sLogin() . ' by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);
              $cMsg = new cMsg($this->cCont->iID());
              $cMsg->Value(_('Contact ') . $cCont->sLogin()
              . _(' merged into Contact ') . $cCurCont->sLogin());
              $cMsg->bSerialize($this->cDb);
              unset($cMsg);
              cChain::MergeCont($this->cDb, $cCont->iID(), $cCurCont->iID());
              cContract::MergeCont($this->cDb, $cCont->iID(), $cCurCont->iID());
              cProduct::MergeCont($this->cDb, $cCont->iID(), $cCurCont->iID());
              $cCont->bDelete($this->cDb);
            }
          }
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t merge Contacts'));
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        default: break;
        }
        if($sCurSearch !== false){$sHref .= sSubAction($sCurSearch, urlencode($mCurSearch));}
        if($iPage > 1){$sHref .= sSubAction(WTS_PARAM_PAGE, $iPage);}
        if($cCurCont !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurCont->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CONT);
      $cElement->Content($cButton->cRoot());
      
      //search for contacts
      $cButton = new cButton();
      $cElementI = new cElement('input', false);
      $cElementI->Attribute('type', 'search');
      $cElementI->Attribute('class', WTS_PFX . '-toolbar-search');
      $cElementI->Attribute('name', WTS_PARAM_SEARCH);
      $cElementI->Attribute('id', WTS_PFX . '-search-box');
      $cElementI->Attribute('placeholder', '&#128270; ' . _('Search for Contacts'));
      $cElementI->Attribute('title', _('Enter Word for Search'));
      if($sCurSearch === WTS_PARAM_SEARCH){
        $cElementI->Attribute('value', htmlentities($mCurSearch));
      }
      $cButton->Content($cElementI);
      
      $cElementUl = new cElement('ul', false);
      $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper');
      $cButton->Content($cElementUl);
      
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CONT);
      
      $cElementForm = $cButton->cRoot();
      $cElementForm->Attribute('id', WTS_PFX . '-search-form');
      $cElementForm->Attribute('autocomplete', 'off');
      
      $cElement->Content($cElementForm);
      
      $cElementS = new cElement('script', false);
      $cElementS->Attribute('type', 'text/javascript');
      $cElementS->Attribute('src', WTS_PATH . 'advice/jquery.min.js');
      $cPage->Head($cElementS);
      
      $cElementS = new cElement('script', false);
      $cElementS->Attribute('type', 'text/javascript');
      $cElementS->Attribute('src', WTS_PATH . 'advice/csearch.js');
      $cPage->Head($cElementS);
      
      //page navbar
      if($iPages > 1){
        $aButtons = array();
        $this->CreateButtonsNav(WTS_PARAM_ACT_CONT, $iPage, $iPages, $aButtons);
        
        foreach($aButtons as &$cButton){
          if($sCurSearch !== false){$cButton->Hidden($sCurSearch, $mCurSearch);}
          $cElement->Content($cButton->cRoot());
        }
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Contact Name'));
      $cTable->Column(_('Login'));
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }

        $iID = 0;
        if($cCurCont){$iID = $cCurCont->iID();}
        
        foreach($aConts as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . ($iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
            . ($sCurSearch === false ? '' : sSubAction($sCurSearch, urlencode($mCurSearch)))
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          if($c->bDisabled()){
            $cElementImg->Attribute('src', IMG_DISABLED);
            $cElementImg->Attribute('title', _('Disabled'));
          }
          else{
            $cElementImg->Attribute('src', IMG_ENABLED);
            $cElementImg->Attribute('title', _('Enabled'));
          }
          
          $cElementTd->Content($cElementImg);
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          
          if($c->iType() == cCont::CSTMR){
            $cElementImg->Attribute('src', IMG_CONTACT);
            $cElementImg->Attribute('title', _('Contact'));
          }
          else{
            $cElementImg->Attribute('src', IMG_AGENT);
            $cElementImg->Attribute('title', _('Agent'));
          }
          
          $cElementTd->Content($cElementImg);

          $cElementTd->Content(new cElementBase($c->sName()));
          $cElementTr->Content($cElementTd);

          //column 2
          $cElementTd = new cElement('td', false);
          
          $cElementTd->Content(new cElementBase($c->sLogin()));
          $cElementTr->Content($cElementTd);
         
          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);

      //050. sidebar - create new or update
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      
      $c = ($cCurCont ? $cCurCont : false);
      $bEdit = true;
      
      if($c){
        $cElementCapt->Content(new cElementBase(_('Contact Properties')));
        $bEdit = ($c->iType() === cCont::CSTMR || $this->cCont->iType() === cCont::ADMIN);
      }
      else{
        $cElementCapt->Content(new cElementBase(_('Add New Contact')));
      }
      $cElement->Content($cElementCapt);
      
      //fill inputs
      $aFields = false;
        
      $aFields[WTS_PARAM_LOGIN] = new cField();
      $aFields[WTS_PARAM_LOGIN]->Label(cField::cElementLabel(_('Login'), WTS_PARAM_LOGIN));
      $cElementInp = cField::cElementInput(WTS_PARAM_LOGIN, ($c ? $c->sLogin() : null));
      

      if($bEdit !== false){
        $cElementInp->Attribute('placeholder', 'my.contact@example.com');
        $cElementInp->Attribute('title', _('Login of Contact must be unique. It must be real email address of Contact.'));
        
        $cElementSpan = new cElement('span', false);
        $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
        $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_LOGIN);
        $aFields[WTS_PARAM_LOGIN]->Label($cElementSpan);

        $sScript = '
  function validate(){
    if(document.forms[\'' . WTS_PARAM_ACT_CONT . '\'][\'' . WTS_PARAM_LOGIN . '\'].value.length==0){
      document.getElementById(\'id_' . WTS_PARAM_LOGIN . '\').innerHTML=\'&#9998;\';
      return false;
    }
    return true;
  }';
          
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Content(new cElementBase($sScript, false));
        $cPage->Head($cElementS);
      }
      else{
        $cElementInp->Attribute('disabled');
      }
      $aFields[WTS_PARAM_LOGIN]->Input($cElementInp);
      
      //attr
      if($c){
        $aAttrView = false;
        if($c->iUnserializeAttr($this->cDb, $aAttrView) > 0){
          foreach($aAttrView as $key => &$cAttrView){
            $sAttr = WTS_PARAM_ATTRIBUTE . '-' . $cAttrView->iID() . '-' . $cAttrView->iType();
            
            $aFields[$sAttr] = new cField();
            $aFields[$sAttr]->Label(cField::cElementLabel($cAttrView->sName(), $sAttr));
            
            $cElementInp = cField::cElementInput($sAttr, ($cAttrView->iID() === 0 ? null : $cAttrView->sValue()));
            if($cAttrView->sColor()){
              $cElementInp->Attribute('style', 'background-color:#' . $cAttrView->sColor());
            }
            if($bEdit === false)$cElementInp->Attribute('disabled');
            $aFields[$sAttr]->Input($cElementInp);
            
            unset($aAttrView[$key]);
          }
        }
      }
      
      $cGroup = $cQueue = false;
      if($c){
        if($c->iGroup() > 0){
          $cGroup = cGroup::mUnserialize($this->cDb, $c->iGroup());
        }
        if($c->iQueue() > 0){
          $cQueue = cQueue::mUnserialize($this->cDb, $c->iQueue());
        }
      }
      else{
        switch($sCurSearch){
          case WTS_PARAM_ACT_GROUP:
            $cGroup = cGroup::mUnserialize($this->cDb, (int)$mCurSearch);
            break;
          case WTS_PARAM_ACT_QUEUE:
            $cQueue = cQueue::mUnserialize($this->cDb, (int)$mCurSearch);
            break;
          default: break;
        }
      }
      
      //group - добавляем, только если можно модифицировать - всё равно в статусе видно от куда он
      if($bEdit === true){
        $aFields[WTS_PARAM_ACT_GROUP] = new cField();
        $aFields[WTS_PARAM_ACT_GROUP]->Label(cField::cElementLabel(_('Group'), WTS_PARAM_ACT_GROUP));
        $cElementInput = cField::cElementInput(WTS_PARAM_ACT_GROUP
                                        , ($cGroup ? $cGroup->sName() : null));
        $cElementInput->Attribute('id', WTS_PFX . '-search-box-to');
        $aFields[WTS_PARAM_ACT_GROUP]->Input($cElementInput);
        
        $cElementUl = new cElement('ul', false);
        $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper-to');
        $aFields[WTS_PARAM_ACT_GROUP]->Input($cElementUl);
      }
      
      //queue - добавляем поле только админу, но ему самому очередь не меняем
      if($this->cCont->iType() === cCont::ADMIN && ($c === false || $c->iID() > 1)){
        $aFields[WTS_PARAM_ACT_QUEUE] = new cField();
        $aFields[WTS_PARAM_ACT_QUEUE]->Label(cField::cElementLabel(_('Queue'), WTS_PARAM_ACT_QUEUE));
        $cElementInput = cField::cElementInput(WTS_PARAM_ACT_QUEUE
                                      , ($cQueue ? $cQueue->sName() : null));
        $cElementInput->Attribute('id', WTS_PFX . '-search-box-cc');
        $aFields[WTS_PARAM_ACT_QUEUE]->Input($cElementInput);
        
        $cElementUl = new cElement('ul', false);
        $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper-cc');
        $aFields[WTS_PARAM_ACT_QUEUE]->Input($cElementUl);
      }
      
      //disabled/enabled
      if($bEdit === true && $c && $c->iID() > 1){
        $aFields[WTS_PARAM_DISABLED] = new cField();
        $aFields[WTS_PARAM_DISABLED]->Label(cField::cElementLabel(_('Is Disabled'), WTS_PARAM_DISABLED));
        
        $cSelect = new cSelect(WTS_PARAM_DISABLED, WTS_PFX . '-input');
        $cSelect->Option($cCurCont->bDisabled(), 'true', _('Yes'));
        $cSelect->Option(!$cCurCont->bDisabled(), 'false', _('No'));
        
        $aFields[WTS_PARAM_DISABLED]->Input($cSelect->cRoot());
      }
      
      //add form
      if($bEdit === true){
        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
          . sSubAction(WTS_PARAM_EDIT, ($c ? WTS_PARAM_EACT_UPDATE : WTS_PARAM_EACT_CREATE))
          . ($c && $iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
          . ($c ? sSubAction(WTS_PARAM_ID, $c->iID()) : '')
          . ($c && $sCurSearch !== false ? sSubAction($sCurSearch, urlencode($mCurSearch)) : ''));

        $cElementForm->Attribute('method', 'post');
        $cElementForm->Attribute('autocomplete', 'off');
        $cElementForm->Attribute('name', WTS_PARAM_ACT_CONT);
        $cElementForm->Attribute('onsubmit', 'return validate()');
        
        foreach($aFields as &$cField){
          $cElementForm->Content($cField->cBuild());
        }
        
        //ok
        $cField = new cField(cField::RIGHT);
        if($c){
          $cField->Input(cField::cElementSubmit(_('Reset'), true));
          $cField->Input(cField::cElementSubmit(_('Apply')));
        }
        else{
          $cField->Input(cField::cElementSubmit(_('Create')));
        }
        $cElementForm->Content($cField->cBuild());

        $cElement->Content($cElementForm);
      }
      else{//or show info
        foreach($aFields as &$cField){
          $cElement->Content($cField->cBuild());
        }
      }
      $cPage->Aside($cElement);
      
      if($c){
        //060. side bar status
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Contact Info')));
        
        $cElement->Content($cElementCapt);
        
        $cToc = new cToc();

        $cToc->Child(new cElementBase(_('Created')), $this->cDoTime($c->iDtCreated()));
        $cToc->AddLinkForLastChild($this->cDoDate($c->iDtCreated()));
        
        $cContCr = $bUnsetCr = false;
        if($c->iCreator() !== $this->cCont->iID()){
          $cContCr = cCont::mUnserialize($this->cDb, $c->iCreator());
          $bUnsetCr = true;
        }
        else{$cContCr = $this->cCont;}
        
        $cToc->Child(new cElementBase(_('Created by'))
                   , ($cContCr === false ? new cElementBase(_('Unknown')) : $this->cDoContInfo($cContCr)));
                   
        if($c->iDtUpdated() > 0){
          $cToc->Child(new cElementBase(_('Updated')), $this->cDoTime($c->iDtUpdated()));
          $cToc->AddLinkForLastChild($this->cDoDate($c->iDtUpdated()));
          
          $cContUp = $bUnsetUp = false;
          
          if($c->iUpdater() != $cContCr->iID()){
            if($c->iUpdater() != $this->cCont->iID()){
              $cContUp = cCont::mUnserialize($this->cDb, $c->iUpdater());
              $bUnsetUp = true;
            }
            else{$cContUp = $this->cCont;}
          }
          else{$cContUp = $cContCr;}
          
          $cToc->Child(new cElementBase(_('Updated by'))
                     , ($cContUp === false ? new cElementBase(_('Unknown')) : $this->cDoContInfo($cContUp)));
          if($bUnsetUp){unset($cContUp);}
        }
        
        if($bUnsetCr){unset($cContCr);}
        
        //last login
        $cElementImg = new cElement('img', false);
        $cElementImg->Attribute('class', WTS_PFX . '-toc-image');
        if($c->bIsOnline($this->cDb, WTS_SESSION_TIMEOUT)){
          $cElementImg->Attribute('src', IMG_ENABLED);
          $cElementImg->Attribute('title', _('Online'));
        }
        else{
          $cElementImg->Attribute('src', IMG_DISABLED);
          $cElementImg->Attribute('title', _('Offline'));
        }
        $cToc->Child(new cElementBase(_('Last Login')), $cElementImg);
        
        if($iLastLogin = $c->mLastLogin($this->cDb)){
          $cToc->AddLinkForLastChild($this->cDoTime($iLastLogin));
          $cToc->AddLinkForLastChild($this->cDoDate($iLastLogin));
        }
        else{
          $cToc->AddLinkForLastChild(new cElementBase(_('Never')));
        }
        
        //group
        if($cGroup){
          $cToc->Child(new cElementBase(_('Group')), $this->cDoGroupInfo($cGroup));
          unset($cGroup);
        }
        
        //queue
        if($cQueue){
          $cElementInfo = false;
          if($this->cCont->iType() === cCont::ADMIN){
            $cElementInfo = new cElement('a', false);
            $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_QUEUE)
            . sSubAction(WTS_PARAM_ID, $cQueue->iID()));
            $cElementInfo->Content(new cElementBase(htmlentities($cQueue->sName())));
          }
          else{
            $cElementInfo = new cElementBase(htmlentities($cQueue->sName()));
          }
          
          $cToc->Child(new cElementBase(_('Queue')), $cElementInfo);
          unset($cQueue);
        }
        
        //contracts
        cContract::bAddFilterCont($c->iID());
        $iContrs = cContract::iCount($this->cDb);
        if($iContrs > 0){
          $cElementInfo = new cElement('a', false);
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CONTR)
          . sSubAction(WTS_PARAM_ACT_CONT, $c->iID()));
          $cElementInfo->Content(new cElementBase($iContrs));
          $cToc->Child(new cElementBase(_('Contracts')), $cElementInfo);
        }
        
        //products
        cProduct::bAddFilterCont($c->iID());
        $iProds = cProduct::iCount($this->cDb);
        if($iProds > 0){
          $cElementInfo = new cElement('a', false);
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_PRODUCT)
          . sSubAction(WTS_PARAM_ACT_CONT, $c->iID()));
          $cElementInfo->Content(new cElementBase($iProds));
          $cToc->Child(new cElementBase(_('Products')), $cElementInfo);
        }

        //open chains
        cChain::bAddFilterCont($c->iID());
        cChain::bAddFilterClosed(false);
        $iChains = cChain::iCount($this->cDb);
        if($iChains > 0){
          $cElementInfo = cSE::cCreate('a', $iChains, WTS_PFX . '-alert ' . WTS_PFX . '-info2');
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CHAIN)
          . sSubAction(WTS_PARAM_ACT_CONT, $c->iID())
          . sSubAction(WTS_PARAM_CLOSED, 'false'));
          $cToc->Child(new cElementBase(_('Open Chains')), $cElementInfo);
        }
        
        //closed chains
        cChain::ClearFilters();
        cChain::bAddFilterCont($c->iID());
        cChain::bAddFilterClosed(true);
        $iChains = cChain::iCount($this->cDb);
        if($iChains > 0){
          $cElementInfo = cSE::cCreate('a', $iChains, WTS_PFX . '-notify ' . WTS_PFX . '-info2');
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CHAIN)
          . sSubAction(WTS_PARAM_ACT_CONT, $c->iID())
          . sSubAction(WTS_PARAM_CLOSED, 'true'));
          $cToc->Child(new cElementBase(_('Closed Chains')), $cElementInfo);
        }
        
        $cElement->Content($cToc->cRoot());
        
        $cPage->Aside($cElement);
        
        //070. side bar merge
        if($bEdit === true && $c->iID() > 1){
          $cElement = new cElement('section', false);
          $cElement->Attribute('class', WTS_PFX . '-aside-section');
          
          $cElementCapt = new cElement('div', false);
          $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
          $cElementCapt->Content(new cElementBase(_('Merge Contact into Selected')));
          
          $cElement->Content($cElementCapt);
          
          $cElementForm = new cElement('form', false);
          $cElementForm->Attribute('action', $sHref
                                           . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_MERGE)
                                           . ($iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
                                           . ($sCurSearch === false ? '' : sSubAction($sCurSearch, urlencode($mCurSearch)))
                                           . sSubAction(WTS_PARAM_ID, $c->iID()));
          $cElementForm->Attribute('method', 'post');
          
          $cField = new cField();
          $cField->Label(cField::cElementLabel(_('Login'), WTS_PARAM_LOGIN));
          $cElementInp = cField::cElementInput(WTS_PARAM_LOGIN);
          $cElementInp->Attribute('placeholder', 'existing.contact@example.com');
          $cElementInp->Attribute('title', _('Login and all attributes of Contact will be lost, Chains, Contracts and Products will be reattached to the Selected Contact.'));
          $cField->Input($cElementInp);
          
          $cElementForm->Content($cField->cBuild());
          
          $cField = new cField(cField::RIGHT);
          $cField->Input(cField::cElementSubmit(_('Merge')));
          
          $cElementForm->Content($cField->cBuild());

          $cElement->Content($cElementForm);
          
          $cPage->Aside($cElement);
        }
      }
    }
  }


}
?>
