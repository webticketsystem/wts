<?php
/*
 * withdate.php (part of WTS) - trait for core classes tWithDate
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('collectionbase.php');

  /**
   * tWithDate - треит для добавления временной хар-ки
   * 
   * @var int FN_DT - название поля для временной хар-ки
   * 
   * ! не забывайте вызывать конструктор треита !
   */
  trait tWithDate{
    
    public function __construct(){
      $this->aMap[FN_DT]['val'] = 0;
      $this->aMap[FN_DT]['mod'] = false;
    }
    
    public function iDt(){return $this->aMap[FN_DT]['val'];}
    
    protected function Dt($iDt){
      $iDt = (int)$iDt;
      if($iDt > 0 && $this->aMap[FN_DT]['val'] !== $iDt){
        $this->aMap[FN_DT]['val'] = $iDt;
        $this->aMap[FN_DT]['mod'] = true;
      }
    }
  }


}
