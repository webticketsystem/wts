<?php
/*
 * attrview.php (part of WTS) - core class cAttrView
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('collectionbase.php');
  require_once('withid.php');
  require_once('withcolor.php');
  require_once('withstr.php');

  /**
   * cAttrView - вспомогательный класс для отображения атрибутов
   * 
   * !!!если iID > 0 && sValue == null - существующий атрибут будет удалён
   * !!!если iID == 0 && sValue not null - атрибут будет создан
   * 
   * @var const int iID
   * @var const int iType - берется из типа атрибута
   * @var const string sName
   * @var const string sColor
   * @var const string sValue
   */
  class cAttrView{
    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithColor{tWithColor::__construct as private __twc_construct;}
    use tWithStr;
    
    protected $aMap;

    public function iType(){return $this->aMap[FN_I_TYP]['val'];}
    public function sValue(){return $this->aMap[FN_S_VAL]['val'];}
    public function sName(){return $this->aMap[FN_S_NM]['val'];}
    public function Name($sName){$this->Str(FN_S_NM, $sName);}

    public function __construct($iID, $iType, $sValue = null){
      $this->__twi_construct($iID);
      $this->__twc_construct();

      $iType = (int)$iType;
      $this->aMap[FN_I_TYP]['val'] = ($iType > 0 ? $iType : 0);

      $this->aMap[FN_S_NM]['val'] = null;
      
      $this->aMap[FN_S_VAL]['val'] = null;
      if(isset($sValue)){$this->Str(FN_S_VAL, $sValue);}
    }
  }


}
