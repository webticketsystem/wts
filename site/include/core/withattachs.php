<?php
/*
 * withattachs.php (part of WTS) - trait for core classes tWithAttachs
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('utils/image.php');

  /**
   * tWithAttachs - attachments
   */
  trait tWithAttachs{
    
    protected $aAttachs;
    protected $bAttachsWereLoaded;//флаг, что восстанавливали список из базы
  /**
   * Проверяйте размер таблицы и создавайте новую при необходимости
   */
    public function bInstallAttach(cDb &$cDb, $sTable){
      $sTable = (string)$sTable;
      if(strlen($sTable) > 0){
        $aMap[FN_I_ID]['def']  = 'bigint unsigned not null';
        $aMap[FN_I_ID]['pk']   = true;
        $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null';
        $aMap[FN_I_PRT]['idx'] = FN_I_PRT;
        $aMap[FN_S_NM]['def']  = 'char(255) not null';
        $aMap[FN_S_CID]['def'] = 'char(255)';
        $aMap[FN_S_PRV]['def'] = 'blob';//max size 64 kb
        $aMap[FN_S_VAL]['def'] = 'mediumblob not null';//max size 16 mb
        
        $this->CreateTable($cDb, $aMap, $sTable, true);
        return true;
      }
      return false;
    }
    //call iUnserializeAttach in first
    public function aAttach(){
      //пока не сохранимся - ничего не вернём - это чревато копированием объектов в памяти...
      if(!$this->iID()){
        return array();
      }
      return $this->aAttachs;
    }
    //нельзя складывать файлы с одинаковым названием
    public function bAddAttach($sValue, $sName, $sCID = null){
      $sValue = (string)$sValue;
      $sName = (string)$sName;
      $bAdd = true;
      if(strlen($sValue) > 0){
        if(strlen($sName) < 3){//because strlen('1.f') < 4
          $bAdd = false;
        }
        else{
          $sName = preg_replace('/[^\d\w\s\-\_\.]/u','_',$sName);//не допускаем символов типа ", *, &...
          if(false === stripos($sName, '.')){$sName .= '.bin';}
        }
        if($bAdd && count($this->aAttachs) > 0){
          foreach($this->aAttachs as &$a){
            if($a[FN_S_NM] === $sName){
              $bAdd = false;
              break;
            }
          }
        }
        
        if($bAdd){
          if(isset($sCID)){$sCID = str_replace(array('<','>'), '', $sCID);}
          $this->aAttachs[] = array(FN_I_ID => 0, FN_S_NM => $sName, FN_S_CID => $sCID, FN_S_VAL => $sValue,);
          return true;
        }
      }
      return false;
    }
    
    public function __construct(){
      $this->aAttachs           = array();
      $this->bAttachsWereLoaded = false;
    }
    
    public function mUnserializeAttach(cDb &$cDb, $iID, $bPreview = false){
      $bPreview = (bool)$bPreview;
      $iID = (int)$iID;
      if($this->iID()
      && $this->bInAttach($iID)){
        return $cDb->sQueryRes('select ' . ($bPreview ? FN_S_PRV : FN_S_VAL)
        . ' from ' . $this->sAttachTable()
        . ' where ' . FN_I_ID . '=' . $iID);
      }
      return null;
    }
    //только заголовки - для доступа к "телу" @see sUnserializeAttach
    public function iUnserializeAttach(cDb &$cDb){
      $iCount = 0;
      if($this->iID()){
        if($this->bAttachsWereLoaded === true){
          $iCount = count($this->aAttachs);
        }
        else{
          $this->bAttachsWereLoaded = true;
          
          $cDb->QueryRes('select ' . FN_I_ID . ', ' . FN_S_NM . ', ' . FN_S_CID
          . ' from ' . $this->sAttachTable()
          . ' where ' . FN_I_PRT . '=' . $this->iID());
          if(($iCount = $cDb->iRowCount()) > 0){
            while($aRow = $cDb->aRow()){
              $this->aAttachs[] = array(FN_I_ID => (int)$aRow[0], FN_S_NM => $aRow[1], FN_S_CID => $aRow[2],);
            }
            $cDb->FreeResult();
          }
        }
      }
      return $iCount;
    }
    
    protected function iSerializeAttach(cDb &$cDb, $iThumbSide){
      $iCount = 0;
      $iThumbSide = (int)$iThumbSide;
      if($iThumbSide < 64){$iThumbSide = 64;}
      if($iThumbSide > 256){$iThumbSide = 256;}
      if(count($this->aAttachs) > 0){
        $i = 1 + (int)$cDb->sQueryRes('select max(' . FN_I_ID .') from ' . $this->sAttachTable());
        foreach($this->aAttachs as &$a){
          if($a[FN_I_ID] === 0){//только мы могли сложить в AddAttach...
            $aMap[FN_I_ID] = $a[FN_I_ID] = $i;
            $i++;
            $aMap[FN_I_PRT] = $this->iID();
            $aMap[FN_S_NM] = '\'' . $cDb->sShield($a[FN_S_NM]) . '\'';
            if($a[FN_S_CID] != null){
              $aMap[FN_S_CID] = '\'' . $cDb->sShield($a[FN_S_CID]) . '\'';
            }
            
            $sPreview = cImage::mCreateThumb($a[FN_S_NM], $iThumbSide, $a[FN_S_VAL]);

            if($sPreview !== false){
              $aMap[FN_S_PRV] = '\'' . $cDb->sShield($sPreview) . '\'';
              unset($sPreview);
            }
            
            $aMap[FN_S_VAL] = '\'' . $cDb->sShield($a[FN_S_VAL]) . '\'';
            unset($a[FN_S_VAL]);
            
            $this->Insert($cDb, $aMap, $this->sAttachTable());
            if(isset($aMap[FN_S_PRV])){unset($aMap[FN_S_PRV]);}
            unset($aMap[FN_S_VAL]);
            unset($aMap);
            $iCount++;
          }
        }
      }
      return $iCount;
    }
    
    protected function DeleteAttach(cDb &$cDb){
      $cDb->Query('delete from ' . $this->sAttachTable()
      . ' where ' . FN_I_PRT . '=' . $this->iID());
    }
    
    protected function bInAttach($iID){
      $iID = (int)$iID;
      $bRet = false;
      if(count($this->aAttachs) > 0 && $iID > 0){
        foreach($this->aAttachs as &$a){
          if($a[FN_I_ID] == $iID){
            $bRet = true;
            break;
          }
        }
      }
      return $bRet;
    }
  }


}
