<?php
/*
 * tag.php (part of WTS) - core class cTag
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('collectionbase.php');
  require_once('withcolor.php');
  require_once('withid.php');

  /**
   * cTag - класс тегов для очередей
   * 
   * теги, в отличие от регулярок общие для всех очередей - так как разрешаем агентам поиск по всем очередям
   * чтобы хитрый пользователь написавший один и тот же вопрос в разные отделы не заставлял делать двойную работу
   */
  class cTag extends cCollectionBase{
    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithColor{tWithColor::__construct as private __twc_construct;}
    use tWithStr;
    
    const TABLE = 'tags';
    
    public function Install(cDb &$cDb){
      $aMap[FN_I_ID]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']   = true;
      $aMap[FN_S_NM]['def']  = 'char(255) not null';
      $aMap[FN_S_NM]['uniq'] = true;
      $aMap[FN_S_SL]['def']  = 'char(255) not null';
      $aMap[FN_S_SL]['uniq'] = true;
      $aMap[FN_S_CLR]['def'] = 'char(6)';

      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
    }
    
    public function sName(){return $this->aMap[FN_S_NM]['val'];}
    public function Name($sName){
      $sName = (string)$sName;
      if(strlen($sName) > 0
        && preg_match('/^[d\w\s-_]+$/u', $sName)){
        if($this->aMap[FN_S_NM]['val'] != $sName){
          $this->aMap[FN_S_NM]['val'] = $sName;
          $this->aMap[FN_S_NM]['mod'] = true;
        }
      }
    }
    public function sSlug(){return $this->aMap[FN_S_SL]['val'];}
    public function Slug($sSlug){
      $sSlug = (string)$sSlug;
      if(strlen($sSlug) > 0
        && preg_match('/^[d\w\-]+$/', $sSlug)){
        if($this->aMap[FN_S_SL]['val'] != $sSlug){
          $this->aMap[FN_S_SL]['val'] = $sSlug;
          $this->aMap[FN_S_SL]['mod'] = true;
        }
      }
    }
    
    public function __construct(){
      $this->__twi_construct();
      
      $this->InitStr(FN_S_NM);
      $this->InitStr(FN_S_SL);

      $this->__twc_construct();
    }
    
    public static function iUnserialize(cDb &$cDb, &$aObjs){

      $s = 'select ' . FN_I_ID . ', '
            . FN_S_NM . ', '
            . FN_S_SL . ', '
            . FN_S_CLR
            . ' from ' . $cDb->sTablePrefix() . static::TABLE
            . ' order by ' . FN_S_NM;
        
      $cDb->QueryRes($s);

      $iCount = $cDb->iRowCount();
      if($iCount > 0){
        while($aRow = $cDb->aRow()){
          $cObj = new cTag();
          $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
          $cObj->aMap[FN_S_NM]['val']  = $aRow[1];
          $cObj->aMap[FN_S_SL]['val']  = $aRow[2];
          $cObj->aMap[FN_S_CLR]['val'] = $aRow[3];
          $aObjs[] = $cObj;
        }
        $cDb->FreeResult();
      }
      return $iCount;
    }
    
    public function bSerialize(cDb &$cDb){
      $bRet = false;
      if($this->iID()){//try to save modified
        $aMap;
        if($this->aMap[FN_S_NM]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{$aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';}
        }
        if($this->aMap[FN_S_CLR]['mod']){
          $aMap[FN_S_CLR] = ($this->sColor() ? '\'' . $cDb->sShield($this->sColor()) . '\'' : 'null');
        }
        
        if(isset($aMap)){
          $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
          $bRet = true;
        }
      }//$this->iID() != null
      else{//try to insert new
        if($this->aMap[FN_S_NM]['mod']
        && $this->aMap[FN_S_SL]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\' or '
          . FN_S_SL . '=\'' . $cDb->sShield($this->sSlug()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{
            $aMap[FN_I_ID] = $this->aMap[FN_I_ID]['val'] = 1 + (int)$cDb->sQueryRes('select max(' . FN_I_ID . ') from ' . $cDb->sTablePrefix() . static::TABLE);
            $aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';
            $aMap[FN_S_SL] = '\'' . $cDb->sShield($this->sSlug()) . '\'';
            if($this->aMap[FN_S_CLR]['mod']){
              $aMap[FN_S_CLR] = ($this->sColor() ? '\'' . $cDb->sShield($this->sColor()) . '\'' : 'null');
            }
            
            $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
            $bRet = true;
          }
        }
      }
      if($bRet){foreach($this->aMap as &$a){
        if(isset($a['mod']) && $a['mod'])$a['mod'] = false;}
      }
      return $bRet;
    }
    //!!! delete tag if not used in chains
    public function bDelete(cDb &$cDb){
      if($this->iID()){
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $this->iID());
        $this->aMap[FN_I_ID]['val'] = 0;
        return true;
      }
      return false;
    }
  }


}
