<?php
/*
 * chain.php (part of WTS) - core class cChain
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('collectionbase.php');
  require_once('withfilter.php');
  require_once('withid.php');
  require_once('withstr.php');
  require_once('withtag.php');
  require_once('withupdated.php');

  /**
   * cChain - цепочки событий
   */
  class cChain extends cCollectionBase{
    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithFilter;
    use tWithTag{tWithTag::__construct as private __twt_construct;}
    use tWithStr;
    use tWithUpdated{tWithUpdated::__construct as private __twu_construct;}
    
    const TABLE = 'chains';
    
    public function Install(cDb &$cDb){
      $aMap[FN_I_ID]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']   = true;
      $aMap[FN_I_ID]['ai']   = true;
      $aMap[FN_S_SBJ]['def'] = 'char(255)';
      $aMap[FN_S_PRV]['def'] = 'char(255)';
      $aMap[FN_I_CNT]['def'] = 'bigint unsigned'; //contact
      $aMap[FN_I_GRP]['def'] = 'bigint unsigned'; //group
      $aMap[FN_I_QUE]['def'] = 'bigint unsigned not null';//queue - same as responsible queue
      $aMap[FN_B_CLS]['def'] = 'bit not null default 0';//open|closed
      
      $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null';
      $aMap[FN_DT]['def']    = 'datetime not null';
      $aMap[FN_I_OWN]['def'] = 'bigint unsigned not null';//responsible
      $aMap[FN_DT_U]['def']  = 'datetime not null';

      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
      $cDb->Query('alter table ' . $cDb->sTablePrefix() . static::TABLE . ' auto_increment=6000');
      $this->InstallTag($cDb);
    }
    
    public function sSubject(){return $this->aMap[FN_S_SBJ]['val'];}
    public function Subject($sSubject){$this->Str(FN_S_SBJ, $sSubject);}
    public function sPreview(){return $this->aMap[FN_S_PRV]['val'];}
    public function Preview($sPreview){$this->Str(FN_S_PRV, $sPreview);}
    
    public function iCont(){return $this->aMap[FN_I_CNT]['val'];}
    public function iGroup(){return $this->aMap[FN_I_GRP]['val'];}
    
    public function Cont(cCont &$cCustomer){
      if(!$cCustomer->bDisabled()
      && $cCustomer->iID() > 1
      && $this->aMap[FN_I_CNT]['val'] != $cCustomer->iID()){
        $this->aMap[FN_I_CNT]['val'] = $cCustomer->iID();
        $this->aMap[FN_I_CNT]['mod'] = true;
        if($this->aMap[FN_I_GRP]['val'] != $cCustomer->iGroup()){
          //у нового контакта может группы и не быть...
          $this->aMap[FN_I_GRP]['val'] = $cCustomer->iGroup();
          $this->aMap[FN_I_GRP]['mod'] = true;
        }
      }
    }

    public function iQueue(){return $this->aMap[FN_I_QUE]['val'];}
    //responsible for chain
    public function Updater(cCont &$cAgent){
      if($cAgent->iType() === cCont::AGENT
      && $this->aMap[FN_I_OWN]['val'] != $cAgent->iID()){
        $this->aMap[FN_I_OWN]['val'] = $cAgent->iID();
        $this->aMap[FN_I_OWN]['mod'] = true;
        if($this->iCreator() < 1){
          $this->aMap[FN_I_PRT]['val'] = $cAgent->iID();
          $this->aMap[FN_I_PRT]['mod'] = true;
        }
        if($this->aMap[FN_I_QUE]['val'] != $cAgent->iQueue()){
          $this->aMap[FN_I_QUE]['val'] = $cAgent->iQueue();
          $this->aMap[FN_I_QUE]['mod'] = true;
        }
      }
    }
    
    public function bClosed(){return $this->aMap[FN_B_CLS]['val'];}
    public function Open(){
      if($this->aMap[FN_B_CLS]['val'] !== false){
        $this->aMap[FN_B_CLS]['val'] = false;
        $this->aMap[FN_B_CLS]['mod'] = true;
      }
    }
    public function Close(){
      if($this->aMap[FN_B_CLS]['val'] !== true){
        $this->aMap[FN_B_CLS]['mod'] = $this->aMap[FN_B_CLS]['val'] = true;
      }
    }

    public function __construct(){
      $this->__twi_construct();
      
      $this->__twt_construct();
      
      $this->InitStr(FN_S_SBJ);
      $this->InitStr(FN_S_PRV);
      $this->InitInt(FN_I_CNT);
      $this->InitInt(FN_I_GRP);
      $this->InitInt(FN_I_OWN);
      $this->InitInt(FN_I_QUE);
      
      $this->aMap[FN_B_CLS]['val'] = $this->aMap[FN_B_CLS]['mod'] = false;
      $this->__twu_construct();
    }
    
    public static function mUnserialize(cDb &$cDb, $iID){
      $cObj = false;
      $iID = (int)$iID;
      if($iID > 0){
         $s = 'select ' . FN_S_SBJ . ', '
                        . FN_S_PRV . ', '
                        . FN_I_CNT . ', '
                        . FN_I_GRP . ', '
                        . FN_I_QUE . ', '
                        . FN_B_CLS . ', '
                        . FN_I_PRT . ', UNIX_TIMESTAMP('
                        . FN_DT . '), '
                        . FN_I_OWN . ', UNIX_TIMESTAMP('
                        . FN_DT_U
        . ') from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $iID;
        $cDb->QueryRes($s);
        if($cDb->iRowCount() > 0){
          $aRow = $cDb->aRow();
          $cObj = new cChain();
          $cObj->aMap[FN_I_ID]['val']  = $iID;
          $cObj->aMap[FN_S_SBJ]['val'] = $aRow[0];
          $cObj->aMap[FN_S_PRV]['val'] = $aRow[1];
          $cObj->aMap[FN_I_CNT]['val'] = (int)$aRow[2];
          $cObj->aMap[FN_I_GRP]['val'] = (int)$aRow[3];
          $cObj->aMap[FN_I_QUE]['val'] = (int)$aRow[4];
          $cObj->aMap[FN_B_CLS]['val'] = (bool)$aRow[5];
          $cObj->aMap[FN_I_PRT]['val'] = (int)$aRow[6];
          $cObj->aMap[FN_DT]['val']    = (int)$aRow[7];
          $cObj->aMap[FN_I_OWN]['val'] = (int)$aRow[8];
          $cObj->aMap[FN_DT_U]['val']  = (int)$aRow[9];
          $cDb->FreeResult();
        }
      }
      return $cObj;
    }
    
    public static function iUnserialize(cDb &$cDb, $iPage, $iPerPage, &$aObjs){
      $iCount   = 0;
      $iPage    = (int)$iPage;
      $iPerPage = (int)$iPerPage;
      if($iPage > 0 && $iPerPage > 0){
        
        $s = 'select ' . FN_I_ID . ', '
                       . FN_S_SBJ . ', '
                       . FN_S_PRV . ', '
                       . FN_I_CNT . ', '
                       . FN_I_GRP . ', '
                       . FN_I_QUE . ', '
                       . FN_B_CLS . ', '
                       . FN_I_PRT . ', UNIX_TIMESTAMP('
                       . FN_DT . '), '
                       . FN_I_OWN . ', UNIX_TIMESTAMP('
                       . FN_DT_U
        . ') from ' . $cDb->sTablePrefix() . static::TABLE;
        if(static::$sWhere !== false){
          $s .= ' where ' . static::$sWhere;
        }
        $s .= ' order by ' . FN_DT . ' desc limit '
            . (($iPage-1) * $iPerPage) . ', ' . $iPerPage;
        
        $cDb->QueryRes($s);
        
        if(($iCount = $cDb->iRowCount()) > 0){
          while($aRow = $cDb->aRow()){
            $cObj = new cChain();
            $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
            $cObj->aMap[FN_S_SBJ]['val'] = $aRow[1];
            $cObj->aMap[FN_S_PRV]['val'] = $aRow[2];
            $cObj->aMap[FN_I_CNT]['val'] = (int)$aRow[3];
            $cObj->aMap[FN_I_GRP]['val'] = (int)$aRow[4];
            $cObj->aMap[FN_I_QUE]['val'] = (int)$aRow[5];
            $cObj->aMap[FN_B_CLS]['val'] = (bool)$aRow[6];
            $cObj->aMap[FN_I_PRT]['val'] = (int)$aRow[7];
            $cObj->aMap[FN_DT]['val']    = (int)$aRow[8];
            $cObj->aMap[FN_I_OWN]['val'] = (int)$aRow[9];
            $cObj->aMap[FN_DT_U]['val']  = (int)$aRow[10];
            $aObjs[] = $cObj;
          }
          $cDb->FreeResult();
        }
      }
      return $iCount;
    }
  /**
   * Восстанавливаем теги цепочек из базы для переданных объектов
   * @see cTagView
   */
    public static function iUnserializeTags(cDb &$cDb, &$aObjs, &$aTagView){
      $aTags;
      if(isset($aObjs)
      && count($aObjs) > 0
      && cTag::iUnserialize($cDb, $aTags) > 0){
        return self::iUnserializeTags_($cDb, $aTags, $aObjs, $aTagView);
      }
      else{return 0;}
    }
  /**
   * Обновление или создание цепочки в базе
   */
    public function bSerialize(cDb &$cDb, $bFootprint = true){
      $bRet = false;
      
      if($this->iID()){//try to save modified
        $aMap;
        if($this->aMap[FN_S_SBJ]['mod']){
          $aMap[FN_S_SBJ] = '\'' . $cDb->sShield($this->sSubject()) . '\'';
        }
        //можно назначить контакт - удалить нельзя
        if($this->aMap[FN_I_CNT]['mod']){$aMap[FN_I_CNT] = $this->iCont();}
        if($this->aMap[FN_I_GRP]['mod']){
          $aMap[FN_I_GRP] = ($this->iGroup() > 0 ? $this->iGroup() : 'null');
        }
        
        if($this->aMap[FN_I_QUE]['mod']){$aMap[FN_I_QUE] = $this->iQueue();}
        
        if($this->aMap[FN_B_CLS]['mod']){
          $aMap[FN_B_CLS] = ($this->bClosed() ? 'true' : 'false');
        }
        
        //ответственный
        if($this->aMap[FN_I_OWN]['mod']){$aMap[FN_I_OWN] = $this->iUpdater();}
        
        if(isset($aMap)){
          if($bFootprint){$aMap[FN_DT_U] = 'now()';}
          $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
          $bRet = true;
        }

      }//$this->iID() != null
      else{//try to insert new
        //тут не важно прикреплять ли начальное событие или нет
        //об этом должен беспокоится интерфейс - базе всё равно
        if($this->iUpdater()){
          
          if($this->aMap[FN_S_SBJ]['mod']){
            $aMap[FN_S_SBJ] = '\'' . $cDb->sShield($this->sSubject()) . '\'';
          }
          
          if($this->aMap[FN_S_PRV]['mod']){
            $aMap[FN_S_PRV] = '\'' . $cDb->sShield($this->sPreview()) . '\'';
          }

          if($this->iCont() > 0){$aMap[FN_I_CNT] = $this->iCont();}
          
          if($this->iGroup() > 0){$aMap[FN_I_GRP] = $this->iGroup();}
          
          $aMap[FN_I_QUE] = $this->iQueue();

          if($this->bClosed()){$aMap[FN_B_CLS] = 'true';}
          
          $aMap[FN_I_PRT] = $this->iCreator();
          $aMap[FN_I_OWN] = $this->iUpdater();
          $aMap[FN_DT] = $aMap[FN_DT_U] = 'now()';
          
          $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
          if($this->aMap[FN_I_ID]['val'] = (int)$cDb->mLastInsertID()){
            $bRet = true;
          }
        }
      }
      if($bRet){
        foreach($this->aMap as &$a){
          if(isset($a['mod']) && $a['mod'])$a['mod'] = false;
        }
      }
      return $bRet;
    }
  /**
   * Сохранение тегов в базе
   */
   public function bSerializeTag(cDb &$cDb, &$aSlugs, $bFootprint = true){
      $aTags;
      if($this->iID()
      && cTag::iUnserialize($cDb, $aTags) > 0
      && $this->bSerializeTag_($cDb, $aTags, $aSlugs)){
        if($bFootprint){
          $aMap;
          $aMap[FN_I_OWN] = $this->iUpdater();
          $aMap[FN_DT_U] = 'now()';
          $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
        }
        return true;
      }
      return false;
    }
    
    public static function bAddFilterOwner($iCont){
      $iCont = (int)$iCont;
      if(static::$sWhere !== false){static::$sWhere .= ' and ';}
      static::$sWhere .= FN_I_OWN . ($iCont > 0 ? '=' . $iCont : ' is null');
      return ($iCont > 0);
    }
    
    public static function bAddFilterQueue($iQueue){
      $iQueue = (int)$iQueue;
      if($iQueue > 1){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= FN_I_QUE . '=' . $iQueue;
        return true;
      }
      return false;
    }
    
    public static function bAddFilterCont($iCont){
      $iCont = (int)$iCont;
      if(static::$sWhere !== false){static::$sWhere .= ' and ';}
      static::$sWhere .= FN_I_CNT . ($iCont > 0 ? '=' . $iCont : ' is null');
      return ($iCont > 0);
    }
    
    public static function bAddFilterGroup($iGroup){
      $iGroup = (int)$iGroup;
      if(static::$sWhere !== false){static::$sWhere .= ' and ';}
      static::$sWhere .= FN_I_GRP . ($iGroup > 0 ? '=' . $iGroup : ' is null');
      return ($iGroup > 0);
    }
    
    public static function bAddFilterClosed($bClosed){
      $bClosed = (bool)$bClosed;
      if(static::$sWhere !== false){static::$sWhere .= ' and ';}
      static::$sWhere .= FN_B_CLS . '=' . ($bClosed ? 'true' : 'false');
      return true;
    }
    
    public static function bAddFilterTag(cDb &$cDb, &$aSlugs){
      $aTags;
      if(isset($aSlugs)
      && count($aSlugs) > 0
      && cTag::iUnserialize($cDb, $aTags) > 0){
        return static::bAddFilterTag_($cDb, $aTags, $aSlugs);
      }
      return false;
    }
    //full text search in subject chain in subject event in plain event
    public static function bAddFilterPartValue(cDb &$cDb, $sValue){
      $sValue = (string)$sValue;
      $iValue = (int)$sValue;
      
      if($sValue !== (string)$iValue){
        $iValue = 0;
      }
      
      if(strlen($sValue) > 0){
        $mChainIDs = cEvent::mFilterPartValue($cDb, $sValue);

        if(static::$sWhere !== false){static::$sWhere .= ' and ';}

        if($iValue > 0){// > 6000, но может и быть импорт
          static::$sWhere .= '(' . FN_I_ID . '=' . $iValue
          . ' or ' . FN_S_SBJ . ' like \'%' . $cDb->sShield($sValue) . '%\'';
          if($mChainIDs !== false){
            static::$sWhere .= ' or ' . FN_I_ID . ' in (' . $mChainIDs . ')';
          }
        }
        else{
          static::$sWhere .= '(' . FN_S_SBJ . ' like \'%' . $cDb->sShield($sValue) . '%\'';
          if($mChainIDs !== false){
            static::$sWhere .= ' or ' . FN_I_ID . ' in (' . $mChainIDs . ')';
          }
        }
        static::$sWhere .= ')';
        return true;
      }
      return false;
    }
  /**
   * fill array of [day, date, created, closed]
   * return max count of chains (closed or created) in day
   */
    public static function iGetStatistics(cDb &$cDb, $iQueue, $iDays, &$aStat){
      $iQueue = (int)$iQueue;
      $iDays  = (int)$iDays;
      if($iDays > 15){$iDays = 15;}
      
      $aCreated = false;
      $cDb->QueryRes('select day(' . FN_DT . '), count(*) from '
        . $cDb->sTablePrefix() . static::TABLE
        . ' where '
        . ($iQueue > 0 ? FN_I_QUE . '=' . $iQueue . ' and ' : '')
        . FN_DT . ' > date(now()) - interval '
        . $iDays . ' day group by day(' . FN_DT . ')');
      if($cDb->iRowCount() > 0){
        while($aRow = $cDb->aRow()){
          $aCreated[(int)$aRow[0]] = (int)$aRow[1];
        }
        $cDb->FreeResult();
      }
      
      $aClosed = false;
      $cDb->QueryRes('select day(' . FN_DT_U . '), count(*) from '
        . $cDb->sTablePrefix() . static::TABLE
        . ' where '
        . ($iQueue > 0 ? FN_I_QUE . '=' . $iQueue . ' and ' : '')
        . FN_DT_U . ' > date(now()) - interval '
        . $iDays . ' day and ' . FN_B_CLS . '=true group by day(' . FN_DT_U . ')');
      if($cDb->iRowCount() > 0){
        while($aRow = $cDb->aRow()){
          $aClosed[(int)$aRow[0]] = (int)$aRow[1];
        }
        $cDb->FreeResult();
      }

      $iDate = strtotime('now');
      $i1Day  = 86400;//24 * 60 * 60;
      $iMaxChains = 0;

      for($i = $iDate - ($iDays - 1) * $i1Day; $i <= $iDate; $i += $i1Day){
        $iCurDay = date('d', $i);
        $j = (int)$iCurDay;
        $iCreated = ($aCreated && isset($aCreated[$j]) ? $aCreated[$j] : 0);
        $iClosed  = ($aClosed && isset($aClosed[$j]) ? $aClosed[$j] : 0);
        
        if($iCreated > $iMaxChains){$iMaxChains = $iCreated;}
        if($iClosed  > $iMaxChains){$iMaxChains = $iClosed;}
        
        $aStat[] = array('day'     => $iCurDay
                             , 'tmstamp' => $i
                             , 'created' => $iCreated
                             , 'closed'  => $iClosed);
      }
      return $iMaxChains;
    }
    
    public static function MergeCont(cDb &$cDb, $iSrc, $iDst){
      $iSrc = (int)$iSrc;
      $iDst = (int)$iDst;
      if($iSrc > 0 && $iDst > 0){
        $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE
        . ' set ' . FN_I_CNT . '=' . $iDst . ' where ' . FN_I_CNT . '=' . $iSrc);
      }
    }
    
    public static function MergeGroup(cDb &$cDb, $iSrc, $iDst){
      $iSrc = (int)$iSrc;
      $iDst = (int)$iDst;
      if($iSrc > 0 && $iDst > 0){
        $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE
        . ' set ' . FN_I_GRP . '=' . $iDst . ' where ' . FN_I_GRP . '=' . $iSrc);
      }
    }

  }


}
