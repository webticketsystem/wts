<?php
/*
 * queue.php (part of WTS) - core class cQueue
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('collectionbase.php');
  require_once('withfilter.php');
  require_once('withstr.php');
  require_once('withemail.php');
  require_once('withid.php');

  /**
   * cQueue - очереди
   * 
   * !!!когда делаем очередь не действительной - обновляем 
   * всех её агентов >> queue_id=null и удаляем все их права (роли)
   * 
   * @var const int iID
   * @var string sName
   * @var string sLogin
   * @var string sPasswd
   * * * * * * * * * * *
   * @var bool bDisabled - if true : почту не отправляем и не принимаем 
   */

  class cQueue extends cCollectionBase{
    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithStr;
    use tWithEmail;
    use tWithFilter;

    const TABLE = 'queues';
    
    //{templates of responses
    const TABLE_RESP = self::TABLE . '_resps';
    
    const EML = 10;//outcoming_email
    const PCL = 20;//phone_call
    const MTG = 30;//meeting
    const FWD = 40;//forward_email
    const PWD = 50;//password
    
    public function mRespsUiName($iType){
      $iType = (int)$iType;
      $s = false;
      switch($iType){
        case self::EML: $s = _('Outcoming Emails');
        break;
        case self::PCL: $s = _('Phone Calls');
        break;
        case self::MTG: $s = _('Meetings');
        break;
        case self::FWD: $s = _('Forwarded Emails');
        break;
        case self::PWD: $s = _('Password Emails');
        break;
        default: break;
      }
      return $s;
    }
    
    protected function InstallResps(cDb &$cDb){
      $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null';
      $aMap[FN_I_PRT]['idx'] = FN_I_PRT;
      $aMap[FN_I_TYP]['def'] = 'int unsigned not null';
      $aMap[FN_I_RSP]['def'] = 'bigint unsigned not null';
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE_RESP);
    }
    
    public function iUnserializeResps(cDb &$cDb, &$aRespIDs){
      $iCount = 0;
      if($this->iID() > 0){
        if($this->iID() === 1){
          $aRespIDs[self::PWD] = 0;
        }
        else{
          $aRespIDs[self::EML] = 0;
          $aRespIDs[self::PCL] = 0;
          $aRespIDs[self::MTG] = 0;
          $aRespIDs[self::FWD] = 0;
        }

        $cDb->QueryRes('select ' . FN_I_TYP. ', ' . FN_I_RSP
              . ' from ' . $cDb->sTablePrefix() . static::TABLE_RESP
              . ' where ' . FN_I_PRT .  '=' . $this->iID() . ' group by ' . FN_I_TYP);

        $iCount = $cDb->iRowCount();
        if($iCount > 0){
          while($aRow = $cDb->aRow()){
            if(isset($aRespIDs[(int)$aRow[0]])){
              $aRespIDs[(int)$aRow[0]] = (int)$aRow[1];
            }
          }
          $cDb->FreeResult();
        }
      }
      return $iCount;
    }
    //create|delete|update
    public function bSerializeResp(cDb &$cDb, $iType, $iResponse){
      $bRet = false;
      $iType = (int)$iType;
      $iResponse = (int)$iResponse;
      if($this->iID() > 0){
      
        $bTrySerialize = false;
      
        if($this->iID() === 1){
          if($iType === self::PWD){$bTrySerialize = true;}
        }
        else{
          if($iType === self::EML
          || $iType === self::PCL
          || $iType === self::MTG
          || $iType === self::FWD){$bTrySerialize = true;}
        }
        
        if($bTrySerialize){
          if($iResponse > 0
          && ($cResponse = cResponse::mUnserialize($cDb, $iResponse))){
            $cDb->QueryRes('select ' . FN_I_RSP . ' from ' . $cDb->sTablePrefix() . static::TABLE_RESP
            . ' where ' . FN_I_PRT . '=' . $this->iID()
            . ' and '. FN_I_TYP . '=' . $iType) ;
            if($cDb->iRowCount() > 0){//update
              $aRow = $cDb->aRow();
              $cDb->FreeResult();
              if($cResponse->iID() != (int)$aRow[0]){
                $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE_RESP
                 . ' set ' . FN_I_RSP . '=' . $cResponse->iID()
                 . ' where ' . FN_I_PRT . '=' . $this->iID()
                 . ' and '. FN_I_TYP . '=' . $iType);
                 $bRet = true;
              }
            }
            else{
              $cDb->Query( 'insert into ' . $cDb->sTablePrefix() . static::TABLE_RESP . ' ('
                . FN_I_PRT . ', ' . FN_I_TYP . ', ' . FN_I_RSP . ') values ('
                . $this->iID() . ', ' . $iType . ', ' . $cResponse->iID() .')');
              $bRet = true;
            }
            unset($cResponse);
          }
          else{
            $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE_RESP
            . ' where ' . FN_I_PRT . '=' . $this->iID()
            . ' and '. FN_I_TYP . '=' . $iType);
            $bRet = true;
          }
        }
      }
      return $bRet;
    }
    
    public static function DeleteResp(cDb &$cDb, $iID){
      $iID = (int)$iID;
      if($iID > 0){
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE_RESP . ' where ' . FN_I_RSP . '=' . $iID);
      }
    }
    //}templates of responses

    const IMAP = 100;
    const SMTP = 110;
    
    const ENC_SSL = 210;
    const ENC_TLS = 220;
    
    public function Install(cDb &$cDb){
      
      $aMap[FN_I_ID]['def']   = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']    = true;
      $aMap[FN_I_ID]['ai']    = true;
      $aMap[FN_S_NM]['def']   = 'char(255) not null';
      $aMap[FN_S_NM]['uniq']  = true;
      $aMap[FN_S_LGN]['def']  = 'char(255) not null'; //this must be valid email address
      $aMap[FN_S_LGN]['uniq'] = true;
      $aMap[FN_S_PWD]['def']  = 'char(255) default null';
      
      $aMap[FN_S_IHST]['def'] = 'char(255) not null';
      $aMap[FN_S_ILGN]['def'] = 'char(255) not null';
      $aMap[FN_I_IENC]['def'] = 'int unsigned';
      $aMap[FN_I_IPRT]['def'] = 'int unsigned not null';
      $aMap[FN_S_SHST]['def'] = 'char(255) not null';
      $aMap[FN_S_SLGN]['def'] = 'char(255) not null';
      $aMap[FN_I_SENC]['def'] = 'int unsigned';
      $aMap[FN_I_SPRT]['def'] = 'int unsigned not null';
     
      $aMap[FN_B_DSB]['def']  = 'bit not null default 0';
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
      $this->InstallResps($cDb);
    }
    
    public function sName(){return $this->aMap[FN_S_NM]['val'];}
    public function Name($sName){$this->Str(FN_S_NM, $sName);}
    public function sLogin(){return $this->aMap[FN_S_LGN]['val'];}
    public function Login($sLogin){
      $this->Email(FN_S_LGN, $sLogin);
      if(!$this->iID() && $this->sLogin()){
        $sLogin = $this->sLogin();
        $this->Login_(self::IMAP, $sLogin);
        $this->Login_(self::SMTP, $sLogin);
        $aLogin = explode('@', $sLogin);
        $this->Name($aLogin[0]);
        $this->Host(self::IMAP, 'imap.' . $aLogin[1]);
        $this->Host(self::SMTP, 'smtp.' . $aLogin[1]);
      }
    }
    
    public function sPasswd(){return $this->aMap[FN_S_PWD]['val'];}
    public function Passwd($sPasswd){$this->Str(FN_S_PWD, $sPasswd);}
    
    public function sHost($iType){return (self::IMAP == $iType ? $this->aMap[FN_S_IHST]['val'] : $this->aMap[FN_S_SHST]['val']);}
    public function Host($iType, $sHost){
      if(self::IMAP == $iType){$this->Str(FN_S_IHST, $sHost);}
      else{$this->Str(FN_S_SHST, $sHost);}
    }
    public function sLogin_($iType){return (self::IMAP == $iType ? $this->aMap[FN_S_ILGN]['val'] : $this->aMap[FN_S_SLGN]['val']);}
    public function Login_($iType, $sLogin){
      if(self::IMAP == $iType){$this->Str(FN_S_ILGN, $sLogin);}
      else{$this->Str(FN_S_SLGN, $sLogin);}
    }
    public function iEncryption($iType){return (self::IMAP == $iType ? $this->aMap[FN_I_IENC]['val'] : $this->aMap[FN_I_SENC]['val']);}
    public function Encryption($iType, $iEncryption){
      $iType = (self::IMAP == $iType) ? FN_I_IENC : FN_I_SENC;
      $iEncryption = (int)$iEncryption;
      if(!$iEncryption == self::ENC_SSL || !$iEncryption == self::ENC_TLS){$iEncryption = 0;}
      if($this->aMap[$iType]['val'] != $iEncryption){
        $this->aMap[$iType]['val'] = $iEncryption;
        $this->aMap[$iType]['mod'] = true;
      }
    }
    public function iPort($iType){return (self::IMAP == $iType ? $this->aMap[FN_I_IPRT]['val'] : $this->aMap[FN_I_SPRT]['val']);}
    public function Port($iType, $iPort){
      $iType = (self::IMAP == $iType) ? FN_I_IPRT : FN_I_SPRT;
      $iPort = (int)$iPort;
      if(($iPort > 0)
      && $this->aMap[$iType]['val'] != $iPort){
        $this->aMap[$iType]['val'] = $iPort;
        $this->aMap[$iType]['mod'] = true;
      }
    }
    
    public function bDisabled(){return $this->aMap[FN_B_DSB]['val'];}
    
    public function Enable(){
      if($this->aMap[FN_B_DSB]['val'] !== false){
        $this->aMap[FN_B_DSB]['val'] = false;
        $this->aMap[FN_B_DSB]['mod'] = true;
      }
    }
    
    public function Disable(){
      if($this->aMap[FN_B_DSB]['val'] !== true){
        $this->aMap[FN_B_DSB]['mod'] = $this->aMap[FN_B_DSB]['val'] = true;
      }
    }
    
    public function __construct(){
      $this->__twi_construct();

      $this->InitStr(FN_S_NM);
      $this->InitStr(FN_S_LGN);
      $this->InitStr(FN_S_PWD);
      
      $this->InitStr(FN_S_IHST);
      $this->InitStr(FN_S_ILGN);
      $this->aMap[FN_I_IENC]['val'] = self::ENC_SSL;
      $this->aMap[FN_I_IENC]['mod'] = false;
      $this->aMap[FN_I_IPRT]['val'] = 993;
      $this->aMap[FN_I_IPRT]['mod'] = false;
      
      $this->InitStr(FN_S_SHST);
      $this->InitStr(FN_S_SLGN);
      $this->aMap[FN_I_SENC]['val'] = self::ENC_SSL;
      $this->aMap[FN_I_SENC]['mod'] = false;
      $this->aMap[FN_I_SPRT]['val'] = 465;
      $this->aMap[FN_I_SPRT]['mod'] = false;
      
      $this->aMap[FN_B_DSB]['val'] = $this->aMap[FN_B_DSB]['mod'] = false;
    }
    
    public static function mUnserialize(cDb &$cDb, $mVal){
      $cObj = $b = false;
      $s = 'select ' . FN_I_ID . ', '
                     . FN_S_NM . ', '
                     . FN_S_LGN . ', '
                     . FN_S_PWD . ', '
                     . FN_S_IHST . ', '
                     . FN_S_ILGN . ', '
                     . FN_I_IENC . ', '
                     . FN_I_IPRT . ', '
                     . FN_S_SHST . ', '
                     . FN_S_SLGN . ', '
                     . FN_I_SENC . ', '
                     . FN_I_SPRT . ', '
                     . FN_B_DSB
        . ' from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ';
      if(is_string($mVal) && strlen($mVal) > 0){
        $s .= FN_S_LGN . '=\'' . $cDb->sShield($mVal)
        . '\' or ' . FN_S_NM . '=\'' . $cDb->sShield($mVal) . '\'';
        $b = true;
      }
      else{
        $mVal = (int)$mVal;
        if($mVal > 0){
          $s .= FN_I_ID . '=' . $mVal;
          $b = true;
        }
      }

      if($b){
        $cDb->QueryRes($s);
        if($cDb->iRowCount() > 0){
          $aRow = $cDb->aRow();
          $cObj = new cQueue();
          $cObj->aMap[FN_I_ID]['val']   = (int)$aRow[0];
          $cObj->aMap[FN_S_NM]['val']   = $aRow[1];
          $cObj->aMap[FN_S_LGN]['val']  = $aRow[2];
          $cObj->aMap[FN_S_PWD]['val']  = $aRow[3];
          $cObj->aMap[FN_S_IHST]['val'] = $aRow[4];
          $cObj->aMap[FN_S_ILGN]['val'] = $aRow[5];
          $cObj->aMap[FN_I_IENC]['val'] = (int)$aRow[6];
          $cObj->aMap[FN_I_IPRT]['val'] = (int)$aRow[7];
          $cObj->aMap[FN_S_SHST]['val'] = $aRow[8];
          $cObj->aMap[FN_S_SLGN]['val'] = $aRow[9];
          $cObj->aMap[FN_I_SENC]['val'] = (int)$aRow[10];
          $cObj->aMap[FN_I_SPRT]['val'] = (int)$aRow[11];
          $cObj->aMap[FN_B_DSB]['val']  = (bool)$aRow[12];
          $cDb->FreeResult();
        }
      }
      return $cObj;
    }
    
    public static function iUnserialize(cDb &$cDb, &$aObjs){
      $iCount   = 0;
        
      $s = 'select ' . FN_I_ID . ', '
                     . FN_S_NM . ', '
                     . FN_S_LGN . ', '
                     . FN_S_PWD . ', '
                     . FN_S_IHST . ', '
                     . FN_S_ILGN . ', '
                     . FN_I_IENC . ', '
                     . FN_I_IPRT . ', '
                     . FN_S_SHST . ', '
                     . FN_S_SLGN . ', '
                     . FN_I_SENC . ', '
                     . FN_I_SPRT . ', '
                     . FN_B_DSB
      . ' from ' . $cDb->sTablePrefix() . static::TABLE;
      if(static::$sWhere !== false){
        $s .= ' where ' . static::$sWhere;
      }
      $s .= ' order by ' . FN_S_NM;
      
      $cDb->QueryRes($s);
      
      if(($iCount = $cDb->iRowCount()) > 0){
        while($aRow = $cDb->aRow()){
          $cObj = new cQueue();
          $cObj->aMap[FN_I_ID]['val']   = (int)$aRow[0];
          $cObj->aMap[FN_S_NM]['val']   = $aRow[1];
          $cObj->aMap[FN_S_LGN]['val']  = $aRow[2];
          $cObj->aMap[FN_S_PWD]['val']  = $aRow[3];
          $cObj->aMap[FN_S_IHST]['val'] = $aRow[4];
          $cObj->aMap[FN_S_ILGN]['val'] = $aRow[5];
          $cObj->aMap[FN_I_IENC]['val'] = (int)$aRow[6];
          $cObj->aMap[FN_I_IPRT]['val'] = (int)$aRow[7];
          $cObj->aMap[FN_S_SHST]['val'] = $aRow[8];
          $cObj->aMap[FN_S_SLGN]['val'] = $aRow[9];
          $cObj->aMap[FN_I_SENC]['val'] = (int)$aRow[10];
          $cObj->aMap[FN_I_SPRT]['val'] = (int)$aRow[11];
          $cObj->aMap[FN_B_DSB]['val']  = (bool)$aRow[12];
          $aObjs[] = $cObj;
        }
        $cDb->FreeResult();
      }

      return $iCount;
    }
    
    public function bSerialize(cDb &$cDb){
      $bRet = false;
    
      if($this->iID()){//try to save modified
        $aHist;
        $aMap;
        if($this->aMap[FN_S_NM]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{
            $aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';
          }
        }
        
        if($this->aMap[FN_S_LGN]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_LGN . '=\'' . $cDb->sShield($this->sLogin()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{
            $aMap[FN_S_LGN] = '\'' . $cDb->sShield($this->sLogin()) . '\'';
          }
        }
        
        if($this->aMap[FN_S_PWD]['mod']){
          $aMap[FN_S_PWD] = '\'' . $cDb->sShield($this->sPasswd()) . '\'';
        }
        if($this->aMap[FN_S_IHST]['mod']){
          $aMap[FN_S_IHST] = '\'' . $cDb->sShield($this->sHost(self::IMAP)) . '\'';
        }
        if($this->aMap[FN_S_ILGN]['mod']){
          $aMap[FN_S_ILGN] = '\'' . $cDb->sShield($this->sLogin_(self::IMAP)) . '\'';
        }
        if($this->aMap[FN_I_IENC]['mod']){
          $aMap[FN_I_IENC] = ($this->iEncryption(self::IMAP) ? $this->iEncryption(self::IMAP) : 'null');
        }
        if($this->aMap[FN_I_IPRT]['mod']){
          $aMap[FN_I_IPRT] = $this->iPort(self::IMAP);
        }
        if($this->aMap[FN_S_SHST]['mod']){
          $aMap[FN_S_SHST] = '\'' . $cDb->sShield($this->sHost(self::SMTP)) . '\'';
        }
        if($this->aMap[FN_S_SLGN]['mod']){
          $aMap[FN_S_SLGN] = '\'' . $cDb->sShield($this->sLogin_(self::SMTP)) . '\'';
        }
        if($this->aMap[FN_I_SENC]['mod']){
          $aMap[FN_I_SENC] = ($this->iEncryption(self::SMTP) ? $this->iEncryption(self::SMTP) : 'null');
        }
        if($this->aMap[FN_I_SPRT]['mod']){
          $aMap[FN_I_SPRT] = $this->iPort(self::SMTP);
        }
        
        if($this->aMap[FN_B_DSB]['mod']){
          $aMap[FN_B_DSB] = ($this->bDisabled() ? 'true' : 'false');
        }
        
        if(isset($aMap)){
          $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
          $bRet = true;
        }

      }//$this->iID() != null
      else{//try to insert new

        if($this->aMap[FN_S_LGN]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\' or '
          . FN_S_LGN . '=\'' . $cDb->sShield($this->sLogin()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{
            
            $aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';
            $aMap[FN_S_LGN] = '\'' . $cDb->sShield($this->sLogin()) . '\'';
            
            if($this->aMap[FN_S_PWD]['mod']){
              $aMap[FN_S_PWD] = '\'' . $cDb->sShield($this->sPasswd()) . '\'';
            }
           
            $aMap[FN_S_IHST] = '\'' . $cDb->sShield($this->sHost(self::IMAP)) . '\'';
            $aMap[FN_S_ILGN] = '\'' . $cDb->sShield($this->sLogin_(self::IMAP)) . '\'';
            $aMap[FN_I_IENC] = ($this->iEncryption(self::IMAP) ? $this->iEncryption(self::IMAP) : 'null');
            $aMap[FN_I_IPRT] = $this->iPort(self::IMAP);
            
            $aMap[FN_S_SHST] = '\'' . $cDb->sShield($this->sHost(self::SMTP)) . '\'';
            $aMap[FN_S_SLGN] = '\'' . $cDb->sShield($this->sLogin_(self::SMTP)) . '\'';
            $aMap[FN_I_SENC] = ($this->iEncryption(self::SMTP) ? $this->iEncryption(self::SMTP) : 'null');
            $aMap[FN_I_SPRT] = $this->iPort(self::SMTP);

            if($this->bDisabled()){$aMap[FN_B_DSB] = 'true';}
            
            $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
            if($this->aMap[FN_I_ID]['val'] = (int)$cDb->mLastInsertID()){
              $bRet = true;
            }
            
            //add templates
            //после создания можно выбрать существующие шаблоны, а можно создать новые и прикрепить их.
            //инсталятор создает шаблоны/приветсвия/подписи для очереди администратора и первой рабочей очереди
          }
        }
      }
      if($bRet){
        foreach($this->aMap as &$a){
          if(isset($a['mod']) && $a['mod'])$a['mod'] = false;
        }
      }
      return $bRet;
    }
  /**
   * Функции поиска очереди(ей)
   */
    public static function bAddFilterDisabled($bDisabled){
      $bDisabled = (bool)$bDisabled;
      if(static::$sWhere !== false){static::$sWhere .= ' and ';}
      static::$sWhere .= FN_B_DSB . '=' . ($bDisabled ? 'true' : 'false');
    }
    
    public static function bAddFilterPartValue(cDb &$cDb, $sValue){
      $sValue = (string)$sValue;
      if(strlen($sValue) > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= '(' . FN_S_NM . ' like \'%' . $cDb->sShield($sValue)
        . '%\' or ' . FN_S_LGN . ' like \'%' . $cDb->sShield($sValue) . '%\') and ' . FN_I_ID . '>1';
        return true;
      }
      return false;
    }
  }


}
