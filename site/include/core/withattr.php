<?php
/*
 * withattr.php (part of WTS) - trait for core classes tWithAttr
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('attrview.php');

  /**
   * tWithAttr - треит для работы с атрибутами
   * 
   */
  trait tWithAttr{
    
    protected static function sAttrTable(){return (static::TABLE . '_attrs');}
    
    protected function InstallAttr(cDb &$cDb){
      $aMap[FN_I_ID]['def']   = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']    = true;
      $aMap[FN_I_PRT]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_PRT]['idx']  = FN_I_PRT;
      $aMap[FN_I_TYP]['def'] = 'int unsigned not null';
      $aMap[FN_S_VAL]['def']  = 'char(255) not null';
      $aMap[FN_S_VAL]['idx']  = FN_S_VAL . '(10)';
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . $this->sAttrTable());
    }
    
    //use only if $this->iID() > 0
    protected function iUnserializeAttr_(cDb &$cDb, &$aAttrTypes, &$aAttrView){
      
      $s = 'select ' . FN_I_ID . ', ' . FN_S_VAL
            . ' from ' . $cDb->sTablePrefix() . $this->sAttrTable()
            . ' where ' . FN_I_PRT .  '=' . $this->iID();
      
      foreach($aAttrTypes as $key => &$cAttrType){
        $cDb->QueryRes($s . ' and ' . FN_I_TYP . '=' . $cAttrType->iID());
        $bAddField = true;
        $i = 1;
        if($cDb->iRowCount() > 0){
          while($aRow = $cDb->aRow()){
            $bAddField = $cAttrType->bMultiple();
            $cAttrView = new cAttrView($aRow[0], $cAttrType->iID(), $aRow[1]);
            $cAttrView->Name(($bAddField) ? $cAttrType->sName() . ' #' . $i : $cAttrType->sName());
            $cAttrView->Color($cAttrType->sColor());
            $aAttrView[] = $cAttrView;
            if(!$bAddField)break;//на случай, если в базе несколько simple
            $i++;
          }
          $cDb->FreeResult();
        }
        if($bAddField){
          $cAttrView = new cAttrView(0, $cAttrType->iID());
          $cAttrView->Name($cAttrType->bMultiple() ? $cAttrType->sName() . ' #' : $cAttrType->sName());
          $cAttrView->Color($cAttrType->sColor());
          $aAttrView[] = $cAttrView;
        }
        unset($aAttrTypes[$key]);
      }
      return (($aAttrView && is_array($aAttrView)) ? count($aAttrView) : 0);
    }
    
    //use only if $this->iID() > 0
    protected function bSerializeAttr_(cDb &$cDb, &$aAttrTypes, &$aAttrView){
      $bRet      = false;
      $aOldAttrs = false;
      $aNewAttrs = false;
      //достанем старые атрибуты
      $cDb->QueryRes('select ' . FN_I_ID . ', ' . FN_S_VAL
            . ' from ' . $cDb->sTablePrefix() . $this->sAttrTable()
            . ' where ' . FN_I_PRT . '=' . $this->iID());
      if($cDb->iRowCount() > 0){
        while($aRow = $cDb->aRow()){
          $aOldAttrs[] = array(
                                'check'  => false,
                                FN_I_ID  => (int)$aRow[0],
                                FN_S_VAL => $aRow[1],
                              );
        }
        $cDb->FreeResult();
      }
      //пройдёмся по новым атрибутам проверяя их на соответствие типам
      if($aAttrView){
        foreach($aAttrTypes as $key => &$cAttrType){
          foreach($aAttrView as &$cAttrView){
            if($cAttrType->iID() == $cAttrView->iType()){
              $aNewAttrs[] = array(
                                    FN_I_ID  => $cAttrView->iID(),
                                    FN_I_TYP => $cAttrView->iType(),
                                    FN_S_VAL => $cAttrView->sValue(),
                                  );
              if(!$cAttrType->bMultiple())break;//на случай, если хотят положить несколько simple
            }
          }
          unset($aAttrTypes[$key]);
        }
      }
      //правим старые, дбавляем новые
      if($aNewAttrs !== false){
        foreach($aNewAttrs as &$a){
          if($a[FN_I_ID] > 0 && $aOldAttrs !== false){//поищем в старых
            foreach($aOldAttrs as &$b){
              if($b[FN_I_ID] == $a[FN_I_ID] && strlen($a[FN_S_VAL]) > 0){//если значения нет - удалим этот атрибут
                if(($a[FN_S_VAL] != $b[FN_S_VAL])){//атрибут изменён
                  $cDb->Query('update ' . $cDb->sTablePrefix() . $this->sAttrTable() . ' set ' . FN_S_VAL . '=\'' . $cDb->sShield($a[FN_S_VAL]) . '\' where ' . FN_I_ID . '=' . $b[FN_I_ID]);
                  $bRet = true;
                }
                $b['check'] = true;
              }
            }
          }
          else{//тогда добавим
            if(strlen($a[FN_S_VAL]) > 0){
              $iID = 1 + (int)$cDb->sQueryRes('select max(' . FN_I_ID . ') from ' . $cDb->sTablePrefix() . $this->sAttrTable());
              $cDb->Query( 'insert into ' . $cDb->sTablePrefix() . $this->sAttrTable() . ' ('
                . FN_I_ID . ', ' . FN_I_PRT . ', ' . FN_I_TYP . ', ' . FN_S_VAL
                . ') values ('
                . $iID . ', '
                . $this->iID() . ', '
                . $a[FN_I_TYP] . ', \''
                . $cDb->sShield($a[FN_S_VAL]) .'\')');
              $bRet = true;
            }
          }
        }
      }
      //они больше не нужны или сложены без учёта типов
      if($aOldAttrs !== false){
        foreach($aOldAttrs as &$a){
          if($a['check'] === false){
            $cDb->Query('delete from ' . $cDb->sTablePrefix() . $this->sAttrTable() . ' where ' . FN_I_ID . '=' . $a[FN_I_ID]);
            $bRet = true;
          }
        }
      }
      return $bRet;
    }
  /**
   * For find parent by attributes
   * 
   * @param string sValue
   */
    protected static function AddFilterPartAttrValue(cDb &$cDb, $sValue){
      static::$sWhere .= FN_I_ID . ' in (select distinct '
      . FN_I_PRT . ' from ' . $cDb->sTablePrefix() . self::sAttrTable()
      . ' where ' . FN_S_VAL . ' like \'' . $cDb->sShield($sValue) . '%\')';
    }
    
  /**
   * Удаляет атрибуты, если Тип атрибута удалён
   * 
   * @param int iID - id of attribute type
   */
    public static function DeleteAttrByType(cDb &$cDb, $iID){
      $iID = (int)$iID;
      if($iID > 0){
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . self::sAttrTable() . ' where ' . FN_I_TYP . '=' . $iID);
      }
    }
  /**
   * For merge parent
   * 
   * удаляем атрибуты из другого контакта/группы ... или надо проверять simple/multiple
   * продукты мёрджить нельзя - они завязаны на контракты
   */
    protected function DeleteAttr(cDb &$cDb){
      $cDb->Query('delete from ' . $cDb->sTablePrefix() . $this->sAttrTable() . ' where ' . FN_I_PRT . '=' . $this->iID());
    }
  }


}
