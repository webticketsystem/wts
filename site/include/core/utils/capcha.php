<?php
/*
 * capcha.php (part of WTS) - create capcha image
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * used imagettftext (php GD) so need real path(s) to ttf fonts
 * 
 */

namespace wts{

  class cCapcha{
    protected $iW;
    protected $iH;
    protected $cImg;
    protected $sFont;
    protected $cWhite;
    protected $cBlack;
    
    public function __construct($iW = 210, $aPathsToFonts){
      $this->iW = (int)($iW < 100 ? 100 : $iW);
      $this->iH = (int)($this->iW/3);

      $this->cImg = imagecreatetruecolor($this->iW, $this->iH);
      imagealphablending($this->cImg, true);
      $this->cWhite = imagecolorallocate($this->cImg, 255, 255, 255);
      $this->cBlack = imagecolorallocate($this->cImg, 0, 0, 0);
      imagefilledrectangle($this->cImg, 0, 0, $this->iW-1, $this->iH-1, $this->cWhite);
      
      $aFonts = array();
      
      if(count($aPathsToFonts) > 0){
        foreach($aPathsToFonts as &$sPath){$aFonts[] = $sPath;}
      }

      $this->sFont = $aFonts[mt_rand(0, count($aFonts)-1)];
    }
    
    public function __destruct(){
      imagedestroy($this->cImg);
      unset($this->cImg);
    }
    
    public function sAddText(){
      $a = array('2', '3', '4', '5', '6', '7', '8', '9'
      , 'a', 'b', 'c', 'd', 'e', 'g', 'i', 'k', 'p', 'q', 's', 'v', 'x', 'y', 'z',);
      $iEnd = count($a) - 1;
      $iLength = mt_rand(4,6);
      $sStr = false;
      
      while(true){
        $sStr ='';
        for($i = 0; $i < $iLength; $i++){
          $sStr .= $a[mt_rand(0, $iEnd)];
        }
        if(!preg_match('/cp|cb|ck|c6|c9|db|qp|qb|dp|ww/', $sStr)) break;
      }
      
      $iSzFont = (int)($this->iH/2);
      
      $aSize = imagettfbbox($iSzFont ,0 , $this->sFont , 'O');
      
      $iFontWidth  = (int)(($aSize[2] - $aSize[0]) * 0.7);
      $iFontHeight = $aSize[1] - $aSize[5];
      
      $iX     = (int)(($this->iW - $iFontWidth * $iLength)/2);
      $iY     = (int)($this->iH - (($this->iH - $iFontHeight)/2));
      $iThrob = (int)($iFontHeight/6);
      
      for($i = 0; $i < $iLength; $i++) {
          imagettftext ($this->cImg 
          , $iSzFont                //размер в в типографских пунктах
          , 0                       //поворот текста против часовой стрелки в градусах
          , $iX
          , mt_rand($iY - $iThrob, $iY + $iThrob)
          , $this->cBlack
          , $this->sFont
          , substr($sStr, $i, 1));
          $iX += $iFontWidth;
      }
      
      return $sStr;
    }
    
    public function AddDistortion(){
      $cImg = imagecreatetruecolor($this->iW, $this->iH);
      imagefilledrectangle($cImg, 0, 0, $this->iW-1, $this->iH-1, $this->cWhite);
      
      // periods
      $n1 = 750000;
      $n2 = 1200000;
      $n3 = 31415926;
      $d = 10000000;
      $rand1=mt_rand($n1,$n2)/$d;
      $rand2=mt_rand($n1,$n2)/$d;
      $rand3=mt_rand($n1,$n2)/$d;
      $rand4=mt_rand($n1,$n2)/$d;
      // phases
      $rand5=mt_rand(0,$n3)/$d;
      $rand6=mt_rand(0,$n3)/$d;
      $rand7=mt_rand(0,$n3)/$d;
      $rand8=mt_rand(0,$n3)/$d;
      // amplitudes
      $rand9=mt_rand(330,420)/110;
      $rand10=mt_rand(330,450)/100;

      //wave distortion
      $color = $color_x = $color_y = $color_xy = 0;
      for($x = 0; $x < $this->iW; $x++){
        for($y = 0; $y < $this->iH; $y++){
          //координаты пикселя-первообраза
          $sx = $x + (sin($x * $rand1 + $rand5) + sin($y * $rand3 + $rand6)) * $rand9;
          $sy = $y + (sin($x * $rand2 + $rand7) + sin($y * $rand4 + $rand8)) * $rand10;

          if($sx < 0 || $sy < 0 || $sx >= $this->iW - 1 || $sy >= $this->iH - 1){
            continue;
          }
          else{
            $color    = imagecolorat($this->cImg, $sx,     $sy) & 0xFF;
            $color_x  = imagecolorat($this->cImg, $sx + 1, $sy) & 0xFF;
            $color_y  = imagecolorat($this->cImg, $sx,     $sy + 1) & 0xFF;
            $color_xy = imagecolorat($this->cImg, $sx + 1, $sy + 1) & 0xFF;
          }
          
          $newcolor = $color;
          //сглаживаем точки, цвета соседей которых отличается
          if($color != $color_x || $color != $color_y || $color != $color_xy){
            $frsx  = $sx - floor($sx);
            $frsy  = $sy - floor($sy);
            $frsx1 = 1 - $frsx;
            $frsy1 = 1 - $frsy;

            $newcolor = (
              $color    * $frsx1 * $frsy1 +
              $color_x  * $frsx  * $frsy1 +
              $color_y  * $frsx1 * $frsy +
              $color_xy * $frsx  * $frsy);
          }
          
          if($newcolor > 255){$newcolor = 255;}
          imagesetpixel($cImg, $x, $y, imagecolorallocate($cImg, $newcolor, $newcolor, $newcolor));
        }
      }
      imagecopy($this->cImg, $cImg, 0, 0, 0, 0, $this->iW, $this->iH);
      imagedestroy($cImg);
      unset($cImg);
    }
    
    public function AddNoise($iCount = 10){
      if($iCount > 100){$iCount = 100;}
      if($iCount > 1){
        if(function_exists('imageantialias')){
          imageantialias($this->cImg, true);
        }
        for($i = 0; $i < $iCount; $i++){
          $j = mt_rand(32, 255);
          $cColor = imagecolorallocate($this->cImg, $j, $j, $j);
          imageline($this->cImg
                  , mt_rand(1, $this->iW - 1)
                  , mt_rand(1, $this->iH - 1)
                  , mt_rand(1, $this->iW - 1)
                  , mt_rand(1, $this->iH - 1)
                  , $cColor);
        }
        
        for($i = 0; $i < $iCount / 2; $i++){
          $j = mt_rand(32, 255);
          $cColor = imagecolorallocate($this->cImg, $j, $j, $j);
          
          $iRad = (int)mt_rand(10, $this->iW/2);
          $iCenterX = (int)mt_rand(0, $this->iW);
          $iCenterY = (int)mt_rand(0, $this->iH);
          $fAngle = 2 * pi() / $iRad;
          
          for ($j = 0; $j < $iRad - 1; $j++){
            $fAngle1 = $fAngle * $j;
            $fAngle2 = $fAngle * ($j + 1);
            imageline($this->cImg
                  , $iCenterX + $iRad * cos($fAngle1)
                  , $iCenterY + $iRad * sin($fAngle1)
                  , $iCenterX + $iRad * cos($fAngle2)
                  , $iCenterY + $iRad * sin($fAngle2)
                  , $cColor);

          }
        }
      }
    }
    
    public function sGetPngImage(){
      ob_start();
      imagepng($this->cImg);
      return ob_get_clean();
    }
  }


}
?>
