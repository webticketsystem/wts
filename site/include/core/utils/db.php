<?php
/*
 * db.php (part of WTS) - class cDb for use with MySQL database
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * used mysqli
 * 
 * @see my.cnf
 * [mysqld]
 * default-storage-engine=MYISAM
 * character-set-server = utf8
 * collation-server = utf8_unicode_ci - for Russia
 * max_allowed_packet = 15M #must be more then WTS_MAX_PKG_SIZE
 */
 
namespace wts{

  define('WTS_DB_EXCEPTION_6001',           'Couldn\'t connect to database');
  define('WTS_DB_EXCEPTION_6002',           'Couldn\'t set utf8 charset for database');
  define('WTS_DB_EXCEPTION_6003',           'Couldn\'t exec query: ');
  define('WTS_DB_EXCEPTION_6004',           'Couldn\'t exec multi query: ');
  define('WTS_DB_EXCEPTION_6005',           'Couldn\'t extract query result: ');

  class cDb{
    
    protected $cConnection;
    protected $cResult;
    protected $sTablePrefix_;
    
    public function __construct($sDbName, $sUserLogin, $sUserPassword, $sTablePrefix){
      $aUserName = explode("@", $sUserLogin);
      $aUserName[0] = strtolower($aUserName[0]);
      
      $this->cConnection = new \mysqli('localhost', $aUserName[0], $sUserPassword, $sDbName);//@localhost добавится по умолчанию
      if($this->cConnection->connect_errno)throw new \Exception(WTS_DB_EXCEPTION_6001, 6001);
      if(!$this->cConnection->set_charset('utf8'))throw new \Exception(WTS_DB_EXCEPTION_6002, 6002);
      
      $sTablePrefix = (string)$sTablePrefix;
      if(strlen($sTablePrefix) > 0){
        $this->sTablePrefix_ = $sTablePrefix . '_';
      }
      else{$this->sTablePrefix_ = '';}

    }
    
    public function sTablePrefix(){return $this->sTablePrefix_;}
    
    public function mLastInsertID(){return $this->cConnection->insert_id;}
    
    public function __destruct(){
      //print_r($this->cResult);
      //$this->FreeResult();//если здесь вызывать - возникает ошибка
      $this->cConnection->close();
      unset($this->cConnection);
    }
    
    public function Query($sQuery){
      if(!$this->cConnection->query($sQuery)){throw new \Exception(WTS_DB_EXCEPTION_6003 . $sQuery . ' error: ' . $this->cConnection->error, 6003);}
    }
    
    public function MultiQuery($sQuery){
      if(!$this->cConnection->multi_query($sQuery)){throw new \Exception(WTS_DB_EXCEPTION_6004 . $sQuery . ' error: ' . $this->cConnection->error, 6004);}
      do{
        if($cResult = $this->cConnection->store_result()){$cResult->free();}
        if(!$this->cConnection->more_results()){break;}
      }while($this->cConnection->next_result());
    }
    
    public function QueryRes($sQuery){
      $this->cResult = $this->cConnection->query($sQuery);
      if($this->cConnection->error){throw new \Exception(WTS_DB_EXCEPTION_6005 . $sQuery . ' error: ' . $this->cConnection->error, 6005);}
    }
    
    public function sQueryRes($sQuery){
      $aRow;
      $this->QueryRes($sQuery);
      if($this->cResult->num_rows > 0){
        if($this->cResult->num_rows == 1){$aRow = $this->cResult->fetch_row();}
        $this->cResult->free();
      }
      if(isset($aRow[0])){return $aRow[0];}
      return null;
    }
    
    public function sShield($sQuery){
      return $sQuery = $this->cConnection->real_escape_string($sQuery);
    }
    
    public function aRow(){
      if(isset($this->cResult)) return $this->cResult->fetch_row();
      return null;
    }
    
    public function aHeader(){
      $aRow = false;
      if(isset($this->cResult)
      && $aCols = $this->cResult->fetch_fields()){
        foreach($aCols as &$c){$aRow[] = $c->name;}
      }
      return $aRow;
    }
    
    public function iRowCount(){
      if(isset($this->cResult))return $this->cResult->num_rows;
      return 0;
    }
    
    public function FreeResult(){
      if(isset($this->cResult)) $this->cResult->free();
    }
  }


}
?>
