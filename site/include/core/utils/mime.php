<?php
/*
 * mime.php (part of WTS) - help class cMIME
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  class cMIME{
    
    public static function sFileExt($sMimeSubType){
      
      $aExts = array(
        'plain'                 => 'txt',
        'html'                  => 'html',
        'css'                   => 'css',
        'javascript'            => 'js',
        'json'                  => 'json',
        'xml'                   => 'xml',
        'x-shockwave-flash'     => 'swf',
        'x-flv'                 => 'flv',
         // images
        'png'                   => 'png',
        'jpeg'                  => 'jpg',
        'gif'                   => 'gif',
        'bmp'                   => 'bmp',
        'vnd.microsoft.icon'    => 'ico',
        'tiff'                  => 'tif',
        'svg+xml'               => 'svg',
        // archives
        'zip'                   => 'zip',
        'x-rar-compressed'      => 'rar',
        'x-msdownload'          => 'msi',
        'vnd.ms-cab-compressed' => 'cab',
        'x-7z-compressed'       => '7z',
        // audio/video
        'mpeg'                  => 'mp3',
        'quicktime'             => 'mov',
        'x-msvideo'             => 'avi',
        // adobe
        'pdf'                   => 'pdf',
        'vnd.adobe.photoshop'   => 'psd',
        'postscript'            => 'ps',
        // ms office
        'msword'                => 'docx',
        'rtf'                   => 'rtf',
        'vnd.ms-excel'          => 'xlsx',
        'vnd.ms-powerpoint'     => 'pptx',
        // open office
        'vnd.oasis.opendocument.text'        => 'odt',
        'vnd.oasis.opendocument.spreadsheet' => 'ods',
      );
      $sMimeSubType = strtolower($sMimeSubType);
      if(array_key_exists($sMimeSubType, $aExts)){
        return $aExts[$sMimeSubType];
      }
      return 'bin';
    }
    
    public static function sMimeType($sFileName){
      
      $aMimeTypes = array(
        'txt'  => 'text/plain',
        'htm'  => 'text/html',
        'html' => 'text/html',
        'php'  => 'text/html',
        'css'  => 'text/css',
        'js'   => 'application/javascript',
        'json' => 'application/json',
        'xml'  => 'application/xml',
        'swf'  => 'application/x-shockwave-flash',
        'flv'  => 'video/x-flv',
        // images
        'png'  => 'image/png',
        'jpe'  => 'image/jpeg',
        'jpeg' => 'image/jpeg',
        'jpg'  => 'image/jpeg',
        'gif'  => 'image/gif',
        'bmp'  => 'image/bmp',
        'ico'  => 'image/vnd.microsoft.icon',
        'tiff' => 'image/tiff',
        'tif'  => 'image/tiff',
        'svg'  => 'image/svg+xml',
        'svgz' => 'image/svg+xml',
        // archives
        'zip'  => 'application/zip',
        'rar'  => 'application/x-rar-compressed',
        'exe'  => 'application/x-msdownload',
        'msi'  => 'application/x-msdownload',
        'cab'  => 'application/vnd.ms-cab-compressed',
        '7z'   => 'application/x-7z-compressed',
        // audio/video
        'mp3'  => 'audio/mpeg',
        'qt'   => 'video/quicktime',
        'mov'  => 'video/quicktime',
        'avi'  => 'video/x-msvideo',
        // adobe
        'pdf'  => 'application/pdf',
        'psd'  => 'image/vnd.adobe.photoshop',
        'ai'   => 'application/postscript',
        'eps'  => 'application/postscript',
        'ps'   => 'application/postscript',
        // ms office
        'doc'  => 'application/msword',
        'rtf'  => 'application/rtf',
        'xls'  => 'application/vnd.ms-excel',
        'ppt'  => 'application/vnd.ms-powerpoint',
        'docx' => 'application/msword',
        'xlsx' => 'application/vnd.ms-excel',
        'pptx' => 'application/vnd.ms-powerpoint',
        // open office
        'odt'  => 'application/vnd.oasis.opendocument.text',
        'ods'  => 'application/vnd.oasis.opendocument.spreadsheet',
      );
      $aExt = explode('.', $sFileName);
      $sExt = strtolower(end($aExt));

      if(array_key_exists($sExt, $aMimeTypes)){
        return $aMimeTypes[$sExt];
      }
      return 'application/octet-stream';
    }
    
    public static function sDecodeMimeHeaderStr($sStr, $sEnc){
      $s = '';
      if($a = imap_mime_header_decode($sStr)){
        for($i = 0; $i < count($a); $i++){
          if(isset($a[$i]->charset)
          && strlen($a[$i]->charset) > 2
          && $a[$i]->charset != 'default'){
            $s .= mb_convert_encoding($a[$i]->text, $sEnc, $a[$i]->charset);
          }
          else{$s .= $a[$i]->text;}
        }
      }
      return $s;
    }
  }

}
?>
