<?php
/*
 * string.php (part of WTS) - help class cString
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  class cString{
    
    const TAB = '  ';
    const EOL = PHP_EOL;
    /**
     * clear string
     * 
     * if null given return empty string
     */
    public static function sClear($sStr){
      //trim() returns an empty string when the argument is an unset/null variable
      $sStr = trim($sStr);
      if(strlen($sStr) > 0){$sStr = str_replace(array("\t", "\n", "\r", "\0", "\x0B", "\xC2\xA0"), ' ', $sStr);}
      //if(strlen($sStr) > 0){$sStr = preg_replace('/\s{2,}/', ' ', $sStr);}
      if(strlen($sStr) > 0){$sStr = preg_replace('/\s\s+/', ' ',$sStr);}
      return $sStr;
    }
    
    /**
     * check if it is email
     */
    public static function bIsEmail($sStr){
      return (isset($sStr) && preg_match('/^[\w\.\-]+@[\w\.\-]+\.[a-zA-Z]{2,8}$/', $sStr));
    }

    /**
     * create random string - use for create password
     */
    public static function sRand($iLenght){
      //str_shuffle()?
      $aUChars = range('A','Z');
      $aDigits = range('0','9');
      $aLChars = range('a','z');
      $a = array_merge($aUChars, $aDigits, $aLChars);
      $iEnd = count($a) - 1;
      
      //shuffle($a);
      //echo implode($a) . self::EOL;
      $sWord = '';
      for($i = 0; $i < $iLenght; $i++){$sWord .= $a[mt_rand(0,$iEnd)];}
      return $sWord;
    }

    /**
     * use for verify db settings 
     */
    public static function iStringToBytes($sValue){
      $sValue = trim($sValue);
      switch(strtolower($sValue[strlen($sValue)-1])){
        case 'g':
          $sValue *= 1024;
        case 'm':
          $sValue *= 1024;
        case 'k':
          $sValue *= 1024;
          break;
        default:
          break;
      }
      return $sValue;
    }
    /**
     * use for verify image size in upload
     */
    public static function sBytesToString($iBytes){
      $iBytes = (int)$iBytes;
      $sBytes = '0 B';
      if($iBytes > 0){
        $iConvention = 1000; //[1000->10^x|1024->2^x]
        $a = array('B', 'kB', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb');
        $e = floor(log($iBytes, $iConvention));
        $sBytes = round($iBytes/pow($iConvention, $e), 2) . ' ' . $a[$e];
      }
      return $sBytes;
    }
    /**
     * use for show chain info
     * @param iDiff - time in sec
     */
    public static function sTimediffToString($iDiff){
      $iDiff = (int)$iDiff;
      $ret = false;
      if($iDiff > 0){
      
        $bit = array(
          _('y') => $iDiff / 31556926 % 12,
          _('w') => $iDiff / 604800 % 52,
          _('d') => $iDiff / 86400 % 7,
          _('h') => $iDiff / 3600 % 24,
          _('m') => $iDiff / 60 % 60,
          _('s') => $iDiff % 60
        );

        foreach($bit as $k => $v){
          if($v > 0)$ret[] = $v . $k;
        }
      }
        
      return ($ret === false ? _('None') : join(' ', $ret));
    }
    
    public static function sRandColorPart(){
      return str_pad(dechex(mt_rand( 0, 255 )), 2, '0', STR_PAD_LEFT);
    }
  }

}
?>
