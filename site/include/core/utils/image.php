<?php
/*
 * image.php (part of WTS) - help class cImage
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  class cImage{

    /**
     * create thumbnail (preview) for image
     * 
     * if image dimension less then iSide image do not stratched
     */
    public static function sThumb(&$cSourceImage, $iSide = 128){
      $sThumb = false;
      if($cSourceImage){
        $iWidth = imagesx($cSourceImage);
        $iHeight = imagesy($cSourceImage);
        $dRatio = ($iWidth > $iHeight) ? $iWidth : $iHeight;
        $dRatio = ($iSide / $dRatio);
        if($dRatio > 1.0){$dRatio = 1.0;}
        $iW = (int)ceil($iWidth * $dRatio);
        $iH = (int)ceil($iHeight * $dRatio);
        if($iW > $iSide){$iW = $iSide;}
        if($iH > $iSide){$iH = $iSide;}
        $iX = $iY = 0;
        if($iH != $iSide){$iY = (int)(($iSide - $iH) / 2);}
        if($iW != $iSide){$iX = (int)(($iSide - $iW) / 2);}

        $cVirtualImage = imagecreatetruecolor($iSide, $iSide);

        imagealphablending($cVirtualImage, false);
        $cColor = imagecolorallocatealpha($cVirtualImage, 0, 0, 0, 127);
        imagefill($cVirtualImage, 0, 0, $cColor);
        imagesavealpha($cVirtualImage, true);

        //copy source image at a resized size
        imagecopyresampled($cVirtualImage, $cSourceImage, $iX, $iY, 0, 0, $iW, $iH, $iWidth, $iHeight);

        ob_start();
        imagepng($cVirtualImage);
        $sThumb = ob_get_clean();
        
        imagedestroy($cVirtualImage);
        unset($cVirtualImage);
      }
      return $sThumb;
    }
    
    /**
     * create image from BMP file
     * 
     * @see http://php.net/manual/ru/function.imagecreatefromstring.php
     */
    public static function imagecreatefrombmp(&$sSrc){
      //
      $iDataLenght = strlen($sSrc);
      
      //image format
      $FILE = unpack("vfile_type/Vfile_size/Vreserved/Vbitmap_offset", substr($sSrc, 0, 14));
      if ($FILE['file_type'] != 19778) return false;
      
      //image header
      $BMP = unpack('Vheader_size/Vwidth/Vheight/vplanes/vbits_per_pixel'
             . '/Vcompression/Vsize_bitmap/Vhoriz_resolution'
             . '/Vvert_resolution/Vcolors_used/Vcolors_important', substr($sSrc, 14, 40));
      
      //DDoS protection
      if($BMP['width'] > 16384){$BMP['width'] = 16384;}
      if($BMP['height'] > 16384){$BMP['height'] = 16384;}
      
      $BMP['colors'] = pow(2,$BMP['bits_per_pixel']);
      
      if($BMP['colors_used'] > 0){$BMP['palette_size'] = $BMP['colors_used'];}
      else{$BMP['palette_size'] = $BMP['colors'];}

      if($BMP['size_bitmap'] == 0){
        $BMP['size_bitmap'] = $FILE['file_size'] - $FILE['bitmap_offset'];
      }
     
      $BMP['bytes_per_pixel'] = $BMP['bits_per_pixel']/8;
      $BMP['bytes_per_pixel2'] = ceil($BMP['bytes_per_pixel']);
      $BMP['decal'] = ($BMP['width']*$BMP['bytes_per_pixel']/4);
      $BMP['decal'] -= floor($BMP['width']*$BMP['bytes_per_pixel']/4);
      $BMP['decal'] = 4 - (4 * $BMP['decal']);
      if($BMP['decal'] == 4){$BMP['decal'] = 0;}
      
      //image palette
      $iPos = 14 + 40;
      $PALETTE = array();
      if($BMP['colors'] < 16777216){
        $PALETTE = unpack('V'.$BMP['colors'], substr($sSrc, $iPos, $BMP['colors'] * 4));
        $iPos += $BMP['colors'] * 4;
      }
      
      //image canvas to draw on
      $VIDE = chr(0);
      $res = imagecreatetruecolor($BMP['width'], $BMP['height']);
      
      $dPY = $BMP['decal'];
      $width = $BMP['width'];
      $Y = $BMP['height'] - 1;

      //begin draw
      if($BMP['bits_per_pixel'] == 32){

        while ($Y >= 0){
          $X = 0;
          $s = substr($sSrc, $iPos, $width * 4);
          $iPos += $width * 4;
          while($X < $width){
              $COLOR = unpack("C4", substr($s, $X * 4, 4));
              imagesetpixel($res, $X, $Y, ($COLOR[3]<<16) | ($COLOR[2]<<8) | ($COLOR[1]));
              $X++;
          }
          unset($s);
          $Y--;
          if($dPY > 0){$iPos += $dPY;}
          if($iPos > $iDataLenght)break;
        }
      }
      elseif($BMP['bits_per_pixel'] == 24){

        while ($Y >= 0){
          $X = 0;
          $s = substr($sSrc, $iPos, $width * 3);
          $iPos += $width * 3;
          while($X < $width){
            $COLOR = unpack("V", substr($s, $X * 3, 3) . $VIDE);
            imagesetpixel($res, $X, $Y, $COLOR[1]);
            $X++;
          }
          unset($s);
          $Y--;
          if($dPY > 0){$iPos += $dPY;}
          if($iPos > $iDataLenght)break;
        }
      }
      elseif($BMP['bits_per_pixel'] == 16 && $BMP['compression'] == 0){

        $iPos = $FILE['bitmap_offset'];

        while ($Y >= 0){
          $X = 0;
          $s = substr($sSrc, $iPos, $width * 2);
          $iPos += $width * 2;
          while($X < $width){
            $COLOR = unpack("C2", substr($s, $X * 2, 2));
            $R = ($COLOR[2] >> 2)  & 0x1f;
            $G = (($COLOR[2] & 0x03) << 3) | ($COLOR[1] >> 5);
            $B = $COLOR[1] & 0x1f;
            imagesetpixel($res, $X, $Y, (($R*8)<<16) | (($G*8)<<8) | ($B*8));
            $X++;
          }
          unset($s);
          $Y--;
          if($dPY > 0){$iPos += $dPY;}
          if($iPos > $iDataLenght)break;
        }
      }
      elseif($BMP['bits_per_pixel'] == 16){
        
        $iPos = $FILE['bitmap_offset'];

        while ($Y >= 0){
          $X = 0;
          $s = substr($sSrc, $iPos, $width * 2);
          $iPos += $width * 2;
          while($X < $width){
            $COLOR = unpack("C2", substr($s, $X * 2, 2));
            $R = $COLOR[2] >> 3;
            $G = ($COLOR[2] & 0x07) << 3 | ($COLOR[1] >> 5);
            $B = $COLOR[1] & 0x1f;
            imagesetpixel($res, $X, $Y, (($R*8)<<16) | (($G*4)<<8) | ($B*8));
            $X++;
          }
          unset($s);
          $Y--;
          if($dPY > 0){$iPos += $dPY;}
          if($iPos > $iDataLenght)break;
        }
      }
      elseif($BMP['bits_per_pixel'] == 8){
        
        $iPos = $FILE['bitmap_offset'];

        while ($Y >= 0){
          $X = 0;
          $s = substr($sSrc, $iPos, $width);
          $iPos += $width;
          while($X < $width){
            $COLOR = unpack("n", $VIDE . $s[$X]);
            imagesetpixel($res, $X, $Y, $PALETTE[$COLOR[1]+1]);
            $X++;
          }
          unset($s);
          $Y--;
          if($dPY > 0){$iPos += $dPY;}
          if($iPos > $iDataLenght)break;
        }
      }
      elseif($BMP['bits_per_pixel'] == 4){
        
        $s = substr($sSrc, $iPos, $BMP['size_bitmap']);
        $iDataLenght = strlen($s);
        $iPos = 0;

        while ($Y >= 0){
          $X = 0;
          $COLORS = unpack("H*", substr($s, floor($iPos), floor($iPos)+$BMP['width']*$BMP['bytes_per_pixel']));
          while ($X < $BMP['width']){
              $C = hexdec($COLORS[1][$X]);
              imagesetpixel($res, $X, $Y, $PALETTE[$C+1]);
              $X++;
              $iPos += $BMP['bytes_per_pixel'];
          }
          $Y--;
          if($dPY > 0){$iPos += $dPY;}
          if($iPos > $iDataLenght)break;
        }
        unset($s);
      }
      elseif($BMP['bits_per_pixel'] == 1){
        
        $COLORS = unpack("H*", substr($sSrc, $iPos, $BMP['size_bitmap']));
        $iPos = 0;

        while ($Y >= 0){
          $i = (int)floor($iPos)*2;
          $X = 0;
          while ($X < $BMP['width'])
          {
            $C = hexdec($COLORS[1][$i]);
            imagesetpixel($res, $X, $Y, $PALETTE[$C & 8? 2: 1]);
            $X++;
            $iPos += $BMP['bytes_per_pixel'];
            if ($X < $BMP['width']){
              imagesetpixel($res, $X, $Y, $PALETTE[$C & 4? 2: 1]);
              $X++;
              $iPos += $BMP['bytes_per_pixel'];
              if ($X < $BMP['width']){
                imagesetpixel($res, $X, $Y, $PALETTE[$C & 2? 2: 1]);
                $X++;
                $iPos += $BMP['bytes_per_pixel'];
                if ($X < $BMP['width']){
                  imagesetpixel($res, $X, $Y, $PALETTE[$C & 1? 2: 1]);
                  $X++;
                  $iPos += $BMP['bytes_per_pixel'];
                }
              }
            }
            $i++;
          }
          $Y--;
          if($dPY > 0){$iPos += $dPY;}
        }
      }
      else{
        if(is_resource($res)){imagedestroy($res);}
        $res = false;
      }
      
      return $res;
    }
    
    static public function mImageFromFile($sFileName){
      $cSourceImage = false;
      if($cStream = fopen(dirname(__FILE__) . '/thumbs/' . $sFileName, 'r')){
        if($sStr = stream_get_contents($cStream)){
          $cSourceImage = imagecreatefromstring($sStr);
          unset($sStr);
        }
        fclose($cStream);
      }
      return $cSourceImage;
    }
    
    static public function mCreateThumb($sFileName, $iThumbSide, &$sImage){
      $sPreview = false;

      //preview
      $sExt = strrchr(strtolower($sFileName), '.');
      
      if($sExt){
        switch ($sExt){
          case '.jpeg':
          case '.png':
          case '.gif':
            $sExt = '.jpg';
            break;
          case '.docx':
            $sExt = '.doc';
            break;
          case '.xlsx':
            $sExt = '.xls';
            break;
          case '.pptx':
            $sExt = '.ppt';
            break;
          case '.fdb':
          case '.mdb':
          case '.mdbx':
          case '.odb':
          case '.dbf':
          case '.sql':
            $sExt = '.db';
            break;
          case '.htm':
          case '.html':
          case '.mht':
            $sExt = '.xml';
            break;
          case '.rar':
          case '.7z':
          case '.tar':
            $sExt = '.zip';
            break;
          default: break;
        }
      }
      else{$sExt = '.bin';}
      
      $cSourceImage = false;
      switch ($sExt){
        case '.jpg':
          $cSourceImage = imagecreatefromstring($sImage);
          break;
        case '.bmp':
          $cSourceImage = static::imagecreatefrombmp($sImage);
          break;
        case '.eml':
          $cSourceImage = static::mImageFromFile('file_eml.png');
          break;
        case '.doc':
          $cSourceImage = static::mImageFromFile('file_doc.png');
          break;
        case '.xls':
          $cSourceImage = static::mImageFromFile('file_xls.png');
          break;
        case '.ppt':
          $cSourceImage = static::mImageFromFile('file_ppt.png');
          break;
        case '.pdf':
          $cSourceImage = static::mImageFromFile('file_pdf.png');
          break;
        case '.db':
          $cSourceImage = static::mImageFromFile('file_db.png');
          break;
        case '.xml':
          $cSourceImage = static::mImageFromFile('file_xml.png');
          break;
        case '.zip':
          $cSourceImage = static::mImageFromFile('file_zip.png');
          break;
        default:
          $cSourceImage = static::mImageFromFile('file.png');
        break;
      }
      
      if(is_resource($cSourceImage)){
        $sPreview = static::sThumb($cSourceImage, $iThumbSide);
        imagedestroy($cSourceImage);
      }
      return $sPreview;
    }
  }

}
?>
