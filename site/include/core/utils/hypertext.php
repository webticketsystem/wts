<?php
/*
 * hypertext.php (part of WTS) - classes for generate/parse HTML/XML
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('string.php');
  
  /**
   * cElementBase - base class for rendering/parsing HTML/XML
   *
   * @var bInline_  - false: render element from new line
   * @var mContent_ - here some text
   */
  
  class cElementBase{
    protected $bInline_;
    protected $mContent_;
    
    public function __construct($sName, $bInline = true){
      $this->mContent_ = (string)$sName;
      $this->bInline_  = (bool)$bInline;
    }
    
    public function mContent(){
      return (isset($this->mContent_) ? $this->mContent_ : false);
    }
    
    public function sSerialize(&$sTab){
      return ($this->bInline_ ? $this->mContent_ : (cString::EOL . $sTab . $this->mContent_));
    }
    
    public function &cCopy(){
      return new cElementBase($this->mContent_, $this->bInline_);
    }
  }
  
  /**
   * cElement - class for rendering/parsing HTML
   *
   * @var aAttributes_ - array of attributes include main tag
   * @var bClosed_     - true - double tag: <tag>...</tag> , false - single tag: <tag>
   * @var bInnerTab_   - true: internal element start from new line - need for render
   * @static var aOpenElements_ - see: http://www.w3.org/TR/html4/index/elements.html
   * @var mContent_    - internal element(s), inherited from parent (cElementBase)
   */
  class cElement extends cElementBase{
    protected $aAttributes_;
    protected $bClosed_;
    protected $bInnerTab_;
    private static $aOpenElements_ = array('area'
                                         , 'base'
                                         , 'br'
                                         , 'col'
                                         , 'frame'
                                         , 'hr'
                                         , 'img'
                                         , 'input'
                                         , 'link'
                                         , 'meta'
                                         , 'param'
                                         );
    
    public function __construct($sName, $bInline = true){
      $sName            = strtolower($sName);
      $this->bClosed_   = in_array($sName, self::$aOpenElements_) ? false : true;
      $this->bInline_   = (bool)$bInline;
      $this->bInnerTab_ = false;
      $this->Attribute($sName);
    }
    
    public function __destruct(){
      if(isset($this->mContent_)){
        foreach($this->mContent_ as &$cElementBase){unset($cElementBase);}
      }
    }
    
    public function Attribute($sName, $sValue = null){
      $this->aAttributes_[$sName] = (isset($sValue) ? $sValue : null);
    }
    
    public function aAttributes(){return $this->aAttributes_;}
    
    protected function UnserializeAttributes(&$sStr){
      $iScope = 0; //1 - name, 2 - value, 3 - in value
      $aPair = false;
      
      //search attributes
      for($i = 0; $i < strlen($sStr); $i++){
        if($iScope == 3 && $sStr[$i] != '"'){
          if(isset($aPair['val'])){$aPair['val'] .= $sStr[$i];}
          else{$aPair['val'] = $sStr[$i];}
          continue;
        }
        switch ($sStr[$i]){
          case '=':
            if($iScope == 1){$iScope++;}
          break;
          case '"':
            if($iScope == 3){
              $iScope = 0;
              if(isset($aPair['val'])){
                $this->Attribute($aPair['name'], $aPair['val']);
              }
              else{$this->Attribute($aPair['name']);}
              $aPair = false;
            }
            if($iScope == 2){$iScope++;}
          break;
          case ' ':
            if($iScope == 1){
              $iScope = 0;
              $this->Attribute($aPair['name']);
              $aPair = false;
            }
          break;
          default:
            if($iScope < 2){
              if($iScope == 0){$iScope++;}
              if(isset($aPair['name'])){$aPair['name'] .= $sStr[$i];}
              else{$aPair['name'] = $sStr[$i];}
            }
          break;
        }
      }//for
      
      if($iScope == 1){$this->Attribute($aPair['name']);}
    }
    
    public function Content(cElementBase &$cElementBase){
      if($this->bClosed_){
        if(!$cElementBase->bInline_ && !$this->bInnerTab_){
          $this->bInnerTab_ = true;
        }
        $this->mContent_[] = $cElementBase;
      }
    }
    
    public function sSerialize(&$sTab){
      $s = false;
      $sTag = false;
      foreach($this->aAttributes_ as $sKey => $sVal){
        if($sTag !== false){
           $s .= ' ' . $sKey;
           if(isset($sVal)){$s .= '="' . $sVal . '"';}
        }
        else{
          $sTag = $sKey;
          if(!$this->bInline_){$s = cString::EOL . $sTab . '<' . $sTag;}
          else{$s = '<' . $sTag;}
        }
      }
      
      $s .= '>';
      
      if($this->bClosed_ && isset($this->mContent_)){
        if($this->bInnerTab_){
          $sNewTab = $sTab . cString::TAB;
          foreach($this->mContent_ as &$cElementBase){
            $s .= $cElementBase->sSerialize($sNewTab);
          }
        }
        else{
          foreach($this->mContent_ as &$cElementBase){
            $s .= $cElementBase->sSerialize($sTab);
          }
        }
      }
      
      if($this->bClosed_){
        if($this->bInnerTab_){$s .= cString::EOL . $sTab;}
        $s .= '</' . $sTag . '>';
      }
      return $s;
    }
    
    /**
     * если находит указанные теги создает из них массив
     * и заполняет их массив свойств которые были найдены в скобках <...>
     * а также что-то между скобками <p>...</p>
     * используется, например, для поиска картинок в теле HTML (XML)
     *
     * @param sName     - tag: img, div ... etc
     * @param sHtml     - source
     * @param aElements - array for fill
     */
    public static function iUnserialize($sName, &$sHtml, &$aElements, $bExtract = false){
      $iCount   = 0;
      $sName    = strtolower($sName);
      $bClosed_ = in_array($sName, self::$aOpenElements_) ? false : true;
      
      $sSearch1 =  '<' . $sName;
      $sSearch2 = '</' . $sName;
      $iSearchLen1 = strlen($sSearch1);
      $iSearchLen2 = $iSearchLen1 + 1;
      $iPos = 0;

      while($iPos !== false && $iPos < strlen($sHtml)){

        $iExtractBgn = $iPos = stripos($sHtml, $sSearch1, $iPos);
        if($iPos === false)break;
        $iEndPos = stripos($sHtml, '>', $iPos);
        if($iEndPos === false)break;
        $iPos += $iSearchLen1; //remove '<tag'
        $s = ($iEndPos > $iPos ? substr($sHtml, $iPos, $iEndPos - $iPos) : false);
        $iPos = ($iEndPos + 1); //ignore '>' for next iteration
        
        $cElement = new cElement($sName);
        //есть атрибуты у тега - будем выковыривать
        if($s !== false){
          //xml tag? - remove '/' in end
          if($s[strlen($s)-1] === '/'){$s = substr($s, 0, strlen($s)-1);}
          if($s[0] === ' '){$cElement->UnserializeAttributes($s);}
        }

        if($bClosed_){
          $iEndPos = stripos($sHtml, $sSearch2, $iPos);
          if($iEndPos !== false){
            $cElement->Content(new cElementBase(substr($sHtml, $iPos, $iEndPos - $iPos)));
            $iPos = $iEndPos + $iSearchLen2;
            $iPos = stripos($sHtml, '>', $iPos);
            if($iPos !== false){$iPos++;}//ignore '>' for next iteration
          }
        }
        
        if($bExtract && $iExtractBgn && $iPos){
          $sHtml = substr($sHtml, 0, $iExtractBgn) . substr($sHtml, $iPos);
        }

        $aElements[] = $cElement;
        $iCount++;
      }
      return $iCount;
    }
    
    public function &cCopy(){
      $cElement = false;
      
      foreach($this->aAttributes_ as $sKey => $sVal){
        if($cElement !== false){
           if(isset($sVal)){$cElement->Attribute($sKey, $sVal);}
           else{$cElement->Attribute($sKey);}
        }
        else{
          $cElement = new cElement($sKey, $this->bInline_);
        }
      }
      
      if(isset($this->mContent_)){
        foreach($this->mContent_ as &$cElementBase){
          $cElement->Content($cElementBase->cCopy());
        }
      }
      return $cElement;//new cElementBase($this->mContent_, $this->bInline_);
    }
    // \r - need delete before parsing
    public static function sToPlainText(&$sHtml){
      $sText = str_replace( "\xc2\xa0", ' ', $sHtml );
      //$sText = str_replace( "\xa0", '&nbsp;', $sText );      
      $sText = str_replace('&nbsp;', ' ', $sText);
      
      //$sText = str_replace("\r", '', $sText);
      $sText = str_replace("\n", ' ', $sText);
      //после параграфа - новая строка
      $sText = str_ireplace('</p>', "</p>\n", $sText);
      //замена xml брейка и брейка со стилями на обычный брейк
      $sText = preg_replace('/<br.*?>/is', '<br>', $sText);
      //после брейка - новая строка
      $sText = str_ireplace('<br>', "<br>\n", $sText);
      //после блока - новая строка
      $sText = str_ireplace('</div>', "</div>\n", $sText);
      //после заголовка - 2 new lines
      $sText = str_ireplace('</h1>', "</h1>\n\n", $sText);
      $sText = str_ireplace('</h2>', "</h2>\n\n", $sText);
      $sText = str_ireplace('</h3>', "</h3>\n\n", $sText);
      $sText = str_ireplace('</h4>', "</h4>\n\n", $sText);
      $sText = str_ireplace('</h5>', "</h5>\n\n", $sText);
      $sText = str_ireplace('</h6>', "</h6>\n\n", $sText);
      $sText = str_ireplace('</h6>', "</h6>\n\n", $sText);
      //после строки в таблице
      $sText = str_ireplace('</td>', "</td>\n", $sText);
      //после строки в списке
      $sText = str_ireplace('</li>', "</li>\n", $sText);
      //после гориз линии
      $sText = str_ireplace('<hr>', "<hr>\n", $sText);

      $sText = strip_tags($sText);
      $sText = html_entity_decode($sText);
      return trim($sText);
    }
    
    public static function sFromPlainText(&$sText){
      $sHtml  = '';
      $aLines = explode("\n", $sText);
      foreach ($aLines as $sLine){
        $s = htmlspecialchars(trim($sLine));
        if(strlen($s) > 0){$sHtml .= '<div>' . $s . '</div>' . PHP_EOL;}
        else{$sHtml .= '<br>' . PHP_EOL;}
      }
      return $sHtml;
    }
  }
}

?>
