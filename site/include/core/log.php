<?php
/*
 * log.php (part of WTS) - core class cLog
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('withdate.php');
  require_once('withfilter.php');
  require_once('withstr.php');
  require_once('collectionbase.php');

  /**
   * cLog - лог системы
   */
  class cLog extends cCollectionBase{
    use tWithDate{tWithDate::__construct as private __twd_construct;}
    use tWithFilter;
    use tWithStr;
    
    const TABLE = 'logs';
    
  /**
   * pre defined log levels
   * 
   * use log levels in App: [MSG:  MSG + WARN + ERR], [WARN: WARN + ERR], [ERR:  ERR]
   */
    const MSG  = 10; // messages
    const WARN = 20; // warnings
    const ERR  = 30; // errors
    
    
    public function Install(cDb &$cDb){
      $aMap[FN_DT]['def']   = 'datetime not null';
      $aMap[FN_I_TYP]['def'] = 'int unsigned not null';
      $aMap[FN_I_TYP]['idx'] = FN_I_TYP;
      $aMap[FN_S_VAL]['def']  = 'char(255) not null';
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
    }
    
    public function iType(){return $this->aMap[FN_I_TYP]['val'];}
    public function sValue(){return $this->aMap[FN_S_VAL]['val'];}
    public function Value($sValue){$this->Str(FN_S_VAL, $sValue);}
    
    public function __construct($iType = self::MSG){
      $this->__twd_construct();
      
      $iType = (int)$iType;
      if($iType == self::MSG
      || $iType == self::WARN
      || $iType == self::ERR){
        $this->aMap[FN_I_TYP]['val'] = $iType;
      }
      else{$this->aMap[FN_I_TYP]['val'] = 0;}
      
      $this->InitStr(FN_S_VAL);
    }
    
    public static function iUnserialize(cDb &$cDb, $iPage, $iPerPage, &$aObjs){
      $iCount   = 0;
      $iPage    = (int)$iPage;
      $iPerPage = (int)$iPerPage;
      if($iPage > 0 && $iPerPage > 0){
        
        $s = 'select UNIX_TIMESTAMP(' . FN_DT . '), '
            . FN_I_TYP . ', '
            . FN_S_VAL
            . ' from ' . $cDb->sTablePrefix() . static::TABLE;
        if(static::$sWhere !== false){
          $s .= ' where ' . static::$sWhere;
        }
        $s .= ' order by ' . FN_DT . ' desc limit '
            . (($iPage-1) * $iPerPage) . ', ' . $iPerPage;
        
        $cDb->QueryRes($s);
        if(($iCount = $cDb->iRowCount()) > 0){
          while($aRow = $cDb->aRow()){
            $cObj = new cLog($aRow[1]);
            $cObj->aMap[FN_DT]['val']    = (int)$aRow[0];
            $cObj->aMap[FN_S_VAL]['val'] = $aRow[2];
            $aObjs[] = $cObj;
          }
          $cDb->FreeResult();
        }
      }
      return $iCount;
    }
    
    public function bSerialize(cDb &$cDb){
      if($this->iType() > 0
      && $this->aMap[FN_S_VAL]['mod']){

        $cDb->Query('insert into ' . $cDb->sTablePrefix() . static::TABLE
        . ' (' . FN_DT . ', ' . FN_I_TYP . ', ' . FN_S_VAL
        . ') values (now(),' . $this->iType() . ', \'' . $cDb->sShield($this->sValue()) . '\')');
        $this->aMap[FN_S_VAL]['mod'] = false;
        return true;
      }
      return false;
    }
    
    public static function bAddFilterType($iType){
      $iType = (int)$iType;
      if($iType === static::MSG
      || $iType === static::WARN
      || $iType === static::ERR){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= FN_I_TYP . '=' . $iType;
        return true;
      }
      return false;
    }
    
    public static function bAddFilterPartValue(cDb &$cDb, $sValue){
      $sValue = (string)$sValue;
      if(strlen($sValue) > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= FN_S_VAL . ' like \'%' . $cDb->sShield($sValue) . '%\'';
        return true;
      }
      return false;
    }
    /* not use
    public static function bAddFilterAge($iAgeInSeconds){
      $iAgeInSeconds = (int)$iAgeInSeconds;
      if($iAgeInSeconds > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= 'TIMESTAMPDIFF(SECOND, ' . FN_DT . ', now())<' . $iAgeInSeconds;
        return true;
      }
      return false;
    }
    */
    //free old logs
    public static function DeleteAllLogs(cDb &$cDb){
      $cDb->Query('truncate ' . $cDb->sTablePrefix() . static::TABLE);
    }
    
    public static function sIP(){
      $sIP = 'Unknown';
      if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        $sIP = $_SERVER['HTTP_CLIENT_IP'];
      }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $sIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
      }else{
        $sIP = $_SERVER['REMOTE_ADDR'];
      }
      return $sIP;
    }
    
    
    
  }


}
