<?php
/*
 * histview.php (part of WTS) - core class cHistView
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('collectionbase.php');
  require_once('withcolor.php');
  require_once('withstr.php');

  /**
   * cHistView - вспомогательный класс для отображения меток
   */
  class cTagView{
    use tWithColor{tWithColor::__construct as private __twc_construct;}
    use tWithStr;
    
    protected $aMap;
    
    public function sName(){return $this->aMap[FN_S_NM]['val'];}
    public function sSlug(){return $this->aMap[FN_S_SL]['val'];}
   

    public function __construct($sName, $sSlug){
      
      $this->aMap[FN_S_NM]['val'] = null;
      if(isset($sName)){$this->Str(FN_S_NM, $sName);}
      $this->aMap[FN_S_SL]['val'] = null;
      if(isset($sSlug)){$this->Str(FN_S_SL, $sSlug);}
      
      $this->__twc_construct();
    }
  }


}
