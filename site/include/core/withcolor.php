<?php
/*
 * withcolor.php (part of WTS) - trait for core classes tWithColor
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  trait tWithColor{
    
    public function __construct($sColor = ''){
      $sColor = (string)$sColor;
      $this->aMap[FN_S_CLR]['mod'] = false;
      if(strlen($sColor) > 0
        && preg_match('/^[a-f0-9]{6}$/i', $sColor)){
        $this->aMap[FN_S_CLR]['val'] = strtoupper($sColor);
      }
      else{$this->aMap[FN_S_CLR]['val'] = null;}
    }
    
    public function sColor(){return $this->aMap[FN_S_CLR]['val'];}
    
    public function Color($sColor){
      $sColor = (string)$sColor;
      if(strlen($sColor) > 0
        && preg_match('/^[a-f0-9]{6}$/i', $sColor)){
        $sColor = strtoupper($sColor);
        if($this->aMap[FN_S_CLR]['val'] != $sColor){
          $this->aMap[FN_S_CLR]['val'] = $sColor;
          $this->aMap[FN_S_CLR]['mod'] = true;
        }
      }
      else{
        if($this->aMap[FN_S_CLR]['val'] !== null){
          $this->aMap[FN_S_CLR]['val'] = null;
          $this->aMap[FN_S_CLR]['mod'] = true;
        }
      }
    }
  }


}
