<?php
/*
 * upload.php (part of WTS) - core class cUpload
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('utils/string.php');
  require_once('collectionbase.php');
  require_once('withstr.php');

  /**
   * cUpload - вложения, пока не прекреплённые к событию
   */
  class cUpload extends cCollectionBase{
    use tWithStr;

    const TABLE = 'uploads';
    const UID_LENGHT = 31;
    
    public function Install(cDb &$cDb){
      $aMap[FN_S_UID]['def']  = 'char(' . static::UID_LENGHT . ') not null';
      $aMap[FN_S_UID]['uniq'] = true;
      $aMap[FN_S_NM]['def']   = 'char(255) not null';
      $aMap[FN_S_VAL]['def']  = 'mediumblob not null';//max size 16 mb
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, true);
    }
    
    public function sUID(){return $this->aMap[FN_S_UID]['val'];}
    public function sName(){return $this->aMap[FN_S_NM]['val'];}
    public function Name($sName){$this->Str(FN_S_NM, $sName);}
    public function sValue(){return $this->aMap[FN_S_VAL]['val'];}
    public function Value($sValue){
      if(isset($sValue) && strlen($sValue) > 0){
        $this->aMap[FN_S_VAL]['val'] = $sValue;
        $this->aMap[FN_S_VAL]['mod'] = true;
      }
    }
    
    public function __construct(){
      $this->aMap[FN_S_UID]['val'] = null;
      
      $this->InitStr(FN_S_NM);
      $this->InitStr(FN_S_VAL);
    }

    public static function mUnserialize(cDb &$cDb, $sUID){
      $cObj = false;
      
      $sUID = (string)$sUID;
      if(strlen($sUID) == static::UID_LENGHT){
      
        $s = 'select ' . FN_S_NM . ', '
              . FN_S_VAL
              . ' from ' . $cDb->sTablePrefix() . static::TABLE
              . ' where ' . FN_S_UID . '=\'' . $cDb->sShield($sUID) . '\'';
        $cDb->QueryRes($s);

        if($cDb->iRowCount() > 0){
          $aRow = $cDb->aRow();
          $cObj = new cUpload();
          $cObj->aMap[FN_S_UID]['val'] = $sUID;
          $cObj->aMap[FN_S_NM]['val']  = $aRow[0];
          $cObj->aMap[FN_S_VAL]['val'] = $aRow[1];
          $cDb->FreeResult();
        }
      }
      return $cObj;
    }

    public function bSerialize(cDb &$cDb){
      if($this->aMap[FN_S_NM]['mod']
      && $this->aMap[FN_S_VAL]['mod']){
        $this->aMap[FN_S_UID]['val'] = cString::sRand(static::UID_LENGHT);
        $cDb->Query('insert into ' . $cDb->sTablePrefix() . static::TABLE . ' (' 
        . FN_S_UID . ', ' . FN_S_NM . ', ' . FN_S_VAL . ') values (\''
        . $cDb->sShield($this->sUID()) . '\', \''
        . $cDb->sShield($this->sName()) . '\', \''
        . $cDb->sShield($this->sValue()) . '\')');
        $this->aMap[FN_S_NM]['mod'] = $this->aMap[FN_S_VAL]['mod'] = false;
        return true;
      }
      return false;
    }
    //free old uploads in cron job
    public static function DeleteAllUploads(cDb &$cDb){
      $cDb->Query('truncate ' . $cDb->sTablePrefix() . static::TABLE);
    }
  }


}
