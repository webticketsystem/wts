<?php
/*
 * group.php (part of WTS) - core class cGroup
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('withid.php');
  require_once('withfilter.php');
  require_once('withattr.php');
  require_once('groupattrtype.php');
  require_once('withupdated.php');

  /**
   * cGroup - группы контактов
   * 
   * @var const int iID
   * @var string sName
   */
  class cGroup extends cCollectionBase{
    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithFilter;
    use tWithAttr;
    use tWithStr;
    use tWithUpdated{tWithUpdated::__construct as private __twu_construct;}
    
    const TABLE = 'groups';
    
    public function Install(cDb &$cDb){
      $aMap[FN_I_ID]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']   = true;
      $aMap[FN_I_ID]['ai']   = true;
      $aMap[FN_S_NM]['def']  = 'char(255) not null';
      $aMap[FN_S_NM]['uidx'] = true;//unique index ?
      
      $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null';
      $aMap[FN_DT]['def']    = 'datetime not null';
      $aMap[FN_I_OWN]['def'] = 'bigint unsigned';
      $aMap[FN_DT_U]['def']  = 'datetime';
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
      $this->InstallAttr($cDb);
    }
    
    public function sName(){return $this->aMap[FN_S_NM]['val'];}
    public function Name($sName){$this->Str(FN_S_NM, $sName);}

    public function __construct(){
      $this->__twi_construct();
      
      
      $this->InitStr(FN_S_NM);
      $this->__twu_construct();
    }
    
    
    //pass id or name
    public static function mUnserialize(cDb &$cDb, $mVal){
      $cObj = $b = false;
      $s = 'select ' . FN_I_ID . ', '
                     . FN_S_NM . ', '
                     . FN_I_PRT . ', UNIX_TIMESTAMP('
                     . FN_DT . '), '
                     . FN_I_OWN . ', UNIX_TIMESTAMP('
                     . FN_DT_U
        . ') from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ';
      if(is_string($mVal) && strlen($mVal) > 0){
        $s .= FN_S_NM . '=\'' . $cDb->sShield($mVal) . '\'';
        $b = true;
      }
      else{
        $mVal = (int)$mVal;
        if($mVal > 0){
          $s .= FN_I_ID . '=' . $mVal;
          $b = true;
        }
      }

      if($b){
        $cDb->QueryRes($s);
        if($cDb->iRowCount() > 0){
          $aRow = $cDb->aRow();
          $cObj = new cGroup();
          $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
          $cObj->aMap[FN_S_NM]['val']  = $aRow[1];
          $cObj->aMap[FN_I_PRT]['val'] = (int)$aRow[2];
          $cObj->aMap[FN_DT]['val']    = (int)$aRow[3];
          $cObj->aMap[FN_I_OWN]['val'] = (int)$aRow[4];
          $cObj->aMap[FN_DT_U]['val']  = (int)$aRow[5];
          $cDb->FreeResult();
        }
      }
      return $cObj;
    }

    public static function iUnserialize(cDb &$cDb, $iPage, $iPerPage, &$aObjs){
      $iCount   = 0;
      $iPage    = (int)$iPage;
      $iPerPage = (int)$iPerPage;
      if($iPage > 0 && $iPerPage > 0){
        
        $s = 'select ' . FN_I_ID . ', '
                       . FN_S_NM . ', '
                       . FN_I_PRT . ', UNIX_TIMESTAMP('
                       . FN_DT . '), '
                       . FN_I_OWN . ', UNIX_TIMESTAMP('
                       . FN_DT_U
        . ') from ' . $cDb->sTablePrefix() . static::TABLE;
        if(static::$sWhere !== false){
          $s .= ' where ' . static::$sWhere;
        }
        $s .= ' order by ' . FN_S_NM . ' limit '
        . (($iPage-1) * $iPerPage) . ', ' . $iPerPage;
        
        $cDb->QueryRes($s);
        
        if(($iCount = $cDb->iRowCount()) > 0){
          while($aRow = $cDb->aRow()){
            $cObj = new cGroup();
            $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
            $cObj->aMap[FN_S_NM]['val']  = $aRow[1];
            $cObj->aMap[FN_I_PRT]['val'] = (int)$aRow[2];
            $cObj->aMap[FN_DT]['val']    = (int)$aRow[3];
            $cObj->aMap[FN_I_OWN]['val'] = (int)$aRow[4];
            $cObj->aMap[FN_DT_U]['val']  = (int)$aRow[5];
            $aObjs[] = $cObj;
          }
          $cDb->FreeResult();
        }
      }
      return $iCount;
    }
  /**
   * Восстанавливаем атрибуты группы из базы
   * @see cAttrView
   */
    public function iUnserializeAttr(cDb &$cDb, &$aAttrView){
      $aAttrTypes;
      if($this->iID() && cGroupAttrType::iUnserialize($cDb, $aAttrTypes) > 0){
        return $this->iUnserializeAttr_($cDb, $aAttrTypes, $aAttrView);
      }
      else{return 0;}
    }
  /**
   * Обновление или создание группы в базе
   */
    public function bSerialize(cDb &$cDb){
      $bRet = false;
      if($this->aMap[FN_S_NM]['mod']){//если вообще имя менялось....
        $cDb->QueryRes('select ' . FN_I_ID
        . ' from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\'');
        if($cDb->iRowCount() > 0){$cDb->FreeResult();}
        else{
          if($this->iID() && $this->iUpdater() > 0){//try to save modified
            $aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';
            
            if($this->aMap[FN_I_OWN]['mod']){
              $aMap[FN_I_OWN] = $this->iUpdater();
            }
            
            $aMap[FN_DT_U] = 'now()';
            
            $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
            $bRet = true;
          }
          else{//try to insert new
            if($this->iCreator() > 0){
              
              $aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';
              
              $aMap[FN_I_PRT] = $this->iCreator();
              $aMap[FN_DT] = 'now()';
              
              $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
              if($this->aMap[FN_I_ID]['val'] = (int)$cDb->mLastInsertID()){
                $bRet = true;
              }
            }
          }
        }
      }
      if($bRet){
        foreach($this->aMap as &$a){
          if(isset($a['mod']) && $a['mod'])$a['mod'] = false;
        }
      }
      return $bRet;
    }
    
    public function bSerializeAttr(cDb &$cDb, &$aAttrView){
      $aAttrTypes;
      if($this->iID()
      && $this->iUpdater()
      && cGroupAttrType::iUnserialize($cDb, $aAttrTypes) > 0
      && $this->bSerializeAttr_($cDb, $aAttrTypes, $aAttrView)){
        $aMap;
        $aMap[FN_I_OWN] = $this->iUpdater();
        $aMap[FN_DT_U] = 'now()';
        $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
        return true;
      }
      return false;
    }
    
  /**
   * Функции поиска групп(ы)
   */
    
    public static function bAddFilterPartValue(cDb &$cDb, $sValue){
      $sValue = (string)$sValue;
      if(strlen($sValue) > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= '(' . FN_S_NM . ' like \'%' . $cDb->sShield($sValue) . '%\' or ';
        static::AddFilterPartAttrValue($cDb, $sValue);
        static::$sWhere .= ')';
        return true;
      }
      return false;
    }
  
    public function bDelete(cDb &$cDb){
      if($this->iID()){
        $this->DeleteAttr($cDb);
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $this->iID());
        $this->aMap[FN_I_ID]['val'] = null;
        return true;
      }
      return false;
    }
    
  }


}
