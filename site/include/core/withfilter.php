<?php
/*
 * withfilter.php (part of WTS) - trait for core classes tWithFilter
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  /**
   * tWithFilter - треит для добавления условия sql - where
   * 
   * @static var string sWhere - тут храним всё после where
   */
  trait tWithFilter{
    protected static $sWhere = false;
    
    public static function ClearFilters(){
      if(static::$sWhere !== false){
        static::$sWhere = false;
      }
    }
    
    public static function iCount(cDb &$cDb){
      
      $s = 'select count(*) from ' . $cDb->sTablePrefix() . static::TABLE;
      if(static::$sWhere !== false){
        $s .= ' where ' . static::$sWhere;
      }
      
      /*echo $s;*/
      return (int)$cDb->sQueryRes($s);
    }
    
    //for rest
    public static function mIDs(cDb &$cDb){
      $aIDs = false;
      $s = 'select ' . FN_I_ID . ' from ' . $cDb->sTablePrefix() . static::TABLE;
      if(static::$sWhere !== false){
        $s .= ' where ' . static::$sWhere;
      }
      
      $cDb->QueryRes($s);
        
      if($cDb->iRowCount() > 0){
        while($aRow = $cDb->aRow()){$aIDs[] = (int)$aRow[0];}
        $cDb->FreeResult();
      }
      return $aIDs;
    }
  }


}
