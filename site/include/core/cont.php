<?php
/*
 * cont.php (part of WTS) - core class cCont
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  //прятать колонку с именем в интерфейсе если все имена настранице (в выборке) == null
  require_once('group.php');
  require_once('contattrtype.php');
  require_once('withemail.php');

  /**
   * cCont - контакты
   */
  class cCont extends cCollectionBase{

    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithFilter;
    use tWithAttr;
    use tWithEmail;
    use tWithUpdated{tWithUpdated::__construct as private __twu_construct;}
    
    //type return
    const CSTMR = 10; //customer
    const AGENT = 20;
    const ADMIN = 30; //only one in the system
    
    const TABLE      = 'conts';
    const TABLE_SSN  = self::TABLE . '_ssns';
    
    //////{tWithMeta - тут же храним и роли role : role_name
    const TABLE_META = self::TABLE . '_meta';
    
    protected function InstallMeta(cDb &$cDb){
      $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null';
      $aMap[FN_I_PRT]['idx'] = FN_I_PRT;
      $aMap[FN_S_NM]['def']  = 'char(255) not null';
      $aMap[FN_S_VAL]['def'] = 'char(255) not null';
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE_META);
    }
    
    public function iUnserializeMeta(cDb &$cDb, &$aMeta){
      $iCount = 0;
      if($this->iID() > 0){
      
        $s = 'select '
              . FN_S_NM . ', '
              . FN_S_VAL
              . ' from ' . $cDb->sTablePrefix() . static::TABLE_META
              . ' where ' . FN_I_PRT .  '=' . $this->iID()
              . ' order by ' . FN_S_NM;
        
        $cDb->QueryRes($s);

        $iCount = $cDb->iRowCount();
        $s = false;
        if($iCount > 0){
          while($aRow = $cDb->aRow()){
            //c повторяющимеся именами метаинформацию не складываем/достаём
            if($s === false){$s = $aRow[0];}
            else{
              if($s === $aRow[0])continue;
            }
            $aMeta[$aRow[0]] = $aRow[1];
          }
          $cDb->FreeResult();
        }
      }
      return $iCount;
    }
    
    public function bSerializeMeta(cDb &$cDb, $sName, $sValue = false){
      $sName = (string)$sName;
      $sName = trim($sName);
      if($sValue !== false){
        $sValue = (string)$sValue;
        $sValue = trim($sValue);
      }
      if($this->iID() > 0
      && strlen($sName) > 0){
        $cDb->QueryRes('select ' . FN_S_VAL . ' from ' . $cDb->sTablePrefix() . static::TABLE_META
            . ' where ' . FN_I_PRT . '=' . $this->iID()
            . ' and '. FN_S_NM . '=\'' . $cDb->sShield($sName) . '\'');
        if($cDb->iRowCount() > 0){//update
          $aRow = $cDb->aRow();
          $cDb->FreeResult();
          if(strlen($sValue) > 0){
             $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE_META
             . ' set ' . FN_S_VAL . '=\'' . $cDb->sShield($sValue)
             . '\' where ' . FN_I_PRT . '=' . $this->iID()
             . ' and '. FN_S_NM . '=\'' . $cDb->sShield($sName) . '\'');
          }
          else{//delete
            $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE_META
            . ' where ' . FN_I_PRT . '=' . $this->iID()
            . ' and '. FN_S_NM . '=\'' . $cDb->sShield($sName) . '\'');
          }
        }
        else{//insert
          if(strlen($sValue) > 0){
            $cDb->Query( 'insert into ' . $cDb->sTablePrefix() . static::TABLE_META . ' ('
            . FN_I_PRT . ', ' . FN_S_NM . ', ' . FN_S_VAL
            . ') values ('
            . $this->iID() . ', \''
            . $cDb->sShield($sName) . '\', \''
            . $cDb->sShield($sValue) .'\')');
          }
        }
      }
    }
    
    protected function DeleteMeta(cDb &$cDb){
      $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE_META . ' where ' . FN_I_PRT . '=' . $this->iID());
    }
    //////}tWithMeta

  /**
   * Create database tables
   */
    protected function InstallSsn(cDb &$cDb){
      $aMap[FN_I_PRT]['def']   = 'bigint unsigned not null';
      $aMap[FN_I_PRT]['idx']   = FN_I_PRT;
      $aMap[FN_S_UIDS]['def']  = 'char(36) not null';
      $aMap[FN_S_UIDS]['uniq'] = true;
      $aMap[FN_DT_S]['def']    = 'datetime not null';
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE_SSN, false, 'MEMORY');
    }
    
    public function Install(cDb &$cDb){
      $aMap[FN_I_ID]['def']   = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']    = true;
      $aMap[FN_I_ID]['ai']    = true;
      $aMap[FN_S_NM]['def']   = 'char(255)';
      $aMap[FN_S_NM]['idx']   = true;
      $aMap[FN_S_LGN]['def']  = 'char(255) not null'; //this must be valid email address
      $aMap[FN_S_LGN]['uidx'] = true;
      $aMap[FN_S_PWD]['def']  = 'char(64) default null';
      $aMap[FN_DT_S]['def']   = 'datetime'; //last successful login
      $aMap[FN_I_GRP]['def']  = 'bigint unsigned'; //group 
      $aMap[FN_I_QUE]['def']  = 'bigint unsigned'; //queue - if exist: load roles
      /**
       * если не определена очередь - это пользователь
       * роль (права) у пользователя: только смотреть 
       * свои (и своей группы) заявки, календарь и отчёты
       * 
       * если queue_id == 1 это админ - он не может
       * только создавать эвенты (но удалять их может)
       * 
       * если queue_id > 1 это агент - для агентов есть роли
       * роли содержат возможности
      */
      $aMap[FN_B_DSB]['def'] = 'bit not null default 0';
      
      $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null';
      $aMap[FN_DT]['def']    = 'datetime not null';
      $aMap[FN_I_OWN]['def'] = 'bigint unsigned';
      $aMap[FN_DT_U]['def']  = 'datetime';
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
      $this->InstallSsn($cDb);
      $this->InstallAttr($cDb);
      $this->InstallMeta($cDb);
    }
    
    public function sName(){return $this->aMap[FN_S_NM]['val'];}
    public function sLogin(){return $this->aMap[FN_S_LGN]['val'];}
    public function Login($sLogin){$this->Email(FN_S_LGN, $sLogin);}
    public function iGroup(){return $this->aMap[FN_I_GRP]['val'];}
    public function Group($iGroup){
      $iGroup = (int)$iGroup;
      if($iGroup < 1){$iGroup = 0;}
      if($this->iType() != static::ADMIN
      && $this->aMap[FN_I_GRP]['val'] !== $iGroup){
        $this->aMap[FN_I_GRP]['val'] = $iGroup;
        $this->aMap[FN_I_GRP]['mod'] = true;
      }
    }
    public function iQueue(){return $this->aMap[FN_I_QUE]['val'];}
    public function Queue($iQueue){
      $iQueue = (int)$iQueue;
      if($iQueue < 2){$iQueue = 0;}
      if($this->iType() !== static::ADMIN
      && $this->aMap[FN_I_QUE]['val'] !== $iQueue){
        $this->aMap[FN_I_QUE]['val'] = $iQueue;
        $this->aMap[FN_I_QUE]['mod'] = true;
      }
    }
    public function iType(){
      $iType = static::CSTMR;
      if($this->iID() > 0 && $this->iQueue()){
        if($this->iQueue() === 1){$iType = static::ADMIN;}
        else{$iType = static::AGENT;}
      }
      return $iType;
    }
    
    public function bDisabled(){return $this->aMap[FN_B_DSB]['val'];}
    
    public function Enable(){
      if($this->aMap[FN_B_DSB]['val'] !== false){
        $this->aMap[FN_B_DSB]['val'] = false;
        $this->aMap[FN_B_DSB]['mod'] = true;
      }
    }
    
    public function Disable(){
      if($this->aMap[FN_B_DSB]['val'] !== true){
        $this->aMap[FN_B_DSB]['mod'] = $this->aMap[FN_B_DSB]['val'] = true;
      }
    }

    public function __construct(){
      $this->__twi_construct();
      
      $this->aMap[FN_S_NM]['val'] = null;//autoupdate field
      $this->InitStr(FN_S_LGN);
      $this->InitInt(FN_I_GRP);
      $this->InitInt(FN_I_QUE);
      
      $this->aMap[FN_B_DSB]['val'] = $this->aMap[FN_B_DSB]['mod'] = false;
      $this->__twu_construct();
    }
  /**
   * Восстанавливаем контакты из базы
   */
    public static function mUnserialize(cDb &$cDb, $mVal){
      $cObj = $b = false;
      $s = 'select ' . FN_I_ID . ', '
                     . FN_S_NM . ', '
                     . FN_S_LGN . ', '
                     . FN_I_GRP . ', '
                     . FN_I_QUE . ', '
                     . FN_B_DSB . ', '
                     . FN_I_PRT . ', UNIX_TIMESTAMP('
                     . FN_DT . '), '
                     . FN_I_OWN . ', UNIX_TIMESTAMP('
                     . FN_DT_U
        . ') from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ';
      if(is_string($mVal) && strlen($mVal) > 0){
        $s .= FN_S_LGN . '=\'' . $cDb->sShield($mVal) . '\'';
        $b = true;
      }
      else{
        $mVal = (int)$mVal;
        if($mVal > 0){
          $s .= FN_I_ID . '=' . $mVal;
          $b = true;
        }
      }

      if($b){
        $cDb->QueryRes($s);
        if($cDb->iRowCount() > 0){
          $aRow = $cDb->aRow();
          $cObj = new cCont();
          $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
          $cObj->aMap[FN_S_NM]['val']  = $aRow[1];
          $cObj->aMap[FN_S_LGN]['val'] = $aRow[2];
          $cObj->aMap[FN_I_GRP]['val'] = (int)$aRow[3];
          $cObj->aMap[FN_I_QUE]['val'] = (int)$aRow[4];
          $cObj->aMap[FN_B_DSB]['val'] = (bool)$aRow[5];
          $cObj->aMap[FN_I_PRT]['val'] = (int)$aRow[6];
          $cObj->aMap[FN_DT]['val']    = (int)$aRow[7];
          $cObj->aMap[FN_I_OWN]['val'] = (int)$aRow[8];
          $cObj->aMap[FN_DT_U]['val']  = (int)$aRow[9];
          $cDb->FreeResult();
        }
      }
      return $cObj;
    }
    
    public static function iUnserialize(cDb &$cDb, $iPage, $iPerPage, &$aObjs){
      $iCount   = 0;
      $iPage    = (int)$iPage;
      $iPerPage = (int)$iPerPage;
      if($iPage > 0 && $iPerPage > 0){
        
        $s = 'select ' . FN_I_ID . ', '
                       . FN_S_NM . ', '
                       . FN_S_LGN . ', '
                       . FN_I_GRP . ', '
                       . FN_I_QUE . ', '
                       . FN_B_DSB . ', '
                       . FN_I_PRT . ', UNIX_TIMESTAMP('
                       . FN_DT . '), '
                       . FN_I_OWN . ', UNIX_TIMESTAMP('
                       . FN_DT_U
        . ') from ' . $cDb->sTablePrefix() . static::TABLE;
        if(static::$sWhere !== false){
          $s .= ' where ' . static::$sWhere;
        }
        $s .= ' order by ' . FN_S_NM . ', ' . FN_S_LGN . ' limit '
            . (($iPage-1) * $iPerPage) . ', ' . $iPerPage;
        
        $cDb->QueryRes($s);
        
        if(($iCount = $cDb->iRowCount()) > 0){
          while($aRow = $cDb->aRow()){
            $cObj = new cCont();
            $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
            $cObj->aMap[FN_S_NM]['val']  = $aRow[1];
            $cObj->aMap[FN_S_LGN]['val'] = $aRow[2];
            $cObj->aMap[FN_I_GRP]['val'] = (int)$aRow[3];
            $cObj->aMap[FN_I_QUE]['val'] = (int)$aRow[4];
            $cObj->aMap[FN_B_DSB]['val'] = (bool)$aRow[5];
            $cObj->aMap[FN_I_PRT]['val'] = (int)$aRow[6];
            $cObj->aMap[FN_DT]['val']    = (int)$aRow[7];
            $cObj->aMap[FN_I_OWN]['val'] = (int)$aRow[8];
            $cObj->aMap[FN_DT_U]['val']  = (int)$aRow[9];
            $aObjs[] = $cObj;
          }
          $cDb->FreeResult();
        }
      }
      return $iCount;
    }
    
  /**
   * Восстанавливаем атрибуты контакта из базы
   * @see cAttrView
   */
    public function iUnserializeAttr(cDb &$cDb, &$aAttrView){
      $aAttrTypes;
      if($this->iID() && cContAttrType::iUnserialize($cDb, $aAttrTypes) > 0){
        return $this->iUnserializeAttr_($cDb, $aAttrTypes, $aAttrView);
      }
      else{return 0;}
    }
  /**
   * Обновление или создание контакта в базе
   */
    public function bSerialize(cDb &$cDb){
      $bRet = false;
      if($this->iID()){//try to save modified
        $aMap;
        if($this->iUpdater() > 0){
          if($this->aMap[FN_S_LGN]['mod']){
            $cDb->QueryRes('select ' . FN_I_ID
            . ' from ' . $cDb->sTablePrefix() . static::TABLE
            . ' where ' . FN_S_LGN . '=\'' . $cDb->sShield($this->sLogin()) . '\'');
            if($cDb->iRowCount() > 0){$cDb->FreeResult();}
            else{
              $aMap[FN_S_LGN] = '\'' . $cDb->sShield($this->sLogin()) . '\'';
            }
          }
          
          if($this->aMap[FN_I_GRP]['mod']){
            $aMap[FN_I_GRP] = ($this->iGroup() > 0 ? $this->iGroup() : 'null');
          }
          
          if($this->aMap[FN_I_QUE]['mod']){
            $aMap[FN_I_QUE] = ($this->iQueue() > 1 ? $this->iQueue() : 'null');
          }
          
          if($this->aMap[FN_B_DSB]['mod']){
            $aMap[FN_B_DSB] = ($this->bDisabled() ? 'true' : 'false');
            $aMap[FN_S_PWD] = 'null';
          }
          if(isset($aMap) && $this->aMap[FN_I_OWN]['mod']){
            $aMap[FN_I_OWN] = $this->iUpdater();
          }
          
          if(isset($aMap)){
            $aMap[FN_DT_U] = 'now()';
            $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
            $bRet = true;
          }
        }
      }//$this->iID() != null
      else{//try to insert new

        if($this->iCreator() > 0 && $this->aMap[FN_S_LGN]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_LGN . '=\'' . $cDb->sShield($this->sLogin()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{
            
            $aMap[FN_S_LGN] = '\'' . $cDb->sShield($this->sLogin()) . '\'';
            
            if($this->iGroup() > 0){$aMap[FN_I_GRP] = $this->iGroup();}
            
            if($this->iQueue() > 1){$aMap[FN_I_QUE] = $this->iQueue();}
            
            if($this->bDisabled()){$aMap[FN_B_DSB] = 'true';}
            
            $aMap[FN_I_PRT] = $this->iCreator();
            $aMap[FN_DT] = 'now()';
            
            $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
            if($this->aMap[FN_I_ID]['val'] = (int)$cDb->mLastInsertID()){
              //insert admin in admin queue
              if($this->aMap[FN_I_ID]['val'] === 1){
                $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE . ' set ' . FN_I_QUE . '=1 where ' . FN_I_ID . '=1');
              }
              $bRet = true;
            }
          }
        }
      }
      if($bRet){
        foreach($this->aMap as &$a){
          if(isset($a['mod']) && $a['mod'])$a['mod'] = false;
        }
      }
      return $bRet;
    }

  /**
   * Сохранение атрибутов в базе
   * 
   * здесь перестраиваем и наше имя
   */
   public function bSerializeAttr(cDb &$cDb, &$aAttrView){
      $aAttrTypes;
      if($this->iID()
      && $this->iUpdater()
      && cContAttrType::iUnserialize($cDb, $aAttrTypes) > 0
      && $this->bSerializeAttr_($cDb, $aAttrTypes, $aAttrView)){
        $aMap;
        if(cContAttrType::iUnserialize($cDb, $aAttrTypes) > 0
        && $sName = $this->mBuildName($cDb, $aAttrTypes)){
          $aMap[FN_S_NM] = '\'' . $cDb->sShield($sName) . '\'';
        }
        else{$aMap[FN_S_NM] = 'null';}
        $aMap[FN_I_OWN] = $this->iUpdater();
        $aMap[FN_DT_U] = 'now()';
        $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
        return true;
      }
      return false;
    }
  /**
   * Построение имени контакта
   */
    protected function mBuildName(cDb &$cDb, &$aAttrTypes){
      $sName = false;

      foreach($aAttrTypes as $key => &$cAttrType){
        if($cAttrType->bInName()){
          $cDb->QueryRes('select ' . FN_S_VAL
          . ' from ' . $cDb->sTablePrefix() . $this->sAttrTable()
          . ' where ' . FN_I_PRT . '='. $this->iID()
          . ' and ' . FN_I_TYP . '=' . $cAttrType->iID());
          if($cDb->iRowCount() > 0){
            while($aRow = $cDb->aRow()){
              if($sName !== false){$sName .= ' ' . $aRow[0];}
              else{$sName = $aRow[0];}
            }
            $cDb->FreeResult();
          }
        }
        unset($aAttrTypes[$key]);
      }
      return $sName;
    }
  /**
   * Перестроение всех имён контактов
   * 
   * !!! вызывайте, когда меняете типы атрибутов контактов
   * , учасствующих в создании имени
   */
    public static function RebuildAllNames(cDb &$cDb){
      $aInName = $aAttrTypes = false;
      
      if(cContAttrType::iUnserialize($cDb, $aAttrTypes) > 0){
        foreach($aAttrTypes as $key => &$cAttrType){
          if($cAttrType->bInName()){
            $aInName[] = $cAttrType->iID();
          }
          unset($aAttrTypes[$key]);
        }
      }
      if($aInName){
        $cDb->QueryRes('select ' . FN_I_ID . ' from ' . $cDb->sTablePrefix() . static::TABLE);
        $aContIDs;
        if($cDb->iRowCount() > 0){
          while($aRow = $cDb->aRow()){$aContIDs[] = $aRow[0];}
          $cDb->FreeResult();
          foreach($aContIDs as $i){
            $sName = false;
            foreach($aInName as $j){
              $cDb->QueryRes('select ' . FN_S_VAL . ' from ' . $cDb->sTablePrefix() . self::sAttrTable() . ' where ' . FN_I_PRT . '='. $i . ' and ' . FN_I_TYP . '=' . $j);
              if($cDb->iRowCount() > 0){
                while($aRow = $cDb->aRow()){
                  if($sName !== false){$sName .= ' ' . $aRow[0];}
                  else{$sName = $aRow[0];}
                }
                $cDb->FreeResult();
              }
            }
            if($sName){
              $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE . ' set '
              . FN_S_NM . '=\'' . $cDb->sShield($sName)
              . '\' where ' . FN_I_ID .  '=' . $i);
            }
            else{
              $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE . ' set '
              . FN_S_NM . '=null where ' . FN_I_ID .  '=' . $i);
            }
          }//foreach($aContIDs as $i){
        }
      }
      else{
        $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE . ' set ' . FN_S_NM . '=null');
      }
    }

  /**
   * Функции поиска контакта(ов)
   */
    public static function bAddFilterDisabled($bDisabled){
      $bDisabled = (bool)$bDisabled;
      if(static::$sWhere !== false){static::$sWhere .= ' and ';}
      static::$sWhere .= FN_B_DSB . '=' . ($bDisabled ? 'true' : 'false');
    }
   
    public static function bAddFilterGroup($iGroup){
      $bRet = false;
      $iGroup = (int)$iGroup;
      if($iGroup === 0){$bRet = FN_I_GRP . ' is null';}
      elseif($iGroup > 0){$bRet = FN_I_GRP . '=' . $iGroup;}
      elseif($iGroup === -1){$bRet = FN_I_GRP . ' is not null';}
      if($bRet !== false){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= $bRet;
        $bRet = true;
      }
      return $bRet;
      
      
      
      $iGroup = (int)$iGroup;
      if($iGroup > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= FN_I_GRP . '=' . $iGroup;
        return true;
      }
      return false;
    }

    
    public static function bAddFilterQueue($iQueue){
      $bRet = false;
      $iQueue = (int)$iQueue;
      if($iQueue === 0){$bRet = FN_I_QUE . ' is null';}
      elseif($iQueue > 1){$bRet = FN_I_QUE . '=' . $iQueue;}
      elseif($iQueue === -1){$bRet = FN_I_QUE . ' is not null and ' . FN_I_QUE . '>1';}
      if($bRet !== false){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= $bRet;
        $bRet = true;
      }
      return $bRet;
    }
    
    public static function bAddFilterPartValue(cDb &$cDb, $sValue){
      $sValue = (string)$sValue;
      if(strlen($sValue) > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= '(' . FN_S_LGN . ' like \'%' . $cDb->sShield($sValue) . '%\' or ';
        static::AddFilterPartAttrValue($cDb, $sValue);
        static::$sWhere .= ')';
        return true;
      }
      return false;
    }

  /**
   * При слиянии групп
   * @param int iSrc - id исходной группы
   * @param int iDst - id группы назначения
   */
    public static function MergeGroup(cDb &$cDb, $iSrc, $iDst){
      $iSrc = (int)$iSrc;
      $iDst = (int)$iDst;
      if($iSrc > 0 && $iDst > 0){
        $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE . ' set ' . FN_I_GRP . '=' . $iDst . ' where ' . FN_I_GRP . '=' . $iSrc);
      }
    }
  /**
   * При слиянии контактов
   */
   public function bDelete(cDb &$cDb){
      if($this->iID() > 1){//не удали админа
        $this->DeleteAttr($cDb);
        $this->DeleteMeta($cDb);
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $this->iID());
        $this->aMap[FN_I_ID]['val'] = null;
        return true;
      }
      return false;
    }
    
  /**
   * Для управления паролями
   * 
   * Если меняем пароль - должны рвать старую сессию!
   * 
   * @param int sUID - @see sBeginSsn - no more ways
   * @param int sOldPasswd - if not set - this value not checked
   */
    public static function bPasswd(cDb &$cDb, $sUID, $sPasswd, $sOldPasswd = null){
      $iCondition = 0;
      $sUID = (string)$sUID;
      $sPasswd = (string)$sPasswd;
      if(strlen($sPasswd) > 0 //длину пароля проверяйте в приложении
      && preg_match('/^[a-f0-9-]{36}$/i', $sUID)
      && ($iID = (int)$cDb->sQueryRes('select ' . FN_I_PRT// FN_S_UIDS
          . ' from ' . $cDb->sTablePrefix() . static::TABLE_SSN
          . ' where ' . FN_S_UIDS . '=\'' . $cDb->sShield($sUID) . '\'')) > 0){//проверим
        if(isset($sOldPasswd) //если указан старый пароль - проверим
        && strlen($sOldPasswd) > 0){
          $iCondition = 1;
          $sRealPasswd = $cDb->sQueryRes('select ' . FN_S_PWD
            . ' from ' . $cDb->sTablePrefix() . static::TABLE
            . ' where ' . FN_I_ID . '=' . $iID);
          if(hash('sha256', $sOldPasswd, false) === $sRealPasswd){$iCondition = 2;}
          
        }
        if($iCondition != 1){
          $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE
            . ' set ' . FN_S_PWD 
            . '=sha2(\'' . $cDb->sShield($sPasswd) . '\', 256)  where '
            . FN_I_ID . '=' . $iID);
          $iCondition = 3;
        }
      }
      return ($iCondition == 3);
    }
    
  /**
   * Для начала работы с приложением
   * 
   * @return string sUID (length 36 chars) or false [sample: 6ccd780c-baba-1026-9564-0040f4311e29]
   * 
   * make pause if sLogin not found
   */
    public static function mBeginSsn(cDb &$cDb, $sLogin){
      $sUID = false;
      $sLogin = (string)$sLogin;
      if($cCont = cCont::mUnserialize($cDb, $sLogin)){
        if(!$cCont->bDisabled()){
          $sUID = $cDb->sQueryRes('select uuid()');
          $cDb->MultiQuery('delete from ' . $cDb->sTablePrefix() . static::TABLE_SSN
            . ' where ' . FN_I_PRT . '=' . $cCont->iID()
            . ';insert into ' . $cDb->sTablePrefix() . static::TABLE_SSN . ' ('
            . FN_I_PRT . ', ' . FN_S_UIDS . ', ' . FN_DT_S
            . ') values ('
            . $cCont->iID() . ', \'' . $sUID . '\', now())');
        }
        else{sleep(3);}//отключен
        unset($cCont);
      }
      else{sleep(3);}//или не в базе или плохой формат логина
      return $sUID;
    }
    
  /**
   * Для авторизации и работы с приложением
   * @param string sHash = sha1($sUID . sha1(sPasswd))
   * @param int iTimeOutInSec - timeout session
   * @param bool bRemember - save login time in main conts table
   * @return object cCont or false
   */
    public static function mCheckSsn(cDb &$cDb, $sLogin, $sHash, $iTimeOutInSec, $bRemember = false){
      $iTimeOutInSec = (int)$iTimeOutInSec;
      if($iTimeOutInSec < 60){$iTimeOutInSec = 60;}

      if(($cCont = cCont::mUnserialize($cDb, (string)$sLogin))
      && $cCont->bDisabled() === false
      && ($sPwd = $cDb->sQueryRes('select ' . FN_S_PWD
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_I_ID . '=' . $cCont->iID()))
      && ($sUID = $cDb->sQueryRes('select ' . FN_S_UIDS
          . ' from ' . $cDb->sTablePrefix() . static::TABLE_SSN
          . ' where ' . FN_I_PRT . '=' . $cCont->iID()
          . ' and TIMESTAMPDIFF(SECOND, ' . FN_DT_S . ', now())<' . $iTimeOutInSec))){
      
        //а вот теперь проверим
        if(preg_match('/^[a-f0-9]{64}$/i', $sPwd)//есть пароль
        && preg_match('/^[a-f0-9-]{36}$/i', $sUID)//начата сессия
        && $sHash === sha1($sUID . $sPwd)){//пароль верный
          //продлим сессию
          $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE_SSN
          . ' set ' . FN_DT_S . '=now() where ' . FN_I_PRT . '=' . $cCont->iID());
          if($bRemember){
            $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE
            . ' set ' . FN_DT_S . '=now() where ' . FN_I_ID . '=' . $cCont->iID());
          }
          return $cCont;
        }
      }
      unset($cCont);
      return false;
    }
  /**
   * last login
   */
    public function mLastLogin(cDb &$cDb){
      $iDateTime = (int)$cDb->sQueryRes('select UNIX_TIMESTAMP(' . FN_DT_S
          . ') from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_I_ID . '=' . $this->iID());
      return ($iDateTime === 0 ? false : $iDateTime);
    }
    
    public function bIsOnline(cDb &$cDb, $iTimeOutInSec){
      $iTimeOutInSec = (int)$iTimeOutInSec;
      if($iTimeOutInSec < 60){$iTimeOutInSec = 60;}
      
      $iOnline = (int)$cDb->sQueryRes('select count(*) from '
          . $cDb->sTablePrefix() . static::TABLE_SSN
          . ' where ' . FN_I_PRT . '=' . $this->iID()
          . ' and TIMESTAMPDIFF(SECOND, ' . FN_DT_S . ', now())<' . $iTimeOutInSec);
      return ($iOnline > 0);
    }
  /**
   * log off
   */
    public function CloseSsn(cDb &$cDb){
      $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE_SSN
            . ' where ' . FN_I_PRT . '=' . $this->iID());
    }
  /**
   * free old sesion in cron job
   * @param int iTimeOutInSec - timeout session
   */
    public static function DeleteOldSsn(cDb &$cDb, $iTimeOutInSec){
      $iTimeOutInSec = (int)$iTimeOutInSec;
      if($iTimeOutInSec < 60){$iTimeOutInSec = 60;}
      $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE_SSN
        . ' where TIMESTAMPDIFF(SECOND, ' . FN_DT_S . ', now())>' . $iTimeOutInSec);
    }
  }


}
