<?php
/*
 * report.php (part of WTS) - core class cReport
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('cont.php');

  /**
   * cReport - отчёты
   */
  class cReport extends cCollectionBase{
    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithFilter;
    use tWithStr;
    
    const TABLE = 'reports';
    
    const RPL_EXP = array(//replaceable expressions
      'CONT_ID'  => '%CONTACTID%',
      'GROUP_ID'  => '%GROUPID%',
      'QUEUE_ID'  => '%QUEUEID%',
    );
    
    public static function mReplExpUiName($sKey){
      $sKey = (string)$sKey;
      $s = false;
      switch($sKey){
        case 'CONT_ID': $s = _('Current Contact');
        break;
        case 'GROUP_ID': $s = _('Group of Current Contact');
        break;
        case 'QUEUE_ID': $s = _('Queue of Current Contact');
        break;
        default: break;
      }
      return $s;
    }
    
    public function Install(cDb &$cDb){
      $aMap[FN_I_ID]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']   = true;
      $aMap[FN_I_TYP]['def'] = 'int unsigned not null';//for: CSTMR or AGENT @see cCont
      $aMap[FN_S_CPB]['def'] = 'char(3)';// @see cRole
      $aMap[FN_S_NM]['def']  = 'char(255) not null';
      $aMap[FN_S_NM]['uniq'] = true;
      $aMap[FN_S_VAL]['def'] = 'blob not null';//max size 64 kb - 1b = 65535 b

      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, true);
    }
    
    public function iType(){return $this->aMap[FN_I_TYP]['val'];}
    public function sCapability(){return $this->aMap[FN_S_CPB]['val'];}
    public function Capability($sName){$this->Str(FN_S_CPB, $sName);}
    public function sName(){return $this->aMap[FN_S_NM]['val'];}
    public function Name($sName){$this->Str(FN_S_NM, $sName);}
    public function sValue(){return $this->aMap[FN_S_VAL]['val'];}
    public function Value($sValue){//is sql query?
      if(isset($sValue) && strlen($sValue) > strlen('select')){
        $iPos = stripos($sValue, 'select ');
        //без from отчёты не сохраняем - это минимельная проверка
        if($iPos !== false){$iPos = stripos($sValue, ' from ', $iPos);}
        if($iPos !== false){
          $this->aMap[FN_S_VAL]['val'] = $sValue;
          $this->aMap[FN_S_VAL]['mod'] = true;
        }
      }
    }
    
    public function __construct($iType = 0){
      $this->__twi_construct();
      
      
      $iType = (int)$iType;
      $this->aMap[FN_I_TYP]['val'] = ($iType == cCont::CSTMR || $iType == cCont::AGENT) ? $iType : 0;
      
      $this->InitStr(FN_S_CPB);
      $this->InitStr(FN_S_NM);
      $this->InitStr(FN_S_VAL);
    }
    
    public static function mUnserialize(cDb &$cDb, $iID){
      $cObj = false;
      $iID = (int)$iID;
      if($iID > 0){
        $s = 'select ' . FN_I_TYP . ', '
                       . FN_S_CPB . ', '
                       . FN_S_NM . ', '
                       . FN_S_VAL
        . ' from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $iID;
        $cDb->QueryRes($s);
        if($cDb->iRowCount() > 0){
          $aRow = $cDb->aRow();
          $cObj = new cReport((int)$aRow[0]);
          $cObj->aMap[FN_I_ID]['val']  = $iID;
          $cObj->aMap[FN_S_CPB]['val'] = $aRow[1];
          $cObj->aMap[FN_S_NM]['val']  = $aRow[2];
          $cObj->aMap[FN_S_VAL]['val'] = $aRow[3];
          $cDb->FreeResult();
        }
      }
      return $cObj;
    }
    
    public static function iUnserialize(cDb &$cDb, &$aObjs){
      $iCount   = 0;

      $s = 'select ' . FN_I_ID . ', '
                     . FN_I_TYP . ', '
                     . FN_S_CPB . ', '
                     . FN_S_NM . ', '
                     . FN_S_VAL
      . ' from ' . $cDb->sTablePrefix() . static::TABLE;
      if(static::$sWhere !== false){
        $s .= ' where ' . static::$sWhere;
      }
      $s .= ' order by ' . FN_S_NM;
      
      $cDb->QueryRes($s);

      if(($iCount = $cDb->iRowCount()) > 0){
        while($aRow = $cDb->aRow()){
          $cObj = new cReport((int)$aRow[1]);
          $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
          $cObj->aMap[FN_S_CPB]['val'] = $aRow[2];
          $cObj->aMap[FN_S_NM]['val']  = $aRow[3];
          $cObj->aMap[FN_S_VAL]['val'] = $aRow[4];
          
          
          $aObjs[] = $cObj;
        }
        $cDb->FreeResult();
      }
      
      return $iCount;
    }

    public function bSerialize(cDb &$cDb){
      $bRet = false;
      if(!$this->iID()){
        if($this->iType()
        && $this->aMap[FN_S_NM]['mod']
        && $this->aMap[FN_S_VAL]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{
            $aMap[FN_I_ID] = $this->aMap[FN_I_ID]['val'] = 1 + (int)$cDb->sQueryRes('select max(' . FN_I_ID . ') from ' . $cDb->sTablePrefix() . static::TABLE);
                
            $aMap[FN_I_TYP] = $this->iType();
            
            if($this->sCapability()){
              $aMap[FN_S_CPB] = '\'' . $cDb->sShield($this->sCapability()) . '\'';
            }
            
            $aMap[FN_S_NM]  = '\'' . $cDb->sShield($this->sName()) . '\'';
            $aMap[FN_S_VAL] = '\'' . $cDb->sShield($this->sValue()) . '\'';

            $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
            $bRet = true;
          }
        }
      }
      else{
        if($this->aMap[FN_S_VAL]['mod']){
          $aMap[FN_S_VAL] = '\'' . $cDb->sShield($this->sValue()) . '\'';
          $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
          $bRet = true;
        }
      }
      return $bRet;
    }
    
    public static function bAddFilterType($iType){
      $iType = (int)$iType;
      if($iType === cCont::CSTMR || $iType === cCont::AGENT){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= FN_I_TYP . '=' . $iType;
        return true;
      }
      return false;
    }
    
    public static function bAddFilterCapability($mCapability){
      $aMap = false;
      if(is_array($mCapability)){
        foreach($mCapability as $sCapability){
          if(is_string($sCapability) && strlen($sCapability) == 3){// @see cRole
            $aMap[] = '\'' . $cDb->sShield($sCapability) . '\'';
          }
        }
      }
      if($aMap){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        if(count($aMap) > 1){
          static::$sWhere .= FN_S_CPB . ' in(';
          static::$sWhere .= implode(', ', $aMap);
          static::$sWhere .= ')';
        }
        else{
          static::$sWhere .= FN_S_CPB . '=' . $aMap[0];
        }
        return true;
      }
      return false;
    }
    
    public function bDelete(cDb &$cDb){
      if($this->iID()){
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $this->iID());
        $this->aMap[FN_I_ID]['val'] = 0;
        return true;
      }
      return false;
    }

  }


}
