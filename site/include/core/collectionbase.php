<?php
/*
 * collectionbase.php (part of WTS) - core class cCollectionBase
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  //field db names - mapping
  define('FN_B_MTP',           'b_is_multiple');//for single/multiple attributes
  define('FN_B_DSB',           'b_is_disabled');//for contacts/queues
  define('FN_B_INN',           'b_in_name');//for contact attributes
  define('FN_B_PRC',           'b_is_processed');//for events, instead of is_sent_or_readed
  define('FN_B_CLS',           'b_is_closed');//for chains
  define('FN_B_N',             'b_is_node');//for categories
  
  define('FN_DT',              'dt_');
  define('FN_DT_U',            'dt_updated');
  define('FN_DT_B',            'dt_begin');
  define('FN_DT_E',            'dt_end');
  define('FN_DT_S',            'dt_ssn');
  
  define('FN_I_ID',            'i_id');
  define('FN_I_SLR',           'i_seller_id');
  define('FN_I_SLRT',          'i_seller_type_id');
  define('FN_I_BR',            'i_buyer_id');
  define('FN_I_BRT',           'i_buyer_type_id');
  define('FN_I_PRT',           'i_parent_id');//also creator in - cont, group, chain, contract
  define('FN_I_OWN',           'i_owner_id'); //also updater, also responsible in - cont, group, chain, contract
  define('FN_I_HLD',           'i_holder_id');//in product
  define('FN_I_HLDT',          'i_holder_type_id');//in product
  define('FN_I_TYP',           'i_type_id');
  define('FN_I_CAT',           'i_category_id');
  define('FN_I_ORD',           'i_order_id');
  define('FN_I_LVL',           'i_level_id');
  define('FN_I_CNT',           'i_cont_id');
  define('FN_I_GRP',           'i_group_id');
  define('FN_I_PDT',           'i_product_id');
  define('FN_I_CTR',           'i_contr_id');
  define('FN_I_QUE',           'i_queue_id');
  define('FN_I_RSP',           'i_response_id');
  define('FN_I_CHN',           'i_chain_id');
  define('FN_I_SLT',           'i_salutation_id');
  define('FN_I_SGN',           'i_signature_id');
  define('FN_I_TAG',           'i_tag_id');
  define('FN_I_IPRT',          'i_imap_port');
  define('FN_I_IENC',          'i_imap_encription');
  define('FN_I_SPRT',          'i_smtp_port');
  define('FN_I_SENC',          'i_smtp_encription');
  define('FN_I_CST',           'i_cost');
  
  define('FN_S_NM',            's_name');//in: attr, group, queue, prod_catalog, prod_category, (regexp, response) - in queue, report, role, salutation, sheme, tag, upload, attach
  define('FN_S_VAL',           's_value');
  define('FN_S_CLR',           's_color');
  define('FN_S_CPB',           's_capability');
  define('FN_S_PLN',           's_plain'); //plain text in messages body
  define('FN_S_PRV',           's_preview');
  define('FN_S_FRM',           's_from');
  define('FN_S_SBJ',           's_subject');
  define('FN_S_LGN',           's_login');
  define('FN_S_PWD',           's_passwd');
  define('FN_S_UID',           's_uid');
  define('FN_S_CID',           's_cid');
  define('FN_S_UIDS',          's_uid_ssn');
  define('FN_S_SL',            's_slug');
  define('FN_S_BT',            's_body_table');
  define('FN_S_AT',            's_attach_table');
  define('FN_S_SLGN',          's_smtp_login');
  define('FN_S_SHST',          's_smtp_host');
  define('FN_S_ILGN',          's_imap_login');
  define('FN_S_IHST',          's_imap_host');

  /**
   * cCollectionBase - класс для работы с таблицей в СУБД
   */
  abstract class cCollectionBase{
    
    protected $aMap; //mapping table column to var in class
    
    protected function InitInt($sFN){
      $this->aMap[$sFN]['val'] = 0;
      $this->aMap[$sFN]['mod'] = false;
    }
    
    protected function InitStr($sFN){
      $this->aMap[$sFN]['val'] = null;
      $this->aMap[$sFN]['mod'] = false;
    }
    
    protected function CreateTable(cDb &$cDb, &$aMap, $sTable, $bDynamic = false, $sEngine = 'MyISAM'){
      $s1 = '';
      $s2 = 'drop table if exists ' . $sTable;
      $s2 .= '; create table ' . $sTable . ' (';
      $b = false;
      foreach($aMap as $key => $a){
        if($b){$s2 .= ', ';}
        else{$b = true;}
        $s2 .= $key . ' ' . $a['def'];
        
        if(isset($a['pk'])){$s1 .= '; alter table ' . $sTable . ' add primary key (' . $key . ')';}
        if(isset($a['ai'])){$s1 .= '; alter table ' . $sTable . ' modify ' . $key . ' ' . $a['def'] . ' auto_increment';}
        if(isset($a['uniq'])){$s1 .= '; alter table ' . $sTable . ' add unique (' . $key . ')';}
        if(isset($a['idx'])){$s1 .= '; create index idx_' . $key. ' on ' . $sTable . ' (' . $a['idx'] . ')';}
        if(isset($a['uidx'])){$s1 .= '; create unique index idx_' . $key. ' on ' . $sTable . ' (' . $a['uidx'] . ')';}
      }
      $s2 .= ') engine=' . $sEngine;
      if($bDynamic){$s2 .= ' ROW_FORMAT=DYNAMIC';}

      $cDb->MultiQuery($s2 . $s1);
    }
    
    protected function Update(cDb &$cDb, &$aMap, $sTable, $sWhere){
      $s = 'update ' . $sTable . ' set ';
      $b = false;
      foreach($aMap as $key => &$val){
        if($b){$s .= ', ';}
        else{$b = true;}
        $s .= $key . '=' . $val;
      }
      $cDb->Query($s . ' where ' . $sWhere);
    }
    
    protected function Insert(cDb &$cDb, &$aMap, $sTable){
      $s1 = 'insert into ' . $sTable . ' (';
      $s2 = ') values (';
      $b = false;
      foreach($aMap as $key => &$val){
        if($b){$s1 .= ', '; $s2 .= ', '; }
        else{$b = true;}
        $s1 .= $key;
        $s2 .= $val;
      }
      $cDb->Query($s1 . $s2 . ')');
    }

    //abstract public function Install(cDb &$cDb);

    //if have changes - save entity or create entity
    //abstract public function bSerialize(cDb &$cDb, $iAuthorID);
  }


}
