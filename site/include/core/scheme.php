<?php
/*
 * scheme.php (part of WTS) - core class cScheme
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('collectionbase.php');

  /**
   * cScheme - класс для хранения текущих имён таблиц с данными
   * можно добавлять свои значения в таблицу схемы - они будут проигнорированны классом
   */
  class cScheme extends cCollectionBase{
    
    const TABLE = 'scheme';
    
    const ROWS = array(
      'VER' => 'scheme_ver',
      'EBT' => 'events_body_table',          //use prefix $cDb->sTablePrefix() . evt_b_
      'EAT' => 'events_attachment_table',    //use prefix $cDb->sTablePrefix() . evt_a_
      'CAT' => 'contracts_attachment_table', //use prefix $cDb->sTablePrefix() . ctr_a_
    );

    public function Install(cDb &$cDb){
      $aMap[FN_S_NM]['def']  = 'char(31) not null';
      $aMap[FN_S_NM]['uniq'] = true;
      $aMap[FN_S_VAL]['def'] = 'char(63) not null';
      $aMap[FN_DT]['def']    = 'datetime not null';
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
    }
    
    public function Value($sName, $sValue){
      $sName = (string)$sName;
      $sValue = (string)$sValue;

      if(strlen($sName) > 0
      && strlen($sValue) > 0
      && array_key_exists($sName, static::ROWS)
      && $this->aMap[$sName]['val'] != $sValue){
        $this->aMap[$sName]['val'] = $sValue;
        $this->aMap[$sName]['mod'] = true;
      }
    }
    
    public function mValue($sName){
      $bRet = false;
      $sName = (string)$sName;
      if(strlen($sName) > 0
      && array_key_exists($sName, $this->aMap)){
        $bRet = $this->aMap[$sName]['val'];
      }
      return $bRet;
    }
    
    public function __construct(){

      foreach(static::ROWS as $sKey => $sRow){
        $this->aMap[$sKey]['val'] = $this->aMap[$sKey]['mod'] = false;
      }
    }

    public static function mUnserialize(cDb &$cDb){
      $cObj = false;
      $cDb->QueryRes('select ' . FN_S_NM . ', ' . FN_S_VAL
      . ' from ' . $cDb->sTablePrefix() . static::TABLE);
      if($cDb->iRowCount() > 0){
        $cObj = new cScheme();
        while($aRow = $cDb->aRow()){
          if(strlen($aRow[1]) > 0 &&
          ($sKey = array_search($aRow[0], static::ROWS))){
            $cObj->aMap[$sKey]['val'] = $aRow[1];
          }
        }
        $cDb->FreeResult();
      }
      return $cObj;
    }
    
    public function bSerialize(cDb &$cDb){
      $s = '';
      $i = 0;
      foreach($this->aMap as $key => &$a){
        if($a['mod']){
          if($i > 0){$s .= '; ';}
          $i++;
          //check if value exist
          $cDb->QueryRes('select ' . FN_S_VAL
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_NM . '=\'' . $cDb->sShield(static::ROWS[$key]) . '\'');
          if($cDb->iRowCount() > 0){
            $cDb->FreeResult();
            $s .= 'update ' . $cDb->sTablePrefix() . static::TABLE
            . ' set ' . FN_S_VAL . '=\'' . $cDb->sShield($a['val']) . '\', '
            . FN_DT . '=now() where ' . FN_S_NM
            . '=\'' . $cDb->sShield(static::ROWS[$key]) . '\'';
          }
          else{
            $s .= 'insert into ' . $cDb->sTablePrefix() . static::TABLE
            . ' (' . FN_S_NM . ', ' . FN_S_VAL . ', ' . FN_DT
            . ') values (\'' . $cDb->sShield(static::ROWS[$key])
            . '\', \'' . $cDb->sShield($a['val']) . '\', now())';
          }
          $a['mod'] = false;
        }
      }
      if($i > 0){
        if($i > 1){$cDb->MultiQuery($s);}
        else{$cDb->Query($s);}
        return true;
      }
      return false;
    }
  }


}
