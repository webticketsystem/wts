<?php
/*
 * contattrtype.php (part of WTS) - core class cContAttrType
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('groupattrtype.php');

  /**
   * cContAttrType - класс типов атрибутов для контактов
   * @see cGroupAttrType
   * 
   * @var bool bInName - отражает использовании атрибута при генерации имени контакта
   */
  class cContAttrType extends cGroupAttrType{
    
    const TABLE = 'conts_attrs_types';
    
    public function Install(cDb &$cDb){
      $aMap[FN_I_ID]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']   = true;
      $aMap[FN_S_NM]['def']  = 'char(255) not null';
      $aMap[FN_S_NM]['uniq'] = true;
      $aMap[FN_S_CLR]['def'] = 'char(6)';
      $aMap[FN_B_MTP]['def'] = 'bit not null default 0';
      $aMap[FN_B_INN]['def'] = 'bit not null default 0';
      $aMap[FN_I_ORD]['def'] = 'int unsigned not null default 1';

      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
    }
    
    public function bInName(){return $this->aMap[FN_B_INN]['val'];}

    public function InName($bInName){
      $bInName = (bool)$bInName;
      if($this->aMap[FN_B_INN]['val'] !== $bInName){
        $this->aMap[FN_B_INN]['val'] = $bInName;
        $this->aMap[FN_B_INN]['mod'] = true;
      }
    }

    public function __construct($bMultiple = false){
      parent::__construct($bMultiple);
      $this->aMap[FN_B_INN]['val'] = $this->aMap[FN_B_INN]['mod'] = false;
    }
    
    public static function iUnserialize(cDb &$cDb, &$aObjs){
      $s = 'select ' . FN_I_ID . ', '
            . FN_S_NM . ', '
            . FN_S_CLR . ', '
            . FN_B_MTP . ', '
            . FN_B_INN
            . ' from ' . $cDb->sTablePrefix() . static::TABLE
            . ' order by ' . FN_I_ORD;
        
      $cDb->QueryRes($s);
      
      $iCount = $cDb->iRowCount();
      if($iCount > 0){
        while($aRow = $cDb->aRow()){
          $cObj = new cContAttrType((bool)$aRow[3]);
          $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
          $cObj->aMap[FN_S_NM]['val']  = $aRow[1];
          $cObj->aMap[FN_S_CLR]['val'] = $aRow[2];
          $cObj->aMap[FN_B_INN]['val'] = (bool)$aRow[4];
          $aObjs[] = $cObj;
        }
        $cDb->FreeResult();
      }
      return $iCount;
    }
    
    public function bSerialize(cDb &$cDb){
      $bRet = false;
      if($this->iID()){//try to save modified
        $aMap;
        if($this->aMap[FN_S_NM]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{$aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';}
        }
        if($this->aMap[FN_S_CLR]['mod']){
          $aMap[FN_S_CLR] = ($this->sColor() ? '\'' . $cDb->sShield($this->sColor()) . '\'' : 'null');
        }
        if($this->aMap[FN_B_INN]['mod']){
          $aMap[FN_B_INN] = ($this->bInName() ? 'true' : 'false');
        }
        
        if(isset($aMap)){
          $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
          $bRet = true;
        }
      }//$this->iID() != null
      else{//try to insert new
        if(!$this->sName()){$this->sRandName();}
        $cDb->QueryRes('select ' . FN_I_ID . ' from ' . $cDb->sTablePrefix() . static::TABLE . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\'');
        if($cDb->iRowCount() > 0){$cDb->FreeResult();}
        else{
          $aMap[FN_I_ID] = $this->aMap[FN_I_ID]['val'] = 1 + (int)$cDb->sQueryRes('select max(' . FN_I_ID . ') from ' . $cDb->sTablePrefix() . static::TABLE);
          $aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';
          if($this->aMap[FN_S_CLR]['mod']){
            $aMap[FN_S_CLR] = ($this->sColor() ? '\'' . $cDb->sShield($this->sColor()) . '\'' : 'null');
          }
          if($this->bMultiple()){$aMap[FN_B_MTP] = 'true';}
          if($this->bInName()){$aMap[FN_B_INN] = 'true';}
          $aMap[FN_I_ORD] = 1 + (int)$cDb->sQueryRes('select max(' . FN_I_ORD . ') from ' . $cDb->sTablePrefix() . static::TABLE);
          
          $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
          $bRet = true;
        }
      }
      if($bRet){foreach($this->aMap as &$a){
        if(isset($a['mod']) && $a['mod'])$a['mod'] = false;}
      }
      return $bRet;
    }
  }

}
