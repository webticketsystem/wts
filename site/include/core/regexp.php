<?php
/*
 * regexp.php (part of WTS) - core class cRegExp
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('collectionbase.php');
  require_once('withid.php');

  /**
   * cRegExp - класс регулярных выражений для очередей
   * 
   * sample: Ваша заявка принята
   * 
   * у очереди администратора нет регулярных выражений
   */
  class cRegExp extends cCollectionBase{
    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithStr;
    
    const TABLE = 'regexps';
    
    public function Install(cDb &$cDb){
      
      $aMap[FN_I_ID]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']   = true;
      $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null';//for queue
      $aMap[FN_I_PRT]['idx'] = FN_I_PRT;
      $aMap[FN_S_NM]['def']  = 'char(255) not null';
      $aMap[FN_S_VAL]['def'] = 'text not null';

      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, true);
    }
    
    public function iParent(){return $this->aMap[FN_I_PRT]['val'];}
    public function sName(){return $this->aMap[FN_S_NM]['val'];}
    public function Name($sName){$this->Str(FN_S_NM, $sName);}
    public function sValue(){return $this->aMap[FN_S_VAL]['val'];}
    //sValue can have html tags
    public function Value($sValue){$this->Str(FN_S_VAL, $sValue);}
    
    public function __construct($iParent = 0){
      $this->__twi_construct();
      
      $iParent = (int)$iParent;
      $this->aMap[FN_I_PRT]['val'] = ($iParent > 1 ? $iParent : 0);

      $this->InitStr(FN_S_NM);
      $this->InitStr(FN_S_VAL);
    }
    
    public static function iUnserialize(cDb &$cDb, $iParent, &$aObjs){
      $iCount = 0;
      $iParent = (int)$iParent;
      if($iParent > 1){
        $s = 'select ' . FN_I_ID . ', '
            . FN_S_NM . ', '
            . FN_S_VAL
            . ' from ' . $cDb->sTablePrefix() . static::TABLE
            . ' where ' . FN_I_PRT . '=' . $iParent
            . ' order by ' . FN_S_NM;
        
        $cDb->QueryRes($s);
        
        if(($iCount = $cDb->iRowCount()) > 0){
          while($aRow = $cDb->aRow()){
            $cObj = new cRegExp($iParent);
            $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
            $cObj->aMap[FN_S_NM]['val']  = $aRow[1];
            $cObj->aMap[FN_S_VAL]['val'] = $aRow[2];
            $aObjs[] = $cObj;
          }
          $cDb->FreeResult();
        }
      }
      return $iCount;
    }
    
    public function bSerialize(cDb &$cDb){
      $bRet = false;
      if($this->iID()){//try to save modified
        if($this->aMap[FN_S_VAL]['mod']){
          $aMap[FN_S_VAL] = '\'' . $cDb->sShield($this->sValue()) . '\'';
          $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
          $bRet = true;
        }
      }
      else{//try to insert new
        if($this->iParent() > 1
        && $this->aMap[FN_S_NM]['mod']
        && $this->aMap[FN_S_VAL]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID
            . ' from ' . $cDb->sTablePrefix() . static::TABLE
            . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName())
            . '\' and ' . FN_I_PRT . '=' . $this->iParent());
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{
            $aMap[FN_I_ID] = $this->aMap[FN_I_ID]['val'] = 1 + (int)$cDb->sQueryRes('select max(' . FN_I_ID . ') from ' . $cDb->sTablePrefix() . static::TABLE);
            $aMap[FN_I_PRT] = $this->iParent();
            $aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';
            $aMap[FN_S_VAL] = '\'' . $cDb->sShield($this->sValue()) . '\'';
            $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
            $bRet = true;
          }
        }
      }
      if($bRet){foreach($this->aMap as &$a){
        if(isset($a['mod']) && $a['mod'])$a['mod'] = false;}
      }
      return $bRet;
    }
    
    public function bDelete(cDb &$cDb){
      if($this->iID()){
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $this->iID());
        $this->aMap[FN_I_ID]['val'] = 0;
        return true;
      }
      return false;
    }
  }


}
