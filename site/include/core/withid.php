<?php
/*
 * withid.php (part of WTS) - trait for core classes tWithID
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  trait tWithID{
    
    public function __construct($iID = 0){
      $iID = (int)$iID;
      $this->aMap[FN_I_ID]['val'] = ($iID > 0 ? $iID : 0);
    }
    
    public function iID(){return $this->aMap[FN_I_ID]['val'];}
    
    public static function &mInArray(&$aObjs, $iID){
      $cRef = false;
      $iID = (int)$iID;
      if($iID > 0 && isset($aObjs) && count($aObjs) > 0){
        foreach($aObjs as &$cObj){
          if($cObj->iID() === $iID){
            $cRef = $cObj;
          }
        }
      }
      return $cRef;
    }
    
  /**
   * сортировка по id - рекурсия
   */
    public static function aQuicksort($aObjs){
      if(count($aObjs) < 2){return $aObjs;}
      $aLeft = $aRight = array( );
      $cPivot = array_pop($aObjs);
      foreach($aObjs as &$cObj){
        if($cObj->iID() < $cPivot->iID()){$aLeft[$cObj->iID()] = $cObj;}
        else{$aRight[$cObj->iID()] = $cObj;}
      }
      return static::aQuicksort($aLeft) + array($cPivot->iID() => $cPivot) + static::aQuicksort($aRight);
    }
  }


}
