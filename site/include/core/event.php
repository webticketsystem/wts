<?php
/*
 * event.php (part of WTS) - core class cEvent
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('utils/hypertext.php');
  require_once('collectionbase.php');
  require_once('withfilter.php');
  require_once('withattachs.php');
  require_once('withid.php');
  require_once('withdateint.php');
  require_once('withstr.php');
  require_once('withemail.php');
  require_once('scheme.php');

  /**
   * cEvent - события
   * 
   * can not be modified after serialize - only set is_sent_or_readed true or change chain
   */
  class cEvent extends cCollectionBase{
    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithDateInt{tWithDateInt::__construct as private __twd_construct;}
    use tWithFilter;
    use tWithAttachs{tWithAttachs::__construct as private __twa_construct;}
    use tWithStr;
    use tWithEmail;

    
    const TABLE = 'events';
    
    const IN_EMAIL  = 10;
    const OUT_EMAIL = 20;
    const IN_CALL   = 30;
    const OUT_CALL  = 40;
    const MEETING   = 50;

    //////{tWithLock - тут храним временную блокировку - используем когда пересылаем или создаём цепочку
    const TABLE_LOCK = self::TABLE . '_locks';
    
    protected $aLock;
    
    public function InstallLock(cDb &$cDb){
      $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null';
      $aMap[FN_I_PRT]['pk']  = true;
      $aMap[FN_DT]['def']    = 'datetime not null';
      $aMap[FN_I_CNT]['def'] = 'bigint unsigned not null';//хозяин блокировки
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE_LOCK);
    }
    
    public function iLockOwner(){return $this->aLock[FN_I_CNT];}
    public function Lock($iCont){
      $iCont = (int)$iCont;
      if($this->iID()
      && $iCont > 0
      && $this->aLock[FN_I_CNT] == 0){
        $this->aLock[FN_I_CNT] = $iCont;
        $this->aLock['mod'] = true;
      }
    }
    
    public function bUnserializeLock(cDb &$cDb){
      $bRet = false;
      if($this->iID()){
        $cDb->QueryRes('select UNIX_TIMESTAMP(' . FN_DT . '), ' . FN_I_CNT
        . ' from ' . $cDb->sTablePrefix() . static::TABLE_LOCK
        . ' where ' . FN_I_PRT . '=' . $this->iID());
        if($cDb->iRowCount() > 0){
          $aRow = $cDb->aRow();
          $this->aLock[FN_DT]    = (int)$aRow[0];
          $this->aLock[FN_I_CNT] = (int)$aRow[1];
          $cDb->FreeResult();
          $bRet = true;
        }
      }
      return $bRet;
    }
    
    public function bSerializeLock(cDb &$cDb){
      if($this->aLock['mod']
      && $this->iID()
      && (int)$cDb->sQueryRes('select count(*) from '
        . $cDb->sTablePrefix() . static::TABLE_LOCK
        . ' where ' . FN_I_PRT . '=' . $this->iID()) === 0){

        $cDb->Query('insert into ' . $cDb->sTablePrefix() . static::TABLE_LOCK
        . ' (' . FN_I_PRT . ', ' . FN_DT . ', ' . FN_I_CNT
        . ') values (' . $this->iID() . ', now(), ' . $this->iLockOwner() . ')');
        $this->aLock['mod'] = false;
        return true;
      }
      return false;
    }

  /**
   * free old locks in cron job
   * @param int iTimeOutInSec - timeout lock = timeout session
   */
    public static function DeleteOldLocks(cDb &$cDb, $iTimeOutInSec){
      $iTimeOutInSec = (int)$iTimeOutInSec;
      if($iTimeOutInSec < 60){$iTimeOutInSec = 60;}
      $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE_LOCK
      . ' where TIMESTAMPDIFF(SECOND, ' . FN_DT . ', now())>' . $iTimeOutInSec);
    }
    //////}tWithLock
    
    //////{tWithAddr - тут храним адреса писем 
    const TABLE_ADDR = self::TABLE . '_addrs';
    
    protected $aAddrs;

    const TO  = 110;
    const CC  = 120;
    const BCC = 130;
    
    protected function InstallAddr(cDb &$cDb){
      $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null';
      $aMap[FN_I_PRT]['idx'] = FN_I_PRT;
      $aMap[FN_I_TYP]['def'] = 'int unsigned not null';
      $aMap[FN_S_VAL]['def'] = 'char(255) not null';
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE_ADDR);
    }
    
    public function aTo(){return $this->aAddrs[self::TO];}
    public function AddTo($sEmail){
      $sEmail = (string)$sEmail;
      $this->AddEmails(self::TO, $sEmail);
    }
    public function aCc(){return $this->aAddrs[self::CC];}
    public function AddCc($sEmail){
      $sEmail = (string)$sEmail;
      $this->AddEmails(self::CC, $sEmail);
    }
    public function aBcc(){return $this->aAddrs[self::BCC];}
    public function AddBcc($sEmail){
      $sEmail = (string)$sEmail;
      $this->AddEmails(self::BCC, $sEmail);
    }
    
    public function iUnserializeAddr(cDb &$cDb){
      $iCount = 0;
      if($this->iID()
      && count($this->aAddrs[self::TO]) === 0
      && count($this->aAddrs[self::CC]) === 0
      && count($this->aAddrs[self::BCC]) === 0){
        $cDb->QueryRes('select ' . FN_I_TYP . ', ' . FN_S_VAL
        . ' from ' . $cDb->sTablePrefix() . static::TABLE_ADDR
        . ' where ' . FN_I_PRT . '=' . $this->iID());
        if(($iCount = $cDb->iRowCount()) > 0){
          while($aRow = $cDb->aRow()){
            switch($aRow[0]){
              case self::TO:
                $this->aAddrs[self::TO][] = $aRow[1]; $iCount++;
              break;
              case self::CC:
                $this->aAddrs[self::CC][] = $aRow[1]; $iCount++;
              break;
              case self::BCC:
                $this->aAddrs[self::BCC][] = $aRow[1]; $iCount++;
              break;
              default: break;
            }
          }
          $cDb->FreeResult();
        }
      }
      return $iCount;
    }
    
    protected function SerializeAddr(cDb &$cDb){
      $aRow;
      $s1 = '(' . $this->iID() . ', ';
      if(count($this->aAddrs[self::TO]) > 0){
        foreach($this->aAddrs[self::TO] as $s2){$aRow[] = $s1 . self::TO . ', \'' . $cDb->sShield($s2) . '\')';}
      }
      if(count($this->aAddrs[self::CC]) > 0){
        foreach($this->aAddrs[self::CC] as $s2){$aRow[] = $s1 . self::CC . ', \'' . $cDb->sShield($s2) . '\')';}
      }
      if(count($this->aAddrs[self::BCC]) > 0){
        foreach($this->aAddrs[self::BCC] as $s2){$aRow[] = $s1 . self::BCC . ', \'' . $cDb->sShield($s2) . '\')';}
      }
      if(isset($aRow)){
        $s1 = 'insert into ' . $cDb->sTablePrefix() . static::TABLE_ADDR
        . ' (' . FN_I_PRT . ', ' . FN_I_TYP . ', ' . FN_S_VAL . ') values ';
        $b = false;
        foreach($aRow as $s2){
          if($b){$s1 .= ', ';}
          else{$b = true;}
          $s1 .= $s2;
        }
        $cDb->Query($s1);
      }
    }
    //add emails from string: a@a.ru; b@a.ru; a@b.ru, c@c.ru
    protected function AddEmails($iType, $sEmail){
      if(!$this->iID() && strlen($sEmail) > 0){
        $sEmail = str_replace(';', ',', $sEmail);
        $sEmail = strtolower($sEmail);
        $aAdrs = explode(',', $sEmail);
        foreach($aAdrs as $s){
          $s = trim($s);
          if(cString::bIsEmail($s)
          && !in_array($s, $this->aAddrs[self::TO])
          && !in_array($s, $this->aAddrs[self::CC])
          && !in_array($s, $this->aAddrs[self::BCC])){
            $this->aAddrs[$iType][] = $s;
          }
        }
      }
    }

    protected function DeleteAddr(cDb &$cDb){
      $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE_ADDR
      . ' where ' . FN_I_PRT . '=' . $this->iID());
    }
    //////}tWithAddr
    
    //////{tWithBody - тут храним тело писем
    protected $aBody;
  /**
   * Проверяйте размер таблицы и создавайте новую при необходимости
   */
    public function bInstallBody(cDb &$cDb, $sTable){
      $sTable = (string)$sTable;
      if(strlen($sTable) > 0){
        $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null';
        $aMap[FN_I_PRT]['pk']  = true;
        $aMap[FN_S_PLN]['def'] = 'text';//max size 64 kb
        $aMap[FN_S_VAL]['def'] = 'mediumblob not null';//max size 16 mb
        
        $this->CreateTable($cDb, $aMap, $sTable, true);
        return true;
      }
      return false;
    }
    
    //add body before serialize (sValue must be in html)
    //готовим preview сразу, чтобы сохранять event с preview (если оно есть)
    public function AddBody($sValue){
      $sValue = (string)$sValue;
      if(!$this->iID() && strlen($sValue) > 0 && !$this->aBody[FN_S_VAL]['mod']){
        //переносы только \n !
        $sValue = str_replace("\r\n", "\n", $sValue);
        $sValue = str_replace("\r", "\n", $sValue);
        
        //обрезка лишнего
        $aElements = false;
        cElement::iUnserialize('body', $sValue, $aElements);
        
        if($aElements !== false){
          $aBodyElements = $aElements[0]->mContent();
          if($aBodyElements){
            $sValue = $aBodyElements[0]->mContent();
          }
          foreach($aElements as $key => &$e){unset($aElements[$key]);}
          unset($aElements);
          $aElements = false;
        }
        if($sValue !== false){
          cElement::iUnserialize('style', $sValue, $aElements, true);
          cElement::iUnserialize('xml', $sValue, $aElements, true);
          cElement::iUnserialize('base', $sValue, $aElements, true);
          cElement::iUnserialize('script', $sValue, $aElements, true);
          
          if($aElements !== false){
            foreach($aElements as $key => &$e){unset($aElements[$key]);}
            unset($aElements);
            $aElements = false;
          }
        }
        
        if(strlen($sValue) > 0){//если все ещё есть буквы ... ыыы
          $this->aBody[FN_S_VAL]['val'] = $sValue;
          $this->aBody[FN_S_VAL]['mod'] = true;
          //простая текстовая форма - для поиска по телу события
          $sText = cElement::sToPlainText($sValue);
          
          if(strlen($sText) > 0){
            $this->aBody[FN_S_PLN]['val'] = $sText;
            $this->aBody[FN_S_PLN]['mod'] = true;
          }
        }
      }
    }
    //call after AddBody
    //48 - by experiment method
    public function GenPreview($sEncoding = 'UTF-8', $iPreviewLenght = 127, $iMaxChunk = 48){
      
      if($this->aBody[FN_S_PLN]['mod']){
      
        $sText = cString::sClear($this->aBody[FN_S_PLN]['val']);

        if(mb_strlen($sText, $sEncoding) > $iMaxChunk){
          $aWords = explode(' ', $sText);
          $iCount = count($aWords);
          for($i = 0; $i < $iCount; $i++) {
            //wery long link or world
            if(mb_strlen($aWords[$i], $sEncoding) > $iMaxChunk){
              $aWords[$i] = mb_substr($aWords[$i], 0, $iMaxChunk, $sEncoding) . '...';
            }
            if($i > 0){
              if(mb_strlen($sText . ' ' . $aWords[$i], $sEncoding) > $iPreviewLenght)break;
              $sText .= ' ' . $aWords[$i];
            }
            else{$sText = $aWords[$i];}
          }
        }
        $this->aMap[FN_S_PRV]['val'] = $sText;
        $this->aMap[FN_S_PRV]['mod'] = true;
      }

    }
    //for rebuild previews
    public function mBody($bPreview = false){
      $bPreview = (bool)$bPreview;
      $sIdx = ($bPreview ? FN_S_PLN : FN_S_VAL);
      if($this->aBody[$sIdx]['val'] !== null){
        return $this->aBody[$sIdx]['val'];
      }
      return false;
    }
    
    public function mUnserializeBody(cDb &$cDb, $bPlain = false){
      $bPlain = (bool)$bPlain;
      if($this->iID()){
        return $cDb->sQueryRes('select ' . ($bPlain ? FN_S_PLN : FN_S_VAL)
        . ' from ' . $this->sBodyTable()
        . ' where ' . FN_I_PRT . '=' . $this->iID());
      }
      return false;
    }
    
    protected function bSerializeBody(cDb &$cDb){
      if($this->iID()
      && $this->aBody[FN_S_VAL]['mod']){
        $aMap[FN_I_PRT] = $this->iID();
        
        if($this->aBody[FN_S_PLN]['mod']){
          $aMap[FN_S_PLN] = '\'' . $cDb->sShield($this->aBody[FN_S_PLN]['val']) . '\'';
        }
        
        $aMap[FN_S_VAL] = '\'' . $cDb->sShield($this->aBody[FN_S_VAL]['val']) . '\'';
        $this->Insert($cDb, $aMap, $this->sBodyTable());
        return true;
      }
      return false;
    }
    
    protected static function sFilterPartBodyValue($sTable, $sValue){
      $s = 'select ' . FN_I_PRT . ' from ' . $sTable
        . ' where ' . FN_S_PLN . ' like \'%' . $sValue . '%\'';
      return $s;
    }
    
    protected function DeleteBody(cDb &$cDb){
      $cDb->Query('delete from ' . $this->sBodyTable()
      . ' where ' . FN_I_PRT . '=' . $this->iID());
    }
    //////}tWithBody
    
    public function Install(cDb &$cDb, $sBodyTable, $sAttachTable){
      $aMap[FN_I_ID]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']   = true;
      $aMap[FN_I_ID]['ai']   = true;
      $aMap[FN_I_TYP]['def'] = 'int unsigned not null';
      $aMap[FN_I_QUE]['def'] = 'bigint unsigned not null';//очередь есть всегда
      $aMap[FN_I_CHN]['def'] = 'bigint unsigned';//если прикрепляем к цепочке
      $aMap[FN_S_FRM]['def'] = 'char(255) not null';
      $aMap[FN_S_SBJ]['def'] = 'char(255)';
      $aMap[FN_S_PRV]['def'] = 'char(255)';
      $aMap[FN_B_PRC]['def'] = 'bit not null default 0';
      $aMap[FN_S_BT]['def']  = 'char(127) not null';
      $aMap[FN_S_BT]['idx']  = FN_S_BT;
      $aMap[FN_S_AT]['def']  = 'char(127) not null';
      $aMap[FN_DT_B]['def']  = 'datetime not null';
      $aMap[FN_DT_E]['def']  = 'datetime not null';//for get work time
      $aMap[FN_I_OWN]['def'] = 'bigint unsigned';//если создано не системой


      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
      
      $this->InstallAddr($cDb);
      $this->InstallLock($cDb);
      //body and attach install
      $this->bInstallBody($cDb, $sBodyTable);
      $this->bInstallAttach($cDb, $sAttachTable);
       
    }
    
    public function iType(){return $this->aMap[FN_I_TYP]['val'];}
    public function iQueue(){return $this->aMap[FN_I_QUE]['val'];}
    public function iChain(){return $this->aMap[FN_I_CHN]['val'];}
  /**
   * Смена цепочки (attach/detach)
   *
   * !!! обнуляет принадлежность к цепочке, если она была
   */
    public function Chain($iChain){
      $iChain = (int)$iChain;
      if($iChain < 1){$iChain = 0;}
      if($this->aMap[FN_I_CHN]['val'] != $iChain){
        $this->aMap[FN_I_CHN]['val'] = $iChain;
        $this->aMap[FN_I_CHN]['mod'] = true;
        if($this->aMap[FN_B_PRC]['val']){//пометим непрочтённым
          $this->aMap[FN_B_PRC]['val'] = false;
          $this->aMap[FN_B_PRC]['mod'] = true;
        }
      }
    
    }
    public function sSubject(){return $this->aMap[FN_S_SBJ]['val'];}
    public function Subject($sSubject){$this->Str(FN_S_SBJ, $sSubject);}
    public function sPreview(){return $this->aMap[FN_S_PRV]['val'];}
    public function sFrom(){return $this->aMap[FN_S_FRM]['val'];}
    public function From($sFrom){$this->Email(FN_S_FRM, $sFrom);}
   
    public function bProcessed(){return $this->aMap[FN_B_PRC]['val'];}
    public function Processed(){
      if($this->aMap[FN_B_PRC]['val'] !== true){
        $this->aMap[FN_B_PRC]['mod'] = $this->aMap[FN_B_PRC]['val'] = true;
      }
    }
    public function sBodyTable(){return $this->aMap[FN_S_BT]['val'];}
    public function sAttachTable(){return $this->aMap[FN_S_AT]['val'];}
    
    public function iOwner(){return $this->aMap[FN_I_OWN]['val'];}

    public function __construct($iType, $iQueue){
      
      $this->__twa_construct();
      
      $this->aLock[FN_I_CNT] = 0;
      $this->aLock['mod'] = false;
      
      $this->aAddrs[self::TO]  = array();
      $this->aAddrs[self::CC]  = array();
      $this->aAddrs[self::BCC] = array();
      
      $this->aBody[FN_S_PLN]['val'] = $this->aBody[FN_S_VAL]['val'] = null;
      $this->aBody[FN_S_PLN]['mod'] = $this->aBody[FN_S_VAL]['mod'] = false;
      
      //self
      $this->__twi_construct();
      
      $iType = (int)$iType;
      if($iType === self::IN_EMAIL
      || $iType === self::OUT_EMAIL
      || $iType === self::IN_CALL
      || $iType === self::OUT_CALL
      || $iType === self::MEETING){
        $this->aMap[FN_I_TYP]['val'] = $iType;
      }
      else{$this->aMap[FN_I_TYP]['val'] = 0;}
      
      $iQueue = (int)$iQueue;
      $this->aMap[FN_I_QUE]['val'] = ($iQueue > 0 ? $iQueue : 0);
      
      $this->InitInt(FN_I_CHN);      
      
      $this->InitStr(FN_S_FRM);
      $this->InitStr(FN_S_SBJ);
      $this->InitStr(FN_S_PRV);
      
      $this->aMap[FN_B_PRC]['mod'] = $this->aMap[FN_B_PRC]['val'] = false;
      
      $this->InitStr(FN_S_BT);
      $this->InitStr(FN_S_AT);

      $this->__twd_construct();
  
      $this->aMap[FN_I_OWN]['val'] = 0;//управляется через bSerialize
    }
    
  /**
   * Восстанавливаем события из базы
   * 
   * !!! Проверяйте принадлежность пользователя к этому письму
   */
    public static function mUnserialize(cDb &$cDb, $iID){
      $cObj = false;
      $iID  = (int)$iID;
      if($iID > 0){
        $s = 'select ' . FN_I_TYP . ', '
                       . FN_I_QUE . ', '
                       . FN_I_CHN . ', '
                       . FN_S_FRM . ', '
                       . FN_S_SBJ . ', '
                       . FN_S_PRV . ', '
                       . FN_B_PRC . ', '
                       . FN_S_BT . ', '
                       . FN_S_AT . ', UNIX_TIMESTAMP('
                       . FN_DT_B . '), UNIX_TIMESTAMP('
                       . FN_DT_E . '), '
                       . FN_I_OWN
        . ' from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $iID;

        $cDb->QueryRes($s);
        if($cDb->iRowCount() > 0){
          $aRow = $cDb->aRow();
          $cObj = new cEvent($aRow[0], $aRow[1]);//проверить состояние?
          $cObj->aMap[FN_I_ID]['val']  = $iID;
          $cObj->aMap[FN_I_CHN]['val'] = (int)$aRow[2];
          $cObj->aMap[FN_S_FRM]['val'] = $aRow[3];
          $cObj->aMap[FN_S_SBJ]['val'] = $aRow[4];
          $cObj->aMap[FN_S_PRV]['val'] = $aRow[5];
          $cObj->aMap[FN_B_PRC]['val'] = (bool)$aRow[6];
          $cObj->aMap[FN_S_BT]['val']  = $aRow[7];
          $cObj->aMap[FN_S_AT]['val']  = $aRow[8];
          
          $cObj->aMap[FN_DT_B]['val']  = (int)$aRow[9];
          $cObj->aMap[FN_DT_E]['val']  = (int)$aRow[10];
          $cObj->aMap[FN_I_OWN]['val'] = (int)$aRow[11];
          
          $cDb->FreeResult();
        }
      }
      return $cObj;
    }
    
    public static function iUnserialize(cDb &$cDb, $iPage, $iPerPage, &$aObjs){
      $iCount   = 0;
      $iPage    = (int)$iPage;
      $iPerPage = (int)$iPerPage;
      if($iPage > 0 && $iPerPage > 0){
        
        $s = 'select ' . FN_I_ID . ', '
                       . FN_I_TYP . ', '
                       . FN_I_QUE . ', '
                       . FN_I_CHN . ', '
                       . FN_S_FRM . ', '
                       . FN_S_SBJ . ', '
                       . FN_S_PRV . ', '
                       . FN_B_PRC . ', '
                       . FN_S_BT . ', '
                       . FN_S_AT . ', UNIX_TIMESTAMP('
                       . FN_DT_B . '), UNIX_TIMESTAMP('
                       . FN_DT_E . '), '
                       . FN_I_OWN
        . ' from ' . $cDb->sTablePrefix() . static::TABLE;
        if(static::$sWhere !== false){
          $s .= ' where ' . static::$sWhere;
        }
        //$s .= ' order by ' . FN_S_NM . 
        $s .= ' order by ' . FN_I_ID . ' limit ' . (($iPage-1) * $iPerPage) . ', ' . $iPerPage;
        
        $cDb->QueryRes($s);
        
        if(($iCount = $cDb->iRowCount()) > 0){
          while($aRow = $cDb->aRow()){
            $cObj = new cEvent($aRow[1], $aRow[2]);//проверить состояние?
            $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
            $cObj->aMap[FN_I_CHN]['val'] = (int)$aRow[3];
            $cObj->aMap[FN_S_FRM]['val'] = $aRow[4];
            $cObj->aMap[FN_S_SBJ]['val'] = $aRow[5];
            $cObj->aMap[FN_S_PRV]['val'] = $aRow[6];
            $cObj->aMap[FN_B_PRC]['val'] = (bool)$aRow[7];
            $cObj->aMap[FN_S_BT]['val']  = $aRow[8];
            $cObj->aMap[FN_S_AT]['val']  = $aRow[9];
            
            $cObj->aMap[FN_DT_B]['val']  = (int)$aRow[10];
            $cObj->aMap[FN_DT_E]['val']  = (int)$aRow[11];
            $cObj->aMap[FN_I_OWN]['val'] = (int)$aRow[12];
            $aObjs[] = $cObj;
          }
          $cDb->FreeResult();
        }
      }
      return $iCount;
    }
    
    public function bSerialize(cDb &$cDb, $iThumbSide = 256, $iCont = 0){
      $bRet = false;
      $iCont = (int)$iCont;
      
      if($this->iID()){//try to save modified
        ///проверить не заблокировано ли - впрочем, эта задача интерфейса
        $aMap;
        if($this->aMap[FN_B_PRC]['mod']){
          $aMap[FN_B_PRC] = ($this->bProcessed() ? 'true' : 'false');
        }
        //исходящее нельзя открепить, можно только удалить, пока не отправлено
        if($this->aMap[FN_I_CHN]['mod'] && $this->iType() != self::OUT_EMAIL){
          $aMap[FN_I_CHN] = ($this->iChain() ? $this->iChain() : 'null');
        }

        //не меняем очередь - в цепочках могут быть события из
        //разных очередей, цепочки переводяться из очереди 
        //в очередь за счёт переназначения агента
        /*
        //изменим очередь, если не в цепочке и запишем ... а кто же это сделал
        if($iCont > 0
        && $this->aMap[FN_I_QUE]['mod']
        && !$this->aMap[FN_I_CHN]['mod']
        && !$this->iChain()){
          $aMap[FN_I_QUE] = $this->iQueue();
          if($iCont > 0){$aMap[FN_I_OWN] = $iCont;}
        }
        */
        if(isset($aMap)){
          $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
          $bRet = true;
        }
      }
      else{
        //try get table name
        if($cScheme = cScheme::mUnserialize($cDb)){
          if($sTable = $cScheme->mValue('EBT')){
            $this->aMap[FN_S_BT]['val'] = $sTable;
          }
          if($sTable = $cScheme->mValue('EAT')){
            $this->aMap[FN_S_AT]['val'] = $sTable;
          }
          unset($cScheme);
        }
        
        if($this->iQueue() > 0
        && $this->iType()
        && $this->sFrom()
        && $this->sBodyTable()
        && $this->sAttachTable()){
          
          $aMap[FN_I_TYP] = $this->iType();
          $aMap[FN_I_QUE] = $this->iQueue();
          if($this->iChain()){$aMap[FN_I_CHN] = $this->iChain();}
            
          $aMap[FN_S_FRM] = '\'' . $cDb->sShield($this->sFrom()) . '\'';
          if($this->sSubject()){
            $aMap[FN_S_SBJ] = '\'' . $cDb->sShield($this->sSubject()) . '\'';
          }
          if($this->sPreview()){
            $aMap[FN_S_PRV] = '\'' . $cDb->sShield($this->sPreview()) . '\'';
          }
          if($this->bProcessed()){$aMap[FN_B_PRC] = 'true';}
          $aMap[FN_S_BT] = '\'' . $cDb->sShield($this->sBodyTable()) . '\'';
          $aMap[FN_S_AT] = '\'' . $cDb->sShield($this->sAttachTable()) . '\'';
          
          if(!$this->aMap[FN_DT_B]['mod']){$this->DtBegin(strtotime('now'));}
          $aMap[FN_DT_B] = 'FROM_UNIXTIME(' . $this->iDtBegin() . ')';
          $aMap[FN_DT_E] = 'FROM_UNIXTIME(' . $this->iDtEnd() . ')';
          
          if($iCont > 0){$aMap[FN_I_OWN] = $iCont;}
          
          //проверить адреса, если исходящее письмо
          if(($this->iType() == self::OUT_EMAIL) && count($this->aTo()) == 0){unset($aMap);}
          //входящие могут быть какими угодно - лишь бы сохранить
          
          if(isset($aMap)){
            $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
            if($this->aMap[FN_I_ID]['val'] = (int)$cDb->mLastInsertID()){
              $this->SerializeAddr($cDb);
              $this->bSerializeBody($cDb);
              $this->iSerializeAttach($cDb, $iThumbSide);
              $bRet = true;
            }
          }
        }
      }
      if($bRet){
        foreach($this->aMap as &$a){
          if(isset($a['mod']) && $a['mod'])$a['mod'] = false;
        }
      }
      return $bRet;
    }
    
    public static function bAddFilterType($iType){
      $iType = (int)$iType;
        if($iType == self::IN_EMAIL || $iType == self::OUT_EMAIL
        || $iType == self::IN_CALL || $iType == self::OUT_CALL
        || $iType == self::MEETING){
          if(static::$sWhere !== false){static::$sWhere .= ' and ';}
          static::$sWhere .= FN_I_TYP . '=' . $iType;
          return true;
        }
      return false;
    }
    
    public static function bAddFilterQueue($iQueue){
      $iQueue = (int)$iQueue;
      if($iQueue > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= FN_I_QUE . '=' . $iQueue;
        return true;
      }
      return false;
    }
    
    public static function AddFilterChain($iChain){
      $iChain = (int)$iChain;
      if(static::$sWhere !== false){static::$sWhere .= ' and ';}
      static::$sWhere .= FN_I_CHN . ($iChain > 0 ? '=' . $iChain : ' is null');
      return ($iChain > 0);
    }
    
    public static function bAddFilterProcessed($bProcessed){
      $bProcessed = (bool)$bProcessed;
      if(static::$sWhere !== false){static::$sWhere .= ' and ';}
      static::$sWhere .= FN_B_PRC . '=' . ($bProcessed ? 'true' : 'false');
      return true;
    }
    
  /**
   * must return chain ids in string: 12, 48, 54  or false
   */
    public static function mFilterPartValue(cDb &$cDb, $sValue){
      $sQ = false;
      $sValue = (string)$sValue;
      
      if(strlen($sValue) > 0){
        $sValue = $cDb->sShield($sValue);
        //main Q
        $sMQ = 'select distinct '. FN_I_CHN .' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where '. FN_I_CHN .' is not null';
        //get body tables names
        $cDb->QueryRes('select distinct ' . FN_S_BT
          . ' from ' . $cDb->sTablePrefix() . static::TABLE);
        //get event ids where finded $sValue
        if($cDb->iRowCount() > 0){
          while($aRow = $cDb->aRow()){
            if($sQ === false){
              $sQ = static::sFilterPartBodyValue($aRow[0], $sValue);
            }
            else{
              $sQ .= ' union ' . static::sFilterPartBodyValue($aRow[0], $sValue);
            }
          }
          $cDb->FreeResult();
        }
        if($sQ !== false){
          $cDb->QueryRes($sQ);
          $sQ = false;
          if($cDb->iRowCount() > 0){
            while($aRow = $cDb->aRow()){
              if($sQ === false){$sQ = ' or ' . FN_I_ID . ' in (';}
              else{$sQ .= ', ';}
              $sQ .= $aRow[0];
            }
            $cDb->FreeResult();
          }
        }
        //build main query
        if($sQ !== false){
          $sMQ .= ' and (' . FN_S_SBJ . ' like \'%' . $sValue . '%\'' . $sQ . '))';
          $sQ = false;
        }
        else{
          $sMQ .= ' and ' . FN_S_SBJ . ' like \'%' . $sValue . '%\'';
        }
        //get chain ids where finded $sValue in body
        $cDb->QueryRes($sMQ);
        if($cDb->iRowCount() > 0){
          while($aRow = $cDb->aRow()){
            if($sQ === false){$sQ = (string)$aRow[0];}
            else{$sQ .= ', ' . $aRow[0];}
          }
          $cDb->FreeResult();
        }
      }
      return $sQ;
    }
    //потом можно поискать в теле
    public function iExpectedChain($sSbjPrefix){
      $iID = 0;
      if($sText = $this->sSubject()){
        $iPos1 = stripos($sText, '[');
        $iPos2 = stripos($sText, ']');
        if($iPos1 !== false && $iPos2 !== false && $iPos2 > $iPos1){
          $iPos1++;
          $sText = substr($sText, $iPos1, $iPos2 - $iPos1);
          $iPos1 = stripos($sText, $sSbjPrefix);
          if($iPos1 !== false){
            $iID = (int)substr($sText, strlen($sSbjPrefix));
          }
        }
      }
      return $iID;
    }
    
    public function bDelete(cDb &$cDb){
      if($this->iID()){
        //нет смысла удалять из цепочек отправленные письма
        if($this->iType() == self::OUT_EMAIL && $this->bProcessed() && $this->iChain() > 0){return false;} 
        $this->DeleteAddr($cDb);
        $this->DeleteBody($cDb);
        $this->DeleteAttach($cDb);
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $this->iID());
        $this->aMap[FN_I_ID]['val'] = null;
        return true;
      }
      return false;
    }
    
    public static function sType($iType){
      $s = false;
      switch($iType){
        case self::IN_EMAIL:  $s = _('Incoming Email');
        break;
        case self::OUT_EMAIL: $s = _('Outcoming Email');
        break;
        case self::IN_CALL:   $s = _('Incoming Call');
        break;
        case self::OUT_CALL:  $s = _('Outcoming Call');
        break;
        case self::MEETING:   $s = _('Meeting');
        break;
        default: break;
      }
      return $s;
    }
  /**
   * quik sort by maximum duration
   * recursive
   */
    public static function aQSByMaxDrt($aObjs){
      if(count($aObjs) < 2){return $aObjs;}
      $aLeft = $aRight = array( );
      $cPivot = array_pop($aObjs);
      foreach($aObjs as &$cObj){
        if(($cObj->iDtEnd() - $cObj->iDtBegin()) > ($cPivot->iDtEnd() - $cPivot->iDtBegin())){$aLeft[] = $cObj;}
        else{$aRight[] = $cObj;}
      }
      return static::aQSByMaxDrt($aLeft) + array($cPivot->iID() => $cPivot) + static::aQSByMaxDrt($aRight);
    }
  }


}
