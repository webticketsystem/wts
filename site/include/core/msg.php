<?php
/*
 * msg.php (part of WTS) - core class cMsg
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('withstr.php');
  require_once('collectionbase.php');

  /**
   * cMsg - сообщения от системы (контакт не создан, пароль сброшен и др.)
   */
  class cMsg extends cCollectionBase{
    use tWithStr;
    
    const TABLE = 'msgs';
    
    const OK   = 10;
    const WARN = 20;
    
    public function Install(cDb &$cDb){
      $aMap[FN_I_PRT]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_PRT]['idx']  = FN_I_PRT;
      $aMap[FN_I_TYP]['def'] = 'int unsigned not null';
      $aMap[FN_S_VAL]['def']  = 'char(255) not null';
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
    }
    
    public function iParent(){return $this->aMap[FN_I_PRT]['val'];}
    public function iType(){return $this->aMap[FN_I_TYP]['val'];}
    public function sValue(){return $this->aMap[FN_S_VAL]['val'];}
    public function Value($sValue){$this->Str(FN_S_VAL, $sValue);}
    
    public function __construct($iParent = 0, $iType = self::OK){
      $iParent = (int)$iParent;
      $this->aMap[FN_I_PRT]['val'] = ($iParent > 0 ? $iParent : 0);
      
      $iType = (int)$iType;
      if($iType == self::OK
      || $iType == self::WARN){
        $this->aMap[FN_I_TYP]['val'] = $iType;
      }
      else{$this->aMap[FN_I_TYP]['val'] = 0;}
      
      $this->InitStr(FN_S_VAL);
    }
    
    public static function iUnserialize(cDb &$cDb, $iParent, &$aObjs){
      $iCount = 0;
      $iParent = (int)$iParent;
      if($iParent > 0){
        $s = ' from ' . $cDb->sTablePrefix() . static::TABLE . ' where ' . FN_I_PRT . '=' . $iParent;
        
        $cDb->QueryRes('select ' . FN_I_TYP . ', ' . FN_S_VAL . $s);
        
        if(($iCount = $cDb->iRowCount()) > 0){
          while($aRow = $cDb->aRow()){
            $cObj = new cMsg($iParent, $aRow[0]);
            $cObj->aMap[FN_S_VAL]['val'] = $aRow[1];
            $aObjs[] = $cObj;
          }
          $cDb->FreeResult();
          //если прочли - сотрём
          $cDb->Query('delete ' . $s);
        }
      }
      return $iCount;
    }
    
    public function bSerialize(cDb &$cDb){
      if($this->iParent()
      && $this->iType()
      && $this->aMap[FN_S_VAL]['mod']){
        $cDb->Query('insert into ' . $cDb->sTablePrefix() . static::TABLE . ' (' 
        . FN_I_PRT . ', ' . FN_I_TYP . ', ' . FN_S_VAL . ') values ('
        . $this->iParent() . ', ' . $this->iType() . ', \'' . $cDb->sShield($this->sValue()) . '\')');
        $this->aMap[FN_S_VAL]['mod'] = false;
        return true;
      }
      return false;
    }
  }


}
