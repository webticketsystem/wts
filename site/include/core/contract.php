<?php
/*
 * contract.php (part of WTS) - core class cContract
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('warehouse.php');
  require_once('withattachs.php');
  require_once('withfilter.php');
  require_once('withtag.php');
  /**
   * cContract - контракты
   *
   * сама история: нельзя удалять, как и цепочки - надо думать прежде чем создавать
   */
  class cContract extends cWareHouse{
    use tWithAttachs{tWithAttachs::__construct as private __twa_construct;}
    use tWithFilter;
    use tWithTag{tWithTag::__construct as private __twt_construct;}
  
    const TABLE = 'contracts';
    
    //////{tWithProd - тут храним все манипуляции/транзакции с продуктами - история с правилами
    //нельзя включать продукты владельцем которого не являются продавец или покупатель
    //можно только их созавать в рамках контракта
    //нельзя исключать продукты уже участвующие в контрактах позже - нарушение истории

    public function bAttach(cDb &$cDb, cProduct &$cProduct){
      if($this->iID()
      && $cProduct->iID()){
        $i = strtotime('now');
        
        $s = 'select count(*) from '
        . $cDb->sTablePrefix() . static::TABLE_WAREHOUSE
        . ' where ' . FN_I_PDT . '=' . $cProduct->iID()
        . ' and (' . FN_I_CTR . '=' . $this->iID() . ' or ' . FN_DT . '> FROM_UNIXTIME(' . $i . '))';

        if((int)$cDb->sQueryRes($s) === 0){
          $cDb->Query( 'insert into ' . $cDb->sTablePrefix() . static::TABLE_WAREHOUSE
          . ' (' . FN_I_CTR . ', ' . FN_I_PDT . ', ' . FN_DT
          . ') values (' . $this->iID() . ', ' . $cProduct->iID() . ', FROM_UNIXTIME(' . $i . '))');
          return true;
        }
      }
      return false;
    }
    
    public function bDetach(cDb &$cDb, cProduct &$cProduct){
      if($this->iID()
      && $cProduct->iID()){
        $cDb->Query( 'delete from '
        . $cDb->sTablePrefix() . static::TABLE_WAREHOUSE
        . ' where ' . FN_I_CTR . '=' . $this->iID()
        . ' and ' . FN_I_PDT . '=' . $cProduct->iID());
        return true;
      }
      return false;
    }
    
    //последний контракт с этим продуктом
    public static function iLast(cDb &$cDb, cProduct &$cProduct){
      $iContract = 0;
      if($cProduct->iID()){
        $iContract = (int)$cDb->sQueryRes('select ' . FN_I_CTR
        . ' from ' . $cDb->sTablePrefix() . static::TABLE_WAREHOUSE
        . ' where ' . FN_I_PDT . '=' . $cProduct->iID()
        . ' order by ' . FN_DT . ' desc limit 1');
      }
      return $iContract;
    }
    
    //////}tWithProd
    
    public function Install(cDb &$cDb, $sAttachTable){
      
      $aMap[FN_I_ID]['def']   = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']    = true;
      $aMap[FN_I_ID]['ai']    = true;
      $aMap[FN_S_SBJ]['def']  = 'char(255)';
      $aMap[FN_DT_B]['def']   = 'datetime not null';
      $aMap[FN_DT_E]['def']   = 'datetime not null';//best before
      
      $aMap[FN_I_SLR]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_SLR]['idx']  = FN_I_SLR;
      $aMap[FN_I_SLRT]['def'] = 'int unsigned not null';
      
      $aMap[FN_I_BR]['def']   = 'bigint unsigned not null';
      $aMap[FN_I_BR]['idx']   = FN_I_BR;
      $aMap[FN_I_BRT]['def']  = 'int unsigned not null';
      
      $aMap[FN_I_CST]['def']  = 'bigint';
      $aMap[FN_S_AT]['def']   = 'char(127) not null';
      
      $aMap[FN_I_PRT]['def']  = 'bigint unsigned not null';
      $aMap[FN_DT]['def']     = 'datetime not null';
      $aMap[FN_I_OWN]['def']  = 'bigint unsigned';
      $aMap[FN_DT_U]['def']   = 'datetime';

      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
      $cDb->Query('alter table ' . $cDb->sTablePrefix() . static::TABLE . ' auto_increment=6000');
      
      $this->Install_($cDb);//from warehouse
      $this->bInstallAttach($cDb, $sAttachTable);
      $this->InstallTag($cDb);
    }
    public function sSubject(){return $this->aMap[FN_S_SBJ]['val'];}
    public function Subject($sSubject){$this->Str(FN_S_SBJ, $sSubject);}
    
    public function sAttachTable(){return $this->aMap[FN_S_AT]['val'];}
    
    public function iSeller(){return $this->aMap[FN_I_SLR]['val'];}
    public function iSellerType(){return $this->aMap[FN_I_SLRT]['val'];}
    
    public function Seller($iSeller, $iType){
      $iSeller = (int)$iSeller;
      $iType = (int)$iType;
      if($iType === self::CONTACT || $iType === self::GROUP){
        
        //админу нечего продавать и продавец должен отличаться от покупателя
        if(($iType === self::CONTACT && $iSeller === 1)
        || ($iSeller === $this->iBuyer() && $iType === $this->iBuyerType())){
          $iSeller = 0;
        }

        if($iSeller > 0){
          if($this->iSeller() !== $iSeller){
            $this->aMap[FN_I_SLR]['val'] = $iSeller;
            $this->aMap[FN_I_SLR]['mod'] = true;
          }
          if($this->iSellerType() !== $iType){
            $this->aMap[FN_I_SLRT]['val'] = $iType;
            $this->aMap[FN_I_SLRT]['mod'] = true;
          }
        }
      }
    }
    
    public function iBuyer(){return $this->aMap[FN_I_BR]['val'];}
    public function iBuyerType(){return $this->aMap[FN_I_BRT]['val'];}
    
    public function Buyer($iBuyer, $iType){
      $iBuyer = (int)$iBuyer;
      $iType = (int)$iType;
      if($iType === self::CONTACT || $iType === self::GROUP){
        
        //админу нечего продавать и продавец должен отличаться от покупателя
        if(($iType === self::CONTACT && $iBuyer === 1)
        || ($iBuyer === $this->iSeller() && $iType === $this->iSellerType())){
          $iBuyer = 0;
        }

        if($iBuyer > 0){
          if($this->iBuyer() !== $iBuyer){
            $this->aMap[FN_I_BR]['val'] = $iBuyer;
            $this->aMap[FN_I_BR]['mod'] = true;
          }
          if($this->iBuyerType() !== $iType){
            $this->aMap[FN_I_BRT]['val'] = $iType;
            $this->aMap[FN_I_BRT]['mod'] = true;
          }
        }
      }
    }

    //стоимость может быть отрицательная или не быть совсем...
    public function fCost(){return $this->aMap[FN_I_CST]['val'];}
    public function Cost($fCost){
      if(is_string($fCost) && strlen($fCost) > 0){
        $fCost = str_replace(',', '.', $fCost);
        $fCost = str_replace(' ', '', $fCost);
      }
      
      $fCost = (float)$fCost;
      if($this->aMap[FN_I_CST]['val'] !== $fCost){
        $this->aMap[FN_I_CST]['val'] = $fCost;
        $this->aMap[FN_I_CST]['mod'] = true;
      }
    }
    
    public function __construct(){
      parent::__construct();
      $this->__twa_construct();
      
      $this->InitStr(FN_S_SBJ);
      $this->InitInt(FN_I_SLR);
      $this->InitInt(FN_I_SLRT);
      $this->InitInt(FN_I_BR);
      $this->InitInt(FN_I_BRT);
      
      $this->aMap[FN_I_CST]['val'] = (float)0;
      $this->aMap[FN_I_CST]['mod'] = false;
      
      $this->InitStr(FN_S_AT);
    }
    
    public static function mUnserialize(cDb &$cDb, $iID){
      $cObj = false;
      $iID = (int)$iID;
      if($iID > 0){
        $s = 'select ' . FN_S_SBJ . ', UNIX_TIMESTAMP('
                       . FN_DT_B . '), UNIX_TIMESTAMP('
                       . FN_DT_E . '), '
                       . FN_I_SLR . ', '
                       . FN_I_SLRT . ', '
                       . FN_I_BR . ', '
                       . FN_I_BRT . ', '
                       . FN_I_CST . ', '
                       . FN_S_AT . ', '
                       . FN_I_PRT . ', UNIX_TIMESTAMP('
                       . FN_DT . '), '
                       . FN_I_OWN . ', UNIX_TIMESTAMP('
                       . FN_DT_U
        . ') from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $iID;
        $cDb->QueryRes($s);
        if($cDb->iRowCount() > 0){
          $aRow = $cDb->aRow();
          $cObj = new cContract();
          $cObj->aMap[FN_I_ID]['val']   = $iID;
          $cObj->aMap[FN_S_SBJ]['val']  = $aRow[0];
          $cObj->aMap[FN_DT_B]['val']   = (int)$aRow[1];
          $cObj->aMap[FN_DT_E]['val']   = (int)$aRow[2];
          $cObj->aMap[FN_I_SLR]['val']  = (int)$aRow[3];
          $cObj->aMap[FN_I_SLRT]['val'] = (int)$aRow[4];
          $cObj->aMap[FN_I_BR]['val']   = (int)$aRow[5];
          $cObj->aMap[FN_I_BRT]['val']  = (int)$aRow[6];
          $cObj->aMap[FN_I_CST]['val']  = (float)$aRow[7] / 100.0;
          $cObj->aMap[FN_S_AT]['val']   = $aRow[8];
          $cObj->aMap[FN_I_PRT]['val']  = (int)$aRow[9];
          $cObj->aMap[FN_DT]['val']     = (int)$aRow[10];
          $cObj->aMap[FN_I_OWN]['val']  = (int)$aRow[11];
          $cObj->aMap[FN_DT_U]['val']   = (int)$aRow[12];
          $cDb->FreeResult();
        }
      }
      return $cObj;
    }
    
    public static function iUnserialize(cDb &$cDb, $iPage, $iPerPage, &$aObjs){
      $iCount   = 0;
      $iPage    = (int)$iPage;
      $iPerPage = (int)$iPerPage;
      if($iPage > 0 && $iPerPage > 0){
        
        $s = 'select ' . FN_I_ID . ', '
                       . FN_S_SBJ . ', UNIX_TIMESTAMP('
                       . FN_DT_B . '), UNIX_TIMESTAMP('
                       . FN_DT_E . '), '
                       . FN_I_SLR . ', '
                       . FN_I_SLRT . ', '
                       . FN_I_BR . ', '
                       . FN_I_BRT . ', '
                       . FN_I_CST . ', '
                       . FN_S_AT . ', '
                       . FN_I_PRT . ', UNIX_TIMESTAMP('
                       . FN_DT . '), '
                       . FN_I_OWN . ', UNIX_TIMESTAMP('
                       . FN_DT_U
        . ') from ' . $cDb->sTablePrefix() . static::TABLE;
        if(static::$sWhere !== false){
          $s .= ' where ' . static::$sWhere;
        }
        $s .= ' order by ' . FN_DT . ' desc limit '
            . (($iPage-1) * $iPerPage) . ', ' . $iPerPage;
        
        $cDb->QueryRes($s);
        
        if(($iCount = $cDb->iRowCount()) > 0){
          while($aRow = $cDb->aRow()){
            $cObj = new cContract();
            $cObj->aMap[FN_I_ID]['val']   = (int)$aRow[0];
            $cObj->aMap[FN_S_SBJ]['val']  = $aRow[1];
            $cObj->aMap[FN_DT_B]['val']   = (int)$aRow[2];
            $cObj->aMap[FN_DT_E]['val']   = (int)$aRow[3];
            $cObj->aMap[FN_I_SLR]['val']  = (int)$aRow[4];
            $cObj->aMap[FN_I_SLRT]['val'] = (int)$aRow[5];
            $cObj->aMap[FN_I_BR]['val']   = (int)$aRow[6];
            $cObj->aMap[FN_I_BRT]['val']  = (int)$aRow[7];
            $cObj->aMap[FN_I_CST]['val']  = (float)$aRow[8] / 100.0;
            $cObj->aMap[FN_S_AT]['val']   = $aRow[9];
            $cObj->aMap[FN_I_PRT]['val']  = (int)$aRow[10];
            $cObj->aMap[FN_DT]['val']     = (int)$aRow[11];
            $cObj->aMap[FN_I_OWN]['val']  = (int)$aRow[12];
            $cObj->aMap[FN_DT_U]['val']   = (int)$aRow[13];
            $aObjs[] = $cObj;
          }
          $cDb->FreeResult();
        }
      }
      return $iCount;
    }
  /**
   * Восстанавливаем теги контрактов из базы для переданных объектов
   * @see cTagView
   */
    public static function iUnserializeTags(cDb &$cDb, &$aObjs, &$aTagView){
      $aTags;
      if(isset($aObjs)
      && count($aObjs) > 0
      && cTag::iUnserialize($cDb, $aTags) > 0){
        return self::iUnserializeTags_($cDb, $aTags, $aObjs, $aTagView);
      }
      else{return 0;}
    }

    public function bSerialize(cDb &$cDb, $iThumbSide = 256){
      $bRet = false;
      
      if($this->iID()){//try to save modified
        if($this->iUpdater() > 0){//try to save modified
          $aMap = false;
          
          if($this->aMap[FN_S_SBJ]['mod']){
            $aMap[FN_S_SBJ] = '\'' . $cDb->sShield($this->sSubject()) . '\'';
          }
          
          if($this->aMap[FN_I_CST]['mod']){
            if($this->fCost() !== (float)0){
              $aMap[FN_I_CST] = (int)round($this->fCost() * 100.0);
            }
            else{$aMap[FN_I_CST] = 'null';}
          }
          
          if($aMap || $this->iSerializeAttach($cDb, $iThumbSide) > 0){
            if($this->aMap[FN_I_OWN]['mod']){
              $aMap[FN_I_OWN] = $this->iUpdater();
            }
            $aMap[FN_DT_U] = 'now()';
            $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
    
            $bRet = true;
          }
        }
      }
      else{//try to insert new
        //try get table name
        if($cScheme = cScheme::mUnserialize($cDb)){
          if($sTable = $cScheme->mValue('CAT')){
            $this->aMap[FN_S_AT]['val'] = $sTable;
          }
          unset($cScheme);
        }
        
        if($this->iCreator()
        && $this->iSeller()
        && $this->iBuyer()
        && $this->sAttachTable()){
          
          if($this->aMap[FN_S_SBJ]['mod']){
            $aMap[FN_S_SBJ] = '\'' . $cDb->sShield($this->sSubject()) . '\'';
          }
          
          if(!$this->aMap[FN_DT_B]['mod']){$this->DtBegin(strtotime('now'));}
          $aMap[FN_DT_B] = 'FROM_UNIXTIME(' . $this->iDtBegin() . ')';
      
          $aMap[FN_DT_E] = 'FROM_UNIXTIME(' . $this->iDtEnd() . ')';
          
          $aMap[FN_I_SLR]  = $this->iSeller();
          $aMap[FN_I_SLRT] = $this->iSellerType();
          $aMap[FN_I_BR]   = $this->iBuyer();
          $aMap[FN_I_BRT]  = $this->iBuyerType();
          
          if($this->fCost() !== (float)0){
            $aMap[FN_I_CST] = (int)round($this->fCost() * 100.0);
          }
          
          $aMap[FN_S_AT] = '\'' . $cDb->sShield($this->sAttachTable()) . '\'';
          
          $aMap[FN_I_PRT] = $this->iCreator();
          $aMap[FN_DT] = 'now()';
          $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
          if($this->aMap[FN_I_ID]['val'] = (int)$cDb->mLastInsertID()){
            $this->iSerializeAttach($cDb, $iThumbSide);
            $bRet = true;
          }
        }
      }
      if($bRet){
        foreach($this->aMap as &$a){
          if(isset($a['mod']) && $a['mod'])$a['mod'] = false;
        }
      }
      return $bRet;
    }
    
  /**
   * Сохранение тегов в базе
   */
   public function bSerializeTag(cDb &$cDb, &$aSlugs, $bFootprint = true){
      $aTags;
      if($this->iID()
      && cTag::iUnserialize($cDb, $aTags) > 0
      && $this->bSerializeTag_($cDb, $aTags, $aSlugs)){
        if($bFootprint){
          $aMap;
          $aMap[FN_I_OWN] = $this->iUpdater();
          $aMap[FN_DT_U] = 'now()';
          $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
        }
        return true;
      }
      return false;
    }

    public static function bAddFilterCont($iCont){
      $iCont = (int)$iCont;
      if($iCont > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= '((' . FN_I_SLRT . '=' . self::CONTACT . ' and ' . FN_I_SLR . '=' . $iCont
        . ') or ('  . FN_I_BRT . '=' . self::CONTACT . ' and ' . FN_I_BR . '=' . $iCont . '))';
      }
      return ($iCont > 0);
    }
    
    public static function bAddFilterGroup($iGroup){
      $iGroup = (int)$iGroup;
      if($iGroup > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= '((' . FN_I_SLRT . '=' . self::GROUP . ' and ' . FN_I_SLR . '=' . $iGroup
        . ') or ('  . FN_I_BRT . '=' . self::GROUP . ' and ' . FN_I_BR . '=' . $iGroup . '))';
      }
      return ($iGroup > 0);
    }
    
    public static function bAddFilterProduct(cDb &$cDb, $iProduct){
      $iProduct = (int)$iProduct;
      if($iProduct > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= FN_I_ID . ' in (select distinct '
        . FN_I_CTR . ' from ' . $cDb->sTablePrefix() . static::TABLE_WAREHOUSE
        . ' where ' . FN_I_PDT . '=' . $iProduct . ')';
        return true;
      }
      return false;
    }

    public static function bAddFilterPartValue(cDb &$cDb, $sValue){
      $sValue = (string)$sValue;
      $iValue = (int)$sValue;
      
      if($sValue !== (string)$iValue){
        $iValue = 0;
      }
      
      if(strlen($sValue) > 0){

        if(static::$sWhere !== false){static::$sWhere .= ' and ';}

        if($iValue > 0){// > 6000, но может и быть импорт
          static::$sWhere .= '(' . FN_I_ID . '=' . $iValue
          . ' or ' . FN_S_SBJ . ' like \'%' . $cDb->sShield($sValue) . '%\')';
        }
        else{
          static::$sWhere .= FN_S_SBJ . ' like \'%' . $cDb->sShield($sValue) . '%\'';
        }
        
        return true;
      }
      return false;
    }
    
    public static function bAddFilterTag(cDb &$cDb, &$aSlugs){
      $aTags;
      if(isset($aSlugs)
      && count($aSlugs) > 0
      && cTag::iUnserialize($cDb, $aTags) > 0){
        return static::bAddFilterTag_($cDb, $aTags, $aSlugs);
      }
      return false;
    }
    //if no products
    public function bDelete(cDb &$cDb){
      if($this->iID() && !$this->iProdCount($cDb)){
        $this->DeleteAttach($cDb);
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $this->iID());
        $this->aMap[FN_I_ID]['val'] = null;
        return true;
      }
      return false;
    }
    
    public static function MergeCont(cDb &$cDb, $iSrc, $iDst){
      $iSrc = (int)$iSrc;
      $iDst = (int)$iDst;
      if($iSrc > 0 && $iDst > 0){
        $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE
        . ' set ' . FN_I_SLR . '=' . $iDst . ' where ' . FN_I_SLRT . '=' . self::CONTACT
        . ' and ' . FN_I_SLR . '=' . $iSrc);
        $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE
        . ' set ' . FN_I_BR . '=' . $iDst . ' where ' . FN_I_BRT . '=' . self::CONTACT
        . ' and ' . FN_I_BR . '=' . $iSrc);
      }
    }
    
    public static function MergeGroup(cDb &$cDb, $iSrc, $iDst){
      $iSrc = (int)$iSrc;
      $iDst = (int)$iDst;
      if($iSrc > 0 && $iDst > 0){
        $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE
        . ' set ' . FN_I_SLR . '=' . $iDst . ' where ' . FN_I_SLRT . '=' . self::GROUP
        . ' and ' . FN_I_SLR . '=' . $iSrc);
        $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE
        . ' set ' . FN_I_BR . '=' . $iDst . ' where ' . FN_I_BRT . '=' . self::GROUP
        . ' and ' . FN_I_BR . '=' . $iSrc);
      }
    }
     

  }


}
