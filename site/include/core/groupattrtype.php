<?php
/*
 * groupattrtype.php (part of WTS) - core class cGroupAttrType
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('collectionbase.php');
  require_once('withcolor.php');
  require_once('withstr.php');
  require_once('withid.php');


  /**
   * cGroupAttrType - класс типов атрибутов для групп контактов
   * 
   * !!! при удалении типа удаляются все атрибуты этого типа
   * 
   * @var const int iID
   * @var string sName
   * @var string sColor
   * @var const bool bMultiple
   * @var int iOrder - only for collection
   */
  class cGroupAttrType extends cCollectionBase{
    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithColor{tWithColor::__construct as private __twc_construct;}
    use tWithStr;
    
    const TABLE = 'groups_attrs_types';
    
    public function Install(cDb &$cDb){
      $aMap[FN_I_ID]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']   = true;
      $aMap[FN_S_NM]['def']  = 'char(255) not null';
      $aMap[FN_S_NM]['uniq'] = true;
      $aMap[FN_S_CLR]['def'] = 'char(6)';
      $aMap[FN_B_MTP]['def'] = 'bit not null default 0';
      $aMap[FN_I_ORD]['def'] = 'int unsigned not null default 1';

      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
    }
    
    public function sName(){return $this->aMap[FN_S_NM]['val'];}
    public function Name($sName){$this->Str(FN_S_NM, $sName);}
    public function bMultiple(){return $this->aMap[FN_B_MTP]['val'];}
    
    public function __construct($bMultiple = false){
      $this->__twi_construct();

      $this->InitStr(FN_S_NM);

      $this->__twc_construct();

      $this->aMap[FN_B_MTP]['val'] = (bool)$bMultiple;
    }
    
    public static function iUnserialize(cDb &$cDb, &$aObjs){
      $s = 'select ' . FN_I_ID . ', '
            . FN_S_NM . ', '
            . FN_S_CLR . ', '
            . FN_B_MTP
            . ' from ' . $cDb->sTablePrefix() . static::TABLE
            . ' order by ' . FN_I_ORD;
        
      $cDb->QueryRes($s);

      $iCount = $cDb->iRowCount();
      if($iCount > 0){
        while($aRow = $cDb->aRow()){
          $sClass = get_called_class();
          $cObj = new $sClass((bool)$aRow[3]);
          $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
          $cObj->aMap[FN_S_NM]['val']  = $aRow[1];
          $cObj->aMap[FN_S_CLR]['val'] = $aRow[2];
          $aObjs[] = $cObj;
        }
        $cDb->FreeResult();
      }
      return $iCount;
    }
    
    public function bSerialize(cDb &$cDb){
      $bRet = false;
      if($this->iID()){//try to save modified
        $aMap;
        if($this->aMap[FN_S_NM]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{$aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';}
        }
        if($this->aMap[FN_S_CLR]['mod']){
          $aMap[FN_S_CLR] = ($this->sColor() ? '\'' . $cDb->sShield($this->sColor()) . '\'' : 'null');
        }
        
        if(isset($aMap)){
          $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
          $bRet = true;
        }
      }//$this->iID() != null
      else{//try to insert new
        
        if($this->aMap[FN_S_NM]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{
            $aMap[FN_I_ID] = $this->aMap[FN_I_ID]['val'] = 1 + (int)$cDb->sQueryRes('select max(' . FN_I_ID . ') from ' . $cDb->sTablePrefix() . static::TABLE);
            $aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';
            if($this->aMap[FN_S_CLR]['mod']){
              $aMap[FN_S_CLR] = ($this->sColor() ? '\'' . $cDb->sShield($this->sColor()) . '\'' : 'null');
            }
            if($this->bMultiple()){$aMap[FN_B_MTP] = 'true';}
            $aMap[FN_I_ORD] = 1 + (int)$cDb->sQueryRes('select max(' . FN_I_ORD . ') from ' . $cDb->sTablePrefix() . static::TABLE);
            
            $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
            $bRet = true;
          }
        }
      }
      if($bRet){foreach($this->aMap as &$a){
        if(isset($a['mod']) && $a['mod'])$a['mod'] = false;}
      }
      return $bRet;
    }
    //!!! also need deleted all attributes of this type
    public function bDelete(cDb &$cDb){
      if($this->iID()){
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $this->iID());
        $this->aMap[FN_I_ID]['val'] = 0;
        return true;
      }
      return false;
    }
    
    public function bMoveUp(cDb &$cDb){
      if($this->iID()){
        $i2 = (int)$cDb->sQueryRes('select ' . FN_I_ORD . ' from ' . $cDb->sTablePrefix() . static::TABLE . ' where ' . FN_I_ID . '=' . $this->iID());
        $i1 = (int)$cDb->sQueryRes('select max(' . FN_I_ORD . ') from ' . $cDb->sTablePrefix() . static::TABLE . ' where ' . FN_I_ORD . '<' . $i2);
        if($i1 > 0){
          $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE . ' set ' . FN_I_ORD . '=if(' . FN_I_ORD . '=' . $i1 . ', '. $i2 . ', ' . $i1 .') where ' . FN_I_ORD . ' in (' . $i1 . ', ' . $i2 . ')');
          return true;
        }
      }
      return false;
    }
    
    public function bMoveDown(cDb &$cDb){
      if($this->iID()){
        $i1 = (int)$cDb->sQueryRes('select ' . FN_I_ORD . ' from ' . $cDb->sTablePrefix() . static::TABLE . ' where ' . FN_I_ID . '=' . $this->iID());
        $i2 = (int)$cDb->sQueryRes('select min(' . FN_I_ORD . ') from ' . $cDb->sTablePrefix() . static::TABLE . ' where ' . FN_I_ORD . '>' . $i1);
        if($i2 > 0){
          $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE . ' set ' . FN_I_ORD . '=if(' . FN_I_ORD . '=' . $i1 . ', '. $i2 . ', ' . $i1 .') where ' . FN_I_ORD . ' in (' . $i1 . ', ' . $i2 . ')');
          return true;
        }
      }
      return false;
    }

  }


}
