<?php
/*
 * withtag.php (part of WTS) - trait for core classes tWithTag
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('tagview.php');

  /**
   * tWithTag - треит для работы с тегами
   * 
   */
  trait tWithTag{
    
    protected $aTags_;
    
    protected static function sTagTable(){return (static::TABLE . '_tags');}
    
    protected function InstallTag(cDb &$cDb){
      $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null';//chain id
      $aMap[FN_I_PRT]['idx'] = FN_I_PRT;
      $aMap[FN_I_TAG]['def'] = 'bigint unsigned not null';
      $aMap[FN_I_TAG]['idx'] = FN_I_TAG;
      //надобы добавить первичный ключ по двум полям (FN_I_PRT, FN_I_TAG)
      //чтобы не было дублирования тэгов у одной цепочки
      //пока будем следить за этим программно 
     
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . $this->sTagTable());
    }
    
    public function __construct(){
      $this->aTags_ = array();
    }
    
    public function aTagIDs(){return $this->aTags_;}
    
    //use only if $this->iID() > 0
    protected static function iUnserializeTags_(cDb &$cDb, &$aTags, &$aObjs, &$aTagView){
      foreach($aObjs as &$cObj){
        $cDb->QueryRes('select ' . FN_I_TAG
            . ' from ' . $cDb->sTablePrefix() . static::sTagTable()
            . ' where ' . FN_I_PRT .  '=' . $cObj->iID());
        if($cDb->iRowCount() > 0){
          //$a;// = array();
          $a = array();
          while($aRow = $cDb->aRow()){$a[$aRow[0]] = true;}
          $cDb->FreeResult();
          
          foreach($aTags as &$cTag){
            if(array_key_exists($cTag->iID(), $a)){
              $cObj->aTags_[] = $cTag->iID();
              //!для отображения делаем цикл по aTags и имеем прямой доступ по индексу в aTagView
              
              if(!isset($aTagView[$cTag->iID()])){
                $aTagView[$cTag->iID()] = new cTagView($cTag->sName(), $cTag->sSlug());
                if($cTag->sColor()){
                  $aTagView[$cTag->iID()]->Color($cTag->sColor());
                }
              }
            }
          }
        }
      }
      foreach($aTags as $key => &$cTag){unset($aTags[$key]);}
      return (($aTagView && is_array($aTagView)) ? count($aTagView) : 0);
    }
    
    //use only if $this->iID() > 0
    protected function bSerializeTag_(cDb &$cDb, &$aTags, &$aSlugs){
      $bRet = false;
      $aSlugs = array_unique($aSlugs);
      
      //поменяем slug на id
      $aNew;
      foreach($aTags as &$cTag){
        if(in_array($cTag->sSlug(), $aSlugs)){
          $aNew[$cTag->iID()] = true;
        }
      }
      
      $cDb->QueryRes('select ' . FN_I_TAG
            . ' from ' . $cDb->sTablePrefix() . $this->sTagTable()
            . ' where ' . FN_I_PRT .  '=' . $this->iID());
      $aOld;
      if($cDb->iRowCount() > 0){//имеющиеся
        while($aRow = $cDb->aRow()){
          //пометим для удаления
          $i = (int)$aRow[0];
          $aOld[$i] = array('act' => 'delete',);
          if(isset($aNew) && isset($aNew[$aRow[0]])){//но если есть в новых тегах
            $aOld[$i]['act'] = 'inaction';
            $aNew[$i] = false;//оставим в покое
          }
        }
        $cDb->FreeResult();
      }
      
      if(isset($aNew)){
        foreach($aNew as $key => $val){
          if($val === true){$aOld[$key]['act'] = 'create';}
        }
      }
      
      if(isset($aOld)){
        foreach($aTags as &$cTag){
          if(isset($aOld[$cTag->iID()])){
            
            switch($aOld[$cTag->iID()]['act']){
              case 'create':
                $cDb->Query( 'insert into ' . $cDb->sTablePrefix() . $this->sTagTable() . ' ('
                  . FN_I_PRT . ', ' . FN_I_TAG
                  . ') values (' . $this->iID() . ', ' . $cTag->iID() . ')');
                $bRet = true;
              break;
              case 'delete':
                $cDb->Query('delete from ' . $cDb->sTablePrefix() . $this->sTagTable()
                . ' where ' . FN_I_PRT . '=' . $this->iID() . ' and '. FN_I_TAG . '=' . $cTag->iID());
                $bRet = true;
              break;
              default: break;
            }
          }
        }
      }
      foreach($aTags as &$cTag){unset($cTag);}
      return $bRet;
    }
  /**
   * For find parent by tags
   * 
   * param string array aSlugs
   */
    protected static function bAddFilterTag_(cDb &$cDb, &$aTags, &$aSlugs){
      $bRet = false;
      $aSlugs = array_unique($aSlugs);
      
      $sIDs = false;
      foreach($aTags as &$cTag){
        if(in_array($cTag->sSlug(), $aSlugs)){
          if($sIDs === false){$sIDs = (string)$cTag->iID();}
          else{$sIDs .= ', ' . $cTag->iID();}
        }
        unset($cTag);
      }
      if($sIDs !== false){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= FN_I_ID . ' in (select distinct '
        . FN_I_PRT . ' from ' . $cDb->sTablePrefix() . self::sTagTable()
        . ' where ' . FN_I_TAG . ' in (' . $sIDs . '))';// group by ' . FN_I_PRT . ')';
        $bRet = true;
      }
      return $bRet;
    }
    //for test before delete tag
    public static function iTagUsage(cDb &$cDb, $iTagID){
      $iTagID = (int)$iTagID;
      if($iTagID > 0){
        return (int)$cDb->sQueryRes('select count(*) from '
        . $cDb->sTablePrefix() . self::sTagTable()
        . ' where ' . FN_I_TAG . '=' . $iTagID);
      }
      return 0;
    }
  }


}
