<?php
/*
 * warehouse.php (part of WTS) - core class cWareHouse
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('collectionbase.php');
  require_once('withdateint.php');
  require_once('withupdated.php');
  require_once('withid.php');
  require_once('withstr.php');

  /**
   * cWarehouse - базовый для контрактов и продуктов 
   */
  class cWareHouse extends cCollectionBase{
    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithDateInt{tWithDateInt::__construct as private __twd_construct;}
    use tWithUpdated{tWithUpdated::__construct as private __twu_construct;}
    use tWithStr;
    
    const CONTACT = 10;
    const GROUP   = 20;
    
    const TABLE_WAREHOUSE = 'warehouse';
    
    protected function Install_(cDb &$cDb){
      $aMap[FN_I_CTR]['def'] = 'bigint unsigned not null';
      $aMap[FN_I_CTR]['idx'] = FN_I_CTR;
      $aMap[FN_I_PDT]['def'] = 'bigint unsigned not null';
      $aMap[FN_I_PDT]['idx'] = FN_I_PDT;
      $aMap[FN_DT]['def']    = 'datetime not null';//чтобы видеть когда продукт проведен в контракте

      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE_WAREHOUSE);
      $cDb->Query('alter table ' . $cDb->sTablePrefix() . static::TABLE_WAREHOUSE . ' add primary key (' . FN_I_CTR . ', ' . FN_I_PDT . ')');
    }
    
    public function __construct(){
      $this->__twi_construct();
      
      $this->__twd_construct();
      $this->__twu_construct();
    }
  }


}
