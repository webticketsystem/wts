<?php
/*
 * category.php (part of WTS) - core class cCategory
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('collectionbase.php');
  require_once('withcolor.php');
  require_once('withstr.php');
  require_once('withid.php');

  /**
   * cCategory - tree of products categories 
   * ...он же дерево!
   */
  class cCategory extends cCollectionBase{
    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithColor{tWithColor::__construct as private __twc_construct;}
    use tWithStr;
    
    const TABLE = 'categories';
    
    public function Install(cDb &$cDb){
      $aMap[FN_I_ID]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']   = true;
      $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null default 0';//tree stuct
      $aMap[FN_I_PRT]['idx'] = FN_I_PRT;
      $aMap[FN_I_LVL]['def'] = 'int unsigned not null default 0';//for build tree view
      $aMap[FN_S_NM]['def']  = 'char(255) not null';
      $aMap[FN_S_NM]['uniq']  = true;
      $aMap[FN_S_CLR]['def'] = 'char(6)';
      $aMap[FN_B_N]['def']   = 'bit not null default 0';//if true - no real product - is item only for structure
      $aMap[FN_I_ORD]['def'] = 'int unsigned not null default 1';//for internal sort

      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
    }
    
    public function iParent(){return $this->aMap[FN_I_PRT]['val'];}
    public function Parent($iParent){
      $iParent = (int)$iParent;
      if($iParent < 1){$iParent = 0;}
      if($this->aMap[FN_I_PRT]['val'] !== $iParent){
        $this->aMap[FN_I_PRT]['val'] = $iParent;
        $this->aMap[FN_I_PRT]['mod'] = true;
      }
    }
    public function iLevel(){return $this->aMap[FN_I_LVL]['val'];}
    public function sName(){return $this->aMap[FN_S_NM]['val'];}
    public function Name($sName){$this->Str(FN_S_NM, $sName);}
    public function bIsNode(){return $this->aMap[FN_B_N]['val'];}
    
    public function __construct($bIsNode = false){
      $this->__twi_construct();
      
      $this->InitInt(FN_I_PRT);
      $this->InitInt(FN_I_LVL);

      $this->InitStr(FN_S_NM);
      $this->__twc_construct();

      $this->aMap[FN_B_N]['val'] = (bool)$bIsNode;
    }
    
    public static function mUnserialize(cDb &$cDb, $iID){
      $cObj = false;

      $iID = (int)$iID;
      if($iID > 0){
        $s = 'select ' . FN_I_PRT . ', '
                       . FN_I_LVL . ', '
                       . FN_S_NM  . ', '
                       . FN_S_CLR . ', '
                       . FN_B_N
        . ' from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $iID;
        $cDb->QueryRes($s);
        if($cDb->iRowCount() > 0){
          $aRow = $cDb->aRow();
          $cObj = new cCategory((bool)$aRow[4]);
          $cObj->aMap[FN_I_ID]['val']  = $iID;
          $cObj->aMap[FN_I_PRT]['val'] = (int)$aRow[0];
          $cObj->aMap[FN_I_LVL]['val'] = (int)$aRow[1];
          $cObj->aMap[FN_S_NM]['val']  = $aRow[2];
          $cObj->aMap[FN_S_CLR]['val'] = $aRow[3];
          $cDb->FreeResult();
        }
      }
      return $cObj;
    }
    
    public static function iUnserialize(cDb &$cDb, &$aObjs){
      $iCount = 0;
     
      $s = 'select ' . FN_I_ID . ', '
                     . FN_I_PRT . ', '
                     . FN_I_LVL . ', '
                     . FN_S_NM  . ', '
                     . FN_S_CLR . ', '
                     . FN_B_N
      . ' from ' . $cDb->sTablePrefix() . static::TABLE
      . ' order by ' . FN_I_ORD;
      $cDb->QueryRes($s);
      
      if(($iCount = $cDb->iRowCount()) > 0){
        while($aRow = $cDb->aRow()){
          $cObj = new cCategory((bool)$aRow[5]);
          $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
          $cObj->aMap[FN_I_PRT]['val'] = (int)$aRow[1];
          $cObj->aMap[FN_I_LVL]['val'] = (int)$aRow[2];
          $cObj->aMap[FN_S_NM]['val']  = $aRow[3];
          $cObj->aMap[FN_S_CLR]['val'] = $aRow[4];
          $aObjs[] = $cObj;
        }
        $cDb->FreeResult();
      }
      return $iCount;
    }
    
    public function bSerialize(cDb &$cDb){
      $bRet = $bRebuild = false;
      if($this->iID()){//try to save modified
        $aMap;
        
        if($this->aMap[FN_S_NM]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{
            $aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';
            $bRebuild = true;
          }
        }

        if($this->aMap[FN_S_CLR]['mod']){
          $aMap[FN_S_CLR] = ($this->sColor() ? '\'' . $cDb->sShield($this->sColor()) . '\'' : 'null');
        }
        if($this->aMap[FN_I_PRT]['mod']){
          $aMap[FN_I_PRT] = $this->iParent();
          $bRebuild = true;
        }
        
        if(isset($aMap)){
          $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
          $bRet = true;
        }
      }//$this->iID() != null
      else{//try to insert new
        
        if($this->aMap[FN_S_NM]['mod']){
          $cDb->QueryRes('select ' . FN_I_ID
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{
        
            $aMap[FN_I_ID] = $this->aMap[FN_I_ID]['val'] = 1 + (int)$cDb->sQueryRes('select max(' . FN_I_ID . ') from ' . $cDb->sTablePrefix() . static::TABLE);
            
            if($this->iParent()){$aMap[FN_I_PRT] = $this->iParent();}
            
            $aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';
            
            if($this->aMap[FN_S_CLR]['mod']){
              $aMap[FN_S_CLR] = ($this->sColor() ? '\'' . $cDb->sShield($this->sColor()) . '\'' : 'null');
            }
            
            if($this->bIsNode()){$aMap[FN_B_N] = 'true';}
            
            $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
            $bRet = $bRebuild = true;
          }
        }
      }
      if($bRebuild){$this->RebuildTree($cDb);}
      if($bRet){foreach($this->aMap as &$a){
        if(isset($a['mod']) && $a['mod'])$a['mod'] = false;}
      }
      return $bRet;
    }

    //!!! delete if not exist products of this category
    public function bDelete(cDb &$cDb){
      if($this->iID()){
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $this->iID());
        $this->aMap[FN_I_ID]['val'] = 0;
        $this->RebuildTree($cDb);
        return true;
      }
      return false;
    }
    
    protected function RebuildTree(cDb &$cDb){
      $s = 'select ' . FN_I_ID . ', '
                     . FN_I_PRT
      . ' from ' . $cDb->sTablePrefix() . static::TABLE
      . ' order by ' . FN_B_N . ' desc, '. FN_S_NM;
      $cDb->QueryRes($s);
      
      $aIDs = $aObjs = $aNodes = false;
      if(($iCount = $cDb->iRowCount()) > 0){
        while($aRow = $cDb->aRow()){
          $aIDs[(int)$aRow[0]] = true;
          $aObjs[] = array('i' => (int)$aRow[0], 'p' => (int)$aRow[1], 'b' => false);
        }
        $cDb->FreeResult();
        
        if($iCount > 1){
          //1. check parents
          foreach($aObjs as &$a){
            if(!array_key_exists($a['p'], $aIDs) || $a['i'] === $a['p']){
              $a['p'] = 0;
              $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE
              . ' set ' . FN_I_PRT . '=0 where ' . FN_I_ID . '=' . $a['i']);
            }
          }
          //build tree
          foreach($aObjs as &$a){
            if($a['p'] === 0){
              $aNodes[] = array('i' => $a['i'], 'c' => false);//new cNode($a['i']);
              $a['b'] = true;
              $iCount--;
            }
            
          }
          if($aNodes && $iCount > 0){
            foreach($aNodes as &$a){
              $this->ParseNode($aObjs, $a, $iCount);
            }
          }
          
          //дерево пересобрано, надо сложить
          $aObjs2 = false;
          $iOrder = 1;
          if($aNodes){
            foreach($aNodes as &$a){
              $this->UpdateCat($cDb, $a, $iOrder, 0);
            }
          }
        }//$iCount > 1
      }
    }

    protected function ParseNode(&$aSrc, &$aNode, &$iCount){
      $bFind = false;
      foreach($aSrc as &$a){
        if(!$a['b']
        && $a['p'] === $aNode['i']){
          $aNode['c'][] = array('i' => $a['i'], 'c' => false);
          $a['b'] = $bFind = true;
          $iCount--;

        }
      }
      if($bFind  && $iCount > 0){
        foreach($aNode['c'] as &$a){
          $this->ParseNode($aSrc, $a, $iCount);
        }
      }
    }
    
    protected function UpdateCat(cDb &$cDb, &$aNode, &$iOrder, $iLevel){
      $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE
      . ' set ' . FN_I_LVL . '=' . $iLevel . ', ' . FN_I_ORD . '=' . $iOrder
      . ' where ' . FN_I_ID . '=' . $aNode['i']);
      
      $iOrder++;
      
      if($aNode['c']){
        $iLevel++;
        foreach($aNode['c'] as &$a){
          $this->UpdateCat($cDb, $a, $iOrder, $iLevel);
        }
      }
    }
  }



}
