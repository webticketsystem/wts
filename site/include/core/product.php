<?php
/*
 * product.php (part of WTS) - core class cProduct
 * 
 * Copyright 2014-2015 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('warehouse.php');
  require_once('withattr.php');
  require_once('withfilter.php');
  require_once('prodattrtype.php');
  require_once('category.php');
  /**
   * cProduct - продукты - у продуктов нет атрибутов, чтобы их можно было группировать по категрии и описанию
   *
   * катагория: запчасти для принтеров/HP/картриджи
   * описание: HP CN056AE 933XL желтый
   */
  class cProduct extends cWareHouse{
    use tWithFilter;
    use tWithAttr;
    
    const TABLE = 'products';

    const PRODUCT = 30;
    
    public function Install(cDb &$cDb){
      $aMap[FN_I_ID]['def']   = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']    = true;
      $aMap[FN_I_ID]['ai']    = true;
      $aMap[FN_I_CAT]['def']  = 'bigint unsigned not null';//category
      $aMap[FN_I_CAT]['idx']  = FN_I_CAT;

      $aMap[FN_DT_B]['def']   = 'datetime not null';
      $aMap[FN_DT_E]['def']   = 'datetime not null';//best before
      
      $aMap[FN_I_HLD]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_HLD]['idx']  = FN_I_OWN;
      $aMap[FN_I_HLDT]['def'] = 'int unsigned not null';//holder type CONTACT | GROUP | PRODUCT

      $aMap[FN_I_PRT]['def']  = 'bigint unsigned not null';
      $aMap[FN_DT]['def']     = 'datetime not null';
      $aMap[FN_I_OWN]['def']  = 'bigint unsigned';
      $aMap[FN_DT_U]['def']   = 'datetime';
      
      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
      $this->InstallAttr($cDb);
    }
    
    public function iCategory(){return $this->aMap[FN_I_CAT]['val'];}

    public function iHolder(){return $this->aMap[FN_I_HLD]['val'];}
    public function iHolderType(){return $this->aMap[FN_I_HLDT]['val'];}
  /**
   * Смена владельца и его тип
   */
    public function Holder($iHolder, $iType){
      $iHolder = (int)$iHolder;
      $iType = (int)$iType;
      if($iType === self::CONTACT
          || $iType === self::GROUP
          || $iType === self::PRODUCT){
        
        if(($iType === self::CONTACT && $iHolder === 1)//у админа нет продуктов
        || ($iType === self::PRODUCT && $iHolder === $this->iID())){//selfownered?
          $iHolder = 0;
        }

        if($iHolder > 0){
          if($this->iHolder() !== $iHolder){
            $this->aMap[FN_I_HLD]['val'] = $iHolder;
            $this->aMap[FN_I_HLD]['mod'] = true;
          }
          if($this->iHolderType() !== $iType){
            $this->aMap[FN_I_HLDT]['val'] = $iType;
            $this->aMap[FN_I_HLDT]['mod'] = true;
          }
        }
      }
    }
    
    public function __construct($iCategory){
      parent::__construct();

      $iCategory = (int)$iCategory;
      $this->aMap[FN_I_CAT]['val'] = (($iCategory > 0) ? $iCategory : 0);

      $this->InitInt(FN_I_HLD);
      $this->InitInt(FN_I_HLDT);
    }
    
    public static function mUnserialize(cDb &$cDb, $iID){
      $cObj = false;
      $iID = (int)$iID;
      if($iID > 0){
        $s = 'select ' . FN_I_CAT . ', UNIX_TIMESTAMP('
                       . FN_DT_B . '), UNIX_TIMESTAMP('
                       . FN_DT_E . '), '
                       . FN_I_HLD . ', '
                       . FN_I_HLDT . ', '
                       . FN_I_PRT . ', UNIX_TIMESTAMP('
                       . FN_DT . '), '
                       . FN_I_OWN . ', UNIX_TIMESTAMP('
                       . FN_DT_U
        . ') from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $iID;
        $cDb->QueryRes($s);
        if($cDb->iRowCount() > 0){
          $aRow = $cDb->aRow();
          $cObj = new cProduct($aRow[0]);
          $cObj->aMap[FN_I_ID]['val']   = $iID;
          $cObj->aMap[FN_DT_B]['val']   = (int)$aRow[1];
          $cObj->aMap[FN_DT_E]['val']   = (int)$aRow[2];
          $cObj->aMap[FN_I_HLD]['val']  = (int)$aRow[3];
          $cObj->aMap[FN_I_HLDT]['val'] = (int)$aRow[4];
          $cObj->aMap[FN_I_PRT]['val']  = (int)$aRow[5];
          $cObj->aMap[FN_DT]['val']     = (int)$aRow[6];
          $cObj->aMap[FN_I_OWN]['val']  = (int)$aRow[7];
          $cObj->aMap[FN_DT_U]['val']   = (int)$aRow[8];
          $cDb->FreeResult();
        }
      }
      return $cObj;
    }
    
    public static function iUnserialize(cDb &$cDb, $iPage, $iPerPage, &$aObjs){
      $iCount   = 0;
      $iPage    = (int)$iPage;
      $iPerPage = (int)$iPerPage;
      if($iPage > 0 && $iPerPage > 0){
        
        $s = 'select ' . FN_I_ID . ', '
                       . FN_I_CAT . ', UNIX_TIMESTAMP('
                       . FN_DT_B . '), UNIX_TIMESTAMP('
                       . FN_DT_E . '), '
                       . FN_I_HLD . ', '
                       . FN_I_HLDT . ', '
                       . FN_I_PRT . ', UNIX_TIMESTAMP('
                       . FN_DT . '), '
                       . FN_I_OWN . ', UNIX_TIMESTAMP('
                       . FN_DT_U
        . ') from ' . $cDb->sTablePrefix() . static::TABLE;
        if(static::$sWhere !== false){
          $s .= ' where ' . static::$sWhere;
        }
        $s .= ' order by ' . FN_I_CAT . ', ' . FN_I_ID . ' limit ' .(($iPage-1) * $iPerPage) . ', ' . $iPerPage;
        
        $cDb->QueryRes($s);
        
        if(($iCount = $cDb->iRowCount()) > 0){
          while($aRow = $cDb->aRow()){
            $cObj = new cProduct($aRow[1]);
            $cObj->aMap[FN_I_ID]['val']   = (int)$aRow[0];
            $cObj->aMap[FN_DT_B]['val']   = (int)$aRow[2];
            $cObj->aMap[FN_DT_E]['val']   = (int)$aRow[3];
            $cObj->aMap[FN_I_HLD]['val']  = (int)$aRow[4];
            $cObj->aMap[FN_I_HLDT]['val'] = (int)$aRow[5];
            $cObj->aMap[FN_I_PRT]['val']  = (int)$aRow[6];
            $cObj->aMap[FN_DT]['val']     = (int)$aRow[7];
            $cObj->aMap[FN_I_OWN]['val']  = (int)$aRow[8];
            $cObj->aMap[FN_DT_U]['val']   = (int)$aRow[9];
            $aObjs[] = $cObj;
          }
          $cDb->FreeResult();
        }
      }
      return $iCount;
    }
    
  /**
   * Восстанавливаем атрибуты продукта из базы
   * @see cAttrView
   */
    public function iUnserializeAttr(cDb &$cDb, &$aAttrView){
      $aAttrTypes;
      if($this->iID() && cProdAttrType::iUnserialize($cDb, $aAttrTypes) > 0){
        return $this->iUnserializeAttr_($cDb, $aAttrTypes, $aAttrView);
      }
      else{return 0;}
    }

    public function bSerialize(cDb &$cDb){
      $bRet = false;
      
      if($this->iID()){//try to save modified
        if($this->iUpdater() > 0){//try to save modified
          $aMap = false;

          if($this->aMap[FN_DT_B]['mod']){//Begin product action
            $aMap[FN_DT_B] = 'FROM_UNIXTIME(' . $this->iDtBegin() . ')';
          }
          
          if($this->aMap[FN_DT_E]['mod']){//End product action
            $aMap[FN_DT_E] = 'FROM_UNIXTIME(' . $this->iDtEnd() . ')';
          }
          
          if($this->aMap[FN_I_HLD]['mod']){
            $aMap[FN_I_HLD] = $this->iHolder();
          }
          
          if($this->aMap[FN_I_HLDT]['mod']){
            $aMap[FN_I_HLDT] = $this->iHolderType();
          }
          
          if($aMap){
            if($this->aMap[FN_I_OWN]['mod']){
              $aMap[FN_I_OWN] = $this->iUpdater();
            }
            $aMap[FN_DT_U] = 'now()';
            
            $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
            $bRet = true;
          }
        }
      }//$this->iID() != null
      else{//try to insert new
        if($this->iCreator()
        && $this->iCategory()
        && $this->iHolder()){
          
          $aMap[FN_I_CAT] = $this->iCategory();
          
          if(!$this->aMap[FN_DT_B]['mod']){$this->DtBegin(strtotime('now'));}
          $aMap[FN_DT_B] = 'FROM_UNIXTIME(' . $this->iDtBegin() . ')';
          $aMap[FN_DT_E] = 'FROM_UNIXTIME(' . $this->iDtEnd() . ')';
          
          $aMap[FN_I_HLD] = $this->iHolder();
          $aMap[FN_I_HLDT] = $this->iHolderType();
          
          $aMap[FN_I_PRT] = $this->iCreator();
          $aMap[FN_DT] = 'now()';
          
          $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
          if($this->aMap[FN_I_ID]['val'] = (int)$cDb->mLastInsertID()){
            $bRet = true;
          }
        }
      }
      if($bRet){
        foreach($this->aMap as &$a){
          if(isset($a['mod']) && $a['mod'])$a['mod'] = false;
        }
      }
      return $bRet;
    }
    
    public function bSerializeAttr(cDb &$cDb, &$aAttrView){
      $aAttrTypes;
      if($this->iID()
      && cProdAttrType::iUnserialize($cDb, $aAttrTypes) > 0
      && $this->bSerializeAttr_($cDb, $aAttrTypes, $aAttrView)){
        $aMap;
        $aMap[FN_I_OWN] = $this->iUpdater();
        $aMap[FN_DT_U] = 'now()';
        $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
        return true;
      }
      return false;
    }
     
    public static function bAddFilterCategory($iCategory){
      $iCategory = (int)$iCategory;
      if($iCategory > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= FN_I_CAT . '=' . $iCategory;
        return true;
      }
      return false;
    }
    
    //childs product filter
    public static function bAddFilterProd($iProd){
      $bRet = false;
      $iProd = (int)$iProd;
      if($iProd > 0){$bRet = FN_I_HLDT . '=' . self::PRODUCT . ' and ' . FN_I_HLD . '=' . $iProd;}
      elseif($iProd === -1){$bRet = FN_I_HLDT . '!=' . self::PRODUCT;}
      if($bRet !== false){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= $bRet;
        $bRet = true;
      }
      return $bRet;
    }
    
    public static function bAddFilterCont($iCont){
      $iCont = (int)$iCont;
      if($iCont > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= FN_I_HLDT . '=' . self::CONTACT . ' and ' . FN_I_HLD . '=' . $iCont;
      }
      return ($iCont > 0);
    }
    
    public static function bAddFilterGroup($iGroup){
      $iGroup = (int)$iGroup;
      if($iGroup > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= FN_I_HLDT . '=' . self::GROUP . ' and ' . FN_I_HLD . '=' . $iGroup;
      }
      return ($iGroup > 0);
    }
    
    public static function bAddFilterContract(cDb &$cDb, $iContract){
      $iContract = (int)$iContract;
      if($iContract > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::$sWhere .= FN_I_ID . ' in (select distinct '
        . FN_I_PDT . ' from ' . $cDb->sTablePrefix() . static::TABLE_WAREHOUSE
        . ' where ' . FN_I_CTR . '=' . $iContract . ')';
        return true;
      }
      return false;
    }
    
    public static function bAddFilterPartValue(cDb &$cDb, $sValue){
      $sValue = (string)$sValue;
      if(strlen($sValue) > 0){
        if(static::$sWhere !== false){static::$sWhere .= ' and ';}
        static::AddFilterPartAttrValue($cDb, $sValue);
        return true;
      }
      return false;
    }
    
  /**
   * fill array of [count, category]
   * return count of products
   */
    public static function iGetStatistics(cDb &$cDb, $iType, $iID, &$aStat){
      $q = false;
      if($iType === self::PRODUCT){
        $q = FN_I_HLDT . '=' . self::PRODUCT . ' and ' . FN_I_HLD . '=' . (int)$iID;
      }
      if($iType === self::GROUP){
        $q = FN_I_HLDT . '=' . self::GROUP . ' and ' . FN_I_HLD . '=' . (int)$iID;
      }
      elseif($iType === self::CONTACT){
        $q = FN_I_HLDT . '=' . self::CONTACT . ' and ' . FN_I_HLD . '=' . (int)$iID;
      }
      elseif($iType === 0){
        $q = FN_I_ID . ' in (select distinct '
        . FN_I_PDT . ' from ' . $cDb->sTablePrefix() . static::TABLE_WAREHOUSE
        . ' where ' . FN_I_CTR . '=' . (int)$iID . ')';
      }
      $s = 'select count(*), ' . FN_I_CAT
      . ' from ' . $cDb->sTablePrefix() . static::TABLE;
      if($q !== false){
        $s .= ' where ' . $q;
      }
      else{
        $s .= ' where ' . FN_I_HLDT . '!=' . self::PRODUCT;
      }
      $s .= ' group by ' . FN_I_CAT . ' order by ' . FN_I_CAT;
      
      $iCount = 0;
      $cDb->QueryRes($s);
      if($cDb->iRowCount() > 0){
        while($aRow = $cDb->aRow()){
          $iCount += (int)$aRow[0];
          $aStat[] = array('count' => (int)$aRow[0], 'category' => (int)$aRow[1]);
        }
        $cDb->FreeResult();
      }
      return $iCount;
    }
    
    public function mChilds(cDb &$cDb){
      $aChilds = false;
      if($this->iID()){
        $this->Childs($cDb, $this->iID(), $aChilds);
      }
      return $aChilds;
    }
    
    protected function Childs(cDb &$cDb, $iID, &$aChilds){
      $cDb->QueryRes('select ' . FN_I_ID
      . ' from ' . $cDb->sTablePrefix() . static::TABLE
      . ' where ' . FN_I_HLD . '=' . $iID . ' and ' . FN_I_HLDT . '=' . static::PRODUCT);
      
      $a = false;
      if(($iCount = $cDb->iRowCount()) > 0){
        while($aRow = $cDb->aRow()){
          $a[] = (int)$aRow[0];
        }
        $cDb->FreeResult();
        
        for($i = 0; $i < $iCount; $i++){
          $aChilds[] = $a[$i];
          $this->Childs($cDb, $a[$i], $aChilds);
        }
      }
    }
    
    public function iRoot(cDb &$cDb){
      $iRoot = 0;
      if($this->iID()){
        $b = true;
        $iRoot = $this->iID();
        while($b){
          $cDb->QueryRes('select ' . FN_I_HLD . ', ' . FN_I_HLDT
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_I_ID . '=' . $iRoot);
          
          if($cDb->iRowCount() > 0){
            $aRow = $cDb->aRow();
            $cDb->FreeResult();
            if((int)$aRow[1] === static::PRODUCT){
              $iRoot = (int)$aRow[0];
              continue;
            }
          }
          $b = false;
        }
      }
      return $iRoot;
    }
    
    //!!! delete if not used in some contract
    public function bDelete(cDb &$cDb){
      if($this->iID()){
        $this->DeleteAttr($cDb);
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $this->iID());
        $this->aMap[FN_I_ID]['val'] = 0;
        return true;
      }
      return false;
    }
    
    public static function MergeCont(cDb &$cDb, $iSrc, $iDst){
      $iSrc = (int)$iSrc;
      $iDst = (int)$iDst;
      if($iSrc > 0 && $iDst > 0){
        $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE
        . ' set ' . FN_I_HLD . '=' . $iDst . ' where ' . FN_I_HLDT . '=' . self::CONTACT
        . ' and ' . FN_I_HLD . '=' . $iSrc);
      }
    }
    
    public static function MergeGroup(cDb &$cDb, $iSrc, $iDst){
      $iSrc = (int)$iSrc;
      $iDst = (int)$iDst;
      if($iSrc > 0 && $iDst > 0){
        $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE
        . ' set ' . FN_I_HLD . '=' . $iDst . ' where ' . FN_I_HLDT . '=' . self::GROUP
        . ' and ' . FN_I_HLD . '=' . $iSrc);
      }
    }
  }


}
