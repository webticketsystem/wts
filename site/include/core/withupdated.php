<?php
/*
 * withupdated.php (part of WTS) - trait for core classes tWithUpdated
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('collectionbase.php');

  //интерфейс хранения/обновления инф-ции об изменении объекта
  trait tWithUpdated{
    
    /*
    public function Install_(&$aMap){
      $aMap[FN_I_PRT]['def'] = 'bigint unsigned not null';
      $aMap[FN_DT]['def']    = 'datetime not null';
      $aMap[FN_I_OWN]['def'] = 'bigint unsigned not null';
      $aMap[FN_DT_U]['def']  = 'datetime not null';
    }
    */
    public function __construct(){
      $this->InitInt(FN_I_PRT);
      $this->InitInt(FN_DT);
      $this->InitInt(FN_I_OWN);
      $this->InitInt(FN_DT_U);
    }
    
    public function iDtCreated(){
      return $this->aMap[FN_DT]['val'];
    }
    
    public function iCreator(){
      return $this->aMap[FN_I_PRT]['val'];
    }
    
    public function Creator($iCreator){
      $iCreator = (int)$iCreator;
      if($iCreator > 0
      && $this->iID() === 0
      && $this->iCreator() != $iCreator){
        $this->aMap[FN_I_PRT]['val'] = $iCreator;
        $this->aMap[FN_I_PRT]['mod'] = true;
      }
    }

    public function iDtUpdated(){
      return $this->aMap[FN_DT_U]['val'];
    }
    
    public function iUpdater(){
      return $this->aMap[FN_I_OWN]['val'];
    }
    
    public function Updater($iUpdater){
      $iUpdater = (int)$iUpdater;
      if($iUpdater > 0
      && $this->iID() > 0
      && $this->iUpdater() !== $iUpdater){
        $this->aMap[FN_I_OWN]['val'] = $iUpdater;
        $this->aMap[FN_I_OWN]['mod'] = true;
      }
    }
    
    //use then contact merge
    public static function bAddFilterCreatorOwner($iCont){
      $iCont = (int)$iCont;
      if(static::$sWhere !== false){static::$sWhere .= ' and ';}
      static::$sWhere .= '(' . FN_I_PRT . '=' . $iCont . ' or '. FN_I_OWN . '=' . $iCont . ')';
    }
    
    //for rest
    public static function bAddFilterUpdated($iTimeOutInSec){
      $iTimeOutInSec = (int)$iTimeOutInSec;
      if($iTimeOutInSec < 60){$iTimeOutInSec = 60;}
      if(static::$sWhere !== false){static::$sWhere .= ' and ';}
      static::$sWhere .= 'TIMESTAMPDIFF(SECOND, ' . FN_DT_U . ', now())<' . $iTimeOutInSec;
      return true;
    }

  }


}
