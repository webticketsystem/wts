<?php
/*
 * withdateint.php (part of WTS) - trait for core classes tWithDateInt
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('collectionbase.php');

 /**
   * tWithDateInt - треит для добавления интервала времени
   * 
   * @var int FN_DT_B - название поля для начала временной хар-ки
   * @var int FN_DT_E - название поля для конца временной хар-ки
   * 
   * ! не забывайте вызывать конструктор треита !
   */
  trait tWithDateInt{

    
    public function __construct(){
      
      $this->InitInt(FN_DT_B);
      $this->InitInt(FN_DT_E);

      //$this->aMap[FN_DT_B]['val'] = $this->aMap[FN_DT_E]['val'] = 0;
      //$this->aMap[FN_DT_B]['mod'] = $this->aMap[FN_DT_E]['mod'] = false;
    }
    
    public function iDtBegin(){
      return $this->aMap[FN_DT_B]['val'];
    }
    
    public function DtBegin($iDt){
      $this->Dt(FN_DT_B, FN_DT_E, (int)$iDt);
    }
    
    public function iDtEnd(){
      return $this->aMap[FN_DT_E]['val'];
    }
    
    public function DtEnd($iDt){
      $this->Dt(FN_DT_E, FN_DT_B, (int)$iDt);
    }

    protected function Dt($t1, $t2, $iDt){
      if($iDt > 0 && $this->aMap[$t1]['val'] !== $iDt){
        $this->aMap[$t1]['val'] = $iDt;
        $this->aMap[$t1]['mod'] = true;
        if($this->iDtEnd() < $this->iDtBegin()){ // set interval to zero
          $this->aMap[$t2]['val'] = $iDt;
          $this->aMap[$t2]['mod'] = true;
        }
      }
    }
    
    //for getsendmail
    public static function bAddFilterEnded($iTimeOutInSec){
      $iTimeOutInSec = (int)$iTimeOutInSec;
      if($iTimeOutInSec < 60){$iTimeOutInSec = 60;}
      if(static::$sWhere !== false){static::$sWhere .= ' and ';}
      static::$sWhere .= 'TIMESTAMPDIFF(SECOND, ' . FN_DT_E . ', now())>' . $iTimeOutInSec;
      return true;
    }
    
  }


}
