<?php
/*
 * response.php (part of WTS) - core class cResponse
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('collectionbase.php');
  require_once('withid.php');
  require_once('withstr.php');

  /**
   * cResponse - класс шаблонов для очередей
   * 
   * админской очереди нужен только один шаблон - для восстановления пароля (id = 1)
   * у рабочей очереди (отдела) max 4 шаблона - емаил, звонок, встреча, форвард
   */
  class cResponse extends cCollectionBase{
    use tWithID{tWithID::__construct as private __twi_construct;}
    use tWithStr;
    
    const TABLE = 'responses';
    
    const RPL_EXP = array(//replaceable expressions
      'SITE_TITLE'  => '%TITLE%',
      'NEW_PASSWD'  => '%PASSW%',
      'AGENT_NAME'  => '%ANAME%',
      'CUR_CHAIN'   => '%CHAIN%',
      'QUEUE_EMAIL' => '%QEMAIL%',
      'QUEUE_NAME'  => '%QNAME%',
      'CSRMR_NAME'  => '%CNAME%',
      'FWD_MESSAGE' => '%FWD%',
    );
    
    public static function mReplExpUiName($sKey){
      $sKey = (string)$sKey;
      $s = false;
      switch($sKey){
        case 'SITE_TITLE': $s = _('Site Title');
        break;
        case 'NEW_PASSWD': $s = _('Restored Password');
        break;
        case 'AGENT_NAME': $s = _('Agent Name');
        break;
        case 'CUR_CHAIN': $s = _('Current Chain');
        break;
        case 'QUEUE_EMAIL': $s = _('Queue E-mail');
        break;
        case 'QUEUE_NAME': $s = _('Queue Name');
        break;
        case 'CSRMR_NAME': $s = _('Customer Name');
        break;
        case 'FWD_MESSAGE': $s = _('Forwarded Message');
        break;
        default: break;
      }
      return $s;
    }
    
    public function Install(cDb &$cDb){
      $aMap[FN_I_ID]['def']  = 'bigint unsigned not null';
      $aMap[FN_I_ID]['pk']   = true;
      $aMap[FN_S_NM]['def']  = 'char(255) not null';
      $aMap[FN_S_NM]['uniq'] = true;
      $aMap[FN_I_SLT]['def'] = 'bigint unsigned';
      $aMap[FN_I_SGN]['def'] = 'bigint unsigned';
      $aMap[FN_S_VAL]['def'] = 'text not null';

      $this->CreateTable($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, true);
    }

    public function sName(){return $this->aMap[FN_S_NM]['val'];}
    public function Name($sName){$this->Str(FN_S_NM, $sName);}
    
    public function iSalutation(){return $this->aMap[FN_I_SLT]['val'];}
    public function Salutation($iSalutation){
      $iSalutation = (int)$iSalutation;
      if($iSalutation < 1){$iSalutation = 0;}
      if($this->aMap[FN_I_SLT]['val'] != $iSalutation){
        $this->aMap[FN_I_SLT]['val'] = $iSalutation;
        $this->aMap[FN_I_SLT]['mod'] = true;
      }
    }
    public function iSignature(){return $this->aMap[FN_I_SGN]['val'];}
    public function Signature($iSignature){
      $iSignature = (int)$iSignature;
      if($iSignature < 1){$iSignature = 0;}
      if($this->aMap[FN_I_SGN]['val'] != $iSignature){
        $this->aMap[FN_I_SGN]['val'] = $iSignature;
        $this->aMap[FN_I_SGN]['mod'] = true;
      }
    }
    public function sValue(){return $this->aMap[FN_S_VAL]['val'];}
    //sValue can have html tags
    public function Value($sValue){$this->Str(FN_S_VAL, $sValue);}
    
    public function __construct(){
      $this->__twi_construct();
      
      $this->InitStr(FN_S_NM);
      $this->InitInt(FN_I_SLT);
      $this->InitInt(FN_I_SGN);
      $this->InitStr(FN_S_VAL);
    }
    
    public static function mUnserialize(cDb &$cDb, $iID){
      $cObj = false;
      $iID = (int)$iID;
      if($iID > 0){
        $s = 'select ' . FN_S_NM . ', '
                       . FN_I_SLT . ', '
                       . FN_I_SGN . ', '
                       . FN_S_VAL
        . ' from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $iID;
        
        $cDb->QueryRes($s);
        if($cDb->iRowCount() > 0){
          $aRow = $cDb->aRow();
          $cObj = new cResponse();
          $cObj->aMap[FN_I_ID]['val']  = $iID;
          $cObj->aMap[FN_S_NM]['val']  = $aRow[0];
          $cObj->aMap[FN_I_SLT]['val'] = (int)$aRow[1];
          $cObj->aMap[FN_I_SGN]['val'] = (int)$aRow[2];
          $cObj->aMap[FN_S_VAL]['val'] = $aRow[3];
          $cDb->FreeResult();
        }
      }
      return $cObj;
    }
    
    public function mBuild(cDb &$cDb){
      
      $sResponse = false;
      if($this->iID()){
        if($this->iSalutation()
        && ($cSalutation = cSalutation::mUnserialize($cDb, $this->iSalutation()))){
          $sResponse = $cSalutation->sValue();
          unset($cSalutation);
        }
        if($this->sValue()){
          $sResponse = ($sResponse ? $sResponse . $this->sValue() : $this->sValue());
        }
        if($this->iSignature()
        && ($cSignature = cSignature::mUnserialize($cDb, $this->iSignature()))){
          $sResponse = ($sResponse ? $sResponse . $cSignature->sValue() : $cSignature->sValue());
          unset($cSignature);
        }
      }
      return $sResponse;
    }
    
    public static function iUnserialize(cDb &$cDb, &$aObjs){
      $iCount   = 0;

      $s = 'select ' . FN_I_ID . ', '
                     . FN_S_NM . ', '
                     . FN_I_SLT . ', '
                     . FN_I_SGN . ', '
                     . FN_S_VAL
      . ' from ' . $cDb->sTablePrefix() . static::TABLE
      . ' order by ' . FN_S_NM;

      $cDb->QueryRes($s);
      if(($iCount = $cDb->iRowCount()) > 0){
        while($aRow = $cDb->aRow()){
          $cObj = new cResponse();
          $cObj->aMap[FN_I_ID]['val']  = (int)$aRow[0];
          $cObj->aMap[FN_S_NM]['val']  = $aRow[1];
          $cObj->aMap[FN_I_SLT]['val'] = (int)$aRow[2];
          $cObj->aMap[FN_I_SGN]['val'] = (int)$aRow[3];
          $cObj->aMap[FN_S_VAL]['val'] = $aRow[4];
          $aObjs[] = $cObj;
        }
        $cDb->FreeResult();
      }
      return $iCount;
    }
    
    public function bSerialize(cDb &$cDb){
      $bRet = false;
      if($this->iID()){//try to save modified
        $aMap;
        if($this->aMap[FN_S_VAL]['mod']){
          $aMap[FN_S_VAL] = '\'' . $cDb->sShield($this->sValue()) . '\'';
        }
        
        if($this->aMap[FN_I_SLT]['mod']){
          if($this->iSalutation()
          && ($cSalutation = cSalutation::mUnserialize($cDb, $this->iSalutation()))){
            $aMap[FN_I_SLT] = $this->iSalutation();
            unset($cSalutation);
          }
          else{
            $aMap[FN_I_SLT] = 'null';
          }
        }
        
        if($this->aMap[FN_I_SGN]['mod']){
          if($this->iSignature()
          && ($cSignature = cSignature::mUnserialize($cDb, $this->iSignature()))){
            $aMap[FN_I_SGN] = $this->iSignature();
            unset($cSignature);
          }
          else{
            $aMap[FN_I_SGN] = 'null';
          }
        }
        
        if(isset($aMap)){
          $this->Update($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE, FN_I_ID . '=' . $this->iID());
          $bRet = true;
        }
      }//$this->iID() != null
      else{//try to insert new
        if($this->sName()
        && $this->sValue()){
          $cDb->QueryRes('select ' . FN_I_ID
          . ' from ' . $cDb->sTablePrefix() . static::TABLE
          . ' where ' . FN_S_NM . '=\'' . $cDb->sShield($this->sName()) . '\'');
          if($cDb->iRowCount() > 0){$cDb->FreeResult();}
          else{
            
            $aMap[FN_I_ID] = $this->aMap[FN_I_ID]['val'] = 1 + (int)$cDb->sQueryRes('select max(' . FN_I_ID . ') from ' . $cDb->sTablePrefix() . static::TABLE);
            
            $aMap[FN_S_NM] = '\'' . $cDb->sShield($this->sName()) . '\'';
        
            if($this->iSalutation()
            && ($cSalutation = cSalutation::mUnserialize($cDb, $this->iSalutation()))){
              $aMap[FN_I_SLT] = $this->iSalutation();
              unset($cSalutation);
            }
          
            if($this->iSignature()
            && ($cSignature = cSignature::mUnserialize($cDb, $this->iSignature()))){
              $aMap[FN_I_SGN] = $this->iSignature();
              unset($cSignature);
            }
            
            $aMap[FN_S_VAL] = '\'' . $cDb->sShield($this->sValue()) . '\'';
            
            $this->Insert($cDb, $aMap, $cDb->sTablePrefix() . static::TABLE);
            $bRet = true;
          }
        }
      }
      if($bRet){foreach($this->aMap as &$a){
        if(isset($a['mod']) && $a['mod'])$a['mod'] = false;}
      }
      return $bRet;
    }
    
    public function bDelete(cDb &$cDb){
      if($this->iID()){
        $cDb->Query('delete from ' . $cDb->sTablePrefix() . static::TABLE
        . ' where ' . FN_I_ID . '=' . $this->iID());
        $this->aMap[FN_I_ID]['val'] = 0;
        return true;
      }
      return false;
    }
    
    public static function ClearSalutation(cDb &$cDb, $iID){
      $iID = (int)$iID;
      if($iID > 0){
        $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE . ' set ' . FN_I_SLT . '=null where ' . FN_I_SLT . '=' . $iID);
      }
    }
    
    public static function ClearSignature(cDb &$cDb, $iID){
      $iID = (int)$iID;
      if($iID > 0){
        $cDb->Query('update ' . $cDb->sTablePrefix() . static::TABLE . ' set ' . FN_I_SGN . '=null where ' . FN_I_SGN . '=' . $iID);
      }
    }
  }
  
}
?>
