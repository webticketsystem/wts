<?php
/*
 * app.php (part of WTS) - Application level class
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */
 
namespace wts{

  require_once('core/utils/db.php');
  require_once('res.php');
  require_once('translate.php');
  require_once('view.php');
  
  require_once('agent.php');
  require_once('admin.php');
  require_once('core/utils/capcha.php');
  require_once('core/cont.php');
  require_once('core/log.php');

  class app{
    protected $cDb;
    protected $cActMForgot;

    public function __construct(){
      $this->cDb = new cDb(WTS_DB_NAME, WTS_DB_USER, WTS_DB_PASS, WTS_DB_TABLE_PREFIX);
      //$this->cActMForgot = new cActMForgot();
    }
    
    public function __destruct(){
      unset($this->cDb);
      //unset($this->cActMForgot);
    }
    
    public function Run(){
      if(!session_id())session_start();

      if(isset($_COOKIE[WTS_COOKIE_PARAM_UILANG])){
        cTranslate::UpdateUiLang($_COOKIE[WTS_COOKIE_PARAM_UILANG], WTS_PATH);
      }

      //validate session by guid and timeout
      if(isset($_SESSION[WTS_SESSION_PARAM_LOGIN]) 
      && isset($_SESSION[WTS_SESSION_PARAM_HASH])
      && ($cCont = cCont::mCheckSsn($this->cDb, $_SESSION[WTS_SESSION_PARAM_LOGIN], $_SESSION[WTS_SESSION_PARAM_HASH], WTS_SESSION_TIMEOUT))){
        //если вход выполнен...
        $aMeta = false;
        //проверим настройки языка
        if($cCont->iUnserializeMeta($this->cDb, $aMeta) > 0
        && array_key_exists(WTS_COOKIE_PARAM_UILANG, $aMeta)){
          $iUiLang = (int)$aMeta[WTS_COOKIE_PARAM_UILANG];
          if(!isset($_COOKIE[WTS_COOKIE_PARAM_UILANG])
          || $_COOKIE[WTS_COOKIE_PARAM_UILANG] != $iUiLang){
            setcookie(WTS_COOKIE_PARAM_UILANG, $iUiLang, time()+(3600*24*10));//10 days
            cTranslate::UpdateUiLang($iUiLang, WTS_PATH);
          }
        }
        else{
          if(isset($_COOKIE[WTS_COOKIE_PARAM_UILANG])){
            setcookie(WTS_COOKIE_PARAM_UILANG, null, time()-1);
            cTranslate::UpdateUiLang(null, WTS_PATH);
          }
        }
        //проверим настройки css
        if($aMeta
        && array_key_exists(WTS_COOKIE_PARAM_CSS, $aMeta)){
          $iCSS = (int)$aMeta[WTS_COOKIE_PARAM_CSS];
          if(!isset($_COOKIE[WTS_COOKIE_PARAM_CSS])
          || $_COOKIE[WTS_COOKIE_PARAM_CSS] != $iCSS){
            setcookie(WTS_COOKIE_PARAM_CSS, $iCSS, time()+(3600*24*10));//10 days
            //???
            $_COOKIE[WTS_COOKIE_PARAM_CSS] = $iCSS;
          }
        }
        else{
          if(isset($_COOKIE[WTS_COOKIE_PARAM_CSS])){
            setcookie(WTS_COOKIE_PARAM_CSS, null, time()-1);
            //???
            $_COOKIE[WTS_COOKIE_PARAM_CSS] = null;
          }
        }
        
        //пусть работает....
        $cContUI = false;
        switch($cCont->iType()){
          case cCont::AGENT:
            $cContUI = new cAgent($this->cDb, $cCont);
            break;
          case cCont::ADMIN:
            $cContUI = new cAdmin($this->cDb, $cCont);
            break;
          default:
            $cContUI = new cCustomer($this->cDb, $cCont);
            break;
        }//switch ($cont_type)
        
        $cContUI->Run();
        unset($cContUI);
        unset($cCont);
      }
      else{//у нас два действия - забыли пароль или логинимся
        if(isset($_GET[WTS_PARAM_ACTION])
        && $_GET[WTS_PARAM_ACTION] === WTS_PARAM_ACT_FORGOT){
          $this->Forgot();
        }
        else{$this->Login();}
      }
    }
    
    protected function Login(){
      $bRet = false;
      if(isset($_POST[WTS_PARAM_LOGIN])){//try login
        if(isset($_POST[WTS_PARAM_PWD])
        && strlen($_POST[WTS_PARAM_PWD]) > 0
        && ($sUID = cCont::mBeginSsn($this->cDb, $_POST[WTS_PARAM_LOGIN]))){
          $sUID = sha1($sUID . hash('sha256', $_POST[WTS_PARAM_PWD], false));
          
          if($cCont = cCont::mCheckSsn($this->cDb
                                     , $_POST[WTS_PARAM_LOGIN]
                                     , $sUID, WTS_SESSION_TIMEOUT
                                     , true)){
            $_SESSION[WTS_SESSION_PARAM_LOGIN] = $cCont->sLogin();
            $_SESSION[WTS_SESSION_PARAM_HASH]  = $sUID;
            
            $cLog = new cLog();
            $cLog->Value($cCont->sLogin() . ' successfully logged in from IP: ' . $cLog->sIP());
            $cLog->bSerialize($this->cDb);
            unset($cLog);
            unset($cCont);
            $bRet = true;
          }
          else{//bad passwd or contact disabled
            $cLog = new cLog(cLog::WARN);
            $cLog->Value('Fail login for: ' . $_POST[WTS_PARAM_LOGIN] . ' from IP: ' . $cLog->sIP());
            $cLog->bSerialize($this->cDb);
            unset($cLog);
            sleep(3);//подождём
          }
        }
      }
      if(!$bRet){
        $this->DoLoginPage(isset($_POST[WTS_PARAM_LOGIN]) ? $_POST[WTS_PARAM_LOGIN] : null);
      }
      else{header('Location: ' . WTS_HREF);}
    }
    
    protected function Forgot(){
      if(isset($_SESSION['capcha'])
      && isset($_POST['capcha'])
      && strtolower($_POST['capcha']) == $_SESSION['capcha']
      && isset($_POST[WTS_SESSION_PARAM_LOGIN])
      && $_POST[WTS_SESSION_PARAM_LOGIN] != WTS_DB_USER){//не надо ресетить админа
        if($sUID = cCont::mBeginSsn($this->cDb, $_POST[WTS_SESSION_PARAM_LOGIN])){
          $sPasswd = cString::sRand(WTS_PASSWD_LENGHT + 4);
          if(cCont::bPasswd($this->cDb, $sUID, $sPasswd)){
            //log
            $cLog = new cLog(cLog::WARN);
            $cLog->Value('Password reset request for login: ' . $_POST[WTS_SESSION_PARAM_LOGIN]
            . ' from IP: ' . $cLog->sIP());
            $cLog->bSerialize($this->cDb);
            unset($cLog);
            //send message
            $sBody = $aResponsesID = false;
            
            if(($cQueue = cQueue::mUnserialize($this->cDb, 1))
            && $cQueue->iUnserializeResps($this->cDb, $aResponsesID) > 0
            && isset($aResponsesID[cQueue::PWD])
            && ($cResponse = cResponse::mUnserialize($this->cDb, $aResponsesID[cQueue::PWD]))
            && ($sBody = $cResponse->mBuild($this->cDb))){
              foreach(cResponse::RPL_EXP as $sKey => $sExpr){
                $s = '';
                switch($sKey){
                  case 'SITE_TITLE': $s = WTS_TITLE;
                  break;
                  case 'NEW_PASSWD': $s = $sPasswd;
                  break;
                  case 'QUEUE_EMAIL': $s = $cQueue->sLogin();
                  break;
                  case 'QUEUE_NAME': $s = $cQueue->sName();
                  break;
                  case 'CSRMR_NAME':
                  if($cCont = cCont::mUnserialize($this->cDb, $_POST[WTS_SESSION_PARAM_LOGIN])){
                    if($cCont->sName()){$s = $cCont->sName();}
                    unset($cCont);
                  }
                  break;
                  default: break;
                }
                $sBody = str_replace($sExpr, $s, $sBody);
              }
              
              $cEvent = new cEvent(cEvent::OUT_EMAIL, 1);
              $cEvent->Subject(WTS_TITLE);
              $cEvent->From($cQueue->sLogin());
              $cEvent->AddTo($_POST[WTS_SESSION_PARAM_LOGIN]);
              
              $cEvent->AddBody($sBody);
              $cEvent->GenPreview(WTS_ENC, WTS_EVENT_PREVIEW_LENGHT);
              $cEvent->bSerialize($this->cDb, WTS_EVENT_THUMB_SIDE);
              
              unset($cQueue);
              unset($cEvent);
              header('Location: '. sAction(WTS_PARAM_ACT_FORGOT) . sSubAction(WTS_PARAM_EDIT, 'true'));
              
            }
            if(isset($aResponsesID)){unset($aResponsesID);}
            if(isset($sBody)){unset($sBody);}
          }
        }
        sleep(3);//ну и подождём
        $this->DoLoginPage();
      }
      else{
        sleep(3);//подождём
        $this->DoForgotPage();
      }
    }

    protected function DoLoginPage($sLogin = null){
      
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-center-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Content(new cElementBase(_('Welcome to') . ' ' . WTS_TITLE));
      
      $cElement->Content($cElementCapt);

      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', WTS_HREF);
      $cElementForm->Attribute('method', 'post');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Login'), WTS_PARAM_LOGIN));
      $cElementInput = cField::cElementInput(WTS_PARAM_LOGIN, (isset($sLogin) ? $sLogin : null));
      $cElementInput->Attribute('autofocus');
      $cField->Input($cElementInput);
      
      $cElementForm->Content($cField->cBuild());
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Password'), WTS_PARAM_PWD));      
      $cField->Input(cField::cElementInput(WTS_PARAM_PWD, null, 'password'));
      
      $cElementForm->Content($cField->cBuild());
      
      $cField = new cField(cField::RIGHT);
      $cField->Input(cField::cElementSubmit(_('Log In')));
      
      $cElementForm->Content($cField->cBuild());
      
      
      $cElement->Content($cElementForm);
      
      $cField = new cField();
      $cElementA = cSE::cCreate('a', _('Forgot your password?'), WTS_PFX . '-alert');
      $cElementA->Attribute('href', sAction(WTS_PARAM_ACT_FORGOT));
      $cElementA->Attribute('title', _('Restore Password for Login'));
      
      $cField->Label($cElementA);
      $cElement->Content($cField->cBuild());
      
      $cPage = new cPage();
      $cPage->Main($cElement);
      
      if(isset($sLogin)){
        $cPage->AddTitle();
        $cPage->Header(cField::cElementMessage(_('No contact match - please try again!'), true));
        echo $cPage->sSerialize(105);
      }
      else{echo $cPage->sSerialize();}
      unset($cPage);
    }

    protected function DoForgotPage(){
      
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-center-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Content(new cElementBase(_('Restore password')));
      
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', sAction(WTS_PARAM_ACT_FORGOT));
      $cElementForm->Attribute('method', 'post');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Login'), WTS_PARAM_LOGIN));
      $cElementInput = cField::cElementInput(WTS_PARAM_LOGIN);
      $cElementInput->Attribute('autofocus');
      $cField->Input($cElementInput);
      
      $cElementForm->Content($cField->cBuild());
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Code'), 'capcha'));
      $cElementInput = cField::cElementInput('capcha');
      $cField->Input($cElementInput);
      
      $cElementForm->Content($cField->cBuild());
      
      $cField = new cField();
      $cField->Label(cSE::cCreate('span', _('Please enter the code seen on the picture below.'), WTS_PFX . '-alert'));
      $cElementForm->Content($cField->cBuild());
      
      //make capcha image
      
      $cCapcha = new cCapcha(210, WTS_FONTS);
      $_SESSION['capcha'] = $cCapcha->sAddText();
      $cCapcha->AddDistortion();
      $cCapcha->AddNoise();
      $sImg = $cCapcha->sGetPngImage();
      unset($cCapcha);
      
      $cElementImg = new cElement('img', false);
      $cElementImg->Attribute('src', 'data:image/png;base64,'  . base64_encode($sImg));
      unset($sImg);
      
      $cField = new cField(cField::CENTER);
      $cField->Label($cElementImg);
      
      $cCapcha = $cField->cBuild();
      $cCapcha->Attribute('class', WTS_PFX . '-capcha');
      
      $cElementForm->Content($cCapcha);
      
      $cField = new cField(cField::RIGHT);
      $cField->Input(cField::cElementSubmit(_('Restore')));
      
      $cElementForm->Content($cField->cBuild());
      
      $cElement->Content($cElementForm);
      
      $cField = new cField();
      $cElementA = cSE::cCreate('a', _('Back to Login page'), WTS_PFX . '-notify');
      $cElementA->Attribute('href', WTS_HREF);
      $cElementA->Attribute('title',  _('Show Login page'));
      
      $cField->Label($cElementA);
      $cElement->Content($cField->cBuild());
      
      $cPage = new cPage();
      $cPage->Main($cElement);
      
      if(isset($_GET[WTS_PARAM_EDIT])){
        $cPage->AddTitle();
        $cPage->Header(cField::cElementMessage(_('Password was successfully restored and will be sent to your E-mail within 15 minutes.')));
      }
      else{
        $cPage->AddTitle();
        $cPage->Header(cField::cElementMessage(_('A new Password will be sent to your E-mail that matches with your Login.'), true));
      }
      
      echo $cPage->sSerialize(105);
      unset($cPage);
    }
  }

}
?>
