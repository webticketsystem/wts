<?php
/*
 * translate.php (part of WTS) - class for switch between languages
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  class cTranslate{
  /**
   * @param iUiLang - 1049 for Russia
   * @param sPath   - must end by /
   */
    public static function UpdateUiLang($iUiLang, $sPath){
      $iUiLang = (int)$iUiLang;
      $sPath   = (string)$sPath;

      switch($iUiLang){
        case 1049:
          putenv('LANG=ru_RU.utf8');
          setlocale(LC_ALL, 'ru_RU.utf8');
          setlocale(LC_NUMERIC, 'en_US.utf8');
          $domain = 'messages';
          bindtextdomain($domain, $sPath . 'locale');
          textdomain($domain);
        break;
        default:
          putenv('LANG=en_US.utf8');
          setlocale(LC_ALL, 'en_US.utf8');
        break;
      }
    }
  }


}

?>
