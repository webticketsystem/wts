<?php
/*
 * customer.php (part of WTS) - classes for Customer level UI
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('customerbase.php');
  
  /*-- helper class calendar --******************************************/
  class cCalendar{
    const YEAR = 10;
    const MNTH = 20;
    const DAY  = 30;
    
    public static function sType($iType){
      $s = false;
      switch($iType){
        case self::YEAR:  $s = _('Years');
        break;
        case self::MNTH: $s = _('Months');
        break;
        case self::DAY:   $s = _('Days');
        break;
        default: break;
      }
      return $s;
    }
  }

  /*-- user --***********************************************************/
  class cCustomer extends cCustomerBase{
    
    public function __construct(cDb &$cDb, cCont &$cCont){
      parent::__construct($cDb, $cCont);
    }
    

    
    public function Run(){
      
      $cPage = new cPage();
      
      $sDefAction = $sAction = WTS_PARAM_ACT_CHAIN;
      
      if(isset($_GET[WTS_PARAM_ACTION])){
        $sAction = $_GET[WTS_PARAM_ACTION];
      }
      
      switch($sAction){
        case WTS_PARAM_ACT_EVENT:
          $this->DoEventPage($cPage);
          break;
        case WTS_PARAM_ACT_CALENDAR:
          $this->DoCalendarPage($cPage);
          break;
        case WTS_PARAM_ACT_REPORT:
          $this->DoReportPage($cPage);
          break;
        case WTS_PARAM_ACT_ABOUT:
          $this->DoAboutPage($cPage);
          break;
        case WTS_PARAM_ACT_PROFILE:
          $this->DoProfilePage($cPage);
          break;
        case WTS_PARAM_ACT_LOGOUT:
          $this->cCont->CloseSsn($this->cDb);
          header('Location: ' . WTS_HREF);
          exit;
        default:
          if($sAction != $sDefAction){$sAction = $sDefAction;}
          $this->DoChainPage($cPage);
          break;
      }

      //--login info
      $cPage->Header($this->cAddAccount());
      $cPage->AddTitle();
      
      //--message - if exist
      $iHeaderHeight = 100;
      if(($i = $this->iAddMessage($cPage)) > 0){
        $iHeaderHeight += ($i * 35);
      }
      
      //--menu
      $cMenu = new cMenu();
      
      $cMenu->Child($this->cAddChainMenuItem($sAction));
      $cMenu->Child($this->cAddCalendarMenuItem($sAction));
      $cMenu->Child($this->cAddReportMenuItem($sAction));
      $cMenu->Child($this->cAddAboutMenuItem($sAction));
      
      $cPage->Header($cMenu->cRoot());

      echo $cPage->sSerialize($iHeaderHeight);
      unset($cPage);
    }
    
    protected function &cAddCalendarMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Calendar')
                         , _('Planned events')
                         , ($sCurAct !== WTS_PARAM_ACT_CALENDAR ? sAction(WTS_PARAM_ACT_CALENDAR) : null));
      return $cMenuItem;
    }
    
    public function DoCalendarPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref = sAction(WTS_PARAM_ACT_CALENDAR);
    
      $iCurCalendar = ((isset($_GET[WTS_PARAM_TYPE])
      && ($_GET[WTS_PARAM_TYPE] == cCalendar::YEAR
        || $_GET[WTS_PARAM_TYPE] == cCalendar::DAY)) ? $_GET[WTS_PARAM_TYPE] : cCalendar::MNTH);
      
      $iCurTime = ((isset($_GET[WTS_PARAM_TIME]) && $_GET[WTS_PARAM_TIME] > 0) ? $_GET[WTS_PARAM_TIME] : strtotime('now'));

      $aMonth = array(_('Jan'), _('Feb'), _('Mar'), _('Apr'), _('May'), _('Jun'), _('Jul'), _('Aug'), _('Sep'), _('Oct'), _('Nov'), _('Dec'),);
      
      $aWeek = false;
      if(WTS_DT_FD == '0'){$aWeek[] = _('Su');}
      $aWeek[] = _('Mo');
      $aWeek[] = _('Tu');
      $aWeek[] = _('We');
      $aWeek[] = _('Th');
      $aWeek[] = _('Fr');
      $aWeek[] = _('Sa');
      if(WTS_DT_FD == '1'){$aWeek[] = _('Su');}

      //020. big calendar table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-calendar-table');

      //fill MEETING-events from own open chains
      if($this->cCont->iType() == cCont::AGENT){
        cChain::bAddFilterOwner($this->cCont->iID());
      }
      else{
        cChain::bAddFilterCont($this->cCont->iID());
      }
      cChain::bAddFilterClosed(false);

      $aChains = false;
      $aEvents = false;
      if(($iRows = cChain::iCount($this->cDb))
      && cChain::iUnserialize($this->cDb, 1, $iRows, $aChains)){//если есть где искать
        foreach($aChains as $key => &$c){
          cEvent::ClearFilters();
          cEvent::AddFilterChain($c->iID());
          cEvent::bAddFilterType(cEvent::MEETING);

          if($iRows = cEvent::iCount($this->cDb)){
            cEvent::iUnserialize($this->cDb, 1, $iRows, $aEvents);
          }
          unset($aChains[$key]);
        }
        unset($aChains);
      }
      
      switch($iCurCalendar){
        case cCalendar::YEAR://show months

          $iTd = 0;
          $cElementTr = false;
          
          $iCurMonth = (int)date('n', $iCurTime) - 1;
          for($i = 0; $i < 12; $i++){
            if($iTd == 0){
              $cElementTr = new cElement('tr', false);
              $cElementTr->Attribute('height', '70px');
            }
            $cElementTd = new cElement('td', false);
            $cElementTd->Attribute('title', _('Overview of this Month'));
            $cElementTd->Content(new cElementBase($aMonth[$i]));
            if($i == $iCurMonth){$cElementTd->Attribute('class', WTS_PFX . '-table-sel');}

            $iNewCurTime = $iCurTime;
            $iCurDay = (int)date('j', $iCurTime);
            $iDaysOfNewMonth = (int)date('t', strtotime('28 ' . $aMonth[$i] . ' ' . date('Y', $iCurTime)));
            if($iCurDay > $iDaysOfNewMonth){
              $iNewCurTime = strtotime(($iDaysOfNewMonth - $iCurDay) . 'day', $iCurTime);
            }
            $cElementTd->Attribute('onclick', 'window.open(\'' . $sHref
            . sSubAction(WTS_PARAM_TYPE, cCalendar::MNTH)
            . sSubAction(WTS_PARAM_TIME, strtotime(($i - $iCurMonth) . 'month', $iNewCurTime))
            .'\', \'_self\')');
            
            $j = 0;
            $sTitle = '';
            if($aEvents){

              $iDtLow = strtotime(($i - $iCurMonth) . 'month', $iNewCurTime);
              $iDtLow = strtotime('00:00:00', $iDtLow);
              $iDtLow = strtotime('first day of this month', $iDtLow);
              $iDtHi = strtotime('first day of next month', $iDtLow) - 1;
              //add in month title event subjects
              foreach($aEvents as $key => &$c){
                if($c->iDtEnd() <= $iDtLow || $c->iDtBegin() >= $iDtHi){
                  continue;
                }
                
                if($j > 0){$sTitle .= ' ';}

                if(mb_strlen($c->sSubject(), WTS_ENC) > 31){
                  $sTitle .= mb_substr($c->sSubject(), 0, 31, WTS_ENC);
                }
                else{$sTitle .= $c->sSubject();}
                $j++;
                
                unset($aEvents[$key]);
              }
            }
            if($j > 0){
              if(mb_strlen($sTitle, WTS_ENC) > 255){
                $sTitle .= mb_substr($sTitle, 0, 255, WTS_ENC);
              }
              $cElementSpan = cSE::cCreate('span', '[' . $j . ']', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
              $cElementSpan->Attribute('title', htmlentities($sTitle));

              $cElementTd->Content($cElementSpan);
            }
            $cElementTr->Content($cElementTd);
            
            $iTd++;
            if($iTd > 2){
              $iTd = 0;
              $cTable->Row($cElementTr);
            }
          }
          $cElement->Content($cTable->cRoot());
        break;
        case cCalendar::MNTH://show days
          foreach($aWeek as &$sDay){
            $cTable->Column($sDay);
          }
          
          $iCurDay      = (int)date('j', $iCurTime);
          $iDayOfWeek   = (int)date('w', strtotime('first day of this month', $iCurTime));//0-6
          $iDaysOfMonth = (int)date('t', $iCurTime);
          if(WTS_DT_FD == '1'){
            if($iDayOfWeek > 0){$iDayOfWeek--;}
            else{$iDayOfWeek = 6;}
          }
          
          $cElementTr = false;

          if($iDayOfWeek > 0){
            $cElementTr = new cElement('tr', false);
            $cElementTr->Attribute('height', '50px');
            
            $iDaysOfPrevMonth = (int)date('j', strtotime('last day of previous month', $iCurTime));
            for($i = $iDaysOfPrevMonth - $iDayOfWeek + 1; $i < $iDaysOfPrevMonth + 1; $i++){
              $cElementTd = new cElement('td', false);
              $cElementTd->Attribute('class', WTS_PFX . '-table-empty');
              $cElementTd->Content(new cElementBase($i));
              $cElementTr->Content($cElementTd);
            }
          }
          for($i = 1; $i < $iDaysOfMonth + 1; $i++){
            
            if($iDayOfWeek == 0){
              $cElementTr = new cElement('tr', false);
              $cElementTr->Attribute('height', '50px');
            }
            $cElementTd = new cElement('td', false);
            $cElementTd->Content(new cElementBase($i));
            $cElementTd->Attribute('title', _('Overview of this Day'));
            if($i == $iCurDay){$cElementTd->Attribute('class', WTS_PFX . '-table-sel');}
            $cElementTd->Attribute('onclick', 'window.open(\'' . $sHref
            . sSubAction(WTS_PARAM_TYPE, cCalendar::DAY)
            . sSubAction(WTS_PARAM_TIME, strtotime(($i - $iCurDay) . 'day', $iCurTime))
            .'\', \'_self\')');

            $j = 0;
            $sTitle = '';
            if($aEvents){

              $iDtLow = strtotime('first day of this month', $iCurTime);
              if($i > 1){
                $iDtLow = strtotime(($i - 1) . ' day', $iDtLow);
              }
              $iDtLow = strtotime('00:00:00', $iDtLow);
              $iDtHi = $iDtLow + (24 * 3600) - 1;
              
              foreach($aEvents as &$c){
                if($c->iDtEnd() <= $iDtLow || $c->iDtBegin() >= $iDtHi){
                  continue;
                }

                if($j > 0){$sTitle .= ' ';}
                if(mb_strlen($c->sSubject(), WTS_ENC) > 31){
                  $sTitle .= mb_substr($c->sSubject(), 0, 31, WTS_ENC);
                }
                else{$sTitle .= $c->sSubject();}
                $j++;

              }
            }
            if($j > 0){
              if(mb_strlen($sTitle, WTS_ENC) > 255){
                $sTitle .= mb_substr($sTitle, 0, 255, WTS_ENC);
              }
              $cElementSpan = cSE::cCreate('span', '[' . $j . ']', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
              $cElementSpan->Attribute('title', htmlentities($sTitle));
              $cElementTd->Content($cElementSpan);
            }
            $cElementTr->Content($cElementTd);
            
            $iDayOfWeek++;
            if($iDayOfWeek > 6){
              $iDayOfWeek = 0;
              $cTable->Row($cElementTr);
            }
          }
          if($iDayOfWeek > 0){
            for($i = 1; $i < 8 - $iDayOfWeek; $i++){
              $cElementTd = new cElement('td', false);
              $cElementTd->Attribute('class', WTS_PFX . '-table-empty');
              $cElementTd->Content(new cElementBase($i));
              $cElementTr->Content($cElementTd);
            }
            $cTable->Row($cElementTr);
          }
          $cElement->Content($cTable->cRoot());
        break;
        case cCalendar::DAY://show hours
          $iHourHeight = 30;
          
          $cElementDivRel = new cElement('div', false);
          $cElementDivRel->Attribute('style', 'position: relative;');
          
          for($i = 0; $i < 24; $i++){
            $j = str_pad($i, 2, '0', STR_PAD_LEFT);
            $cElementTr = new cElement('tr', false);
            $cElementTr->Attribute('height', $iHourHeight . 'px');
            $cElementTd = new cElement('td', false);
            $cElementTd->Attribute('class', WTS_PFX . '-table-empty');
            $cElementTd->Attribute('style', 'text-align: left;');
            $cElementTd->Content(new cElementBase($j . ':00 - ' . $j . ':59'));
            
            $cElementTr->Content($cElementTd);
            $cTable->Row($cElementTr);
          }

          $aEvents2 = false;
          if($aEvents){
            $iDtLow = strtotime('00:00:00', $iCurTime);
            $iDtHi = $iDtLow + (24 * 3600) - 1;
            foreach($aEvents as $key => &$c){
              if($c->iDtEnd() <= $iDtLow || $c->iDtBegin() >= $iDtHi){
                unset($aEvents[$key]);
              }
              else{
                //--cut time
                if($c->iDtBegin() < $iDtLow){$c->DtBegin($iDtLow);}
                if($c->iDtEnd() > $iDtHi){$c->DtEnd($iDtHi);}
                $aEvents2[] = $c;
              }
            }
          }
          if($aEvents2){
            $cElementDivRel = new cElement('div', false);
            $cElementDivRel->Attribute('style', 'position: relative;');
            $cElementDivRel->Content($cTable->cRoot());
            
            $iDtStep = 3600 / $iHourHeight;
            
            $aEvents = cEvent::aQSByMaxDrt($aEvents2);

            foreach($aEvents as $key => &$c){
              $iShift  = ceil(($c->iDtBegin() - $iDtLow) / $iDtStep);
              $iHeight = ceil(($c->iDtEnd() - $c->iDtBegin()) / $iDtStep);
              
              $cElementBox = new cElement('div', false);
              $cElementBox->Attribute('class', WTS_PFX . '-calendar-offset-box');
              $cElementBox->Attribute('style', 'height:'. $iHeight . 'px; top: ' . $iShift . 'px;');
              
              $cElementEvt = new cElement('div', false);
              $cElementEvt->Attribute('class', WTS_PFX . '-calendar-offset');
              $cElementEvt->Attribute('title', htmlentities(date(WTS_D_FMT, $iCurTime) . ' ' . date(WTS_T_FMT, $c->iDtBegin()) . ' - ' . date(WTS_T_FMT, $c->iDtEnd()) . ' ' . _('Meeting for Chain:') . ' ' . $c->iChain()));
              $cElementEvt->Attribute('onclick', 'window.open(\''
              . sAction(WTS_PARAM_ACT_EVENT)
              . sSubAction(WTS_PARAM_ACT_CHAIN, $c->iChain())
              . sSubAction(WTS_PARAM_ID, $c->iID())
              . '\', \'_self\')');//page?
              if($iHeight > 20){//padding-top: 5px; + font size
                $cElementEvt->Content(new cElementBase($c->sSubject()));
              }
              
              $cElementBox->Content($cElementEvt);
              $cElementDivRel->Content($cElementBox);
              unset($aEvents[$key]);
            }
            
            $cElement->Content($cElementDivRel);
          }
          else{
            $cElement->Content($cTable->cRoot());
          }
        break;
        default:
        break;
      }

      $cPage->Main($cElement);

      //030. side bar calendar
      
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Content(new cElementBase(cCalendar::sType($iCurCalendar)));
      
      $cElement->Content($cElementCapt);
      
      $cTable = new cTable(WTS_PFX . '-aside-calendar-table');
      switch($iCurCalendar){
        case cCalendar::YEAR:
          $iCurYear = (int)date('Y', $iCurTime);
          $iTd = 0;

          $cElementTr = false;
          for($i = $iCurYear - 4; $i < $iCurYear + 5; $i++){
            if($iTd == 0){$cElementTr = new cElement('tr', false);}
            $cElementTd = new cElement('td', false);
            
            if($i == $iCurYear){
              $cElementTd->Attribute('class', WTS_PFX . '-table-sel');
            }
            else{
              $iNewCurTime = $iCurTime;
              if(2 == (int)date('n', $iCurTime) && 29 == (int)date('j', $iCurTime)){
                $iDaysOfNewMonth = (int)date('t', strtotime('28 ' . $aMonth[1] . ' ' . $i));
                if($iDaysOfNewMonth == 28){
                  $iNewCurTime = strtotime('-1day', $iCurTime);
                }
              }
              $cElementTd->Attribute('onclick', 'window.open(\'' . $sHref
              . sSubAction(WTS_PARAM_TYPE, $iCurCalendar)
              . sSubAction(WTS_PARAM_TIME, strtotime(($i - $iCurYear) . 'year', $iNewCurTime))
              .'\', \'_self\')');
            }
            
            $cElementTd->Content(new cElementBase($i));
            $cElementTr->Content($cElementTd);
            
            $iTd++;
            if($iTd > 2){
              $iTd = 0;
              $cTable->Row($cElementTr);
            }
          }
        break;
        case cCalendar::MNTH:
          $iCurMonth = (int)date('n', $iCurTime) - 1;
          
          $iTd = 0;
          $cElementTr = false;
          for($i = 0; $i < 12; $i++){
            if($iTd == 0){$cElementTr = new cElement('tr', false);}
            $cElementTd = new cElement('td', false);
           
            if($i == $iCurMonth){
              $cElementTd->Attribute('class', WTS_PFX . '-table-sel');
            }
            else{
              $iNewCurTime = $iCurTime;
              $iCurDay = (int)date('j', $iCurTime);
              $iDaysOfNewMonth = (int)date('t', strtotime('28 ' . $aMonth[$i] . ' ' . date('Y', $iCurTime)));
              if($iCurDay > $iDaysOfNewMonth){
                $iNewCurTime = strtotime(($iDaysOfNewMonth - $iCurDay) . 'day', $iCurTime);
              }
              
              $cElementTd->Attribute('onclick', 'window.open(\'' . $sHref
              . sSubAction(WTS_PARAM_TYPE, $iCurCalendar)
              . sSubAction(WTS_PARAM_TIME, strtotime(($i - $iCurMonth) . 'month', $iNewCurTime))
              .'\', \'_self\')');
            }
            
            $cElementTd->Content(new cElementBase($aMonth[$i]));
            $cElementTr->Content($cElementTd);
            
            $iTd++;
            if($iTd > 2){
              $iTd = 0;
              $cTable->Row($cElementTr);
            }
          }
        break;
        case cCalendar::DAY:
          foreach($aWeek as &$sDay){$cTable->Column($sDay);}
          
          $iCurDay      = (int)date('j', $iCurTime);
          $iDayOfWeek   = (int)date('w', strtotime('first day of this month', $iCurTime));
          $iDaysOfMonth = (int)date('t', $iCurTime);
          if(WTS_DT_FD == '1'){
            if($iDayOfWeek > 0){$iDayOfWeek--;}
            else{$iDayOfWeek = 6;}
          }
          
          $cElementTr = false;
          
          if($iDayOfWeek > 0){
            $cElementTr = new cElement('tr', false);
            
            $iDaysOfPrevMonth = (int)date('j', strtotime('last day of previous month', $iCurTime));
            for($i = $iDaysOfPrevMonth - $iDayOfWeek + 1; $i < $iDaysOfPrevMonth + 1; $i++){
              $cElementTd = new cElement('td', false);
              $cElementTd->Attribute('class', WTS_PFX . '-table-empty');
              $cElementTd->Content(new cElementBase($i));
              $cElementTr->Content($cElementTd);
            }
          }
          for($i = 1; $i < $iDaysOfMonth + 1; $i++){
            if($iDayOfWeek == 0){
              $cElementTr = new cElement('tr', false);
            }
            $cElementTd = new cElement('td', false);
            
            if($i == $iCurDay){
              $cElementTd->Attribute('class', WTS_PFX . '-table-sel');
            }
            else{
              $cElementTd->Attribute('onclick', 'window.open(\'' . $sHref
              . sSubAction(WTS_PARAM_TYPE, $iCurCalendar)
              . sSubAction(WTS_PARAM_TIME, strtotime(($i - $iCurDay) . 'day', $iCurTime))
              .'\', \'_self\')');
            }
            
            $cElementTd->Content(new cElementBase($i));
            $cElementTr->Content($cElementTd);
            
            $iDayOfWeek++;
            if($iDayOfWeek > 6){
              $iDayOfWeek = 0;
              $cTable->Row($cElementTr);
            }
          }
          if($iDayOfWeek > 0){
            for($i = 1; $i < 8 - $iDayOfWeek; $i++){
              $cElementTd = new cElement('td', false);
              $cElementTd->Attribute('class', WTS_PFX . '-table-empty');
              $cElementTd->Content(new cElementBase($i));
              $cElementTr->Content($cElementTd);
            }
            $cTable->Row($cElementTr);
          }
        break;
        default:
        break;
      }
      
      $cField = new cField();
      $cField->Label($cTable->cRoot());
      
      $cElement->Content($cField->cBuild());
      
      $cField = false;
      
      switch($iCurCalendar){
        case cCalendar::DAY:
          $cField = new cField(cField::CENTER);
          
          $cElementA = cSE::cCreate('a', $aMonth[(int)date('n', $iCurTime) - 1], WTS_PFX . '-link');
          $cElementA->Attribute('href', $sHref
          . sSubAction(WTS_PARAM_TYPE, cCalendar::MNTH)
          . sSubAction(WTS_PARAM_TIME, $iCurTime));
          $cElementA->Attribute('title', _('Change Month'));
          $cField->Label($cElementA);

        case cCalendar::MNTH:
          if($cField === false){$cField = new cField(cField::CENTER);}
          
          $cElementA = cSE::cCreate('a', date('Y', $iCurTime), WTS_PFX . '-link');
          $cElementA->Attribute('href', $sHref
          . sSubAction(WTS_PARAM_TYPE, cCalendar::YEAR)
          . sSubAction(WTS_PARAM_TIME, $iCurTime));
          $cElementA->Attribute('title', _('Change Year'));
          $cField->Label($cElementA);

          
        break;
        default:
        break;
      }
      
      if(date(WTS_D_FMT, $iCurTime) != date(WTS_D_FMT)){
        if($cField === false){$cField = new cField(cField::CENTER);}
        
        $cElementA = cSE::cCreate('a', _('Reset'), WTS_PFX . '-link');
        $cElementA->Attribute('href', $sHref
        . sSubAction(WTS_PARAM_TYPE, $iCurCalendar)
        . sSubAction(WTS_PARAM_TIME, strtotime('now')));
        $cElementA->Attribute('title', _('Current date & time'));
        $cField->Label($cElementA);
      }
      
      if($cField !== false){$cElement->Content($cField->cBuild());}
      
      
      $cPage->Aside($cElement);
    }
    
    public function DoReportPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref      = sAction(WTS_PARAM_ACT_REPORT);

      $aReports   = false;
      $cCurReport = false;
      
      cReport::bAddFilterType($this->cCont->iType());
      
      $iRows = cReport::iUnserialize($this->cDb, $aReports);
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurReport = &cReport::mInArray($aReports, $_GET[WTS_PARAM_ID]);
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_REPORT);
      $cElement->Content($cButton->cRoot());
      
      //select cur report
      $cButton = new cButton();
        
      $cSelect = new cSelect(WTS_PARAM_ID, WTS_PFX . '-toolbar');
      if($cCurReport === false){
        if($aReports){$cSelect->Option(true, 0, _('Please select Report...'));}
        else{$cSelect->Option(true, 0, _('No report found...'));}
      }
      
      if($aReports){
        foreach($aReports as &$c){
          $cSelect->Option(($cCurReport ? $cCurReport->iID() === $c->iID() : false), $c->iID(), $c->sName());
        }
      }
      
      $cElementS = $cSelect->cRoot();
      $cElementS->Attribute('onchange', 'this.form.submit();');
      $cButton->Content($cElementS);
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_REPORT);
      $cElement->Content($cButton->cRoot());
      
      //button save as
      if($cCurReport){
        $cButton = new cButton(WTS_PATH . 'greport.php');
        $cButton->Image(IMG_EDIT_SAVE, _('Save Report'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ID, $cCurReport->iID());
        $cElement->Content($cButton->cRoot());
      }
      $cPage->Main($cElement);
      
      //prepare query
      if($cCurReport
      && ($sQuery = $cCurReport->sValue())){
        //replace expr
        foreach(cReport::RPL_EXP as $sKey => $sExpr){
          $s = '';
          switch($sKey){

            case 'CONT_ID': $s = (string)$this->cCont->iID();
            break;
            case 'GROUP_ID':
            if($this->cCont->iGroup() > 0){$s = (string)$this->cCont->iGroup();}
            break;
            case 'QUEUE_ID':
            if($this->cCont->iQueue() > 0){$s = (string)$this->cCont->iQueue();}
            break;
            default: break;
          }
          $sQuery = str_replace($sExpr, $s, $sQuery);
        }
        
        $this->cDb->QueryRes($sQuery);
        if($aHdr = $this->cDb->aHeader()){
          
          //040. table
          $cElement = new cElement('section', false);
          $cElement->Attribute('class', WTS_PFX . '-main-section');
          
          $cTable = new cTable(WTS_PFX . '-table');
          
          foreach($aHdr as &$s){$cTable->Column($s);}
          
          if($this->cDb->iRowCount() > 0){
            while($aRow = $this->cDb->aRow()){
            
              $cElementTr = new cElement('tr', false);
              
              foreach($aRow as &$s){
                $cElementTd = new cElement('td', false);
                $cElementTd->Content(new cElementBase($s));
                $cElementTr->Content($cElementTd);
              }
              $cTable->Row($cElementTr);
            }
            $this->cDb->FreeResult();
          }
          else{$cTable->ColSpan(_('No data found'));}
      
          $cElement->Content($cTable->cRoot());

          $cPage->Main($cElement);
        }
      }
    }

  }


}
?>
