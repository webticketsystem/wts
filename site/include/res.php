<?php
/*
 * res.php (part of WTS) - resources for UI
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('defs.php');
  
  define('WTS_IMG_PATH',               WTS_PATH . 'img/');
  define('WTS_FNT_PATH',               WTS_PATH . 'fnt/');
  define('WTS_CSS_PATH',               WTS_PATH . 'css/');
  
  //fonts
  const WTS_FONTS = array(WTS_FNT_PATH . 'Arvo.ttf'
                        , WTS_FNT_PATH . 'DroidSerif.ttf'
                        , WTS_FNT_PATH . 'EBGaramond.ttf'
                        , WTS_FNT_PATH . 'LiberationSerif.ttf'
                        , WTS_FNT_PATH . 'Lora.ttf'
                        , WTS_FNT_PATH . 'OldStandard.ttf'
                        , WTS_FNT_PATH . 'PTF.ttf'
                        , WTS_FNT_PATH . 'RobotoSlab.ttf'
                         );
                         
  //styles
  const WTS_STYLES = array(WTS_CSS_PATH . 'default.css'
                         , WTS_CSS_PATH . 'blue.css'
                         , WTS_CSS_PATH . 'green.css'
                         , WTS_CSS_PATH . 'pink.css'
                         , WTS_CSS_PATH . 'sand.css'
                          );
                          
  //languages
  const WTS_LANGS  = array(1033 => 'English (en_US)'
                         , 1049 => 'Русский (ru_RU)'
                          );

  //images
  define('IMG_CONTACT',                WTS_IMG_PATH . 'ic_contact.png');
  define('IMG_AGENT',                  WTS_IMG_PATH . 'ic_agent.png');
  define('IMG_GROUP',                  WTS_IMG_PATH . 'ic_group.png');
  define('IMG_PRODUCT',                WTS_IMG_PATH . 'ic_product.png');
  define('IMG_CHILD_PRODUCTS',         WTS_IMG_PATH . 'ic_action_share.png');
  define('IMG_DISABLED',               WTS_IMG_PATH . 'ic_disabled.png');
  define('IMG_SUSPENDED',              WTS_IMG_PATH . 'ic_suspended.png');
  define('IMG_ENABLED',                WTS_IMG_PATH . 'ic_enabled.png');

  define('IMG_EDIT_CHAIN_LOCK',        WTS_IMG_PATH . 'ic_action_chain_lock.png');
  define('IMG_EDIT_MOVE_UP',           WTS_IMG_PATH . 'ic_action_collapse.png');
  define('IMG_EDIT_MOVE_DOWN',         WTS_IMG_PATH . 'ic_action_expand.png');
  define('IMG_EDIT_REMOVE',            WTS_IMG_PATH . 'ic_action_remove.png');
  define('IMG_EDIT_SAVE',              WTS_IMG_PATH . 'ic_action_save.png');
  define('IMG_EDIT_FORWARD',           WTS_IMG_PATH . 'ic_action_redirect.png');
  define('IMG_EDIT_REFRESH',           WTS_IMG_PATH . 'ic_action_refresh.png');
  define('IMG_EDIT_NEW_EVENT',         WTS_IMG_PATH . 'ic_action_event_add.png');
  define('IMG_EDIT_NEW_CHAIN',         WTS_IMG_PATH . 'ic_action_chain_add.png');
  define('IMG_EDIT_MERGE',             WTS_IMG_PATH . 'ic_action_merge.png');
  define('IMG_EDIT_SPLIT',             WTS_IMG_PATH . 'ic_action_split.png');
  define('IMG_EDIT_PASTE',             WTS_IMG_PATH . 'ic_action_paste.png');

  define('IMG_WARN',                   WTS_IMG_PATH . 'ic_action_warning.png');
  define('IMG_ACPT',                   WTS_IMG_PATH . 'ic_action_accept.png');

  define('IMG_EVENT_IN_CALL',          WTS_IMG_PATH . 'ic_action_event_in_call.png');
  define('IMG_EVENT_IN_EMAIL',         WTS_IMG_PATH . 'ic_action_event_in_email.png');
  define('IMG_EVENT_OUT_CALL',         WTS_IMG_PATH . 'ic_action_event_out_call.png');
  define('IMG_EVENT_OUT_EMAIL',        WTS_IMG_PATH . 'ic_action_event_out_email.png');
  define('IMG_EVENT_MEETING',          WTS_IMG_PATH . 'ic_action_event_meeting.png');
  define('IMG_EVENT_NEW',              WTS_IMG_PATH . 'ic_action_event_new.png');
  define('IMG_EVENT_LOCK',             WTS_IMG_PATH . 'ic_action_event_lock.png');
  

  define('IMG_ATTR_SIMPLE',            WTS_IMG_PATH . 'ic_simple_attr.png');
  define('IMG_ATTR_MULTIPLE',          WTS_IMG_PATH . 'ic_multiple_attr.png');

  
  //define('IMG_CHAIN_BKGRND',           WTS_IMG_PATH . 'ic_action_chain_bkgrnd.png');

  define('IMG_MAIN_ICO',               WTS_IMG_PATH . 'logo.ico');
  define('IMG_MAIN_LOGO',              WTS_IMG_PATH . 'logo.png');
  define('IMG_MAIN_BKGRND',            WTS_IMG_PATH . 'bg.png');
  
  define('IMG_NAV_FIRST',              WTS_IMG_PATH . 'ic_action_first_item.png');
  define('IMG_NAV_PREV',               WTS_IMG_PATH . 'ic_action_previous_item.png');
  define('IMG_NAV_NEXT',               WTS_IMG_PATH . 'ic_action_next_item.png');
  define('IMG_NAV_LAST',               WTS_IMG_PATH . 'ic_action_last_item.png');
  
  //image size setings
  //define('WTS_IMG_TABLE_SIDE',      '24');//width and height for tables images in pixels
  //define('WTS_IMG_TABLE_WIDTH',     WTS_IMG_TABLE_SIDE);
  //define('WTS_IMG_TABLE_HEIGHT',    WTS_IMG_TABLE_SIDE);
  //define('WTS_IMG_THUMB_SIDE',     '160');//width and height for thumbnails (previews) in pixels
  
  
  
  
  /*-- strings --******************************************************/
  
  //SESSION param names
  define('WTS_COOKIE_PARAM_UILANG', 'uilang');
  define('WTS_COOKIE_PARAM_CSS',    'css');
  
  //SESSION param names
  define('WTS_SESSION_PARAM_LOGIN', 'login');
  define('WTS_SESSION_PARAM_HASH',  'hash');
  
  
  //POST or GET param names
  define('WTS_PARAM_LOGIN',          WTS_SESSION_PARAM_LOGIN);
  define('WTS_PARAM_PWD',           'passwd');
  define('WTS_PARAM_NAME',          'name');
  define('WTS_PARAM_VALUE',         'value');
  define('WTS_PARAM_SLUG',          'slug');
  define('WTS_PARAM_IN_NAME',       'in_name');
  define('WTS_PARAM_COLOR',         'color');
  define('WTS_PARAM_LANG',          'language');
  define('WTS_PARAM_MULTIPLE',      'multiple');
  define('WTS_PARAM_NODE',          'node');
  define('WTS_PARAM_DISABLED',      'disabled');
  define('WTS_PARAM_CLOSED',        'closed');
  define('WTS_PARAM_FORWARD',       'fwd');
  define('WTS_PARAM_IMAP',          'imap');
  define('WTS_PARAM_SMTP',          'smtp');
  define('WTS_PARAM_TEMPLATE',      'template');
  define('WTS_PARAM_ATTRIBUTE',     'attr');
  define('WTS_PARAM_TIME',          'time');//in calendar
  define('WTS_PARAM_COUNT',         'count');//in products
  
  define('WTS_PARAM_FROM',          'from');
  define('WTS_PARAM_TO',            'to');
  define('WTS_PARAM_CC',            'cc');
  define('WTS_PARAM_BCC',           'bcc');
  define('WTS_PARAM_SUBJECT',       'sbj');
  define('WTS_PARAM_BEGIN',         'bgn');
  define('WTS_PARAM_END',           'end');
  define('WTS_PARAM_FILE',          'file');
  define('WTS_PARAM_THUMB',         'thumb');
  define('WTS_PARAM_RESPONSIBLE',   'resp');
  define('WTS_PARAM_CONTACT',       'cont');
  define('WTS_PARAM_COST',          'cost');
  define('WTS_PARAM_PARENT',        'parent');
  
  define('WTS_PARAM_SCROLL',        's');
  define('WTS_PARAM_ID',            'i');
  define('WTS_PARAM_PAGE',          'p');
  define('WTS_PARAM_SEARCH',        'q');
  define('WTS_PARAM_ACTION',        'a');
  define('WTS_PARAM_EDIT',          'e');
  define('WTS_PARAM_TYPE',          't');
  //--action
  define('WTS_PARAM_ACT_FORGOT',    'fgt');
  define('WTS_PARAM_ACT_LOGOUT',    'lgt');
  define('WTS_PARAM_ACT_PROFILE',   'prf');
  define('WTS_PARAM_ACT_DASHBOARD', 'dbd');
  define('WTS_PARAM_ACT_REPORT',    'rpt');
  define('WTS_PARAM_ACT_LOG',       'log');
  define('WTS_PARAM_ACT_QUEUE',     'que');
  define('WTS_PARAM_ACT_SALUTATION','slt');
  define('WTS_PARAM_ACT_SIGNATURE', 'sgt');
  define('WTS_PARAM_ACT_RESPONSE',  'res');
  define('WTS_PARAM_ACT_REGEXP',    'reg');
  define('WTS_PARAM_ACT_CHAIN',     'chn');
  define('WTS_PARAM_ACT_EVENT',     'evt');
  define('WTS_PARAM_ACT_CONT',      'cnt');
  define('WTS_PARAM_ACT_CONT_ATTR', 'cat');
  define('WTS_PARAM_ACT_GROUP',     'grp');
  define('WTS_PARAM_ACT_GROUP_ATTR','gat');
  define('WTS_PARAM_ACT_ABOUT',     'abt');
  define('WTS_PARAM_ACT_TAG',       'tag');
  define('WTS_PARAM_ACT_CALENDAR',  'cal'); // )))
  define('WTS_PARAM_ACT_CONTR',     'ctr');
  define('WTS_PARAM_ACT_PRODUCT',   'pdt');
  define('WTS_PARAM_ACT_PROD_ATTR', 'pat');
  define('WTS_PARAM_ACT_CATEGORY',  'ctg');
  
  //--edit-action
  define('WTS_PARAM_EACT_CREATE',   'crt');
  define('WTS_PARAM_EACT_DELETE',   'del');
  define('WTS_PARAM_EACT_MERGE',    'mrg');
  define('WTS_PARAM_EACT_UPDATE',   'upd');
  define('WTS_PARAM_EACT_MOVE_UP',  'mvu');
  define('WTS_PARAM_EACT_MOVE_DOWN','mvd');
  define('WTS_PARAM_EACT_CLOSE',    'cls');
  define('WTS_PARAM_EACT_DETACH',   'dtc');
  define('WTS_PARAM_EACT_CHOWN',    'cwn');
                         
  function sAction($sValue){
    return (WTS_HREF . '?' . WTS_PARAM_ACTION . '=' . $sValue);
  }
  
  function sSubAction($sName, $sValue){
    return ('&' . $sName . '=' . $sValue);
  }

}
?>
