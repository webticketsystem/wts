<?php
/*
 * customerbase.php (part of WTS) - base class cCustomerBase (for Customer and Agent and Admin UI)
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('view.php');
  require_once('core/utils/db.php');
  require_once('core/log.php');
  require_once('core/msg.php');
  require_once('core/event.php');
  require_once('core/tag.php');
  require_once('core/chain.php');
  require_once('core/group.php');
  require_once('core/report.php');
  require_once('core/signature.php');
  require_once('core/response.php');
  require_once('core/queue.php');
  require_once('core/regexp.php');
  require_once('core/upload.php');
  //require_once('./core/*.php');
  
    /*-- helper class to build chain page --*******************************/
  class cChActivity{
    const MY_OPN  = 10; //my open chains          - customer & agent (default for customer)
    const QUE_OPN = 20; //open chains in my queue - agent (default for agent)
    const ALL_OPN = 30; //all open chains         - admin (default for admin)
    const ALL_MY  = 40; //all my chains           - customer & agent
    const ALL_GRP = 50; //all chain in my group   - customer
    const ALL     = 60; //all chain               - agent & admin
    
    public static function AddActivity($iType, &$aArray){
      $iType = (int)$iType;
      switch($iType){
        case self::MY_OPN:
          $aArray[self::MY_OPN]  = _('My Open Chains');
        break;
        case self::QUE_OPN:
          $aArray[self::QUE_OPN] = _('Open Chains in My Queue');
        break;
        case self::ALL_OPN:
          $aArray[self::ALL_OPN] = _('All Open Chains');
        break;
        case self::ALL_MY:
          $aArray[self::ALL_MY]  = _('All My Chains');
        break;
        case self::ALL_GRP:
          $aArray[self::ALL_GRP] = _('All Chains in My Group');
        break;
        case self::ALL:
          $aArray[self::ALL]     = _('All Chains');
        break;
        default: break;
      }
    }
  }
  
  /*-- customerbase --*************************************************/
  abstract class cCustomerBase{
    protected $cDb;
    protected $cCont;
    
    public function __construct(cDb &$cDb, cCont &$cCont){
      $this->cDb   = $cDb;
      $this->cCont = $cCont;
     
    }
    
    protected function &cAddAccount(){
      $cElement = new cElement('hgroup', false);
      $cElement->Attribute('id', WTS_PFX . '-account');
      
      //new events info
      if($this->cCont->iType() === cCont::AGENT){
        cChain::ClearFilters();
        cChain::bAddFilterOwner($this->cCont->iID());
        cChain::bAddFilterClosed(false);
        
        $aChains = false;
        $aEvents = false;
        if(($iRows = cChain::iCount($this->cDb))
        && cChain::iUnserialize($this->cDb, 1, $iRows, $aChains)){//если есть где искать
          foreach($aChains as $key => &$c){
            cEvent::ClearFilters();
            cEvent::AddFilterChain($c->iID());
            cEvent::bAddFilterProcessed(false);

            if($iRows = cEvent::iCount($this->cDb)){
              
              //svg star
              $cElementSvg = new cElement('svg', false);
              $cElementSvg->Attribute('class', WTS_PFX . '-svg-ico');
              $cElementSvg->Attribute('viewBox', '-2 -2 74 74');
              
              $sStyle = 'stroke: none; fill: white;';
              
              $cElementP = new cElement('path', false);
              $cElementP->Attribute('d', 'M 35 0 L 43 26 L 70 27 L 49 43 L 57 70 L 35 54 L 13 70 L 21 43 L 0 27 L 27 26 Z');
              $cElementP->Attribute('style', 'fill: rgba(255, 255, 0, .5); stroke:rgb(255, 255, 0); stroke-linejoin: round; stroke-width:5%;');
              $cElementSvg->Content($cElementP);

              $cElementA = new cElement('a', false);
              $cElementA->Attribute('class', WTS_PFX . '-svg-ico-a');
              $cElementA->Attribute('href', sAction(WTS_PARAM_ACT_EVENT)
              . sSubAction(WTS_PARAM_ACT_CHAIN, $c->iID()));
              $cElementA->Attribute('title', $iRows . _(' New Event(s) for Chain ') . $c->iID());
              
              $cElementA->Content($cElementSvg);

              $cElement->Content($cElementA);
            }
            unset($aChains[$key]);
          }
          unset($aChains);
        }
      }
      
      $cElementA = new cElement('a', false);
      $cElementA->Attribute('href', sAction(WTS_PARAM_ACT_PROFILE));
      $cElementA->Attribute('title', _('Change Account Settings'));
      $cElementA->Content(new cElementBase($this->cCont->sLogin()));
      
      $cElement->Content($cElementA);
      
      //svg exit
      $cElementSvg = new cElement('svg', false);
      $cElementSvg->Attribute('class', WTS_PFX . '-svg-ico');
      $cElementSvg->Attribute('viewBox', '0 0 70 70');
      
      $sStyle = 'stroke: none; fill: white;';
      
      $cElementP = new cElement('path', false);
      $cElementP->Attribute('d', 'M 15 20 L 40 20 L 40 0 L 70 35 L 40 70 L 40 50 L 15 50 Z');
      $cElementP->Attribute('style', $sStyle);
      $cElementSvg->Content($cElementP);
      
      $cElementP = new cElement('path', false);
      $cElementP->Attribute('d', 'M 30 0 L 30 5 L 10 5 Q 5 5 5 10 L 5 60 Q 5 65 10 65 L 30 65 L 30 70 L 10 70 Q 0 70 0 60 L 0 10 Q 0 0 10 0 Z');
      $cElementP->Attribute('style', $sStyle);
      $cElementSvg->Content($cElementP);
      
      $cElementA = new cElement('a', false);
      $cElementA->Attribute('class', WTS_PFX . '-svg-ico-a');
      $cElementA->Attribute('href', sAction(WTS_PARAM_ACT_LOGOUT));
      $cElementA->Attribute('title', _('Exit from system'));
      $cElementA->Content($cElementSvg);
      
      $cElement->Content($cElementA);
      
      return $cElement;
      
    }

    protected function iAddMessage(cPage &$cPage){
      $iMsg = 0;
      $aMsgs = false;

      if(($iMsg = cMsg::iUnserialize($this->cDb, $this->cCont->iID(), $aMsgs)) > 0){
        foreach($aMsgs as $key => &$c){
          $cPage->Header(cField::cElementMessage($c->sValue(), ($c->iType() === cMsg::WARN)));
          unset($aMsgs[$key]);
        }
      }
      return $iMsg;
    }
    
    /**
     * create Element Time
     */
    protected function &cDoTime($iUnixTimeStamp){
      return cSE::cCreate('b', date(WTS_ST_FMT, $iUnixTimeStamp));
    }
    /**
     * create Element Date
     */
    protected function &cDoDate($iUnixTimeStamp){
      $sDate = date(WTS_D_FMT, $iUnixTimeStamp);
      if($sDate == date(WTS_D_FMT)){$sDate = _('Today');}
      elseif($sDate == date(WTS_D_FMT, strtotime('-1 day'))){$sDate = _('Yesterday');}
      elseif($sDate == date(WTS_D_FMT, strtotime('+1 day'))){$sDate = _('Tomorrow');}
      //$cElement->Content(cSE::cCreate('small', $sDate));
      return cSE::cCreate('small', $sDate);
    }
    /**
     * create Element of Contact Info dependent by User Type
     * 
     * return cElement
     */
    protected function &cDoContInfo(cCont &$cCont){
      $cElement = false;
      
      $sName = ($cCont->sName() ? htmlentities($cCont->sName()) : _('Name not set'));
      
      if($this->cCont->iType() === cCont::CSTMR){
        $cElement = new cElementBase($sName);
      }
      else{
        $cElement = new cElement('a', false);
        $cElement->Attribute('title', '&lt;' .  $cCont->sLogin() . '&gt;');
        $cElement->Attribute('href', sAction(WTS_PARAM_ACT_CONT)
        . sSubAction(WTS_PARAM_SEARCH, urlencode($cCont->sLogin()))
        . sSubAction(WTS_PARAM_ID, $cCont->iID()));
        $cElement->Content(new cElementBase($sName));
      }
      
      return $cElement;
    }
    
    protected function &cDoGroupInfo(cGroup &$cGroup){
      $cElement = false;
      
      $sName = htmlentities($cGroup->sName());

      if($this->cCont->iType() === cCont::CSTMR){
        $cElement = new cElementBase($sName);
      }
      else{
        $cElement = new cElement('a', false);
        $cElement->Attribute('title', $sName);
        $cElement->Attribute('href', sAction(WTS_PARAM_ACT_GROUP)
        . sSubAction(WTS_PARAM_SEARCH, urlencode($cGroup->sName()))
        . sSubAction(WTS_PARAM_ID, $cGroup->iID()));
        $cElement->Content(new cElementBase($sName));
      }
      return $cElement;
    }
    
    protected function DoChainInfo(cToc &$cToc, cChain &$cChain){
      
      $cToc->Child(new cElementBase(_('Created')), $this->cDoTime($cChain->iDtCreated()));
      $cToc->AddLinkForLastChild($this->cDoDate($cChain->iDtCreated()));
      
      $cContCr = $bUnsetCr = false;
      if($cChain->iCreator() !== $this->cCont->iID()){
        $cContCr = cCont::mUnserialize($this->cDb, $cChain->iCreator());
        $bUnsetCr = true;
      }
      else{$cContCr = $this->cCont;}
      
      $cToc->Child(new cElementBase(_('Created by'))
                 , ($cContCr === false ? new cElementBase(_('Unknown')) : $this->cDoContInfo($cContCr)));
                 
      $cToc->Child(new cElementBase(_('Updated')), $this->cDoTime($cChain->iDtUpdated()));
      $cToc->AddLinkForLastChild($this->cDoDate($cChain->iDtUpdated()));
      
      $cContRp = $bUnsetRp = false;
      
      if($cChain->iUpdater() != $cContCr->iID()){
        if($cChain->iUpdater() != $this->cCont->iID()){
          $cContRp = cCont::mUnserialize($this->cDb, $cChain->iUpdater());
          $bUnsetRp = true;
        }
        else{$cContRp = $this->cCont;}
      }
      else{$cContRp = $cContCr;}
        
      $cToc->Child(new cElementBase(_('Responsible'))
                  , ($cContRp === false ? new cElementBase(_('Unknown')) : $this->cDoContInfo($cContRp)));
                   
      //get owner
      if($cChain->bClosed() === false
      && $this->cCont->iQueue() === $cChain->iQueue()
      && $cContRp !== false
      && $cContRp->iID() !== $this->cCont->iID()
      && $cContRp->bIsOnline($this->cDb, WTS_SESSION_TIMEOUT) === false){
        $cElementA = cSE::cCreate('a', _('Get Owner'), WTS_PFX . '-link');
        $cElementA->Attribute('href', sAction(WTS_PARAM_ACT_EVENT)
        . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_CHOWN)
        . sSubAction(WTS_PARAM_ACT_CHAIN, $cChain->iID()));
        $cElementA->Attribute('title', _('Let me be Responsible'));
        $cToc->AddLinkForLastChild($cElementA);
      }
      
      if($bUnsetRp){unset($cContRp);}
      if($bUnsetCr){unset($cContCr);}
      
      //contact
      $cContCu = $bUnsetCu = false;
      
      if($cChain->iCont() > 0){
        if($cChain->iCont() != $this->cCont->iID()){
          $cContCu = cCont::mUnserialize($this->cDb, $cChain->iCont());
          $bUnsetCu = true;
        }
        else{$cContCu = $this->cCont;}
      }
      
      $cToc->Child(new cElementBase(_('Contact'))
                  , ($cContCu === false ? new cElementBase(_('Not set')) : $this->cDoContInfo($cContCu)));
      
      if($bUnsetCu){unset($cContCu);}
      
      //group
      if($cChain->iGroup() > 0
      && ($cGroup = cGroup::mUnserialize($this->cDb, $cChain->iGroup()))){
        $cToc->Child(new cElementBase(_('Group')), $this->cDoGroupInfo($cGroup));
        unset($cGroup);
      }
    }
    
    protected function &cElementEventImg($iEventType){
      $cElementImg = new cElement('img', false);
      $cElementImg->Attribute('class', WTS_PFX . '-table-image');
      switch($iEventType){
        case cEvent::IN_CALL:
          $cElementImg->Attribute('src', IMG_EVENT_IN_CALL);
          $cElementImg->Attribute('title', cEvent::sType(cEvent::IN_CALL));
        break;
        case cEvent::IN_EMAIL:
          $cElementImg->Attribute('src', IMG_EVENT_IN_EMAIL);
          $cElementImg->Attribute('title', cEvent::sType(cEvent::IN_EMAIL));
        break;
        case cEvent::OUT_CALL:
          $cElementImg->Attribute('src', IMG_EVENT_OUT_CALL);
          $cElementImg->Attribute('title', cEvent::sType(cEvent::OUT_CALL));
        break;
        case cEvent::OUT_EMAIL:
          $cElementImg->Attribute('src', IMG_EVENT_OUT_EMAIL);
          $cElementImg->Attribute('title', cEvent::sType(cEvent::OUT_EMAIL));
        break;
        case cEvent::MEETING:
          $cElementImg->Attribute('src', IMG_EVENT_MEETING);
          $cElementImg->Attribute('title', cEvent::sType(cEvent::MEETING));
        break;
        default: break;
      }
      return $cElementImg;
    }

    protected function CreateButtonsNav($sAction, $iPage, $iPages, &$aButtons){
      //last
      $aButtons['last'] = new cButton();
      $aButtons['last']->Image(IMG_NAV_LAST, _('Go to Last Page'), WTS_PFX . '-toolbar-nav-button');
      $aButtons['last']->Hidden(WTS_PARAM_ACTION, $sAction);
      $aButtons['last']->Hidden(WTS_PARAM_PAGE, $iPages);
      //next
      $aButtons['next'] = new cButton();
      $aButtons['next']->Image(IMG_NAV_NEXT, _('Go to Next Page'), WTS_PFX . '-toolbar-nav-button');
      $aButtons['next']->Hidden(WTS_PARAM_ACTION, $sAction);
      $aButtons['next']->Hidden(WTS_PARAM_PAGE, ($iPage < $iPages ? $iPage + 1 : $iPages));
      
      //searchbox (goto)
      $aButtons['search'] = new cButton();
      $cElementInp = new cElement('input', false);
      $cElementInp->Attribute('type', 'search');
      $cElementInp->Attribute('class', WTS_PFX . '-toolbar-nav-search');
      $sPages = (string)$iPages;
      switch(strlen($sPages)){
        case 3:
        $cElementInp->Attribute('style', 'width: 68px;');
        break;
        case 4:
        $cElementInp->Attribute('style', 'width: 102px;');
        break;
        default: break;
      }
      $cElementInp->Attribute('name', WTS_PARAM_PAGE);
      $cElementInp->Attribute('placeholder', $iPage . '/' . $iPages);
      $cElementInp->Attribute('title', _('Enter Page to Go'));
      $aButtons['search']->Content($cElementInp);
      $aButtons['search']->Hidden(WTS_PARAM_ACTION, $sAction);
      //prev
      $aButtons['prev'] = new cButton();
      $aButtons['prev']->Image(IMG_NAV_PREV, _('Go to Previous Page'), WTS_PFX . '-toolbar-nav-button');
      $aButtons['prev']->Hidden(WTS_PARAM_ACTION, $sAction);
      $aButtons['prev']->Hidden(WTS_PARAM_PAGE, ($iPage > 1 ? $iPage - 1 : 1));
      //first
      $aButtons['first'] = new cButton();
      $aButtons['first']->Image(IMG_NAV_FIRST, _('Go to First Page'), WTS_PFX . '-toolbar-nav-button');
      $aButtons['first']->Hidden(WTS_PARAM_ACTION, $sAction);
      $aButtons['first']->Hidden(WTS_PARAM_PAGE, 1);
    }
    
    abstract public function Run();
    
    //для заполнения свойств в fwd
    protected function sDoPropValue($sProp, $sValue){

      $cElement = new cElement('p', false);
      $cElement->Content(cSE::cCreate('b', $sProp . ':'));
      $cElement->Content(cSE::cCreate('span', $sValue));
      
      $s = '';
      
      return $cElement->sSerialize($s);
    }
    
    protected function &cDoEvent(cEvent &$cEvent){
      
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('From'), WTS_PARAM_FROM . 1));
      
      $cElementInput = cField::cElementInput(WTS_PARAM_FROM . 1, $cEvent->sFrom());
      $cElementInput->Attribute('disabled');
          
      $cField->Input($cElementInput);
        
      $cElement->Content($cField->cBuild(true));
      
      if($cEvent->iUnserializeAddr($this->cDb) > 0){
        $aAddr = $cEvent->aTo();
        if(count($aAddr) > 0){
          $cField = new cField();
          $cField->Label(cField::cElementLabel(_('To'), WTS_PARAM_TO . 1));
          
          $cElementInput = cField::cElementInput(WTS_PARAM_TO . 1, implode(', ', $aAddr));
          $cElementInput->Attribute('disabled');
              
          $cField->Input($cElementInput);
          $cElement->Content($cField->cBuild(true));
        }
        $aAddr = $cEvent->aCc();
        if(count($aAddr) > 0){
          $cField = new cField();
          $cField->Label(cField::cElementLabel(_('Cc'), WTS_PARAM_CC . 1));
          
          $cElementInput = cField::cElementInput(WTS_PARAM_CC . 1, implode(', ', $aAddr));
          $cElementInput->Attribute('disabled');
              
          $cField->Input($cElementInput);
          $cElement->Content($cField->cBuild(true));
        }
        $aAddr = $cEvent->aBcc();
        if(count($aAddr) > 0){
          $cField = new cField();
          $cField->Label(cField::cElementLabel(_('Bcc'), WTS_PARAM_BCC . 1));
          
          $cElementInput = cField::cElementInput(WTS_PARAM_BCC . 1, implode(', ', $aAddr));
          $cElementInput->Attribute('disabled');
              
          $cField->Input($cElementInput);
          $cElement->Content($cField->cBuild(true));
        }
      }
      
      if($cEvent->sSubject()){
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Subject'), WTS_PARAM_SUBJECT . 1));
        
        $cElementInput = cField::cElementInput(WTS_PARAM_SUBJECT . 1, $cEvent->sSubject());
        $cElementInput->Attribute('disabled');
            
        $cField->Input($cElementInput);
        $cElement->Content($cField->cBuild(true));
      }
      
      //date begin
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Begin'), WTS_PARAM_BEGIN . 1));

      
      $cElementInput = cField::cElementInput(WTS_PARAM_BEGIN . 1, date(WTS_DT_FMT, $cEvent->iDtBegin()));
      $cElementInput->Attribute('style', 'width: 140px;');
      $cElementInput->Attribute('disabled');
      
      $cField->Input($cElementInput);
      $cField->Input(cField::cElementCalImg());
      $cElement->Content($cField->cBuild(true));
        
      //date end
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('End'), WTS_PARAM_END . 1));

      
      $cElementInput = cField::cElementInput(WTS_PARAM_END . 1, date(WTS_DT_FMT, $cEvent->iDtEnd()));
      $cElementInput->Attribute('style', 'width: 140px;');
      $cElementInput->Attribute('disabled');
      
      $cField->Input($cElementInput);
      $cField->Input(cField::cElementCalImg());
      $cElement->Content($cField->cBuild(true));
      
      //load body for replace cid to real link
      $sBody = $cEvent->mUnserializeBody($this->cDb);
      //attachment
      $cAttachmentView = false;
      if($cEvent->iUnserializeAttach($this->cDb) > 0){
        
        $aAttachs = $cEvent->aAttach();
        foreach($aAttachs as $att){
          $bShow = false;
          $sLink = WTS_PATH . 'attachmente.php?eid='
          . $cEvent->iID() . '&aid=' . $att[FN_I_ID];

          if(strlen($att[FN_S_CID]) > 0){
            if($sBody
            && stripos($sBody, 'cid:' . $att[FN_S_CID])){
              $sBody = str_replace('cid:' . $att[FN_S_CID] , $sLink, $sBody);
            }
            //сид есть, а в теле его нет
            else{$bShow = true;}
          }
          else{$bShow = true;}

          if($bShow){
            if($cAttachmentView === false){$cAttachmentView = new cAttachmentView();}
            $cAttachmentView->Attachment($sLink, $att[FN_S_NM]);
          }
        }
      }
      if($cAttachmentView !== false){
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Attachment')));
        $cField->Input($cAttachmentView->cRoot());
        
        $cElement->Content($cField->cBuild(true));
      }
      
      //body
      $cElementDiv = new cElement('div', false);
      $cElementDiv->Attribute('class', WTS_PFX . '-event');
      $cElementDiv->Content(new cElementBase(($sBody ? $sBody : WTS_EVENT_NOTEXT), false));
      
      $cElement->Content($cElementDiv);
      
      return $cElement;
      
    }
    //find & add attachment from url into event, return cid
    protected function mAddAttachmentFromUrl($sUrl, cEvent &$cEvent){
      //image paste from other event
      $sCid = false;
      $sPath = 'attachmente.php?eid=';
      if(($iPos = stripos($sUrl, $sPath)) !== false){
        $sPath = substr($sUrl, $iPos + strlen($sPath));
        $aParams = explode('&', $sPath); //12345&aid=12345
        $sPath = 'aid=';
        if(isset($aParams[0])
        && isset($aParams[1])
        && ($iPos = stripos($aParams[1], $sPath)) !== false
        && ($cTmpEvent = cEvent::mUnserialize($this->cDb, $aParams[0]))
        && $cTmpEvent->iUnserializeAttach($this->cDb) > 0){
          $sPath = substr($aParams[1], $iPos + strlen($sPath));
          $aAttachs = $cTmpEvent->aAttach();
          $aCurAttach = false;
          foreach($aAttachs as &$a){
            if($a[FN_I_ID] === (int)$sPath){
              $aCurAttach = $a; break;
            }
          }

          if($aCurAttach
          && ($sSrc = $cTmpEvent->mUnserializeAttach($this->cDb, $aCurAttach[FN_I_ID]))){
            $sName = $aCurAttach[FN_S_NM];
            //в названии файла может быть кирилица - убрал
            //$sCid  = $sName . '@' . cString::sRand(6) . '.wts.' . cString::sRand(6);
            $sCid  = cString::sRand(6) . '-' . WTS_DB_NAME . '-' . cString::sRand(6);
            $cEvent->bAddAttach($sSrc, $sName, $sCid);
            unset($sSrc);
          }
          unset($cTmpEvent);
        }
      }
      else{
        //image paste from editor
        $sPath = 'gimgupload.php?uid=';
        if(($iPos = stripos($sUrl, $sPath)) !== false){
          $sPath = substr($sUrl, $iPos + strlen($sPath));
          if($cUpload = cUpload::mUnserialize($this->cDb, $sPath)){
            $sName = $cUpload->sName();
            //$sCid  = $sName . '@' . cString::sRand(6) . '.wts.' . cString::sRand(6);
            $sCid  = cString::sRand(6) . '-' . WTS_DB_NAME . '-' . cString::sRand(6);
            $cEvent->bAddAttach($cUpload->sValue(), $sName, $sCid);
            unset($cUpload);
          }
        }
      }
      return $sCid;
    }
    
    protected function &cAddChainMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Chains')
                         , _('Sequences of Events')
                         , ($sCurAct !== WTS_PARAM_ACT_CHAIN ? sAction(WTS_PARAM_ACT_CHAIN) : null));
      return $cMenuItem;
    }
    
    protected function &cAddReportMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Reports')
                         , _('Reporting tools')
                         , ($sCurAct !== WTS_PARAM_ACT_REPORT ? sAction(WTS_PARAM_ACT_REPORT) : null));
      return $cMenuItem;
    }
    
    protected function &cAddAboutMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('About')
                         , _('About System')
                         , ($sCurAct !== WTS_PARAM_ACT_ABOUT ? sAction(WTS_PARAM_ACT_ABOUT) : null));
      return $cMenuItem;
    }
  /**
   * Функции создания/просмотра/редактирования цепочки
   * 
   * есть на входе цепочка - смотрим привелегии - просмотр/редактирование
   * есть на входе событие - форвард - если нет цепочки/новая цепочка, если её нет/
   */
    public function DoEventPage(cPage &$cPage){
      $sHref     = sAction(WTS_PARAM_ACT_EVENT);
      $aEvents   = false;
      $cCurEvent = false;
      $iPage     = ((isset($_GET[WTS_PARAM_PAGE]) && $_GET[WTS_PARAM_PAGE] > 0) ? $_GET[WTS_PARAM_PAGE] : 1);
      $iPages = 1;
      
      $bAddAdviceScript = false;
      
      //010. проверка цепочки
      $cCurChain = false;
      if(isset($_GET[WTS_PARAM_ACT_CHAIN])
      && $_GET[WTS_PARAM_ACT_CHAIN] > 0
      && ($cCurChain = cChain::mUnserialize($this->cDb, $_GET[WTS_PARAM_ACT_CHAIN]))){
        if($this->cCont->iType() === cCont::CSTMR){//имеем отношение к ней?
          $bOwn = false;
          if($cCurChain->iCont() === $this->cCont->iID()
          || ($this->cCont->iGroup()
            && $this->cCont->iGroup() === $cCurChain->iGroup())){
            $bOwn = true;
          }
          if(!$bOwn){
            unset($cCurChain);
            header('Location: ' . sAction(WTS_PARAM_ACT_CHAIN));
            exit;
          }
        }
      }
      
      //020. проверка события(ий) и номера страницы
      if($cCurChain !== false){
        
        cEvent::AddFilterChain($cCurChain->iID());
        
        $iRows = cEvent::iCount($this->cDb);

        if($iRows > 0){
          //normalize page
          if($iRows > WTS_WIDE_ROW_PER_PAGE){
            $iPages = ceil($iRows / WTS_WIDE_ROW_PER_PAGE);
          }
          //goto last page by default
          if($iPage > $iPages || !isset($_GET[WTS_PARAM_PAGE])){$iPage = $iPages;}

          $iRows = cEvent::iUnserialize($this->cDb, $iPage, WTS_WIDE_ROW_PER_PAGE, $aEvents);
        }
        
        if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
          $cCurEvent = &cEvent::mInArray($aEvents, $_GET[WTS_PARAM_ID]);
        }
      }
      else{
        if($this->cCont->iType() === cCont::CSTMR){
          header('Location: ' . sAction(WTS_PARAM_ACT_CHAIN));
          exit;
        }
        
        if(isset($_GET[WTS_PARAM_ID])
        && ($cCurEvent = cEvent::mUnserialize($this->cDb, $_GET[WTS_PARAM_ID]))){
          
          //проверка на правомерность использование нового события
          if($cCurEvent->iChain() > 0 //если уже прикреплено
          || $cCurEvent->iType() === cEvent::OUT_EMAIL //или если уже форварднуто
          || $cCurEvent->iQueue() === 1 //или админсокй очереди (можно только удалить)
          /*|| $this->cCont->iQueue() !== $cCurEvent->iQueue() //из чужой очереди*/
          || (isset($_GET[WTS_PARAM_FORWARD]) //переслать звонок?
              && $cCurEvent->iType() !== cEvent::IN_EMAIL)
          || (isset($_GET[WTS_PARAM_FORWARD]) //админы не форвардят
              && $this->cCont->iType() === cCont::ADMIN)){
            unset($cCurEvent);
            //отправим смотреть дашборд
            header('Location: ' . sAction(WTS_PARAM_ACT_DASHBOARD));
            exit;
          }
          
          //блокировки
          if($cCurEvent->bUnserializeLock($this->cDb)){
            if($cCurEvent->iLockOwner() !== $this->cCont->iID()){
              //message lock
              $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
              
              if($cCont = cCont::mUnserialize($this->cDb, $cCurEvent->iLockOwner())){
                $cMsg->Value(_('Event from \'') . $cCurEvent->sFrom()
                         . _('\' locked by ') . ($cCont->sName() ? $cCont->sName() : '\'' . $cCont->sLogin() . '\''));
                unset($cCont);
              }
              else{
                $cMsg->Value(_('Event from \'') . $cCurEvent->sFrom() . _('\' locked by ') . _('Unknown'));
              }
              
              $cMsg->bSerialize($this->cDb);
              unset($cMsg);
              unset($cCurEvent);
              //отправим смотреть дашборд
              header('Location: ' . sAction(WTS_PARAM_ACT_DASHBOARD));
              exit;
            }
          }
          else{
            $cCurEvent->Lock($this->cCont->iID());
            $cCurEvent->bSerializeLock($this->cDb);
          }
        }

      }

      //030. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false
      && $this->cCont->iQueue() > 0){
        
        switch($sAction){
        case (WTS_PARAM_EACT_CREATE . 1): //create new event
          //админ не создаёт эвенты
          if($this->cCont->iType() !== cCont::ADMIN
          && isset($_GET[WTS_PARAM_TYPE])
          && ((int)$_GET[WTS_PARAM_TYPE] === cEvent::OUT_EMAIL
            || (int)$_GET[WTS_PARAM_TYPE] === cEvent::IN_CALL
            || (int)$_GET[WTS_PARAM_TYPE] === cEvent::OUT_CALL
            || (int)$_GET[WTS_PARAM_TYPE] === cEvent::MEETING)
          && ($cQueue = cQueue::mUnserialize($this->cDb, $this->cCont->iQueue()))){
            
            $cObj = new cEvent($_GET[WTS_PARAM_TYPE], $cQueue->iID());
            if(isset($_POST[WTS_PARAM_SUBJECT])
            && strlen($_POST[WTS_PARAM_SUBJECT]) > 0){
              $cObj->Subject($_POST[WTS_PARAM_SUBJECT]);
            }
            $cObj->From($cQueue->sLogin());
            //addrs
            if(isset($_POST[WTS_PARAM_TO])){$cObj->AddTo($_POST[WTS_PARAM_TO]);}
            if(isset($_POST[WTS_PARAM_CC])){$cObj->AddCc($_POST[WTS_PARAM_CC]);}
            if(isset($_POST[WTS_PARAM_BCC])){$cObj->AddBcc($_POST[WTS_PARAM_BCC]);}
            //dates
            if(isset($_POST[WTS_PARAM_BEGIN])){$cObj->DtBegin(strtotime($_POST[WTS_PARAM_BEGIN]));}
            if(isset($_POST[WTS_PARAM_END])){$cObj->DtEnd(strtotime($_POST[WTS_PARAM_END]));}
            else{$cObj->DtEnd(strtotime('now'));}
            //body & pasted images
            //body
            $sBody = ((isset($_POST[WTS_PARAM_VALUE]) && strlen($_POST[WTS_PARAM_VALUE]) > 0 ) ? $_POST[WTS_PARAM_VALUE] : false);
            //images from body
            $aImgs = false;
            if($sBody && cElement::iUnserialize('img', $sBody, $aImgs) > 0){
              foreach($aImgs as $key => &$cImg){
                $aAttr = $cImg->aAttributes();
                if(isset($aAttr['src']) && strlen($aAttr['src']) > 0){
                  $sUrl = html_entity_decode($aAttr['src']);
                  if(strlen($sUrl) > 0
                  && ($sCid = $this->mAddAttachmentFromUrl($sUrl, $cObj))){
                    $sBody = str_replace($aAttr['src'], 'cid:' . $sCid, $sBody);
                    //за остальные картинки/банеры мы не отвечаем....
                  }
                }
                unset($aAttr);
                unset($aImgs[$key]);
              }
            }
            $cObj->AddBody($sBody);
            $cObj->GenPreview(WTS_ENC, WTS_EVENT_PREVIEW_LENGHT);
            
            //attachments
            if(isset($_FILES[WTS_PARAM_FILE])){
              for($i = 0; $i < 100; $i++){
                if(isset($_FILES[WTS_PARAM_FILE]['size'][$i])
                && $_FILES[WTS_PARAM_FILE]['size'][$i] > 0){
                  if($cStream = fopen($_FILES[WTS_PARAM_FILE]['tmp_name'][$i], 'r')){
                    $cObj->bAddAttach(stream_get_contents($cStream), $_FILES[WTS_PARAM_FILE]['name'][$i]);
                    fclose($cStream);
                  }
                }
                else{break;}
              }
            }
            elseif($cCurEvent //attachments from fwd
            && isset($_POST[WTS_PARAM_FILE])
            && $cCurEvent->iUnserializeAttach($this->cDb) > 0){
              
              $aAttachs = $cCurEvent->aAttach();
              //sort + unique?
              for($i = 0; $i < count($_POST[WTS_PARAM_FILE]); $i++){
                foreach($aAttachs as &$a){
                  
                  if($a[FN_S_CID])continue;
                  
                  if($a[FN_I_ID] === (int)$_POST[WTS_PARAM_FILE][$i]
                  && ($sSrc = $cCurEvent->mUnserializeAttach($this->cDb, $a[FN_I_ID]))){
                    $cObj->bAddAttach($sSrc, $a[FN_S_NM]);
                  }
                }
              }
              unset($aAttachs);
            }

            //cahin
            if($cCurChain
            && $this->cCont->iID() === $cCurChain->iUpdater()){
              $cObj->Chain($cCurChain->iID());
              //reopen chain
              if($cCurChain->bClosed()){
                $cCurChain->Open();
                $cCurChain->bSerialize($this->cDb);
              }
            }
            
            if($cObj->bSerialize($this->cDb, WTS_EVENT_THUMB_SIDE, $this->cCont->iID())){
              $cCurEvent = &$cObj;
            }
            else{
              unset($cObj);
              $cCurEvent = false;
              $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
              $cMsg->Value( _('Can\'t create New Event.'));
              $cMsg->bSerialize($this->cDb);
              unset($cMsg);
            }
          }
          break;
        case WTS_PARAM_EACT_CREATE: //new chain
          //разрешаем админу создавать цепочки
          if($cCurEvent
          && $cCurChain === false
          && isset($_POST[WTS_PARAM_RESPONSIBLE])
          && cString::bIsEmail($_POST[WTS_PARAM_RESPONSIBLE])
          && ($cContResp = cCont::mUnserialize($this->cDb, $_POST[WTS_PARAM_RESPONSIBLE]))){
            if($cContResp->iQueue() > 1){
              
              $cObj = new cChain();
              //creator
              if($cContResp->iID() !== $this->cCont->iID()){
                $cObj->Creator($this->cCont->iID());
              }
              //responsible & queue
              $cObj->Updater($cContResp);
              //subject
              if(isset($_POST[WTS_PARAM_SUBJECT])
              && strlen($_POST[WTS_PARAM_SUBJECT]) > 0){
                $cObj->Subject($_POST[WTS_PARAM_SUBJECT]);
              }
              //contact & group
              if(isset($_POST[WTS_PARAM_CONTACT])
              && cString::bIsEmail($_POST[WTS_PARAM_CONTACT])
              && ($cContCust = cCont::mUnserialize($this->cDb, $_POST[WTS_PARAM_CONTACT]))){
                $cObj->Cont($cContCust);
                unset($cContCust);
              }
              //init preview from event
              if($cCurEvent->sPreview()){
                $cObj->Preview($cCurEvent->sPreview());
              }
              
              if($cObj->bSerialize($this->cDb)){
                //tags
                if(isset($_POST[WTS_PARAM_SLUG])
                && count($_POST[WTS_PARAM_SLUG]) > 0){
                  $cObj->bSerializeTag($this->cDb, $_POST[WTS_PARAM_SLUG], false);
                }
                //set chain id for event
                $cCurEvent->Chain($cObj->iID());
                //читали, когда создавали
                if($cContResp->iID() === $this->cCont->iID()){
                  $cCurEvent->Processed();
                }
                $cCurEvent->bSerialize($this->cDb);

                $cCurChain = $cObj;
                
                $cLog = new cLog();
                $cLog->Value('Chain ' . $cCurChain->iID() . ' created by ' . $this->cCont->sLogin());
                $cLog->bSerialize($this->cDb);
                unset($cLog);
              }
              else{
                unset($cObj);
                $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
                $cMsg->Value( _('Can\'t create New Chain.'));
                $cMsg->bSerialize($this->cDb);
                unset($cMsg);
              }
            }
            else{
              $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
              $cMsg->Value( _('Can\'t set Admin as responsible for New Chain.'));
              $cMsg->bSerialize($this->cDb);
              unset($cMsg);
            }
            unset($cContResp);
          }
          break; 
        case WTS_PARAM_EACT_UPDATE: //update chain
          //разрешаем админу править цепочки, сами редактируем только пока открыта
          if($cCurChain
          && ($this->cCont->iType() === cCont::ADMIN
          || ($this->cCont->iID() === $cCurChain->iUpdater()
            && $cCurChain->bClosed() === false))){
              
            //subject
            if(isset($_POST[WTS_PARAM_SUBJECT])){
              $cCurChain->Subject($_POST[WTS_PARAM_SUBJECT]);
            }
            //responsible & queue
            if(isset($_POST[WTS_PARAM_RESPONSIBLE])
            && cString::bIsEmail($_POST[WTS_PARAM_RESPONSIBLE])
            && ($cContResp = cCont::mUnserialize($this->cDb, $_POST[WTS_PARAM_RESPONSIBLE]))){
              $cCurChain->Updater($cContResp);
              unset($cContResp);
            }
            //contact & group
            if(isset($_POST[WTS_PARAM_CONTACT])
            && cString::bIsEmail($_POST[WTS_PARAM_CONTACT])
            && ($cContCust = cCont::mUnserialize($this->cDb, $_POST[WTS_PARAM_CONTACT]))){
              $cCurChain->Cont($cContCust);
              unset($cContCust);
            }
            //save
            if($cCurChain->bSerialize($this->cDb, $this->cCont->iType() === cCont::AGENT)){
              $cLog = new cLog();
              $cLog->Value('Chain ' . $cCurChain->iID() . ' updated by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);
            }
            //tags
            if(isset($_POST[WTS_PARAM_SLUG])
            && $cCurChain->bSerializeTag($this->cDb, $_POST[WTS_PARAM_SLUG], $this->cCont->iType() === cCont::AGENT)){
              $cLog = new cLog();
              $cLog->Value('Tags for Chain ' . $cCurChain->iID() . ' updated by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);
            }
          }
          break;
        case WTS_PARAM_EACT_CHOWN: //let me be owner
          if($cCurChain
          && $cCurChain->bClosed() === false
          && $this->cCont->iQueue() === $cCurChain->iQueue()
          && $cCurChain->iUpdater()
          && $this->cCont->iID() !== $cCurChain->iUpdater()
          && ($cContResp = cCont::mUnserialize($this->cDb, $cCurChain->iUpdater()))
          && $cContResp->bIsOnline($this->cDb, WTS_SESSION_TIMEOUT) === false){
            unset($cContResp);
            $cCurChain->Updater($this->cCont);
            if($cCurChain->bSerialize($this->cDb)){
              $cLog = new cLog();
              $cLog->Value('Change owner for Chain ' . $cCurChain->iID() . ' by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);
            }
          }
          break;
        case WTS_PARAM_EACT_CLOSE: //close chain
          if($cCurChain
          && $cCurChain->bClosed() === false
          && ($this->cCont->iType() === cCont::ADMIN
            || $this->cCont->iID() === $cCurChain->iUpdater())){
              $cCurChain->Close();
              if($cCurChain->bSerialize($this->cDb)){
                $cLog = new cLog();
                $cLog->Value('Chain ' . $cCurChain->iID() . ' closed by ' . $this->cCont->sLogin());
                $cLog->bSerialize($this->cDb);
                unset($cLog);
                
                //goto dashboard
                unset($cCurChain);
                $cCurChain = false;
              }
            }
          break; 
        case WTS_PARAM_EACT_DELETE: //delete event
          //удалить исходящее пока не послали
          if($cCurChain
          && $cCurEvent
          && $cCurEvent->iType() === cEvent::OUT_EMAIL
          && !$cCurEvent->bProcessed()
          && ($this->cCont->iType() === cCont::ADMIN
            || ($this->cCont->iID() === $cCurChain->iUpdater()
              && $cCurChain->bClosed() === false))){

            $cLog = new cLog();
            $cLog->Value('Delete not sent Event ' . $cCurEvent->iID()
            . ' from Chain ' . $cCurChain->iID() . ' by ' . $this->cCont->sLogin());
            $cLog->bSerialize($this->cDb);
            unset($cLog);
            $cCurEvent->bDelete($this->cDb);
            unset($cCurEvent);
            $cCurEvent = false;
          }
          break; 
        case WTS_PARAM_EACT_DETACH: //delete event
          //открепить событие от цепочки
          if($cCurChain
          && $cCurEvent
          && $cCurEvent->iType() !== cEvent::OUT_EMAIL
          && ($this->cCont->iType() === cCont::ADMIN
            || ($this->cCont->iID() === $cCurChain->iUpdater()
              && $cCurChain->bClosed() === false))){
            
            $cCurEvent->Chain(0);
            if($cCurEvent->bSerialize($this->cDb)){
              $cLog = new cLog();
              $cLog->Value('Detach Event ' . $cCurEvent->iID()
              . ' from Chain ' . $cCurChain->iID() . ' by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);
              //goto dashboard
              unset($cCurChain);
              $cCurChain = false;
            }
          }
          break; 
        default: break;
        }
        if($cCurChain === false){$sHref = sAction(WTS_PARAM_ACT_DASHBOARD);}
        else{$sHref .= sSubAction(WTS_PARAM_ACT_CHAIN, $cCurChain->iID());}
        if($cCurEvent !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurEvent->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //040. display
      if($cCurChain){
        //041. toolbar for exist chain
        
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
        
        //refresh
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_EVENT);
        $cButton->Hidden(WTS_PARAM_ACT_CHAIN, $cCurChain->iID());
        $cElement->Content($cButton->cRoot());
        
        if($cCurChain->bClosed() === false
        && ($this->cCont->iType() === cCont::ADMIN
          || $this->cCont->iID() === $cCurChain->iUpdater())){
          //--btn close chain
          $cButton = new cButton();
          $cButton->Image(IMG_EDIT_CHAIN_LOCK, _('Close Chain'), WTS_PFX . '-toolbar-button');
          $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_EVENT);
          $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_CLOSE);
          $cButton->Hidden(WTS_PARAM_ACT_CHAIN, $cCurChain->iID());
          $cElement->Content($cButton->cRoot());
        }
        
        if($cCurEvent
        && ($this->cCont->iType() === cCont::ADMIN
          || ($this->cCont->iID() === $cCurChain->iUpdater()
            && $cCurChain->bClosed() === false))){
          
          //--btn remove
          if($cCurEvent->iType() === cEvent::OUT_EMAIL //удалить, пока не отправили
          && !$cCurEvent->bProcessed()){
            $cButton = new cButton();
            $cButton->Image(IMG_EDIT_REMOVE, _('Delete Event'), WTS_PFX . '-toolbar-button');
            $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_EVENT);
            $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
            $cButton->Hidden(WTS_PARAM_ACT_CHAIN, $cCurChain->iID());
            $cButton->Hidden(WTS_PARAM_ID, $cCurEvent->iID());
            $cElement->Content($cButton->cRoot());
          }
          //--btn detach
          if($cCurEvent->iType() !== cEvent::OUT_EMAIL //нет смысла откреплять отправленные
          && $cCurEvent->bProcessed()){
            $cButton = new cButton();
            $cButton->Image(IMG_EDIT_SPLIT, _('Detach Event'), WTS_PFX . '-toolbar-button');
            $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_EVENT);
            $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DETACH);
            $cButton->Hidden(WTS_PARAM_ACT_CHAIN, $cCurChain->iID());
            $cButton->Hidden(WTS_PARAM_ID, $cCurEvent->iID());
            $cElement->Content($cButton->cRoot());
          }
        }
        
        //page navbar
        if($iPages > 1){
          $aButtons = array();
          $this->CreateButtonsNav(WTS_PARAM_ACT_EVENT, $iPage, $iPages, $aButtons);
          
          foreach($aButtons as &$cButton){
            $cButton->Hidden(WTS_PARAM_ACT_CHAIN, $cCurChain->iID());
            $cElement->Content($cButton->cRoot());
          }
        }
        $cPage->Main($cElement);
        
        //042. table events for exist chain
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-main-section');
        
        $cTable = new cTable(WTS_PFX . '-table');
        $cTable->Column(_('Date'), '165px');
        $cTable->Column(_('Subject & Preview'));
        
        if($iRows > 0){
          if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
            $cElementS = new cElement('script', false);
            $cElementS->Attribute('type', 'text/javascript');
            $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                                 . $_GET[WTS_PARAM_SCROLL] . ');}'));
            $cPage->Head($cElementS);
          }

          $iID = 0;
          if($cCurEvent){$iID = $cCurEvent->iID();}
          
          foreach($aEvents as &$c){
            //row
            $cElementTr = new cElement('tr', false);
            if($c->iID() == $iID){
              $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
            }
            else{
              $cElementTr->Attribute('onclick', 'window.open(\''
              . $sHref
              . (isset($_GET[WTS_PARAM_PAGE]) ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
              . sSubAction(WTS_PARAM_ACT_CHAIN, $cCurChain->iID())
              . sSubAction(WTS_PARAM_ID, $c->iID())
              . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
            }

            //column 1
            $cElementTd = new cElement('td', false);
            $cElementTd->Attribute('class', WTS_PFX . '-table-chain');
            
            $cElementTd->Content($this->cElementEventImg($c->iType()));
            
            //new event in list
            if(!$c->bProcessed()){
              $cElementImg = new cElement('img', false);
              $cElementImg->Attribute('class', WTS_PFX . '-table-narrow-image'); //?
              $cElementImg->Attribute('src', IMG_EVENT_NEW);
              $cElementImg->Attribute('title', _('New Event'));
              
              $cElementTd->Content($cElementImg);
            }
            
            $cElementTd->Content($this->cDoTime($c->iDtEnd()));
            $cElementTd->Content($this->cDoDate($c->iDtEnd()));

            $cElementTr->Content($cElementTd);
            
            //column 2
            $cElementTd = new cElement('td', false);
            
            if($c->sSubject()){
              $cElementTd->Content(cSE::cCreate('b', $c->sSubject()));
              $cElementTd->Content(new cElement('br', false));
            }
            
            if($c->sPreview()){
              //$cElementTd->Content(cSE::cCreate('small', $c->sPreview()));
              $cElementTd->Content(new cElementBase(htmlentities($c->sPreview())));
            }
            $cElementTr->Content($cElementTd);
           
            $cTable->Row($cElementTr);
          }
        }
        else{$cTable->ColSpan(_('No data found'));}
        
        $cElement->Content($cTable->cRoot());

        $cPage->Main($cElement);

      }
      
      //050. cur event
      if($cCurEvent && !isset($_GET[WTS_PARAM_FORWARD])){
        $cPage->Main($this->cDoEvent($cCurEvent));
        //mark as readed
        if($cCurEvent->iType() != cEvent::OUT_EMAIL
        && !$cCurEvent->bProcessed()
        && $cCurChain
        && !$cCurChain->bClosed()
        && $this->cCont->iID() == $cCurChain->iUpdater()){
          $cCurEvent->Processed();
          $cCurEvent->bSerialize($this->cDb);
        }
      }
      
      
      if($this->cCont->iQueue() > 1
      && ($cQueue = cQueue::mUnserialize($this->cDb, $this->cCont->iQueue()))){
        //060. toolbar - new event type + regexps
        $aTypes    = false;
        $iCurType  = false;
        
        if($cCurChain
        && $this->cCont->iID() === $cCurChain->iUpdater()){//новое событие в цепочке
          // если цепочка закрыта - она откроется
          $iCurType = (isset($_GET[WTS_PARAM_TYPE])
          && ($_GET[WTS_PARAM_TYPE] == cEvent::IN_CALL
           || $_GET[WTS_PARAM_TYPE] == cEvent::OUT_CALL
           || $_GET[WTS_PARAM_TYPE] == cEvent::MEETING)) ? (int)$_GET[WTS_PARAM_TYPE] : cEvent::OUT_EMAIL;
           
          $aTypes[cEvent::OUT_EMAIL] = cEvent::sType(cEvent::OUT_EMAIL);
          $aTypes[cEvent::IN_CALL]   = cEvent::sType(cEvent::IN_CALL);
          $aTypes[cEvent::OUT_CALL]  = cEvent::sType(cEvent::OUT_CALL);
          $aTypes[cEvent::MEETING]   = cEvent::sType(cEvent::MEETING);
        }
        elseif($cCurEvent //fwd -нет тулбара
        && isset($_GET[WTS_PARAM_FORWARD])
        && $_GET[WTS_PARAM_FORWARD] === 'true'){
              $iCurType = cEvent::OUT_EMAIL;
        }
        elseif($cCurChain === false //новое событие без цепочки
        && $cCurEvent === false){
          $iCurType = (isset($_GET[WTS_PARAM_TYPE])
          && ($_GET[WTS_PARAM_TYPE] == cEvent::MEETING
           || $_GET[WTS_PARAM_TYPE] == cEvent::OUT_CALL)) ? (int)$_GET[WTS_PARAM_TYPE] : cEvent::IN_CALL;

          $aTypes[cEvent::IN_CALL]   = cEvent::sType(cEvent::IN_CALL);
          $aTypes[cEvent::OUT_CALL]  = cEvent::sType(cEvent::OUT_CALL);
          $aTypes[cEvent::MEETING]   = cEvent::sType(cEvent::MEETING);
        }
        
        if($aTypes){
          $cElement = new cElement('section', false);
          $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
          
          //select event type
          $cButton = new cButton();
        
          $cSelect = new cSelect(WTS_PARAM_TYPE, WTS_PFX . '-toolbar');
          
          foreach($aTypes as $key => $sName){
            $cSelect->Option($key === $iCurType, $key, $sName);
          }
          
          $cElementS = $cSelect->cRoot();
          $cElementS->Attribute('onchange', 'this.form.elements[\''
          . WTS_PARAM_SCROLL . '\'].value=((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop),this.form.submit();');
          $cButton->Content($cElementS);
          $cButton->Hidden(WTS_PARAM_SCROLL, '0');
          $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_EVENT);
          if($cCurChain){
            $cButton->Hidden(WTS_PARAM_ACT_CHAIN, $cCurChain->iID());
            if($cCurEvent){$cButton->Hidden(WTS_PARAM_ID, $cCurEvent->iID());}
            if(isset($_GET[WTS_PARAM_PAGE])){$cButton->Hidden(WTS_PARAM_PAGE, $iPage);}
          }
          $cElement->Content($cButton->cRoot());
          
          //regexps only in chain - if fwd - no $aTypes
          if($iCurType == cEvent::OUT_EMAIL){
            $aRegExps = false;
            if(cRegExp::iUnserialize($this->cDb, $cQueue->iID(), $aRegExps) > 0){
              $cButton = new cButton();
        
              $cSelect = new cSelect(WTS_PARAM_ACT_REGEXP, WTS_PFX . '-toolbar');
              
              foreach($aRegExps as $cRegExp){
                $cSelect->Option(false, htmlentities($cRegExp->sValue()), $cRegExp->sName());
                unset($cRegExp);
              }
              
              $cElementS = $cSelect->cRoot();
              $cElementS->Attribute('id', 'id_' . WTS_PARAM_ACT_REGEXP);
              $cButton->Content($cElementS);
              $cButton->Image(IMG_EDIT_PASTE, _('Paste Expression'), WTS_PFX . '-toolbar-button');
              $cElementB = $cButton->cRoot();
              $cElementB->Attribute('onsubmit', 'return pasteregexp();');
              
              $cElement->Content($cElementB);
              
              $sScript = '
/*nicEdit::paste expression*/
function pasteregexp(){
  editor = document.getElementsByClassName(\'nicEdit-main\')[0];
  editor.focus();
  if(typeof window.getSelection != \'undefined\'){
    sel = window.getSelection();
    if(sel.rangeCount){
      range = sel.getRangeAt(0);
      divnode = document.createElement(\'div\');
      divnode.innerHTML = document.getElementById(\'id_' . WTS_PARAM_ACT_REGEXP . '\').value;
      range.insertNode(divnode);
    }
  }else if(document.selection && document.selection.createRange){
    range = document.selection.createRange();
    range.text = document.getElementById(\'id_' . WTS_PARAM_ACT_REGEXP . '\').value;
  }
  return false;
}';
              
              $cElementS = new cElement('script', false);
              $cElementS->Attribute('type', 'text/javascript');
              $cElementS->Content(new cElementBase($sScript, false));
              $cPage->Head($cElementS);
            }
          }
          $cPage->Main($cElement);
        }
        //070. new event form
        if($iCurType){
          $cElement = new cElement('section', false);
          $cElement->Attribute('class', WTS_PFX . '-main-section');
          
          $cElementForm = new cElement('form', false);
          $cElementForm->Attribute('action', $sHref
                . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_CREATE . 1)
                . sSubAction(WTS_PARAM_TYPE, $iCurType)
                . ($cCurChain ? sSubAction(WTS_PARAM_ACT_CHAIN, $cCurChain->iID()) : '')
                . ($cCurEvent ? sSubAction(WTS_PARAM_ID, $cCurEvent->iID()) : ''));
          $cElementForm->Attribute('method', 'post');
          $cElementForm->Attribute('name', WTS_PARAM_ACT_EVENT);
          $cElementForm->Attribute('enctype',  'multipart/form-data');
          $cElementForm->Attribute('onsubmit', 'return validate()');
            
          $sScript = false;
          //или форвард или ответ в цепочке
          if($iCurType == cEvent::OUT_EMAIL){
            //будут поля с адресами и подсказками
            $cElementForm->Attribute('autocomplete', 'off');
            //from
            $cField = new cField();
            $cField->Label(cField::cElementLabel(_('From'), WTS_PARAM_FROM));
            
            $cElementInput = cField::cElementInput(WTS_PARAM_FROM, $cQueue->sLogin());
            $cElementInput->Attribute('disabled');
                
            $cField->Input($cElementInput);
              
            $cElementForm->Content($cField->cBuild(true));
            //to
            $cField = new cField();
            $cField->Label(cField::cElementLabel(_('To'), WTS_PARAM_TO));
            
            $cElementSpan = new cElement('span', false);
            $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
            $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_TO);
            $cField->Label($cElementSpan);
            
            $sScript = '
function validate(){
  var ret=true;
  if(document.forms[\'' . WTS_PARAM_ACT_EVENT . '\'][\'' . WTS_PARAM_TO . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_TO . '\').innerHTML=\'&#9998;\';
    window.content_changed=true;
    ret=false;
  }
  if(document.forms[\'' . WTS_PARAM_ACT_EVENT . '\'][\'' . WTS_PARAM_SUBJECT . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_SUBJECT . '\').innerHTML=\'&#9998;\';
    window.content_changed=true;
    ret=false;
  }
  return ret;
}';
            
            $sTo = false;
            //вставим адрес контакта если в цепочке
            if($cCurChain
            && $cCurChain->iCont()
            && ($cCont = cCont::mUnserialize($this->cDb, $cCurChain->iCont()))){
              $sTo = $cCont->sLogin();
              unset($cCont);
            }
            //если пришло с другого адреса, добавим его в получатели
            if($cCurChain
            && $cCurEvent
            && $cCurEvent->sFrom()
            && $cCurEvent->sFrom() != $cQueue->sLogin()
            && (!$sTo || $sTo != $cCurEvent->sFrom())){
              $sTo = ($sTo ? $sTo . ', ' .  $cCurEvent->sFrom() : $cCurEvent->sFrom());
            }

            $cElementInput = cField::cElementInput(WTS_PARAM_TO, ($sTo ? $sTo : null));
            $cElementInput->Attribute('id', WTS_PFX . '-search-box-to');
            $cField->Input($cElementInput);
              
            $cElementUl = new cElement('ul', false);
            $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper-to');
            $cField->Input($cElementUl);
            $cElementForm->Content($cField->cBuild(true));
            
            //cc
            $cField = new cField();
            $cField->Label(cField::cElementLabel(_('Cc'), WTS_PARAM_CC));
            
            //if forward - paste owner in copy
            $cElementInput = cField::cElementInput(WTS_PARAM_CC, ($cCurChain ? null : $cCurEvent->sFrom()));
            $cElementInput->Attribute('id', WTS_PFX . '-search-box-cc');
            $cField->Input($cElementInput);
              
            $cElementUl = new cElement('ul', false);
            $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper-cc');
            $cField->Input($cElementUl);
            $cElementForm->Content($cField->cBuild(true));
            
            //bcc
            $cField = new cField();
            $cField->Label(cField::cElementLabel(_('Bcc'), WTS_PARAM_BCC));
            
            $cElementInput = cField::cElementInput(WTS_PARAM_BCC);
            $cElementInput->Attribute('id', WTS_PFX . '-search-box-bcc');
            $cField->Input($cElementInput);
              
            $cElementUl = new cElement('ul', false);
            $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper-bcc');
            $cField->Input($cElementUl);
            $cElementForm->Content($cField->cBuild(true));
            
            $cElementS = new cElement('script', false);
            $cElementS->Attribute('type', 'text/javascript');
            $cElementS->Attribute('src', WTS_PATH . 'advice/jquery.min.js');
            $cPage->Head($cElementS);
            
            $cElementS = new cElement('script', false);
            $cElementS->Attribute('type', 'text/javascript');
            $cElementS->Attribute('src', WTS_PATH . 'advice/esearch.js');
            $cPage->Head($cElementS);
            
            $bAddAdviceScript = true;

          }
          else{//calls, meetings
            $sScript = '
function validate(){
  if(document.forms[\'' . WTS_PARAM_ACT_EVENT . '\'][\'' . WTS_PARAM_SUBJECT . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_SUBJECT . '\').innerHTML=\'&#9998;\';
    window.content_changed=true;
    return false;
  }
  return true;
}';
          }
          //subject
          $cField = new cField();
          $cField->Label(cField::cElementLabel(_('Subject'), WTS_PARAM_SUBJECT));
          
          $cElementSpan = new cElement('span', false);
          $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
          $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_SUBJECT);
          $cField->Label($cElementSpan);
          
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase($sScript, false));
          $cPage->Head($cElementS);
          
          $sSbj = false;
          if($cCurChain){
            $sSbj = '[' . WTS_SBJ_PREFIX . $cCurChain->iID() . ']';
            if($cCurChain->sSubject()){$sSbj .= ' ' . $cCurChain->sSubject();}
          }
          else{
            if($iCurType == cEvent::OUT_EMAIL){
              $sSbj = 'Fwd:';
              if($cCurEvent->sSubject()){$sSbj .= ' ' . $cCurEvent->sSubject();}
            }
          }
          $cField->Input(cField::cElementInput(WTS_PARAM_SUBJECT, ($sSbj ? $sSbj : null)));
          $cElementForm->Content($cField->cBuild(true));
        
          //datetime
          $cField = new cField();
          $cField->Label(cField::cElementLabel(_('Begin'), WTS_PARAM_BEGIN));
          $cElementInput = cField::cElementInput(WTS_PARAM_BEGIN, date(WTS_DT_FMT, strtotime('now')));
          $cElementInput->Attribute('style', 'width: 140px;');
          $cElementInput->Attribute('readonly');
          
          if($iCurType != cEvent::OUT_EMAIL){
            
            $cElementInput->Attribute('id', 'id_' . WTS_PARAM_BEGIN);
            $cField->Input($cElementInput);
            $cField->Input(cField::cElementCalImg('id_' . WTS_PARAM_BEGIN));
            
            $cElementForm->Content($cField->cBuild(true));
            
            $cField = new cField();
            $cField->Label(cField::cElementLabel(_('End'), WTS_PARAM_END));
            $cElementInput = cField::cElementInput(WTS_PARAM_END
                                            , date(WTS_DT_FMT, strtotime('now')  + (60 * ($iCurType === cEvent::MEETING ? 60 : 10))));
            $cElementInput->Attribute('style', 'width: 140px;');
            $cElementInput->Attribute('readonly');
            $cElementInput->Attribute('id', 'id_' . WTS_PARAM_END);
            $cField->Input($cElementInput);
            $cField->Input(cField::cElementCalImg('id_' . WTS_PARAM_END));
            
            $cElementForm->Content($cField->cBuild(true));

            $cElementS = new cElement('script', false);
            $cElementS->Attribute('type', 'text/javascript');
            $cElementS->Attribute('src', WTS_PATH . 'datetimepick/datetimepicker_css.js');
            $cPage->Head($cElementS);
          }
          else{
            
            $cField->Input($cElementInput);
            $cField->Input(cField::cElementCalImg());
            
            $cElementForm->Content($cField->cBuild(true));
          }
  
          //attachment
          if($cCurChain || $iCurType != cEvent::OUT_EMAIL){
            $cField = new cField();
            $cField->Label(cField::cElementLabel(_('Attachment')));
            
            $cElementButton = new cElement('button', false);
            $cElementButton->Attribute('onclick', 'add_file();return false;');
            $cElementButton->Content(new cElementBase(_('Add Attachment')));
            
            $cField->Input($cElementButton);
            
            $cElementButton = new cElement('button', false);
            $cElementButton->Attribute('onclick', 'clear_file();return false;');
            $cElementButton->Content(new cElementBase(_('Clear')));
            
            $cField->Input($cElementButton);
            
            $cElementSpan = new cElement('span', false);
            $cElementSpan->Attribute('id', WTS_PFX . '-attachment-size');
            $cField->Input($cElementSpan);
            
            $cElementDiv = new cElement('div', false);
            $cElementDiv->Attribute('id', WTS_PFX . '-attachment-info');
            $cField->Input($cElementDiv);
           
            $cElementForm->Content($cField->cBuild(true));
          
            $cElementS = new cElement('script', false);
            $cElementS->Attribute('type', 'text/javascript');
            $cElementS->Content(new cElementBase(cField::sAddAttachmentInitScript(), false));
            $cPage->Head($cElementS);
          }
          else{//fwd - old attachment
            
            $cField = new cField();
            $b = false;
            
            if($cCurEvent->iUnserializeAttach($this->cDb) > 0){
              $aAttachs = $cCurEvent->aAttach();
              foreach($aAttachs as $a){
                if(strlen($a[FN_S_CID]) === 0){
                  if($b === false){$b = true;}
                  $sLink = WTS_PATH . 'attachmente.php?eid='
                  . $cCurEvent->iID() . '&aid=' . $a[FN_I_ID];
                  
                  $cElementInput = new cElement('input', false);
                  $cElementInput->Attribute('type', 'checkbox');
                  $cElementInput->Attribute('value', $a[FN_I_ID]);
                  $cElementInput->Attribute('name', WTS_PARAM_FILE . '[]');
                  $cElementInput->Attribute('class', WTS_PFX . '-' . WTS_PARAM_FILE);
                  $cElementInput->Attribute('style', 'vertical-align: middle;');

                  $cElementInput->Attribute('checked');
                  $cElementInput->Attribute('onclick', 'clear_file();return false;');
                  
                  $sScript  = '

function clear_file(){
  var boxes = document.getElementsByClassName(\'' . WTS_PFX . '-' . WTS_PARAM_FILE . '\');
  for(var i = 0; i < boxes.length; i++){
    box = boxes[i];
    if(!box.checked){
      var lbl = box.parentNode;
      lbl.parentNode.removeChild(lbl);
    }
  }
}';
          
                  $cElementS = new cElement('script', false);
                  $cElementS->Attribute('type', 'text/javascript');
                  $cElementS->Content(new cElementBase($sScript, false));
                  $cPage->Head($cElementS);

                  $cElementLabel = new cElement('label', false);
                  $cElementLabel->Attribute('class', WTS_PFX . '-attachment');
                  $cElementLabel->Attribute('title', _('Remove file \'') . htmlentities($a[FN_S_NM]) . ('\' from Attachment'));
                  
                  $cElementLabel->Content($cElementInput);
                  $cElementLabel->Content(new cElementBase(htmlentities($a[FN_S_NM])));
                  
                  $cField->Input($cElementLabel);
                }
              }
            }
            
            if($b){
              $cField->Label(cField::cElementLabel(_('Attachment')));
              $cElementForm->Content($cField->cBuild(true));
            }
            else{unset($cField);}
          }
          
          //body
          $sBody = $aResponsesID = false;
          $cQueue->iUnserializeResps($this->cDb, $aResponsesID);
          if($aResponsesID !== false){
            switch($iCurType){
              case cEvent::OUT_EMAIL:
              if($cCurChain){
                if(isset($aResponsesID[cQueue::EML])
                && ($cResponse = cResponse::mUnserialize($this->cDb, $aResponsesID[cQueue::EML]))
                && ($sBody = $cResponse->mBuild($this->cDb))){
                  unset($cResponse);

                }
              }
              else{//forward
                if(isset($aResponsesID[cQueue::FWD])
                && ($cResponse = cResponse::mUnserialize($this->cDb, $aResponsesID[cQueue::FWD]))
                && ($sBody = $cResponse->mBuild($this->cDb))){
                  unset($cResponse);

                }
              }
              break;
              case cEvent::IN_CALL:
              case cEvent::OUT_CALL:
              if(isset($aResponsesID[cQueue::PCL])
              && ($cResponse = cResponse::mUnserialize($this->cDb, $aResponsesID[cQueue::PCL]))
              && ($sBody = $cResponse->mBuild($this->cDb))){
                unset($cResponse);
              }
              break;
              case cEvent::MEETING:
              if(isset($aResponsesID[cQueue::MTG])
              && ($cResponse = cResponse::mUnserialize($this->cDb, $aResponsesID[cQueue::MTG]))
              && ($sBody = $cResponse->mBuild($this->cDb))){
                unset($cResponse);
              }
              break;
              default: break;
            }
          }
          
          //replace expr
          if($sBody){
            foreach(cResponse::RPL_EXP as $sKey => $sExpr){
              $s = '';
              switch($sKey){
                case 'SITE_TITLE': $s = WTS_TITLE;
                break;
                case 'AGENT_NAME':
                if($this->cCont->sName()){$s = $this->cCont->sName();}
                break;
                case 'CUR_CHAIN':
                if($cCurChain){$s = (string)$cCurChain->iID();}
                break;
                case 'QUEUE_EMAIL': $s = $cQueue->sLogin();
                break;
                case 'QUEUE_NAME': $s = $cQueue->sName();
                break;
                case 'CSRMR_NAME':
                if($cCurChain
                && $cCurChain->iCont()
                && ($cCont = cCont::mUnserialize($this->cDb, $cCurChain->iCont()))){
                  if($cCont->sName()){$s = $cCont->sName();}
                  unset($cCont);
                }
                break;
                case 'FWD_MESSAGE':
                if($cCurEvent
                && ($sBody2 = $cCurEvent->mUnserializeBody($this->cDb))){
                  //replace attah
                  if($cCurEvent->iUnserializeAttach($this->cDb) > 0){
                    $aAttachs = $cCurEvent->aAttach();
                    
                    foreach($aAttachs as $a){
                      if(strlen($a[FN_S_CID]) > 0){
                        $sLink = WTS_PATH . 'attachmente.php?eid='
                        . $cCurEvent->iID() . '&aid=' . $a[FN_I_ID];
                        $sBody2 = str_replace('cid:' . $a[FN_S_CID] , $sLink, $sBody2);
                      }
                    }
                  }
                  if(!$cCurChain){//fwd - add addresses, sbj, datetime
                    $aElements = false;
                    if($cCurEvent->sFrom()){
                      $s .= $this->sDoPropValue(_('From'), $cCurEvent->sFrom());
                    }
                    $s .= $this->sDoPropValue(_('Sent'), date('r', $cCurEvent->iDtEnd()));
                    if($cCurEvent->iUnserializeAddr($this->cDb) > 0){
                      $aAddr = $cCurEvent->aTo();
                      if(count($aAddr) > 0){
                        $s .= $this->sDoPropValue(_('To'), implode(', ', $aAddr));
                      }
                      $aAddr = $cCurEvent->aCc();
                      if(count($aAddr) > 0){
                        $s .= $this->sDoPropValue(_('Cc'), implode(', ', $aAddr));
                      }
                    }
                    if($cCurEvent->sSubject()){
                      $s .= $this->sDoPropValue(_('Subject'), $cCurEvent->sSubject());
                    }
                  }
                  $s .= '<br>' . $sBody2;
                  unset($sBody2);
                }
                break;
                default: break;
              }
              $sBody = str_replace($sExpr, $s, $sBody);
            }
          }
          
          $cField = new cField();
          if($sBody !== false){
            $cField->Input(cField::cElementTextArea(WTS_PARAM_VALUE, $sBody));
            unset($sBody);
          }
          else{
            $cField->Input(cField::cElementTextArea(WTS_PARAM_VALUE));
          }
          
          $cElementForm->Content($cField->cBuild());
          
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Attribute('src', WTS_PATH . 'nicedit/nicEdit.js');
          $cPage->Head($cElementS);
          
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $sScript = cField::sNicEditInitScript(cField::sNicEditCreateScript(WTS_PARAM_VALUE, 500, true));
          $cElementS->Content(new cElementBase($sScript, false));

          $cPage->Head($cElementS);
          
          //save btn
          $cField = new cField(cField::CENTER);
          
          $cElementSmb = cField::cElementSubmit(
            ($iCurType === cEvent::OUT_EMAIL ? _('Save & Send') : _('Create Event')));
          $cElementSmb->Attribute('id', 'id_' . WTS_PARAM_VALUE);
          $cElementSmb->Attribute('onclick', 'window.content_changed = false;');
          $cField->Input($cElementSmb);

          $cElementForm->Content($cField->cBuild());
          
          $cElement->Content($cElementForm);
          
          $cPage->Main($cElement);
        }
        
        
        unset($cQueue);
      }
      
      //080 - chain properties or new chain
      $cElement = false;
      if($cCurChain){
        //разрешаем админам править заявки не зависимо от состояния
        //агенты правят только свои и только открытые
        if($this->cCont->iType() === cCont::ADMIN
        || ($this->cCont->iID() === $cCurChain->iUpdater()
           && !$cCurChain->bClosed())){
             
          $cElement = new cElement('section', false);
          $cElement->Attribute('class', WTS_PFX . '-aside-section');
          
          $cElementCapt = new cElement('div', false);
          $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
          $cElementCapt->Content(new cElementBase(_('Chain Properties')));
          
          $cElement->Content($cElementCapt);
        }
      }
      else{
        //new chain
        if($cCurEvent && !isset($_GET[WTS_PARAM_FORWARD])){
          
          $cElement = new cElement('section', false);
          $cElement->Attribute('class', WTS_PFX . '-aside-section');
          
          $cElementCapt = new cElement('div', false);
          $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
          $cElementCapt->Content(new cElementBase(_('Add New Chain')));
          
          $cElement->Content($cElementCapt);
        }
      }
      if($cElement){
        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
              . sSubAction(WTS_PARAM_EDIT, ($cCurChain ? WTS_PARAM_EACT_UPDATE : WTS_PARAM_EACT_CREATE))
              . (isset($_GET[WTS_PARAM_PAGE]) ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
              . ($cCurChain ? sSubAction(WTS_PARAM_ACT_CHAIN, $cCurChain->iID()) : '')
              . ($cCurEvent ? sSubAction(WTS_PARAM_ID, $cCurEvent->iID()) : ''));
        $cElementForm->Attribute('method', 'post');
        $cElementForm->Attribute('autocomplete', 'off');
        $cElementForm->Attribute('name', WTS_PARAM_ACT_CHAIN);
        $cElementForm->Attribute('onsubmit', 'return validate2()');
        
        //sbj
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Subject'), WTS_PARAM_SUBJECT));
        
        $c = ($cCurChain ? $cCurChain : $cCurEvent);
        $cField->Input(cField::cElementInput(WTS_PARAM_SUBJECT
                                          , ($c->sSubject() ? $c->sSubject() : null)));
        
        $cElementForm->Content($cField->cBuild());
        
        //resp
        $c = $bUnset = false;
        if($cCurChain){
          if($cCurChain->iUpdater() != $this->cCont->iID()){
            $c = cCont::mUnserialize($this->cDb, $cCurChain->iUpdater());
            $bUnset = true;
          }
          else{$c = $this->cCont;}
        }
        elseif($this->cCont->iQueue() > 1){$c = $this->cCont;}

        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Responsible'), WTS_PARAM_RESPONSIBLE));
        $cElementSpan = new cElement('span', false);
        $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
        $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_RESPONSIBLE);
        $cField->Label($cElementSpan);
        
        $sScript = '
function validate2(){
  if(document.forms[\'' . WTS_PARAM_ACT_CHAIN . '\'][\'' . WTS_PARAM_RESPONSIBLE . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_RESPONSIBLE . '\').innerHTML=\'&#9998;\';
    return false;
  }
  return true;
}';
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Content(new cElementBase($sScript, false));
        $cPage->Head($cElementS);
        
        $cElementInput = cField::cElementInput(WTS_PARAM_RESPONSIBLE, ($c ? $c->sLogin() : null));
        
        if($bUnset){unset($c);}
        
        $cElementInput->Attribute('id', WTS_PFX . '-search-box-agt');
        $cField->Input($cElementInput);
          
        $cElementUl = new cElement('ul', false);
        $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper-agt');
        $cField->Input($cElementUl);
        
        $cElementForm->Content($cField->cBuild());
        
        //cont
        $c = false;
        if($cCurChain){
          if($cCurChain->iCont() > 0){
            $c = cCont::mUnserialize($this->cDb, $cCurChain->iCont());
          }
        }
        else{//поищем по from
          $aConts = false;
          //cCont::ClearFilters();
          cCont::bAddFilterPartValue($this->cDb, $cCurEvent->sFrom());
          if(cCont::iCount($this->cDb) > 0
          && cCont::iUnserialize($this->cDb, 1, 1, $aConts) > 0){
            $c = $aConts[0];
          }
        }
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Contact'), WTS_PARAM_CONTACT));
        
        $cElementInput = cField::cElementInput(WTS_PARAM_CONTACT, ($c ? $c->sLogin() : null));
        
        if($c){unset($c);}
        
        $cElementInput->Attribute('id', WTS_PFX . '-search-box-cnt');
        $cField->Input($cElementInput);
          
        $cElementUl = new cElement('ul', false);
        $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper-cnt');
        $cField->Input($cElementUl);
        
        $cElementForm->Content($cField->cBuild());
        
        if(!$bAddAdviceScript){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Attribute('src', WTS_PATH . 'advice/jquery.min.js');
          $cPage->Head($cElementS);
          
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Attribute('src', WTS_PATH . 'advice/esearch.js');
          $cPage->Head($cElementS);
        }
        
        //tags
        $aTags = $aTagViews = false;
      
        if(cTag::iUnserialize($this->cDb, $aTags) > 0){
          
          $cField = new cField();
          
          if($cCurChain){
            $aChains = array($cCurChain);
            cChain::iUnserializeTags($this->cDb, $aChains, $aTagViews);
          }
        
          foreach($aTags as &$cTag){

            $cElementInput = new cElement('input', false);
            $cElementInput->Attribute('type', 'checkbox'); 
            $cElementInput->Attribute('name', WTS_PARAM_SLUG . '[]'); 
            $cElementInput->Attribute('value', $cTag->sSlug());
            $cElementInput->Attribute('style', 'vertical-align: middle;');
            
            if(isset($aTagViews) && isset($aTagViews[$cTag->iID()])){
              $cElementInput->Attribute('checked');
              unset($aTagViews[$cTag->iID()]);
            }
            
            $cElementLabel = new cElement('label', false);
            $cElementLabel->Attribute('class', WTS_PFX . '-table-color ' . WTS_PFX . '-tag-checkbox');
            
            if($cTag->sColor()){
              $cElementLabel->Attribute('style', 'background-color:#'
                                              . $cTag->sColor() . ';');
            }
            
            $cElementLabel->Content($cElementInput);
            $cElementLabel->Content(new cElementBase(htmlentities($cTag->sName())));
            
            unset($cTag);
            
            $cField->Input($cElementLabel);
          }
          
          //$cElementField = $cField->cBuild();
          //$cElementField->Attribute('style', 'display: inline-block;');
          
          $cElementForm->Content($cField->cBuild());
        }
        
        
        if($cCurChain){
          $cField = new cField(cField::RIGHT);
          $cField->Input(cField::cElementSubmit(_('Reset'), true));
          $cField->Input(cField::cElementSubmit(_('Apply')));
          
          $cElementForm->Content($cField->cBuild());
        }
        else{
          $cField = new cField(cField::RIGHT);
          $cField->Input(cField::cElementSubmit(_('Create')));
          
          $cElementForm->Content($cField->cBuild());
        }
        $cElement->Content($cElementForm);
        
        $cPage->Aside($cElement);
      }
      
      //090 - chain info
      if($cCurChain){

        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Chain Info')));
        
        $cElement->Content($cElementCapt);
        
        $cToc = new cToc();
        
        $cElementInfo = false;
        if($cCurChain->bClosed()){
          $cElementInfo = cSE::cCreate('span', $cCurChain->iID(), WTS_PFX . '-notify ' . WTS_PFX . '-info2');
          $cElementInfo->Attribute('title', _('Closed'));
          
        }
        else{
          $cElementInfo = cSE::cCreate('span', $cCurChain->iID(), WTS_PFX . '-alert ' . WTS_PFX . '-info2');
          $cElementInfo->Attribute('title', _('Open'));
        }
        $cToc->Child(new cElementBase(_('Chain ID')), $cElementInfo);

        $this->DoChainInfo($cToc, $cCurChain);

        //life time
        $iDiff = 0;
        if($cCurChain->bClosed()){$iDiff = $cCurChain->iDtUpdated() - $cCurChain->iDtCreated();}
        else{$iDiff = strtotime('now') - $cCurChain->iDtCreated();}
        
        $cToc->Child(new cElementBase(_('Life Time')), new cElementBase(cString::sTimediffToString($iDiff)));
        
        //proc time
        if($iDiff > 0){$iDiff = 0;}
        
        //--outbox
        //уже добавлен cEvent::AddFilterChain($cCurChain->iID());
        if($aEvents !== false){
          foreach($aEvents as $key => &$cEvent){unset($aEvents[$key]);}
          unset($aEvents);
          $aEvents = false;
        }
        
        cEvent::bAddFilterType(cEvent::OUT_EMAIL);
        $iEvents = cEvent::iCount($this->cDb);

        if($iEvents > 0
        && cEvent::iUnserialize($this->cDb, 1, $iEvents, $aEvents) > 0){
          foreach($aEvents as $key => &$cEvent){
            $iDiff += $cEvent->iDtEnd() - $cEvent->iDtBegin();
            unset($aEvents[$key]);
          }
          unset($aEvents);
          $aEvents = false;
        }
        
        //--in call
        cEvent::ClearFilters();
        cEvent::AddFilterChain($cCurChain->iID());
        cEvent::bAddFilterType(cEvent::IN_CALL);
        $iEvents = cEvent::iCount($this->cDb);
        if($iEvents > 0
        && cEvent::iUnserialize($this->cDb, 1, $iEvents, $aEvents) > 0){
          foreach($aEvents as $key => &$cEvent){
            $iDiff += $cEvent->iDtEnd() - $cEvent->iDtBegin();
            unset($aEvents[$key]);
          }
          unset($aEvents);
          $aEvents = false;
        }
        //--out call
        cEvent::ClearFilters();
        cEvent::AddFilterChain($cCurChain->iID());
        cEvent::bAddFilterType(cEvent::OUT_CALL);
        $iEvents = cEvent::iCount($this->cDb);
        if($iEvents > 0
        && cEvent::iUnserialize($this->cDb, 1, $iEvents, $aEvents) > 0){
          foreach($aEvents as $key => &$cEvent){
            $iDiff += $cEvent->iDtEnd() - $cEvent->iDtBegin();
           unset($aEvents[$key]);
          }
          unset($aEvents);
          $aEvents = false;
        }
        //--meetings
        cEvent::ClearFilters();
        cEvent::AddFilterChain($cCurChain->iID());
        cEvent::bAddFilterType(cEvent::MEETING);
        $iEvents = cEvent::iCount($this->cDb);
        if($iEvents > 0
        && cEvent::iUnserialize($this->cDb, 1, $iEvents, $aEvents) > 0){
          foreach($aEvents as $key => &$cEvent){
            $iDiff += $cEvent->iDtEnd() - $cEvent->iDtBegin();
            unset($aEvents[$key]);
          }
          unset($aEvents);
          $aEvents = false;
        }
        $cToc->Child(new cElementBase(_('Process Time')), new cElementBase(cString::sTimediffToString($iDiff)));
        
        $cElement->Content($cToc->cRoot());
        
        $cPage->Aside($cElement);
      }
    }
    
    /*
    const MY_OPN  = 10; //my open chains          - customer & agent (default for customer)
    const QUE_OPN = 20; //open chains in my queue - agent (default for agent)
    const ALL_OPN = 30; //all open chains         - admin (default for admin)
    const ALL_MY  = 40; //all my chains           - customer & agent
    const ALL_GRP = 50; //all chain in my group   - customer
    const ALL     = 60; //all chain               - agent & admin
    */
    public function DoChainPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref      = sAction(WTS_PARAM_ACT_CHAIN);
      $aChains    = false;
      $cCurChain  = false;
      $iPage      = ((isset($_GET[WTS_PARAM_PAGE]) && $_GET[WTS_PARAM_PAGE] > 0) ? $_GET[WTS_PARAM_PAGE] : 1);
      
      
      $iCurActivity = false;
      if(isset($_GET[WTS_PARAM_TYPE])){
        $iCurActivity = (int)$_GET[WTS_PARAM_TYPE];
      }
      
      //parse search
      $sCurSearch = $mCurSearch = false;
      $sCurSearchParamClosed    = false;
      
      //поиск и выборка по тегам разрешены вне зависимости от типа активити
      if(isset($_GET[WTS_PARAM_SEARCH])){
        $sCurSearch = WTS_PARAM_SEARCH;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_TAG])){
        $sCurSearch = WTS_PARAM_ACT_TAG;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_CONT])){
        $sCurSearch = WTS_PARAM_ACT_CONT;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_GROUP])){
        $sCurSearch = WTS_PARAM_ACT_GROUP;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_QUEUE])){//use admin from queue page
        $sCurSearch = WTS_PARAM_ACT_QUEUE;
      }
      
      switch($sCurSearch){
        case WTS_PARAM_SEARCH:
          if(cChain::bAddFilterPartValue($this->cDb, $_GET[WTS_PARAM_SEARCH])){
            $mCurSearch = $_GET[WTS_PARAM_SEARCH];
          }
          break;
        case WTS_PARAM_ACT_TAG:
          $sSlugs = substr($_GET[WTS_PARAM_ACT_TAG], 1, -1);
          $aSlugs = explode(',', $sSlugs);
          if(cChain::bAddFilterTag($this->cDb, $aSlugs)){
            $mCurSearch = $_GET[WTS_PARAM_ACT_TAG];
          }
          break;
        case WTS_PARAM_ACT_CONT:
          if(cChain::bAddFilterCont($_GET[WTS_PARAM_ACT_CONT])){
            $mCurSearch = $_GET[WTS_PARAM_ACT_CONT];
          }
          break;
        case WTS_PARAM_ACT_GROUP:
          if($this->cCont->iType() !== cCont::CSTMR //может пролезть, когда перевели в другую организацию - сможет смотреть заявки из старой - а это не положено
          && cChain::bAddFilterGroup($_GET[WTS_PARAM_ACT_GROUP])){
            $mCurSearch = $_GET[WTS_PARAM_ACT_GROUP];
          }
          break;
        case WTS_PARAM_ACT_QUEUE:
          if(cChain::bAddFilterQueue($_GET[WTS_PARAM_ACT_QUEUE])){
            $mCurSearch = $_GET[WTS_PARAM_ACT_QUEUE];
          }
          break;
        default: break;
      }
      if($mCurSearch === false && $sCurSearch !== false){$sCurSearch = false;}

      if($sCurSearch === WTS_PARAM_ACT_CONT
      || $sCurSearch === WTS_PARAM_ACT_GROUP
      || $sCurSearch === WTS_PARAM_ACT_QUEUE){
        
        $iCurActivity = cChActivity::ALL;
        
        if(isset($_GET[WTS_PARAM_CLOSED])){
          $sCurSearchParamClosed = ($_GET[WTS_PARAM_CLOSED] === 'false' ? 'false' : 'true');
          cChain::bAddFilterClosed($sCurSearchParamClosed   === 'false' ? false : true);
        }
      }
      
      //parse activity - security level
      $aActivities = false;
      switch($this->cCont->iType()){
        case cCont::ADMIN:
          cChActivity::AddActivity(cChActivity::ALL_OPN, $aActivities);
          cChActivity::AddActivity(cChActivity::ALL, $aActivities);
          if($iCurActivity !== cChActivity::ALL){
            cChain::bAddFilterClosed(false);
            if($iCurActivity !== cChActivity::ALL_OPN){
              $iCurActivity = cChActivity::ALL_OPN;
            }
          }
        break;
        case cCont::AGENT:
          cChActivity::AddActivity(cChActivity::MY_OPN, $aActivities);
          cChActivity::AddActivity(cChActivity::QUE_OPN, $aActivities);
          cChActivity::AddActivity(cChActivity::ALL_MY, $aActivities);
          cChActivity::AddActivity(cChActivity::ALL, $aActivities);
        
          switch($iCurActivity){
            case cChActivity::ALL: break;
            case cChActivity::ALL_MY:
              cChain::bAddFilterOwner($this->cCont->iID());
            break;
            case cChActivity::QUE_OPN:
              cChain::bAddFilterQueue($this->cCont->iQueue());
              cChain::bAddFilterClosed(false);
            break;
            default:
              cChain::bAddFilterOwner($this->cCont->iID());
              cChain::bAddFilterClosed(false);
              $iCurActivity = cChActivity::MY_OPN;
            break;
          }
        break;
        case cCont::CSTMR://кастомер не должен увидеть лишнего
          cChActivity::AddActivity(cChActivity::MY_OPN, $aActivities);
          cChActivity::AddActivity(cChActivity::ALL_MY, $aActivities);
          if($this->cCont->iGroup() > 0){
            cChActivity::AddActivity(cChActivity::ALL_GRP, $aActivities);
          }
          switch($iCurActivity){
            case cChActivity::ALL_MY:
              cChain::bAddFilterCont($this->cCont->iID());
            break;
            case cChActivity::ALL_GRP:
              if($this->cCont->iGroup()){
                cChain::bAddFilterGroup($this->cCont->iGroup());
                break;//именно тут - если нет группы - провалится в дефолт!
              }
            default://my opened
              cChain::bAddFilterCont($this->cCont->iID());
              cChain::bAddFilterClosed(false);
              $iCurActivity = cChActivity::MY_OPN;
            break;
          }
        break;
        default: break;
      }

      $iRows  = cChain::iCount($this->cDb);
      $iPages = 1;

      if($iRows > 0){
        //normalize page
        if($iRows > WTS_WIDE_ROW_PER_PAGE){
          $iPages = ceil($iRows / WTS_WIDE_ROW_PER_PAGE);
        }
        if($iPage > $iPages){$iPage = $iPages;}

        $iRows = cChain::iUnserialize($this->cDb, $iPage, WTS_WIDE_ROW_PER_PAGE, $aChains);
      }
      
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurChain = &cChain::mInArray($aChains, $_GET[WTS_PARAM_ID]);
      }


      //020. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CHAIN);
      $cElement->Content($cButton->cRoot());
      
      //select activity type
      $cButton = new cButton();
        
      $cSelect = new cSelect(WTS_PARAM_TYPE, WTS_PFX . '-toolbar');
      
      foreach($aActivities as $key => $sName){
        $cSelect->Option($key === $iCurActivity, $key, $sName);
      }
      
      $cElementS = $cSelect->cRoot();
      $cElementS->Attribute('onchange', 'this.form.submit();');
      $cButton->Content($cElementS);
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CHAIN);
      $cElement->Content($cButton->cRoot());
      
      //search for chains
      $cButton = new cButton();
      $cElementI = new cElement('input', false);
      $cElementI->Attribute('type', 'search');
      $cElementI->Attribute('class', WTS_PFX . '-toolbar-search');
      $cElementI->Attribute('name', WTS_PARAM_SEARCH);
      $cElementI->Attribute('placeholder', '&#128270; ' . _('Search for Chains'));
      $cElementI->Attribute('title', _('Enter Word for Search'));
      if($sCurSearch === WTS_PARAM_SEARCH){
        $cElementI->Attribute('value', htmlentities($mCurSearch));
      }
      $cButton->Content($cElementI);
      $cButton->Hidden(WTS_PARAM_TYPE, $iCurActivity);
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CHAIN);
      $cElement->Content($cButton->cRoot());
      
      //page navbar
      if($iPages > 1){
        $aButtons = array();
        $this->CreateButtonsNav(WTS_PARAM_ACT_CHAIN, $iPage, $iPages, $aButtons);
        foreach($aButtons as &$cButton){
          $cButton->Hidden(WTS_PARAM_TYPE, $iCurActivity);
          if($sCurSearch !== false){$cButton->Hidden($sCurSearch, $mCurSearch);}
          if($sCurSearchParamClosed !== false){
            $cButton->Hidden(WTS_PARAM_CLOSED, $sCurSearchParamClosed);
          }
          $cElement->Content($cButton->cRoot());
        }
      }
      $cPage->Main($cElement);
      
      //030. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Chain'), '96px'); //?? experiment
      $cTable->Column(_('Subject & Preview'));
      $cTable->Column(_('Responsible'), '15%');
      $cTable->Column(_('Tags'), '15%');
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }
        
        $aResps = $aTagViews = false;
        cChain::iUnserializeTags($this->cDb, $aChains, $aTagViews);

        $iID = 0;
        if($cCurChain){$iID = $cCurChain->iID();}
        
        foreach($aChains as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . sSubAction(WTS_PARAM_TYPE, $iCurActivity)
            . ($iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
            . ($sCurSearch === false ? '' : sSubAction($sCurSearch, urlencode($mCurSearch)))
            . ($sCurSearchParamClosed === false ? '' : sSubAction(WTS_PARAM_CLOSED, $sCurSearchParamClosed))
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          
          if($c->bClosed()){
            $cElementImg->Attribute('src', IMG_ENABLED);
            $cElementImg->Attribute('title', _('Closed'));
          }
          else{
            $cElementImg->Attribute('src', IMG_DISABLED);
            $cElementImg->Attribute('title', _('Open'));
          }
          
          $cElementA = cSE::cCreate('a', $c->iID(), WTS_PFX . '-link2' );
          $cElementA->Attribute('title', _('View Events'));
          $cElementA->Attribute('href'
                            , sAction(WTS_PARAM_ACT_EVENT)
                            . sSubAction(WTS_PARAM_ACT_CHAIN, $c->iID()));//last page?
          
          $cElementTd->Content($cElementImg);
          $cElementTd->Content($cElementA);

          $cElementTr->Content($cElementTd);

          //column 2
          $cElementTd = new cElement('td', false);
          
          if($c->sSubject()){
            $cElementTd->Content(cSE::cCreate('b', $c->sSubject()));
            $cElementTd->Content(new cElement('br', false));
          }
          
          if($c->sPreview()){
            //$cElementTd->Content(cSE::cCreate('small', $c->sPreview()));
            $cElementTd->Content(new cElementBase(htmlentities($c->sPreview())));
          }
          $cElementTr->Content($cElementTd);
          
          //column 3 - queue & resp
          $cElementTd = new cElement('td', false);
          
          if(!isset($aResps[$c->iUpdater()])){
            $aResps[$c->iUpdater()] = cCont::mUnserialize($this->cDb, $c->iUpdater());
          }
          if($aResps[$c->iUpdater()]){
            $cElementTd->Content($this->cDoContInfo($aResps[$c->iUpdater()]));
          }
          
          $cElementTr->Content($cElementTd);
          
          //column 4 - tags
          $cElementTd = new cElement('td', false);
          
          
          $aTagIDs = $c->aTagIDs();

          if($aTagViews
          && count($aTagIDs) > 0){
            foreach($aTagIDs as $iTagID){
              $cElementDiv = new cElement('div', false);

              if($aTagViews[$iTagID]->sColor()){
                $cElementDiv->Attribute('style', 'background-color:#'
                . $aTagViews[$iTagID]->sColor() . ';');
              }
                                    
              $cElementDiv->Attribute('class', WTS_PFX . '-table-color');
              $cElementDiv->Content(new cElementBase(htmlentities($aTagViews[$iTagID]->sName())));
              $cElementTd->Content($cElementDiv);
            }
          }
          
          $cElementTr->Content($cElementTd);
         
          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //040. side bar tags
      $aTags = false;
      //triple load tag table if using filter by tag
      if(cTag::iUnserialize($this->cDb, $aTags) > 0){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Tags')));
        
        $cElement->Content($cElementCapt);
        
        $cField = new cField();
        
        foreach($aTags as &$cTag){
          $cElementA = new cElement('a', false);

          if($cTag->sColor()){
            $cElementA->Attribute('style', 'background-color:#' . $cTag->sColor() . ';');
          }
          $cElementA->Attribute('href', $sHref
          . sSubAction(WTS_PARAM_TYPE, $iCurActivity)
          . sSubAction(WTS_PARAM_ACT_TAG, '{' . $cTag->sSlug() . '}'));
                                
          $cElementA->Attribute('class', WTS_PFX . '-table-color ' . WTS_PFX . '-tag');
          $cElementA->Content(new cElementBase(htmlentities($cTag->sName())));
          $cField->Input($cElementA);
        }
        
        $cElement->Content($cField->cBuild());
        
        $cPage->Aside($cElement);
      }
      
      //050. side bar info
      if($cCurChain){
        
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Chain Info')));
        
        $cElement->Content($cElementCapt);
        
        $cToc = new cToc();
        
        $this->DoChainInfo($cToc, $cCurChain);
        
        //events
        cEvent::AddFilterChain($cCurChain->iID());
        $iEvents = cEvent::iCount($this->cDb);
        if($iEvents > 0){
          $cElementInfo = new cElement('a', false);
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_EVENT)
          . sSubAction(WTS_PARAM_ACT_CHAIN, $cCurChain->iID()));//last page?
          $cElementInfo->Content(new cElementBase($iEvents));
          $cToc->Child(new cElementBase(_('Events')), $cElementInfo);
        }
        
        $cElement->Content($cToc->cRoot());
        
        $cPage->Aside($cElement);
      }
    }

    public function DoProfilePage(cPage &$cPage){
      
      $sHref = sAction(WTS_PARAM_ACT_PROFILE);
      
      //010. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_PWD:
          if(isset($_POST[WTS_PARAM_PWD . 1])
          && mb_strlen($_POST[WTS_PARAM_PWD . 1], WTS_ENC) > 0
          && isset($_POST[WTS_PARAM_PWD . 2])
          && mb_strlen($_POST[WTS_PARAM_PWD . 2], WTS_ENC) >= WTS_PASSWD_LENGHT
          && isset($_POST[WTS_PARAM_PWD . 3])
          && $_POST[WTS_PARAM_PWD . 2] === $_POST[WTS_PARAM_PWD . 3]
          && $_POST[WTS_PARAM_PWD . 1] !== $_POST[WTS_PARAM_PWD . 3]){ //тоже на тоже не меняем
          
            if($sUID = cCont::mBeginSsn($this->cDb, $this->cCont->sLogin())){
              if(cCont::bPasswd($this->cDb, $sUID, $_POST[WTS_PARAM_PWD . 3], $_POST[WTS_PARAM_PWD . 1])){
                $sUID = sha1($sUID . hash('sha256', $_POST[WTS_PARAM_PWD . 3], false));
                
                $_SESSION[WTS_SESSION_PARAM_HASH]  = $sUID;
                
                $cMsg = new cMsg($this->cCont->iID());
                $cMsg->Value(_('Password was successfully updated!'));
                $cMsg->bSerialize($this->cDb);
                unset($cMsg);
              }
              else{//bad current password - Goodby!
                $cLog = new cLog(cLog::WARN);
                $cLog->Value('Fail change passford for: ' . $this->cCont->sLogin() . ' from IP: ' . $cLog->sIP());
                $cLog->bSerialize($this->cDb);
                unset($cLog);
              }
            }

          }
          
          break;
        case WTS_PARAM_EACT_UPDATE:
          $mVal = false;
          if(isset($_POST[WTS_PARAM_LANG]) && (int)$_POST[WTS_PARAM_LANG] !== 1033){
            $mVal = $_POST[WTS_PARAM_LANG];
          }
          
          $this->cCont->bSerializeMeta($this->cDb, WTS_COOKIE_PARAM_UILANG, $mVal);

          $mVal = false;
          if(isset($_POST[WTS_PARAM_COLOR]) && (int)$_POST[WTS_PARAM_COLOR] !== 0){
            $mVal = $_POST[WTS_PARAM_COLOR];
          }
          $this->cCont->bSerializeMeta($this->cDb, WTS_COOKIE_PARAM_CSS, $mVal);
          break;
        default: break;
        }
        header('Location: ' . $sHref);
        exit;
      }
      
      //020. widgets
      
      //load cont meta
      $aMeta = false;
      $iRows = $this->cCont->iUnserializeMeta($this->cDb, $aMeta);
      
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-float-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Content(new cElementBase(_('Settings')));
      
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
                                       . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_UPDATE));
      $cElementForm->Attribute('method', 'post');
      
      //uilang
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Language'), WTS_PARAM_LANG));
      
      $cSelect = new cSelect(WTS_PARAM_LANG, WTS_PFX . '-input');
      
      $iUilang = 0;
      if($iRows > 0
      && array_key_exists(WTS_COOKIE_PARAM_UILANG, $aMeta)){
        $iUilang = (int)$aMeta[WTS_COOKIE_PARAM_UILANG];
      }
      if($iUilang === 0){$iUilang = 1033;}

      foreach(WTS_LANGS as $key => $s){
        $cSelect->Option($key === $iUilang, $key, $s);
      }
      
      $cField->Input($cSelect->cRoot());
      
      $cElementForm->Content($cField->cBuild());
      
      //color
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Color Scheme'), WTS_PARAM_COLOR));
      
      $cSelect = new cSelect(WTS_PARAM_COLOR, WTS_PFX . '-input');
      
      $iCSS = 0;
      if($iRows > 0
      && array_key_exists(WTS_COOKIE_PARAM_CSS, $aMeta)){
        $iCSS = (int)$aMeta[WTS_COOKIE_PARAM_CSS];
      }

      foreach(WTS_STYLES as $key => $s){
        $cSelect->Option($key === $iCSS, $key, basename($s, '.css'));
      }
      
      $cField->Input($cSelect->cRoot());
      
      $cElementForm->Content($cField->cBuild());
      
      
      $cField = new cField(cField::RIGHT);
      $cField->Input(cField::cElementSubmit(_('Update')));
      
      $cElementForm->Content($cField->cBuild());

      $cElement->Content($cElementForm);

      $cPage->Main($cElement);
      
      //passwd
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-float-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Content(new cElementBase(_('Password')));
      
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
                                       . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_PWD));
      $cElementForm->Attribute('method', 'post');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Current Password'), WTS_PARAM_PWD . 1));
      $cField->Input(cField::cElementInput(WTS_PARAM_PWD . 1, null, 'password'));
      
      $cElementForm->Content($cField->cBuild());
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('New Password'), WTS_PARAM_PWD . 2));
      
      $cElementInp = cField::cElementInput(WTS_PARAM_PWD . 2, null, 'password');
      $cElementInp->Attribute('placeholder', _('min. password lenght ') . WTS_PASSWD_LENGHT . _(' symbols'));
      
      $cField->Input($cElementInp);
      
      $cElementForm->Content($cField->cBuild());
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Confirm New Password'), WTS_PARAM_PWD . 3));
      $cField->Input(cField::cElementInput(WTS_PARAM_PWD . 3, null, 'password'));
      
      $cElementForm->Content($cField->cBuild());
      
      
      $cField = new cField(cField::RIGHT);
      $cField->Input(cField::cElementSubmit(_('Change')));
      
      $cElementForm->Content($cField->cBuild());

      $cElement->Content($cElementForm);
      
      $cPage->Main($cElement);
    }
    
    public function DoAboutPage(cPage &$cPage){

      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-center-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      $cElementCapt->Attribute('style', 'text-align: center;');
      $cElementCapt->Content(new cElementBase('Web Ticket System'));
      
      $cElement->Content($cElementCapt);
      
      $cToc = new cToc();
      $cToc->Child(new cElementBase(_('Architecture')), new cElementBase(php_uname('m')));
      $cToc->Child(new cElementBase(_('WEB Server')), new cElementBase($_SERVER['SERVER_SOFTWARE']));
      $cToc->Child(new cElementBase('PHP'), new cElementBase(phpversion()));
      $cToc->Child(new cElementBase('MySQL'), new cElementBase($this->cDb->sQueryRes('select version()')));
      $cToc->Child(new cElementBase(_('Scheme')), new cElementBase(WTS_DB_SCHEME));
      $cElement->Content($cToc->cRoot());
      
      $cField = new cField(cField::CENTER);
      $cElementImg = new cElement('img', false);
      $cElementImg->Attribute('src', WTS_IMG_PATH . 'wts-logo.png');
      $cElementImg->Attribute('width', '130px');
      $cField->Label($cElementImg);
      $cElement->Content($cField->cBuild());
      
      $cField = new cField(cField::CENTER);
      $cElementA = cSE::cCreate('a', 'webticketsystem@gmail.com', WTS_PFX . '-alert');
      $cElementA->Attribute('href', 'mailto:webticketsystem@gmail.com');
      $cElementA->Attribute('title', _('Contact Us By Email'));
      $cField->Label($cElementA);
      $cElement->Content($cField->cBuild());
      
      $cPage->Main($cElement);

    }
  }

}

?>
