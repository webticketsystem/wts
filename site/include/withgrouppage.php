<?php
/*
 * withgrouppage.php (part of WTS) - trait UI tWithGroupPage
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  trait tWithGroupPage{
    
    protected function &cAddGroupMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Groups')
                         , _('Groups management')
                         , ($sCurAct !== WTS_PARAM_ACT_GROUP ? sAction(WTS_PARAM_ACT_GROUP) : null));
      return $cMenuItem;
    }
    
    public function DoGroupPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref     = sAction(WTS_PARAM_ACT_GROUP);
      $aGroups   = false;
      $cCurGroup = false;
      $iPage     = ((isset($_GET[WTS_PARAM_PAGE]) && $_GET[WTS_PARAM_PAGE] > 0) ? $_GET[WTS_PARAM_PAGE] : 1);
      
      $sCurSearch = false;
      if(isset($_GET[WTS_PARAM_SEARCH]) && cGroup::bAddFilterPartValue($this->cDb, $_GET[WTS_PARAM_SEARCH])){
        $sCurSearch = $_GET[WTS_PARAM_SEARCH];
      }
      $iRows  = cGroup::iCount($this->cDb);
      $iPages = 1;

      if($iRows > 0){
        //normalize page
        if($iRows > WTS_NARROW_ROW_PER_PAGE){
          $iPages = ceil($iRows / WTS_NARROW_ROW_PER_PAGE);
        }
        if($iPage > $iPages){$iPage = $iPages;}

        $iRows = cGroup::iUnserialize($this->cDb, $iPage, WTS_NARROW_ROW_PER_PAGE, $aGroups);
      }
      
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurGroup = &cGroup::mInArray($aGroups, $_GET[WTS_PARAM_ID]);
      }

      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $cObj = new cGroup();
          if(isset($_POST[WTS_PARAM_NAME])){
            $cObj->Name($_POST[WTS_PARAM_NAME]);
            $cObj->Creator($this->cCont->iID());
          }
          if($cObj->bSerialize($this->cDb)){
            $cCurGroup = &$cObj;
            $sCurSearch = $cObj->sName();
            $cLog = new cLog();
            $cLog->Value('Group ' . $cCurGroup->sName() . ' created by ' . $this->cCont->sLogin());
            $cLog->bSerialize($this->cDb);
            unset($cLog);
          }
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Group using Name \'') . $_POST[WTS_PARAM_NAME] . '\'');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_UPDATE:
          if($cCurGroup){
            $cCurGroup->Updater($this->cCont->iID());
            if(isset($_POST[WTS_PARAM_NAME])){
              $sOldName = $cCurGroup->sName();
              $cCurGroup->Name($_POST[WTS_PARAM_NAME]);
              if($cCurGroup->bSerialize($this->cDb)){
                $cLog = new cLog();
                $cLog->Value('Group ' . $sOldName . ' renamed to '
                . $cCurGroup->sName() . ' by ' . $this->cCont->sLogin());
                $cLog->bSerialize($this->cDb);
                unset($cLog);
              }
            }
            //update attributes: attr-0-1
            $aAttrView = false;
            foreach($_POST as $key => &$s){
              if(strlen($key) > 7 && (WTS_PARAM_ATTRIBUTE . '-' === substr($key, 0, 5))){
                $a = explode('-', $key);
                if(count($a) === 3 && strlen($s) > 0){//[0] - attr, [1] - id, [2] - type
                  $aAttrView[] = new cAttrView($a[1], $a[2], $s);
                }
              }
            }
            if($cCurGroup->bSerializeAttr($this->cDb, $aAttrView)){
              $cLog = new cLog();
              $cLog->Value('Attributes for Group ' . $cCurGroup->sName()
              . ' updated by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);
            }
          }
          break;
        case WTS_PARAM_EACT_MERGE:
          if($cCurGroup
          && isset($_POST[WTS_PARAM_NAME])
          && ($cGroup = cGroup::mUnserialize($this->cDb, $_POST[WTS_PARAM_NAME]))
          && $cGroup->iID() !== $cCurGroup->iID()){
            $cLog = new cLog(cLog::WARN);
            $cLog->Value('Merge Group ' . $cGroup->sName()
            . ' into Group ' . $cCurGroup->sName() . ' by ' . $this->cCont->sLogin());
            $cLog->bSerialize($this->cDb);
            unset($cLog);
            $cMsg = new cMsg($this->cCont->iID());
            $cMsg->Value(_('Group ') . $cGroup->sName()
            . _(' merged into Group ') . $cCurGroup->sName());
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
            cCont::MergeGroup($this->cDb, $cGroup->iID(), $cCurGroup->iID());
            cChain::MergeGroup($this->cDb, $cGroup->iID(), $cCurGroup->iID());
            cContract::MergeGroup($this->cDb, $cGroup->iID(), $cCurGroup->iID());
            cProduct::MergeGroup($this->cDb, $cGroup->iID(), $cCurGroup->iID());
            $cGroup->bDelete($this->cDb);
          }
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t merge Groups'));
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        default: break;
        }
        if($sCurSearch !== false){$sHref .= sSubAction(WTS_PARAM_SEARCH, urlencode($sCurSearch));}
        if($iPage > 1){$sHref .= sSubAction(WTS_PARAM_PAGE, $iPage);}
        if($cCurGroup !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurGroup->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_GROUP);
      $cElement->Content($cButton->cRoot());
      
      //search for groups
      $cButton = new cButton();
      $cElementI = new cElement('input', false);
      $cElementI->Attribute('type', 'search');
      $cElementI->Attribute('class', WTS_PFX . '-toolbar-search');
      $cElementI->Attribute('name', WTS_PARAM_SEARCH);
      $cElementI->Attribute('id', WTS_PFX . '-search-box');
      $cElementI->Attribute('placeholder', '&#128270; ' . _('Search for Groups'));
      $cElementI->Attribute('title', _('Enter Word for Search'));
      if($sCurSearch !== false){
        $cElementI->Attribute('value', htmlentities($sCurSearch));
      }
      $cButton->Content($cElementI);
      
      $cElementUl = new cElement('ul', false);
      $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper');
      $cButton->Content($cElementUl);
      
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_GROUP);
      
      $cElementForm = $cButton->cRoot();
      $cElementForm->Attribute('id', WTS_PFX . '-search-form');
      $cElementForm->Attribute('autocomplete', 'off');
      
      $cElement->Content($cElementForm);
      
      $cElementS = new cElement('script', false);
      $cElementS->Attribute('type', 'text/javascript');
      $cElementS->Attribute('src', WTS_PATH . 'advice/jquery.min.js');
      $cPage->Head($cElementS);
      
      $cElementS = new cElement('script', false);
      $cElementS->Attribute('type', 'text/javascript');
      $cElementS->Attribute('src', WTS_PATH . 'advice/gsearch.js');
      $cPage->Head($cElementS);
      
      //page navbar
      if($iPages > 1){
        $aButtons = array();
        $this->CreateButtonsNav(WTS_PARAM_ACT_GROUP, $iPage, $iPages, $aButtons);
        foreach($aButtons as &$cButton){
          if($sCurSearch !== false){$cButton->Hidden(WTS_PARAM_SEARCH, $sCurSearch);}
          $cElement->Content($cButton->cRoot());
        }
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Group Name'));
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }

        $iID = 0;
        if($cCurGroup){$iID = $cCurGroup->iID();}
        
        foreach($aGroups as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . ($iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
            . ($sCurSearch === false ? '' : sSubAction(WTS_PARAM_SEARCH, urlencode($sCurSearch)))
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          $cElementImg->Attribute('src', IMG_GROUP);
          $cElementImg->Attribute('title', _('Group'));
          
          $cElementTd->Content($cElementImg);

          $cElementTd->Content(new cElementBase($c->sName()));
          $cElementTr->Content($cElementTd);
         
          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);

      //050. side bar create or update
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      
      $c = ($cCurGroup ? $cCurGroup : false);
      
      if($c){
        $cElementCapt->Content(new cElementBase(_('Group Properties')));
      }
      else{
        $cElementCapt->Content(new cElementBase(_('Add New Group')));
      }
      $cElement->Content($cElementCapt);
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
          . sSubAction(WTS_PARAM_EDIT, ($c ? WTS_PARAM_EACT_UPDATE : WTS_PARAM_EACT_CREATE))
          . ($c && $iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
          . ($c ? sSubAction(WTS_PARAM_ID, $c->iID()) : '')
          . ($c && $sCurSearch !== false ? sSubAction(WTS_PARAM_SEARCH, urlencode($sCurSearch)) : ''));
      $cElementForm->Attribute('method', 'post');
      $cElementForm->Attribute('name', WTS_PARAM_ACT_GROUP);
      $cElementForm->Attribute('onsubmit', 'return validate()');
      
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
      
      $cElementInp = cField::cElementInput(WTS_PARAM_NAME, ($c ? $c->sName() : null));
      $cElementInp->Attribute('placeholder', _('My Group'));
      $cElementInp->Attribute('title', _('Name of Group must be unique.'));
      
      $cField->Input($cElementInp);
      
      $cElementSpan = new cElement('span', false);
      $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
      $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_NAME);
      $cField->Label($cElementSpan);

      $sScript = '
function validate(){
  if(document.forms[\'' . WTS_PARAM_ACT_GROUP . '\'][\'' . WTS_PARAM_NAME . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_NAME . '\').innerHTML=\'&#9998;\';
    return false;
  }
  return true;
}';
        
      $cElementS = new cElement('script', false);
      $cElementS->Attribute('type', 'text/javascript');
      $cElementS->Content(new cElementBase($sScript, false));
      $cPage->Head($cElementS);
      
      $cElementForm->Content($cField->cBuild());
      
      //attr
      if($c){
        $aAttrView = false;
        if($c->iUnserializeAttr($this->cDb, $aAttrView) > 0){
          foreach($aAttrView as $key => &$cAttrView){
            $sAttr = WTS_PARAM_ATTRIBUTE . '-' . $cAttrView->iID() . '-' . $cAttrView->iType();
            
            $cField = new cField();
            $cField->Label(cField::cElementLabel($cAttrView->sName(), $sAttr));

            $cElementInput = cField::cElementInput($sAttr, ($cAttrView->iID() === 0 ? null : $cAttrView->sValue()));
            if($cAttrView->sColor()){
              $cElementInput->Attribute('style', 'background-color:#' . $cAttrView->sColor());
            }
            $cField->Input($cElementInput);
            $cElementForm->Content($cField->cBuild());
            
            unset($aAttrView[$key]);
          }
        }
      }
      
      //ok
      $cField = new cField(cField::RIGHT);
      if($c){
        $cField->Input(cField::cElementSubmit(_('Reset'), true));
        $cField->Input(cField::cElementSubmit(_('Apply')));
      }
      else{
        $cField->Input(cField::cElementSubmit(_('Create')));
      }
      $cElementForm->Content($cField->cBuild());
      
      $cElement->Content($cElementForm);
      
      $cPage->Aside($cElement);

      if($c){
        //060. side bar status
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Group Info')));
        
        $cElement->Content($cElementCapt);
        
        $cToc = new cToc();

        $cToc->Child(new cElementBase(_('Created')), $this->cDoTime($c->iDtCreated()));
        $cToc->AddLinkForLastChild($this->cDoDate($c->iDtCreated()));
        
        $cContCr = $bUnsetCr = false;
        if($c->iCreator() !== $this->cCont->iID()){
          $cContCr = cCont::mUnserialize($this->cDb, $c->iCreator());
          $bUnsetCr = true;
        }
        else{$cContCr = $this->cCont;}
        
        $cToc->Child(new cElementBase(_('Created by'))
                   , ($cContCr === false ? new cElementBase(_('Unknown')) : $this->cDoContInfo($cContCr)));
                   
        if($c->iDtUpdated() > 0){
          $cToc->Child(new cElementBase(_('Updated')), $this->cDoTime($c->iDtUpdated()));
          $cToc->AddLinkForLastChild($this->cDoDate($c->iDtUpdated()));
          
          $cContUp = $bUnsetUp = false;
          
          if($c->iUpdater() != $cContCr->iID()){
            if($c->iUpdater() != $this->cCont->iID()){
              $cContUp = cCont::mUnserialize($this->cDb, $c->iUpdater());
              $bUnsetUp = true;
            }
            else{$cContUp = $this->cCont;}
          }
          else{$cContUp = $cContCr;}
          
          $cToc->Child(new cElementBase(_('Updated by'))
                     , ($cContUp === false ? new cElementBase(_('Unknown')) : $this->cDoContInfo($cContUp)));
          if($bUnsetUp){unset($cContUp);}
        }
        
        if($bUnsetCr){unset($cContCr);}
        
        //contacts in group
        cCont::bAddFilterGroup($c->iID());
        $iConts = cCont::iCount($this->cDb);

        $cElementInfo = new cElement('a', false);
        $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CONT)
        . sSubAction(WTS_PARAM_ACT_GROUP, $c->iID()));
        $cElementInfo->Content(new cElementBase($iConts > 0 ? $iConts : _('Not set')));
        $cToc->Child(new cElementBase(_('Contacts')), $cElementInfo);

        //contracts
        cContract::bAddFilterGroup($c->iID());
        $iContrs = cContract::iCount($this->cDb);
        if($iContrs > 0){
          $cElementInfo = new cElement('a', false);
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CONTR)
          . sSubAction(WTS_PARAM_ACT_GROUP, $c->iID()));
          $cElementInfo->Content(new cElementBase($iContrs));
          $cToc->Child(new cElementBase(_('Contracts')), $cElementInfo);
        }
        
        //products
        cProduct::bAddFilterGroup($c->iID());
        $iProds = cProduct::iCount($this->cDb);
        if($iProds > 0){
          $cElementInfo = new cElement('a', false);
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_PRODUCT)
          . sSubAction(WTS_PARAM_ACT_GROUP, $c->iID()));
          $cElementInfo->Content(new cElementBase($iProds));
          $cToc->Child(new cElementBase(_('Products')), $cElementInfo);
        }

        //open chains
        cChain::bAddFilterGroup($c->iID());
        cChain::bAddFilterClosed(false);
        $iChains = cChain::iCount($this->cDb);
        if($iChains > 0){
          $cElementInfo = cSE::cCreate('a', $iChains, WTS_PFX . '-alert ' . WTS_PFX . '-info2');
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CHAIN)
          . sSubAction(WTS_PARAM_ACT_GROUP, $c->iID())
          . sSubAction(WTS_PARAM_CLOSED, 'false'));
          $cToc->Child(new cElementBase(_('Open Chains')), $cElementInfo);
        }
        
        //closed chains
        cChain::ClearFilters();
        cChain::bAddFilterGroup($c->iID());
        cChain::bAddFilterClosed(true);
        $iChains = cChain::iCount($this->cDb);
        if($iChains > 0){
          $cElementInfo = cSE::cCreate('a', $iChains, WTS_PFX . '-notify ' . WTS_PFX . '-info2');
          $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_CHAIN)
          . sSubAction(WTS_PARAM_ACT_GROUP, $c->iID())
          . sSubAction(WTS_PARAM_CLOSED, 'true'));
          $cToc->Child(new cElementBase(_('Closed Chains')), $cElementInfo);
        }
        
        $cElement->Content($cToc->cRoot());
        
        $cPage->Aside($cElement);
        
        //070. side bar merge
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Merge Group into Selected')));
        
        $cElement->Content($cElementCapt);
        
        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('action', $sHref
                                         . sSubAction(WTS_PARAM_EDIT, WTS_PARAM_EACT_MERGE)
                                         . ($iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
                                         . ($sCurSearch === false ? '' : sSubAction(WTS_PARAM_SEARCH, urlencode($sCurSearch)))
                                         . sSubAction(WTS_PARAM_ID, $c->iID()));
        $cElementForm->Attribute('method', 'post');
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Name'), WTS_PARAM_NAME));
        
        $cElementInp = cField::cElementInput(WTS_PARAM_NAME);
        $cElementInp->Attribute('placeholder', _('Existing Group'));
        $cElementInp->Attribute('title', _('Name and all attributes of Group will be lost, Chains, Contracts, Products and Contacts will be reattached to the Selected Group.'));
        $cField->Input($cElementInp);
        
        $cElementForm->Content($cField->cBuild());
        
        $cField = new cField(cField::RIGHT);
        $cField->Input(cField::cElementSubmit(_('Merge')));
        
        $cElementForm->Content($cField->cBuild());

        $cElement->Content($cElementForm);
        
        $cPage->Aside($cElement);
      }
    }
  }

}
?>
