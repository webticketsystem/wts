<?php
/*
 * withcontractpage.php (part of WTS) - trait UI tWithContractPage
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('core/contract.php');

  trait tWithContractPage{
    
    protected function &cAddContractMenuItem($sCurAct){
      $cMenuItem = new cMenuItem(_('Contracts')
                         , _('Contract management')
                         , ($sCurAct !== WTS_PARAM_ACT_CONTR ? sAction(WTS_PARAM_ACT_CONTR) : null));
      return $cMenuItem;
    }
    
    protected function DoContractPage(cPage &$cPage){
      //010. fill arrays and verify post/get data
      $sHref     = sAction(WTS_PARAM_ACT_CONTR);
      $aContrs   = false;
      $cCurContr = false;
      $iPage     = ((isset($_GET[WTS_PARAM_PAGE]) && $_GET[WTS_PARAM_PAGE] > 0) ? $_GET[WTS_PARAM_PAGE] : 1);
      
      $aTypes[10] = _('Contact') . ' → ' . _('Contact');
      $aTypes[20] = _('Contact') . ' → ' . _('Group');
      $aTypes[30] = _('Group') . ' → ' . _('Contact');
      $aTypes[40] = _('Group') . ' → ' . _('Group');
      
      $iCurType = (isset($_GET[WTS_PARAM_TYPE])
          && array_key_exists((int)$_GET[WTS_PARAM_TYPE], $aTypes)) ? (int)$_GET[WTS_PARAM_TYPE] : 10;
      
      $sCurSearch = $mCurSearch = false;
      
      if(isset($_GET[WTS_PARAM_SEARCH])){
        $sCurSearch = WTS_PARAM_SEARCH;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_TAG])){
        $sCurSearch = WTS_PARAM_ACT_TAG;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_CONT])){
        $sCurSearch = WTS_PARAM_ACT_CONT;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_GROUP])){
        $sCurSearch = WTS_PARAM_ACT_GROUP;
      }
      elseif(isset($_GET[WTS_PARAM_ACT_PRODUCT])){
        $sCurSearch = WTS_PARAM_ACT_PRODUCT;
      }
      
      switch($sCurSearch){
        case WTS_PARAM_SEARCH:
          if(cContract::bAddFilterPartValue($this->cDb, $_GET[WTS_PARAM_SEARCH])){
            $mCurSearch = $_GET[WTS_PARAM_SEARCH];
          }
          break;
        case WTS_PARAM_ACT_TAG:
          $sSlugs = substr($_GET[WTS_PARAM_ACT_TAG], 1, -1);
          $aSlugs = explode(',', $sSlugs);
          if(cContract::bAddFilterTag($this->cDb, $aSlugs)){
            $mCurSearch = $_GET[WTS_PARAM_ACT_TAG];
          }
          break;
        case WTS_PARAM_ACT_CONT:
          if(cContract::bAddFilterCont($_GET[WTS_PARAM_ACT_CONT])){
            $mCurSearch = $_GET[WTS_PARAM_ACT_CONT];
          }
          break;
        case WTS_PARAM_ACT_GROUP:
          if(cContract::bAddFilterGroup($_GET[WTS_PARAM_ACT_GROUP])){
            $mCurSearch = $_GET[WTS_PARAM_ACT_GROUP];
          }
          break;
        case WTS_PARAM_ACT_PRODUCT:
          if(cContract::bAddFilterProduct($this->cDb, $_GET[WTS_PARAM_ACT_PRODUCT])){
            $mCurSearch = $_GET[WTS_PARAM_ACT_PRODUCT];
          }
          break;
        default: break;
      }
      if($mCurSearch === false && $sCurSearch !== false){$sCurSearch = false;}
      
      $iRows  = cContract::iCount($this->cDb);
      $iPages = 1;

      if($iRows > 0){
        //normalize page
        if($iRows > WTS_NARROW_ROW_PER_PAGE){
          $iPages = ceil($iRows / WTS_NARROW_ROW_PER_PAGE);
        }
        if($iPage > $iPages){$iPage = $iPages;}

        $iRows = cContract::iUnserialize($this->cDb, $iPage, WTS_NARROW_ROW_PER_PAGE, $aContrs);
      }
      
      if($iRows > 0 && isset($_GET[WTS_PARAM_ID])){
        $cCurContr = &cContract::mInArray($aContrs, $_GET[WTS_PARAM_ID]);
      }
      
      //020. do edits
      $sAction = false;
      if(isset($_GET[WTS_PARAM_EDIT])){$sAction = $_GET[WTS_PARAM_EDIT];}
      
      if($sAction !== false){
        switch($sAction){
        case WTS_PARAM_EACT_CREATE:
          $cObj = new cContract();
          $cObj->Creator($this->cCont->iID());
          //seller - buyer
          if(isset($_POST[WTS_PARAM_FROM])
          && isset($_POST[WTS_PARAM_TO])){
            switch($iCurType){
              case 10: //Contact → Contact
              if($cSeller = cCont::mUnserialize($this->cDb, $_POST[WTS_PARAM_FROM])){
                $cObj->Seller($cSeller->iID(), cContract::CONTACT);
                unset($cSeller);
              }
              if($cBuyer  = cCont::mUnserialize($this->cDb, $_POST[WTS_PARAM_TO])){
                $cObj->Buyer($cBuyer->iID(), cContract::CONTACT);
                unset($cBuyer);
              }
              break;
              case 20: //Contact → Group
              
              if($cSeller = cCont::mUnserialize($this->cDb, $_POST[WTS_PARAM_FROM])){
                $cObj->Seller($cSeller->iID(), cContract::CONTACT);
                unset($cSeller);
              }
              if($cBuyer  = cGroup::mUnserialize($this->cDb, $_POST[WTS_PARAM_TO])){
                $cObj->Buyer($cBuyer->iID(), cContract::GROUP);
                unset($cBuyer);
              }
              break;
              case 30: //Group → Contact
              if($cSeller = cGroup::mUnserialize($this->cDb, $_POST[WTS_PARAM_FROM])){
                $cObj->Seller($cSeller->iID(), cContract::GROUP);
                unset($cSeller);
              }
              if($cBuyer  = cCont::mUnserialize($this->cDb, $_POST[WTS_PARAM_TO])){
                $cObj->Buyer($cBuyer->iID(), cContract::CONTACT);
                unset($cBuyer);
              }
              break;
              case 40: //Group → Group
              if($cSeller = cGroup::mUnserialize($this->cDb, $_POST[WTS_PARAM_FROM])){
                $cObj->Seller($cSeller->iID(), cContract::GROUP);
                unset($cSeller);
              }
              if($cBuyer  = cGroup::mUnserialize($this->cDb, $_POST[WTS_PARAM_TO])){
                $cObj->Buyer($cBuyer->iID(), cContract::GROUP);
                unset($cBuyer);
              }
              break;
              default: break;
            }
          }
          //subject
          if(isset($_POST[WTS_PARAM_SUBJECT])
          && strlen($_POST[WTS_PARAM_SUBJECT]) > 0){
            $cObj->Subject($_POST[WTS_PARAM_SUBJECT]);
          }
          //cost
          if(isset($_POST[WTS_PARAM_COST])){
            $cObj->Cost($_POST[WTS_PARAM_COST]);
          }
          //dates
          if(isset($_POST[WTS_PARAM_BEGIN])){$cObj->DtBegin(strtotime($_POST[WTS_PARAM_BEGIN]));}
          else{$cObj->DtBegin(strtotime('now'));}
          if(isset($_POST[WTS_PARAM_END])){$cObj->DtEnd(strtotime($_POST[WTS_PARAM_END]));}
          else{$cObj->DtEnd(strtotime('now'));}
          
          //attachments
          if(isset($_FILES[WTS_PARAM_FILE])){
            for($i = 0; $i < 100; $i++){
              if(isset($_FILES[WTS_PARAM_FILE]['size'][$i])
              && $_FILES[WTS_PARAM_FILE]['size'][$i] > 0){
                if($cStream = fopen($_FILES[WTS_PARAM_FILE]['tmp_name'][$i], 'r')){
                  $sCid  = cString::sRand(12) . '-' . WTS_DB_NAME
                  . '-' . date(WTS_DT_BACKUP_FMT, strtotime('now'))
                  . '-' . $this->cCont->sLogin();
                  $cObj->bAddAttach(stream_get_contents($cStream)
                                  , $_FILES[WTS_PARAM_FILE]['name'][$i]
                                  , $sCid);
                  fclose($cStream);
                }
              }
              else{break;}
            }
          }

          if($cObj->bSerialize($this->cDb, WTS_EVENT_THUMB_SIDE)){
            //tags
            if(isset($_POST[WTS_PARAM_SLUG])
            && count($_POST[WTS_PARAM_SLUG]) > 0){
              $cObj->bSerializeTag($this->cDb, $_POST[WTS_PARAM_SLUG], false);
            }
            $cCurContr = &$cObj;
            if($iPage > 1){$iPage = 1;}
            $sCurSearch = $mCurSearch = false;
            $cLog = new cLog();
            $cLog->Value('Contract ' . $cCurContr->iID() . ' created by ' . $this->cCont->sLogin());
            $cLog->bSerialize($this->cDb);
            unset($cLog);
          }
          else{
            $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
            $cMsg->Value(_('Can\'t create Contract using Seller \'') . $_POST[WTS_PARAM_FROM] . _('\' and Buyer \'') . $_POST[WTS_PARAM_TO] . '\'');
            $cMsg->bSerialize($this->cDb);
            unset($cMsg);
          }
          break;
        case WTS_PARAM_EACT_UPDATE:
          if($cCurContr){
            $cCurContr->Updater($this->cCont->iID());
            
            //subject
            if(isset($_POST[WTS_PARAM_SUBJECT])){
              $cCurContr->Subject($_POST[WTS_PARAM_SUBJECT]);
            }
            //cost
            if(isset($_POST[WTS_PARAM_COST])){
              $cCurContr->Cost($_POST[WTS_PARAM_COST]);
            }
            
            //attachments
            if(isset($_FILES[WTS_PARAM_FILE])){
              $cCurContr->iUnserializeAttach($this->cDb);
              for($i = 0; $i < 100; $i++){
                if(isset($_FILES[WTS_PARAM_FILE]['size'][$i])
                && $_FILES[WTS_PARAM_FILE]['size'][$i] > 0){
                  if($cStream = fopen($_FILES[WTS_PARAM_FILE]['tmp_name'][$i], 'r')){
                    $sCid  = cString::sRand(12) . '-' . WTS_DB_NAME
                    . '-' . date(WTS_DT_BACKUP_FMT, strtotime('now'))
                    . '-' . $this->cCont->sLogin();
                    $cCurContr->bAddAttach(stream_get_contents($cStream)
                                         , $_FILES[WTS_PARAM_FILE]['name'][$i]
                                         , $sCid);
                    fclose($cStream);
                  }
                }
                else{break;}
              }
            }

            if($cCurContr->bSerialize($this->cDb, WTS_EVENT_THUMB_SIDE)){
              $cLog = new cLog();
              $cLog->Value('Contract ' . $cCurContr->iID() . ' updated by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);
            }
            
            //tags
            if(isset($_POST[WTS_PARAM_SLUG])
            && $cCurContr->bSerializeTag($this->cDb, $_POST[WTS_PARAM_SLUG])){
              $cLog = new cLog();
              $cLog->Value('Tags for Contract ' . $cCurContr->iID() . ' updated by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);
            }
          }
          break;
        case WTS_PARAM_EACT_DELETE: //delete contract
        
          if($cCurContr){
            cProduct::bAddFilterContract($this->cDb, $cCurContr->iID());
            if(cProduct::iCount($this->cDb) > 0){
              $cMsg = new cMsg($this->cCont->iID(), cMsg::WARN);
              $cMsg->Value(_('Can\'t delete Contract that has Products.'));
              $cMsg->bSerialize($this->cDb);
              unset($cMsg);
            }
            else{
              $cLog = new cLog();
              $cLog->Value('Delete empty Contract ' . $cCurContr->iID() . ' by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);
              $cCurContr->bDelete($this->cDb);
              unset($cCurContr);
              $cCurContr = false;
            }
          }
          break;
        default: break;
        }
        if($sCurSearch !== false){$sHref .= sSubAction($sCurSearch, urlencode($mCurSearch));}
        if($iPage > 1){$sHref .= sSubAction(WTS_PARAM_PAGE, $iPage);}
        if($cCurContr !== false){$sHref .= sSubAction(WTS_PARAM_ID, $cCurContr->iID());}
        header('Location: ' . $sHref);
        exit;
      }
      
      //030. toolbar
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-toolbar-section');
      
      //refresh
      $cButton = new cButton();
      $cButton->Image(IMG_EDIT_REFRESH, _('Refresh'), WTS_PFX . '-toolbar-button');
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CONTR);
      $cElement->Content($cButton->cRoot());
      
      //search for contracts
      $cButton = new cButton();
      $cElementI = new cElement('input', false);
      $cElementI->Attribute('type', 'search');
      $cElementI->Attribute('class', WTS_PFX . '-toolbar-search');
      $cElementI->Attribute('name', WTS_PARAM_SEARCH);
      $cElementI->Attribute('id', WTS_PFX . '-search-box');
      $cElementI->Attribute('placeholder', '&#128270; ' . _('Search for Contracts'));
      $cElementI->Attribute('title', _('Enter Word for Search'));
      if($sCurSearch === WTS_PARAM_SEARCH){
        $cElementI->Attribute('value', htmlentities($mCurSearch));
      }
      $cButton->Content($cElementI);
      $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CONTR);
      $cElement->Content($cButton->cRoot());
      
      //delete if prods count = 0
      if($cCurContr){
        $cButton = new cButton();
        $cButton->Image(IMG_EDIT_REMOVE, _('Delete Contract'), WTS_PFX . '-toolbar-button');
        $cButton->Hidden(WTS_PARAM_ACTION, WTS_PARAM_ACT_CONTR);
        $cButton->Hidden(WTS_PARAM_EDIT, WTS_PARAM_EACT_DELETE);
        if($sCurSearch !== false){$cButton->Hidden($sCurSearch, $mCurSearch);}
        $cButton->Hidden(WTS_PARAM_ID, $cCurContr->iID());
        $cElement->Content($cButton->cRoot());
      }
      
      //page navbar
      if($iPages > 1){
        $aButtons = array();
        $this->CreateButtonsNav(WTS_PARAM_ACT_CONTR, $iPage, $iPages, $aButtons);
        
        foreach($aButtons as &$cButton){
          if($sCurSearch !== false){$cButton->Hidden($sCurSearch, $mCurSearch);}
          $cElement->Content($cButton->cRoot());
        }
      }
      $cPage->Main($cElement);
      
      //040. table
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-main-section');
      
      $cTable = new cTable(WTS_PFX . '-table');
      $cTable->Column(_('Contract'), '96px');
      $cTable->Column(_('Subject'));
      $cTable->Column(_('Seller'), '15%');
      $cTable->Column(_('Buyer'), '15%');
      $cTable->Column(_('Tags'), '15%');
      
      if($iRows > 0){
        if(isset($_GET[WTS_PARAM_SCROLL]) && ((int)$_GET[WTS_PARAM_SCROLL]) > 0){
          $cElementS = new cElement('script', false);
          $cElementS->Attribute('type', 'text/javascript');
          $cElementS->Content(new cElementBase('window.onload=function(){window.scroll(0,'
                                               . $_GET[WTS_PARAM_SCROLL] . ');}'));
          $cPage->Head($cElementS);
        }
        
        $iNow = strtotime('now');
        $aConts = $aGroups = $aTagViews = false;
        cContract::iUnserializeTags($this->cDb, $aContrs, $aTagViews);

        $iID = 0;
        if($cCurContr){$iID = $cCurContr->iID();}
        
        foreach($aContrs as &$c){
          //row
          $cElementTr = new cElement('tr', false);
          if($c->iID() == $iID){
            $cElementTr->Attribute('class', WTS_PFX . '-table-sel');
          }
          else{
            $cElementTr->Attribute('onclick', 'window.open(\''
            . $sHref
            . ($iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
            . ($sCurSearch === false ? '' : sSubAction($sCurSearch, urlencode($mCurSearch)))
            . sSubAction(WTS_PARAM_ID, $c->iID())
            . sSubAction(WTS_PARAM_SCROLL, '\'+((document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop), \'_self\')'));
          }

          //column 1
          $cElementTd = new cElement('td', false);
          
          $cElementImg = new cElement('img', false);
          $cElementImg->Attribute('class', WTS_PFX . '-table-image');
          
          if($c->iDtBegin() <= $iNow && $c->iDtEnd() >= $iNow){
            $cElementImg->Attribute('src', IMG_ENABLED);
            $cElementImg->Attribute('title', _('Valid'));
          }
          else{
            $cElementImg->Attribute('src', IMG_DISABLED);
            $cElementImg->Attribute('title', _('Invalid'));
          }
          $cElementTd->Content($cElementImg);
          
          $cElementA = cSE::cCreate('a', $c->iID(), WTS_PFX . '-link2' );
          $cElementA->Attribute('title', _('View Products'));
          $cElementA->Attribute('href'
                            , sAction(WTS_PARAM_ACT_PRODUCT)
                            . sSubAction(WTS_PARAM_ACT_CONTR, $c->iID()));
          
          $cElementTd->Content($cElementA);

          $cElementTr->Content($cElementTd);
          
          //column 2
          $cElementTd = new cElement('td', false);
          
          if($c->sSubject()){
            $cElementTd->Content(new cElementBase(htmlentities($c->sSubject())));
          }
          $cElementTr->Content($cElementTd);
          
          //column 3 - seller
          $cElementTd = new cElement('td', false);
          
          if($c->iSellerType() === cContract::CONTACT){
            if(!isset($aConts[$c->iSeller()])){
              $aConts[$c->iSeller()] = cCont::mUnserialize($this->cDb, $c->iSeller());
            }
            
            if($aConts[$c->iSeller()]){
              $cElementTd->Content($this->cDoContInfo($aConts[$c->iSeller()]));
            }
          }
          else{
            if(!isset($aGroups[$c->iSeller()])){
              $aGroups[$c->iSeller()] = cGroup::mUnserialize($this->cDb, $c->iSeller());
            }
            if($aGroups[$c->iSeller()]){
              $cElementTd->Content($this->cDoGroupInfo($aGroups[$c->iSeller()]));
            }
          }
          
          $cElementTr->Content($cElementTd);
          
          //column 4 - buyer
          $cElementTd = new cElement('td', false);
          
          if($c->iBuyerType() === cContract::CONTACT){
            if(!isset($aConts[$c->iBuyer()])){
              $aConts[$c->iBuyer()] = cCont::mUnserialize($this->cDb, $c->iBuyer());
            }
            
            if($aConts[$c->iBuyer()]){
              $cElementTd->Content($this->cDoContInfo($aConts[$c->iBuyer()]));
            }
            
          }
          else{
            if(!isset($aGroups[$c->iBuyer()])){
              $aGroups[$c->iBuyer()] = cGroup::mUnserialize($this->cDb, $c->iBuyer());
            }
            if($aGroups[$c->iBuyer()]){
              $cElementTd->Content($this->cDoGroupInfo($aGroups[$c->iBuyer()]));
            }
          }
          
          $cElementTr->Content($cElementTd);
          
          //column 5 - tags
          $cElementTd = new cElement('td', false);
          
          
          $aTagIDs = $c->aTagIDs();

          if($aTagViews
          && count($aTagIDs) > 0){
            foreach($aTagIDs as $iTagID){
              $cElementDiv = new cElement('div', false);

              if($aTagViews[$iTagID]->sColor()){
                $cElementDiv->Attribute('style', 'background-color:#'
                . $aTagViews[$iTagID]->sColor() . ';');
              }
                                    
              $cElementDiv->Attribute('class', WTS_PFX . '-table-color');
              $cElementDiv->Content(new cElementBase(htmlentities($aTagViews[$iTagID]->sName())));
              $cElementTd->Content($cElementDiv);
            }
          }
          
          $cElementTr->Content($cElementTd);
         
          $cTable->Row($cElementTr);
        }
      }
      else{$cTable->ColSpan(_('No data found'));}
      
      $cElement->Content($cTable->cRoot());

      $cPage->Main($cElement);
      
      //050. side bar tags
      $aTags = false;
      if(cTag::iUnserialize($this->cDb, $aTags) > 0){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Tags')));
        
        $cElement->Content($cElementCapt);
        
        $cField = new cField();
        
        foreach($aTags as &$cTag){
          $cElementA = new cElement('a', false);

          if($cTag->sColor()){
            $cElementA->Attribute('style', 'background-color:#' . $cTag->sColor() . ';');
          }
          $cElementA->Attribute('href', $sHref
          . sSubAction(WTS_PARAM_ACT_TAG, '{' . $cTag->sSlug() . '}'));
                                
          $cElementA->Attribute('class', WTS_PFX . '-table-color ' . WTS_PFX . '-tag');
          $cElementA->Content(new cElementBase(htmlentities($cTag->sName())));
          $cField->Input($cElementA);
        }
        
        $cElement->Content($cField->cBuild());
        
        $cPage->Aside($cElement);
      }
      
      //060. sidebar - create new or update
      $cElement = new cElement('section', false);
      $cElement->Attribute('class', WTS_PFX . '-aside-section');
      
      $cElementCapt = new cElement('div', false);
      $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
      
      $c = ($cCurContr ? $cCurContr : false);
      
    
      if($c){
        $cElementCapt->Content(new cElementBase(_('Contract Properties')));
        $cElement->Content($cElementCapt);
      }
      else{
        //new contract
        $cElementCapt->Content(new cElementBase(_('Add New Contract')));
        $cElement->Content($cElementCapt);

        //select type
        $cElementForm = new cElement('form', false);
        $cElementForm->Attribute('method', 'get');
        
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Type'), WTS_PARAM_TYPE));
        
        $cSelect = new cSelect(WTS_PARAM_TYPE, WTS_PFX . '-input');
        
        foreach($aTypes as $iKey => $sVal){
          $cSelect->Option($iCurType === $iKey, $iKey, $sVal);
        }
        $cElementS = $cSelect->cRoot();
        $cElementS->Attribute('onchange', 'this.form.submit();');
       
        $cField->Input($cElementS);
        
        $cElementForm->Content($cField->cBuild());
        
        $cElementH = new cElement('input', false);
        $cElementH->Attribute('type', 'hidden');
        $cElementH->Attribute('name', WTS_PARAM_ACTION);
        $cElementH->Attribute('value', WTS_PARAM_ACT_CONTR);
        $cElementForm->Content($cElementH);
        
        $cElement->Content($cElementForm);
      }
      
      $cElementForm = new cElement('form', false);
      $cElementForm->Attribute('action', $sHref
          . sSubAction(WTS_PARAM_EDIT, ($c ? WTS_PARAM_EACT_UPDATE : WTS_PARAM_EACT_CREATE))
          . ($c && $iPage > 1 ? sSubAction(WTS_PARAM_PAGE, $iPage) : '')
          . ($c ? sSubAction(WTS_PARAM_ID, $c->iID()) : sSubAction(WTS_PARAM_TYPE, $iCurType))
          . ($c && $sCurSearch !== false ? sSubAction($sCurSearch, urlencode($mCurSearch)) : ''));
      $cElementForm->Attribute('method', 'post');
      $cElementForm->Attribute('enctype',  'multipart/form-data');
      
      if($c === false){
        $cElementForm->Attribute('autocomplete', 'off');
        $cElementForm->Attribute('name', WTS_PARAM_ACT_CONTR);
        $cElementForm->Attribute('onsubmit', 'return validate()');
        
        //seller
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Seller'), WTS_PARAM_FROM));
        
        $cElementSpan = new cElement('span', false);
        $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
        $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_FROM);
        $cField->Label($cElementSpan);

        $cElementInp = cField::cElementInput(WTS_PARAM_FROM);
        $cElementInp->Attribute('title', _('Seller must be different from Buyer.'));
        if($iCurType === 10 || $iCurType === 20){
          $cElementInp->Attribute('placeholder', 'contact@example.com');
          
          $cElementInp->Attribute('id', WTS_PFX . '-search-box-cnt1');
          $cField->Input($cElementInp);
            
          $cElementUl = new cElement('ul', false);
          $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper-cnt1');
          $cField->Input($cElementUl);
        }
        else{
          $cElementInp->Attribute('placeholder', _('Group'));
          
          $cElementInp->Attribute('id', WTS_PFX . '-search-box-grp1');
          $cField->Input($cElementInp);
            
          $cElementUl = new cElement('ul', false);
          $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper-grp1');
          $cField->Input($cElementUl);
        }
        
        $cElementForm->Content($cField->cBuild());
        
        //buyer
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Buyer'), WTS_PARAM_TO));
        
        $cElementSpan = new cElement('span', false);
        $cElementSpan->Attribute('class', WTS_PFX . '-alert ' . WTS_PFX . '-info2');
        $cElementSpan->Attribute('id', 'id_' . WTS_PARAM_TO);
        $cField->Label($cElementSpan);
        
        $cElementInp = cField::cElementInput(WTS_PARAM_TO);
        $cElementInp->Attribute('title', _('Buyer must be different from Seller.'));
        if($iCurType === 10 || $iCurType === 30){
          $cElementInp->Attribute('placeholder', 'contact@example.com');
          
          $cElementInp->Attribute('id', WTS_PFX . '-search-box-cnt2');
          $cField->Input($cElementInp);
            
          $cElementUl = new cElement('ul', false);
          $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper-cnt2');
          $cField->Input($cElementUl);
        }
        else{
          $cElementInp->Attribute('placeholder', _('Group'));
          $cElementInp->Attribute('id', WTS_PFX . '-search-box-grp2');
          $cField->Input($cElementInp);
            
          $cElementUl = new cElement('ul', false);
          $cElementUl->Attribute('id', WTS_PFX . '-search-advice-wrapper-grp2');
          $cField->Input($cElementUl);
        }
        
        $cElementForm->Content($cField->cBuild());
        
        $sScript = '
function validate(){
  var ret=true;
  if(document.forms[\'' . WTS_PARAM_ACT_CONTR . '\'][\'' . WTS_PARAM_FROM . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_FROM . '\').innerHTML=\'&#9998;\';
    ret=false;
  }
  if(document.forms[\'' . WTS_PARAM_ACT_CONTR . '\'][\'' . WTS_PARAM_TO . '\'].value.length==0){
    document.getElementById(\'id_' . WTS_PARAM_TO . '\').innerHTML=\'&#9998;\';
    ret=false;
  }
  return ret;
}';
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Content(new cElementBase($sScript, false));
        $cPage->Head($cElementS);
        
        //autocomplite script
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Attribute('src', WTS_PATH . 'advice/jquery.min.js');
        $cPage->Head($cElementS);
        
        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Attribute('src', WTS_PATH . 'advice/tsearch.js');
        $cPage->Head($cElementS);
      }
      
      //sbj
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Subject'), WTS_PARAM_SUBJECT));
      
      $cField->Input(cField::cElementInput(WTS_PARAM_SUBJECT
                                        , ($c && $c->sSubject() ? $c->sSubject() : null)));
      
      $cElementForm->Content($cField->cBuild());

      //cost
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Cost'), WTS_PARAM_COST));
      $cField->Label(new cElement('br', false));
      $cElementInput = cField::cElementInput(WTS_PARAM_COST,
           number_format(($c ? $c->fCost() : 0.0), 2, '.', ' ' ));
      $cElementInput->Attribute('style', 'width: 140px;');
          
      $cField->Input($cElementInput);
      
      $cElementB = new cElement('b', false);
      $cElementB->Content(new cElementBase(WTS_CURRENCY));
      $cField->Input($cElementB);
      
      $cElementForm->Content($cField->cBuild());
      
      if($c === false){
        //begin
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('Begin'), WTS_PARAM_BEGIN));
        $cField->Label(new cElement('br', false));
        $cElementInput = cField::cElementInput(WTS_PARAM_BEGIN,
              date(WTS_DT_FMT, strtotime('now')));
        $cElementInput->Attribute('style', 'width: 140px;');
            
        $cElementInput->Attribute('readonly');
        $cElementInput->Attribute('id', 'id_' . WTS_PARAM_BEGIN);
        $cField->Input($cElementInput);
        $cField->Input(cField::cElementCalImg('id_' . WTS_PARAM_BEGIN));
        
        $cElementForm->Content($cField->cBuild());
        
        //end
        $cField = new cField();
        $cField->Label(cField::cElementLabel(_('End'), WTS_PARAM_END));
        $cField->Label(new cElement('br', false));
        $cElementInput = cField::cElementInput(WTS_PARAM_END,
              date(WTS_DT_FMT, strtotime('now')  + (60 * 60 * 24)));
        $cElementInput->Attribute('style', 'width: 140px;');
        $cElementInput->Attribute('readonly');
        $cElementInput->Attribute('id', 'id_' . WTS_PARAM_END);
        $cField->Input($cElementInput);
        $cField->Input(cField::cElementCalImg('id_' . WTS_PARAM_END));
        
        $cElementForm->Content($cField->cBuild());

        $cElementS = new cElement('script', false);
        $cElementS->Attribute('type', 'text/javascript');
        $cElementS->Attribute('src', WTS_PATH . 'datetimepick/datetimepicker_css.js');
        $cPage->Head($cElementS);
      }
        
      //attachment
      $cField = new cField();
      $cField->Label(cField::cElementLabel(_('Attachment')));
      $cField->Label(new cElement('br', false));
      
      $cElementButton = new cElement('button', false);
      $cElementButton->Attribute('onclick', 'add_file();return false;');
      $cElementButton->Content(new cElementBase(_('Add Attachment')));
      
      $cField->Input($cElementButton);
      
      $cElementButton = new cElement('button', false);
      $cElementButton->Attribute('onclick', 'clear_file();return false;');
      $cElementButton->Content(new cElementBase(_('Clear')));
      
      $cField->Input($cElementButton);
      
      $cElementSpan = new cElement('span', false);
      $cElementSpan->Attribute('id', WTS_PFX . '-attachment-size');
      $cField->Input($cElementSpan);
      
      $cElementDiv = new cElement('div', false);
      $cElementDiv->Attribute('id', WTS_PFX . '-attachment-info');
      $cField->Input($cElementDiv);
     
      $cElementForm->Content($cField->cBuild());
          
      $cElementS = new cElement('script', false);
      $cElementS->Attribute('type', 'text/javascript');
      $cElementS->Content(new cElementBase(cField::sAddAttachmentInitScript(), false));
      $cPage->Head($cElementS);
      
      //tags
      $aTagViews = false;
      if($aTags){
        
        $cField = new cField();
        
        if($c){
          $aContracts = array($c);
          cContract::iUnserializeTags($this->cDb, $aContracts, $aTagViews);
        }
      
        foreach($aTags as &$cTag){

          $cElementInput = new cElement('input', false);
          $cElementInput->Attribute('type', 'checkbox'); 
          $cElementInput->Attribute('name', WTS_PARAM_SLUG . '[]'); 
          $cElementInput->Attribute('value', $cTag->sSlug());
          $cElementInput->Attribute('style', 'vertical-align: middle;');
          
          if(isset($aTagViews) && isset($aTagViews[$cTag->iID()])){
            $cElementInput->Attribute('checked');
            unset($aTagViews[$cTag->iID()]);
          }
          
          $cElementLabel = new cElement('label', false);
          $cElementLabel->Attribute('class', WTS_PFX . '-table-color ' . WTS_PFX . '-tag-checkbox');
          
          if($cTag->sColor()){
            $cElementLabel->Attribute('style', 'background-color:#'
                                            . $cTag->sColor() . ';');
          }
          
          $cElementLabel->Content($cElementInput);
          $cElementLabel->Content(new cElementBase(htmlentities($cTag->sName())));
          
          unset($cTag);
          
          $cField->Input($cElementLabel);
        }
        $cElementForm->Content($cField->cBuild());
      }

      //ok
      $cField = new cField(cField::RIGHT);
      if($c){
        $cField->Input(cField::cElementSubmit(_('Reset'), true));
        $cField->Input(cField::cElementSubmit(_('Apply')));
      }
      else{
        $cField->Input(cField::cElementSubmit(_('Create')));
      }
      $cElementForm->Content($cField->cBuild());
      
      $cElement->Content($cElementForm);
      
      $cPage->Aside($cElement);
      
      //070. contract info
      if($cCurContr){
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Contract Info')));
        
        $cElement->Content($cElementCapt);
        
        $cToc = new cToc();
        
        /*
        $cElementInfo = false;
        $iNow = strtotime('now');
        //действующий контракт или нет
        if($cCurContr->iDtBegin() <= $iNow && $cCurContr->iDtEnd() >= $iNow){
          $cElementInfo = cSE::cCreate('span', $cCurContr->iID(), WTS_PFX . '-notify ' . WTS_PFX . '-info2');
          $cElementInfo->Attribute('title', _('Valid'));
        }
        else{
          $cElementInfo = cSE::cCreate('span', $cCurContr->iID(), WTS_PFX . '-alert ' . WTS_PFX . '-info2');
          $cElementInfo->Attribute('title', _('Invalid'));
        }
        $cToc->Child(new cElementBase(_('Contract ID')), $cElementInfo);
        */


        $cToc->Child(new cElementBase(_('Created')), $this->cDoTime($cCurContr->iDtCreated()));
        $cToc->AddLinkForLastChild($this->cDoDate($cCurContr->iDtCreated()));
        
        $cContCr = $bUnsetCr = false;
        if($cCurContr->iCreator() !== $this->cCont->iID()){
          $cContCr = cCont::mUnserialize($this->cDb, $cCurContr->iCreator());
          $bUnsetCr = true;
        }
        else{$cContCr = $this->cCont;}
        
        $cToc->Child(new cElementBase(_('Created by'))
                   , ($cContCr === false ? new cElementBase(_('Unknown')) : $this->cDoContInfo($cContCr)));
                   
        if($cCurContr->iDtUpdated() > 0){
          $cToc->Child(new cElementBase(_('Updated')), $this->cDoTime($cCurContr->iDtUpdated()));
          $cToc->AddLinkForLastChild($this->cDoDate($cCurContr->iDtUpdated()));
          
          $cContUp = $bUnsetUp = false;
          
          if($cCurContr->iUpdater() != $cContCr->iID()){
            if($cCurContr->iUpdater() != $this->cCont->iID()){
              $cContUp = cCont::mUnserialize($this->cDb, $cCurContr->iUpdater());
              $bUnsetUp = true;
            }
            else{$cContUp = $this->cCont;}
          }
          else{$cContUp = $cContCr;}
          
          $cToc->Child(new cElementBase(_('Updated by'))
                     , ($cContUp === false ? new cElementBase(_('Unknown')) : $this->cDoContInfo($cContUp)));
          if($bUnsetUp){unset($cContUp);}
        }
        
        if($bUnsetCr){unset($cContCr);}
        
        //begin - end
        //$cToc->Child(new cElementBase(_('Period')), $this->cDoTime($cCurContr->iDtBegin()));
        //$cToc->AddLinkForLastChild($this->cDoDate($cCurContr->iDtBegin()));
        
        //$cToc->AddLinkForLastChild(new cElementBase('&rarr;', false));
        //$cToc->AddLinkForLastChild($this->cDoTime($cCurContr->iDtEnd()));
        //$cToc->AddLinkForLastChild($this->cDoDate($cCurContr->iDtEnd()));

        //life time
        $iDiff = 0;
        $iDiff = $cCurContr->iDtEnd() - $cCurContr->iDtBegin();
        $cToc->Child(new cElementBase(_('Life Time')), new cElementBase(cString::sTimediffToString($iDiff)));
        
        $cToc->Child(new cElementBase(_('Begin')), $this->cDoTime($cCurContr->iDtBegin()));
        $cToc->AddLinkForLastChild($this->cDoDate($cCurContr->iDtBegin()));
        $cToc->Child(new cElementBase(_('End')), $this->cDoTime($cCurContr->iDtEnd()));
        $cToc->AddLinkForLastChild($this->cDoDate($cCurContr->iDtEnd()));
        
        //products in contact
        cProduct::bAddFilterContract($this->cDb, $c->iID());
        $iProducts = cProduct::iCount($this->cDb);

        $cElementInfo = new cElement('a', false);
        $cElementInfo->Attribute('href', sAction(WTS_PARAM_ACT_PRODUCT)
        . sSubAction(WTS_PARAM_ACT_CONTR, $c->iID()));
        $cElementInfo->Content(new cElementBase($iProducts > 0 ? $iProducts : _('Not set')));
        $cToc->Child(new cElementBase(_('Products')), $cElementInfo);
        
        $cElement->Content($cToc->cRoot());
        
        $cPage->Aside($cElement);
      }
      
      //080. contract attachment
      if($cCurContr
      && $cCurContr->iUnserializeAttach($this->cDb) > 0){
        
        $cElement = new cElement('section', false);
        $cElement->Attribute('class', WTS_PFX . '-aside-section');
        
        $cElementCapt = new cElement('div', false);
        $cElementCapt->Attribute('class', WTS_PFX . '-section-caption');
        $cElementCapt->Content(new cElementBase(_('Contract Attachment')));
        
        $cElement->Content($cElementCapt);

        $cAttachmentView = new cAttachmentView();
        
        $aAttachs = $cCurContr->aAttach();
        foreach($aAttachs as $att){
          $sLink = WTS_PATH . 'attachmentc.php?cid='
          . $cCurContr->iID() . '&aid=' . $att[FN_I_ID];

          $cAttachmentView->Attachment($sLink, $att[FN_S_NM]);

        }

        $cField = new cField();
        $cField->Input($cAttachmentView->cRoot());
          
        $cElement->Content($cField->cBuild());

        $cPage->Aside($cElement);
      }
      
    }
  }
  
  
}
