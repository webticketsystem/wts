<?php
/*
 * gimgupload.php (part of WTS) - get uploaded images for events
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('include/defs.php');
  require_once('include/res.php');
  require_once('include/core/utils/db.php');
  require_once('include/core/utils/mime.php');
  require_once('include/core/cont.php');
  require_once('include/core/upload.php');

  try{
    if(isset($_GET['uid'])){
      if(!session_id())session_start();
      
      $cDb = new cDb(WTS_DB_NAME, WTS_DB_USER, WTS_DB_PASS, WTS_DB_TABLE_PREFIX);
      
      //validate session by guid and timeout
      if(isset($_SESSION[WTS_SESSION_PARAM_LOGIN]) 
      && isset($_SESSION[WTS_SESSION_PARAM_HASH])
      && ($cCont = cCont::mCheckSsn($cDb, $_SESSION[WTS_SESSION_PARAM_LOGIN], $_SESSION[WTS_SESSION_PARAM_HASH], WTS_SESSION_TIMEOUT))
      && $cCont->iType() !== cCont::CSTMR){

        //если вход выполнен - отдадим картинку
        if(($cUpload = cUpload::mUnserialize($cDb, $_GET['uid']))
        && ($sSrc = $cUpload->sValue())){
          header('Content-type: ' . cMIME::sMimeType($cUpload->sName()));
          header('Content-Description: File Transfer');
          header('Content-Disposition: attachment; filename="' . $cUpload->sName() . '"');
          header('Content-Transfer-Encoding: binary');
          header('Cache-Control: private');
          Header('Vary: Content-ID');
          header('Content-Length: ' . strlen($sSrc));
          Header('Content-ID: ' . $_GET['uid']);
          echo $sSrc;
          unset($cUpload);
          unset($sSrc);
        }
        else{header(WTS_HTTP_404);}
        unset($cCont);
      }
      else{header(WTS_HTTP_401);}
      unset($cDb);
    }
  }
  catch (\Exception $e){
    echo 'Exception ' . $e->GetCode() . ': ' . $e->GetMessage() . PHP_EOL . 'In file: ' . basename($e->GetFile()) . PHP_EOL . 'line: ' . $e->GetLine() . PHP_EOL;
    exit;
  }

}
?>
