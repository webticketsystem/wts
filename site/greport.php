<?php
/*
 * greport.php (part of WTS) - get report csv
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('include/defs.php');
  require_once('include/res.php');
  require_once('include/core/utils/db.php');
  require_once('include/core/utils/string.php');
  require_once('include/core/cont.php');
  require_once('include/core/report.php');

  try{
    if(isset($_GET[WTS_PARAM_ID])){
      if(!session_id())session_start();
      
      $cDb = new cDb(WTS_DB_NAME, WTS_DB_USER, WTS_DB_PASS, WTS_DB_TABLE_PREFIX);
      
      //validate session by guid and timeout
      if(isset($_SESSION[WTS_SESSION_PARAM_LOGIN]) 
      && isset($_SESSION[WTS_SESSION_PARAM_HASH])
      && ($cCont = cCont::mCheckSsn($cDb, $_SESSION[WTS_SESSION_PARAM_LOGIN], $_SESSION[WTS_SESSION_PARAM_HASH], WTS_SESSION_TIMEOUT))){
        
        //если вход выполнен - отдадим отчёт
        if($cReport = cReport::mUnserialize($cDb, $_GET[WTS_PARAM_ID])){
          
          if($cReport->iType() === $cCont->iType()){
            
            $sSrc = '- No text report -';
            
            if($sQuery = $cReport->sValue()){
              
              //replace expr
              foreach(cReport::RPL_EXP as $sKey => $sExpr){
                $s = '';
                switch($sKey){

                  case 'CONT_ID': $s = (string)$cCont->iID();
                  break;
                  case 'GROUP_ID':
                  if($cCont->iGroup() > 0){$s = (string)$cCont->iGroup();}
                  break;
                  case 'QUEUE_ID':
                  if($cCont->iQueue() > 0){$s = (string)$cCont->iQueue();}
                  break;
                  default: break;
                }
                $sQuery = str_replace($sExpr, $s, $sQuery);
              }
              
              //fill file
              $cDb->QueryRes($sQuery);
              if($aHdr = $cDb->aHeader()){
                
                $sSrc = implode(';', $aHdr);
                $sSrc .= cString::EOL;
                
                if($cDb->iRowCount() > 0){
                  while($aRow = $cDb->aRow()){
                    
                    $sSrc .= implode(';', $aRow);
                    $sSrc .= cString::EOL;
                  }
                  $cDb->FreeResult();
                }
              }
              
            }
            
            header('Content-type: text/csv');
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename="' . $cReport->sName() . '.csv"');
            header('Content-Transfer-Encoding: binary');
            header('Cache-Control: private');
            Header('Vary: Content-ID');
            header('Content-Length: ' . strlen($sSrc));
            Header('Content-ID: ' . md5($cReport->sName()));
            echo $sSrc;
            
            unset($sSrc);
          }
          else{header(WTS_HTTP_403);}
          unset($cReport);
        }
        else{header(WTS_HTTP_404);}
        unset($cCont);
      }
      else{header(WTS_HTTP_401);}
      unset($cDb);
    }
  }
  catch (\Exception $e){
    echo 'Exception ' . $e->GetCode() . ': ' . $e->GetMessage() . PHP_EOL . 'In file: ' . basename($e->GetFile()) . PHP_EOL . 'line: ' . $e->GetLine() . PHP_EOL;
    exit;
  }

}
?>
