<?php
/*
 * backup.php (part of WTS) - for backup database
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
/*
 * 
 * for restore use
 * gunzip < /path/to/backup/file.sql.gz | mysql -u USER -pPASSWORD DATABASE
 * or
 * mysql -u USER -pPASSWORD DATABASE < /path/to/backup/file.sql
 * 
 * check WTS_DB_SCHEME before restore!
 * 
 */
 
namespace wts{

  require_once('../site/include/defs.php');
  require_once('../site/include/core/utils/string.php');


  $aMap['-d']['lkey'] = '--mysql-dump-path';
  $aMap['-d']['sum']  = 'path to mysqldump'
  . cString::EOL . '(for example - \'/usr/bin\')';
  $aMap['-d']['val']  = null;

  $aMap['-a']['lkey'] = '--backup-path'; //archive
  $aMap['-a']['sum']  = 'path to backup files'
  . cString::EOL . '(must exist)';
  $aMap['-a']['val']  = null;

  if($argc > 1){ 
    for ($i = 1; $i < $argc; $i++){
      $sKey = $argv[$i];
      $sValue = null;
      if(isset($argv[$i + 1])){
        $i++;
        $sValue = $argv[$i];
      }
      else{
        if($sKey == '--help'){
          echo 'Usage: php -f backup.php -- {[PARAMETER] [VALUE]}...' . cString::EOL;
          echo 'WTS backup script.'. cString::EOL;
          echo '* If [VALUE] is not given it\'s asked from the tty.' . cString::EOL . cString::EOL;
          echo 'Parameters:' . cString::EOL;
          foreach($aMap as $key => &$a){
            $s = str_replace(cString::EOL, cString::EOL . '                             ', $a['sum']);
            echo '  ' . $key . ', ' . str_pad($a['lkey'], 23) . $s . cString::EOL;
          }
          echo '      ' . str_pad('--help', 23) . 'without [VALUE] display this help and exit' . cString::EOL . cString::EOL;
          echo 'Report backup.php bugs to webticketsystem@gmail.com' . cString::EOL;
          exit;
        }
        
        break;
      }

      foreach($aMap as $key => &$a){
        if($key == $sKey || $a['lkey'] == $sKey){
          switch($key){
            case '-a': //len > 1 == not for /
              if(strlen($sValue) > 1 && file_exists($sValue)){$a['val'] = $sValue;}
            break;
            case '-d':
              $sDump = $sValue . '/mysqldump';
              if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){$sDump .= '.exe';}
              if(file_exists($sDump)){$a['val'] = $sValue;}
            break;
            default: break;
          }
        }
      }
    }
  }

  echo 'backup.php: WTS backup script' . cString::EOL;
  echo 'Try \'php - f backup.php -- --help\' for more information.' . cString::EOL. cString::EOL;

  while(!$aMap['-d']['val']){
    echo 'Enter ' . $aMap['-d']['sum'] . ': ';
    $aMap['-d']['val'] = trim(fgets(STDIN));
    $sValue = $aMap['-d']['val'] . '/mysqldump';
    if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){$sValue .= '.exe';}
    if(!file_exists($sValue)){
      echo 'Bad mysqldump path. Please Try again.' . cString::EOL;
      $aMap['-d']['val'] = null;
    }
  }

  while(!$aMap['-a']['val']){
    echo 'Enter ' . $aMap['-a']['sum'] . ': ';
    $aMap['-a']['val'] = trim(fgets(STDIN));
    if(strlen($sValue) < 2 || !file_exists($aMap['-a']['val'])){
      echo 'Bad path. Please Try again.' . cString::EOL;
      $aMap['-a']['val'] = null;
    }
  }

  $aUserName = explode("@", WTS_DB_USER);
  $aUserName[0] = strtolower($aUserName[0]);

  echo 'Backup will use next parameters:'. cString::EOL;
  foreach($aMap as &$a){
    echo str_pad($a['lkey'], 23) . $a['val'] . cString::EOL;
  }

  if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
    exec($aMap['-d']['val'] . '\\mysqldump.exe -u ' . $aUserName[0] . ' -p' . WTS_DB_PASS
    . ' --quick ' . WTS_DB_NAME . ' > "' . $aMap['-a']['val']. '\\'
    . WTS_DB_NAME . '_' . WTS_DB_SCHEME. '_' . date(WTS_DT_BACKUP_FMT, strtotime('now')) . '.sql"');
    
    //http://www.sql.ru/forum/631316/nuzhen-skrip-udaleniya-ustarevshih-faylov-po-date
    exec('FORFILES -p "' . $aMap['-a']['val'] . '" -m *.sql -d -' . WTS_BACKUP_LC . ' -c "CMD /C if @isdir==FALSE DEL /f /q @path"');
  }
  else{
    exec($aMap['-d']['val'] . '/mysqldump -u ' . $aUserName[0] . ' -p' . WTS_DB_PASS
    . ' --quick ' . WTS_DB_NAME . ' | gzip > \'' . $aMap['-a']['val']. '/'
    . WTS_DB_NAME . '_' . WTS_DB_SCHEME. '_' . date(WTS_DT_BACKUP_FMT, strtotime('now')) . '.sql.gz\'');
    exec('find ' . $aMap['-a']['val'] . ' -type f -mtime +' . WTS_BACKUP_LC .' | xargs rm -f');
  }

}
?>
