<?php
/*
 * deloldssn.php (part of WTS) - clean up old sessions
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('../site/include/defs.php');
  require_once('../site/include/core/utils/db.php');
  require_once('../site/include/core/cont.php');
  require_once('../site/include/core/log.php');

  try{
    $cDb = new cDb(WTS_DB_NAME, WTS_DB_USER, WTS_DB_PASS, WTS_DB_TABLE_PREFIX);
    
    cCont::DeleteOldSsn($cDb, WTS_SESSION_TIMEOUT);
    
    if(defined('WTS_DEBUG_LOG')){
      $cLog = new cLog(cLog::MSG);
      $cLog->Value('Cron job: clean up old sessions');
      $cLog->bSerialize($cDb);
      unset($cLog);
    }
    
    unset($cDb);
  }
  catch (\Exception $e){
    echo 'Exception ' . $e->GetCode() . ': ' . $e->GetMessage() . cString::EOL . 'In file: ' . basename($e->GetFile()) . cString::EOL . 'line: ' . $e->GetLine() . cString::EOL;
    exit;
  }

}
?>
