<?php
/*
 * getsendmail.php (part of WTS) - email worker 
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('../site/include/defs.php');
  require_once('../site/include/core/utils/db.php');
  require_once('../site/include/core/utils/string.php');
  require_once('../site/include/core/utils/mime.php');
  require_once('../site/include/core/utils/hypertext.php');
  require_once('../site/include/core/event.php');
  require_once('../site/include/core/chain.php');
  require_once('../site/include/core/queue.php');
  require_once('../site/include/core/log.php');

  /*-- IMAP helper --****************************************************/

  class cImapMsg{
    protected $cEvent;
    protected $sTextBody;
    protected $sHtmlBody;
    public function __construct(cEvent &$cEvent){
      $this->cEvent = $cEvent;
      $this->sTextBody = false;
      $this->sHtmlBody = false;
    }
    public function ParseMsg(&$cImapStream, $iMsgID){
      $aStructure = imap_fetchstructure($cImapStream, $iMsgID);

      if(!isset($aStructure->parts)){
        $this->ParsePart($cImapStream, $iMsgID, $aStructure, 0);
      }
      else{
        foreach ($aStructure->parts as $sPart0 => $cPart){
          $this->ParsePart($cImapStream, $iMsgID, $cPart, $sPart0 + 1);
        }
      }
      //если есть HTML - добавляем только его!
      if($this->sHtmlBody){
        $this->cEvent->AddBody($this->sHtmlBody);
        $this->cEvent->GenPreview(WTS_ENC, WTS_EVENT_PREVIEW_LENGHT);
      }
      else{
        if($this->sTextBody){
          $this->cEvent->AddBody(cElement::sFromPlainText($this->sTextBody));
          $this->cEvent->GenPreview(WTS_ENC, WTS_EVENT_PREVIEW_LENGHT);
        }
      }
    }
    
    public function ParsePart(&$cImapStream, $iMsgID, $cPart, $sPart){
      // $sPart = '1', '2', '2.1', '2.1.3', etc. for multipart, 0 if simple
      
      //decode DATA
      $sSrc = ($sPart ? imap_fetchbody($cImapStream, $iMsgID ,$sPart)//multi
                      : imap_body($cImapStream, $iMsgID));//simple
      // Any part may be encoded, even plain text messages, so check everything.
      if($cPart->encoding     == 4){$sSrc = quoted_printable_decode($sSrc);}//imap_qprint
      elseif($cPart->encoding == 3){$sSrc = base64_decode($sSrc);}//imap_base64
      
      //get all PARAMETERS
      $aParams = false;
      if($cPart->ifparameters){
        foreach ($cPart->parameters as $c){
          $aParams[strtolower($c->attribute)] = $c->value;
        }
      }
      if($cPart->ifdparameters){
        foreach ($cPart->dparameters as $c){
          $aParams[strtolower($c->attribute)] = $c->value;
        }
      }
      
      if($cPart->type == 0
      && $aParams
      && isset($aParams['charset'])
      && strlen($aParams['charset']) > 2){
        $sEnc = strtoupper($aParams['charset']);
        if($sEnc != WTS_ENC){
          $sSrc = mb_convert_encoding($sSrc, WTS_ENC, ('KS_C_5601-1987' != $sEnc ? $sEnc : 'EUC-KR'));
          //если это html - сменить кодировку в теле
          if($cPart->ifsubtype && strtolower($cPart->subtype) == 'html'){
            $sSrc = str_ireplace('charset="' . $aParams['charset'] . '"', 'charset="' . WTS_ENC . '"', $sSrc);
          }
        }
      }
      //ATTACHMENT
      //Any part with a filename or with an id is an attachment,
      //so an attached text file (type 0) is not mistaken as the message.
      if($aParams
      && $cPart->type != 2
      && (isset($aParams['filename'])
      || isset($aParams['name'])
      || ($cPart->ifid && $cPart->type != 0))){//skip eml
        
        $sName = WTS_DB_TABLE_PREFIX . cString::sRand(6);

        //filename may be encoded, so see imap_mime_header_decode() ? cid to ?
        if(isset($aParams['filename']) && strlen($aParams['filename']) > 0){
          $sName = cMIME::sDecodeMimeHeaderStr($aParams['filename'], WTS_ENC);
        }else{
          if(isset($aParams['name']) && strlen($aParams['name']) > 0){
            $sName = cMIME::sDecodeMimeHeaderStr($aParams['name'], WTS_ENC);
          }
        }

        if((false === stripos($sName, '.')) && $cPart->ifsubtype == 1){
          $sName .= '.' . cMIME::sFileExt($cPart->subtype);
        }
        if(!$this->cEvent->bAddAttach($sSrc, $sName, ($cPart->ifid ? $cPart->id : null))){
          //name is not unique
          $this->cEvent->bAddAttach($sSrc, WTS_DB_TABLE_PREFIX . cString::sRand(6) . $sName, ($cPart->ifid ? $cPart->id : null));
        }
        unset($sSrc);
        $sSrc = false;
      }
      
      //TEXT
      if($cPart->type == 0 && $sSrc){
        //Messages may be split in different parts because of inline attachments,
        //so append parts together with blank row.
        if(strtolower($cPart->subtype) == 'plain'){
          if($this->sTextBody){$this->sTextBody .= "\n" . trim($sSrc);}
          else{$this->sTextBody = trim($sSrc);}
        }
        else{//'html'
          if($this->sHtmlBody){$this->sHtmlBody .= '<br>' . trim($sSrc);}
          else{$this->sHtmlBody = trim($sSrc);}
        }
        unset($sSrc);
        $sSrc = false;
      }
      //EMBEDDED MESSAGE
      elseif($cPart->type == 2 && $sSrc){
        $this->cEvent->bAddAttach($sSrc, WTS_DB_TABLE_PREFIX . cString::sRand(6) . '.eml');
        unset($sSrc);
        $sSrc = false;
      }
      //SUBPART RECURSION
      if(isset($cPart->parts)){
        foreach($cPart->parts as $sPart0 => $cPart2){
          $this->ParsePart($cImapStream, $iMsgID, $cPart2, $sPart . '.' . ($sPart0 + 1));// 1.2, 1.2.1, etc.
        }
      }
    }
  }


  /*-- IMAP --***********************************************************/
  class cImap{
    //20 emails for one pass 
    public function Load(cDb &$cDb, cQueue &$cQueue){
      $iCount = 0;

      $sConn = $cQueue->sHost(cQueue::IMAP)
      . ':' . $cQueue->iPort(cQueue::IMAP) . '/imap';
      if($cQueue->iEncryption(cQueue::IMAP)){
        $sConn .= ($cQueue->iEncryption(cQueue::IMAP) == cQueue::ENC_SSL ? '/ssl' : '/tls');
        $sConn .= '/novalidate-cert';
      }
      
      $cImapStream = imap_open('{' . $sConn . '}INBOX', $cQueue->sLogin_(cQueue::IMAP), $cQueue->sPasswd(cQueue::IMAP));
      
      if($cImapStream){
        if($aIDs = imap_search($cImapStream, 'UNSEEN' /*'ALL'*/)){
          foreach($aIDs as $iID){
            //get size
            $aHeader = imap_headerinfo($cImapStream, $iID);
            if($aHeader->Size >= WTS_MAX_PKG_SIZE){
              $cLog = new cLog(cLog::ERR);
              $cLog->Value('Can not load message ' . $iID
              . ' from ' . $cQueue->sHost(cQueue::IMAP) . ' as ' . $cQueue->sLogin_(cQueue::IMAP)
              . '. Message size ' . cString::sBytesToString($aHeader->Size));
              $cLog->bSerialize($cDb);
              unset($cLog);
              continue;//so BIG for us
            }
            //parse header
            $aHeader = imap_rfc822_parse_headers(imap_fetchheader($cImapStream, $iID));
            
            $cEvent = new cEvent(cEvent::IN_EMAIL, $cQueue->iID());
           
            if(isset($aHeader->date)){$cEvent->DtBegin(strtotime($aHeader->date));}
            else{
              if(isset($aHeader->Date)){$cEvent->DtBegin(strtotime($aHeader->Date));}
            }
            //время забора письма
            $cEvent->DtEnd(strtotime('now'));

            if(isset($aHeader->subject)){
              $cEvent->Subject(cMIME::sDecodeMimeHeaderStr($aHeader->subject, WTS_ENC));

              if((WTS_SBJ_AUTOBIND == 'true')
              && ($iChainID = $cEvent->iExpectedChain(WTS_SBJ_PREFIX))
              && ($cChain = cChain::mUnserialize($cDb, $iChainID))){
                $cEvent->Chain($cChain->iID());
                //откроем цепочку
                if($cChain->bClosed()){
                  $cChain->Open();
                  $cChain->bSerialize($cDb);
                }
                unset($cChain);
              }
            }
            if(isset($aHeader->from)){
              $cEvent->From($aHeader->from[0]->mailbox . '@' . $aHeader->from[0]->host);
            }
            if(isset($aHeader->to)){
              foreach($aHeader->to as $sTo){$cEvent->AddTo($sTo->mailbox . '@' . $sTo->host);}
            }
            if(isset($aHeader->cc)){
              foreach($aHeader->cc as $sCc){$cEvent->AddCc($sCc->mailbox . '@' . $sCc->host);}
            }
            
            $cImapMsg = new cImapMsg($cEvent);
            $cImapMsg->ParseMsg($cImapStream, $iID);
            
            if($cEvent->bSerialize($cDb, WTS_EVENT_THUMB_SIDE)){
              //пометим, что прочли
              imap_setflag_full($cImapStream, $iID, '\Seen');
              //пометим для удаления
              imap_delete($cImapStream, $iID);
              $iCount++;
            }
            unset($cEvent);
            unset($cImapMsg);
            if($iCount === 20){break;}
          }
          //write to log
          if($iCount != count($aIDs)){
            $cLog = new cLog(cLog::WARN);
            $cLog->Value('Loaded ' . $iCount . ' of ' . count($aIDs) . ' emails from imap mail server: '
            . $cQueue->sHost(cQueue::IMAP) . ' for user: ' . $cQueue->sLogin(cQueue::IMAP));
            $cLog->bSerialize($cDb);
            unset($cLog);
          }
          else{
            if(defined('WTS_DEBUG_LOG')){
              $cLog = new cLog();
              $cLog->Value('Loaded ' . $iCount . ' emails from imap mail server: '
              . $cQueue->sHost(cQueue::IMAP) . ' for user: ' . $cQueue->sLogin(cQueue::IMAP));
              $cLog->bSerialize($cDb);
              unset($cLog);
            }
          }
        }//if($mails)

        //удалим все помеченные для удаления и закроем соединение
        imap_close($cImapStream, CL_EXPUNGE);
      }
      else{
        $cLog = new cLog(cLog::ERR);
        if($aImapErr = imap_errors()){
          foreach($aImapErr as $e){
            $cLog->Value('Imap error: ' . $e);
            $cLog->bSerialize($cDb);
          }
        }
        $cLog->Value('Connection failed by imap to mail server: '
        . $cQueue->sHost(cQueue::IMAP) . ' as user: ' . $cQueue->sLogin(cQueue::IMAP));
        $cLog->bSerialize($cDb);
        unset($cLog);
      }
    }
  }

  /*-- SMTP helper --****************************************************/
  class cMimeMsg{
    protected $CRLF;
    protected $cEvent;
    protected $iChuck;
    protected $sUID;
    
    public function __construct($CRLF, cEvent &$cEvent){
      $this->CRLF   = $CRLF;
      $this->cEvent = $cEvent;
      $this->iChuck = 76;//@see php chunk_split
      $this->sUID   = cString::sRand(6) . '-' . WTS_DB_NAME . '-' . cString::sRand(6);
    }
    
    protected function sPart($iLevel){
      return '==b' . $iLevel . '-' . $this->sUID .  '-b' . $iLevel . '==';
    }
    
    protected function sBeginPart($iLevel){
      return '--' . $this->sPart($iLevel);
    }
    
    protected function sEndPart($iLevel){
      return '--' . $this->sPart($iLevel) . '--';
    }
    
    protected function AddMash(&$sSrc, &$aMimeMes, $sType = 'base64'){
      switch($sType){
        case 'base64':
          $aMimeMes[] = 'Content-Transfer-Encoding: ' . $sType . $this->CRLF . $this->CRLF;
          $sSrc = base64_encode($sSrc);
          
          if($a = str_split($sSrc, $this->iChuck)){
            foreach($a as $key => $s){
              $aMimeMes[] = $s . $this->CRLF;
              unset($a[$key]);
            }
          }
        break;
        default: break;
      }
    }
    
    protected function AddTextPart(&$sTextBody, &$aMimeMes){
      $aMimeMes[] = 'Content-Type: text/plain; charset="utf-8"' . $this->CRLF;
      $this->AddMash($sTextBody, $aMimeMes, 'base64');
    }
    
    protected function AddHtmlPart(&$sHtmlBody, &$aMimeMes){
      $aMimeMes[] = 'Content-Type: text/html; charset="utf-8"' . $this->CRLF;
      $this->AddMash($sHtmlBody, $aMimeMes, 'base64');
    }
    
    protected function bAddAttachPart(cDb &$cDb, &$aAttach, &$aMimeMes){
      if($sAttachBody = $this->cEvent->mUnserializeAttach($cDb, $aAttach[FN_I_ID])){
        $aMimeMes[] = 'Content-Type: ' . cMIME::sMimeType($aAttach[FN_S_NM]) . $this->CRLF;
        
        if($aAttach[FN_S_CID]){//inline
          $aMimeMes[] = 'Content-ID: <' . $aAttach[FN_S_CID] . '>' . $this->CRLF;
          $aMimeMes[] = 'Content-Disposition: inline;' . $this->CRLF;
        }
        else{
          $aMimeMes[] = 'Content-Disposition: attachment;' . $this->CRLF;
        }
        
        $sBase64Name = base64_encode($aAttach[FN_S_NM]);
        $aMimeMes[] = "\t" . 'filename="=?utf-8?b?'
        . substr($sBase64Name, 0, $this->iChuck) . '?="' . $this->CRLF;
        
        for($i = $this->iChuck; $i < strlen($sBase64Name); $i += $this->iChuck){
          $aMimeMes[] = "\t" . '=?utf-8?b?'
          . substr($sBase64Name, $i, $this->iChuck) . '?=' . $this->CRLF;
        }
        $this->AddMash($sAttachBody, $aMimeMes, 'base64');
        return true;
      }
      return false;
    }
    
    public function bComposeMsg(cDb &$cDb, &$aMimeMes){
      $bRet = true;
      //each string must contain less than 1000 symbols
      //if data have long headers - it's must be break by 997 and start by "\t"
      if(!$this->cEvent->sFrom()){$bRet = false;}
      else{$aMimeMes[] = 'From: <' . $this->cEvent->sFrom() . '>' . $this->CRLF;}
      $aMimeMes[] = 'Date: ' . date(DATE_RFC822, $this->cEvent->iDtEnd()) . $this->CRLF;
      if($this->cEvent->iUnserializeAddr($cDb) > 0){
        $aAddr = $this->cEvent->aTo();
        if(count($aAddr) > 0){
          $sRcp = false;
          foreach($aAddr as $sAddr){
            if($sRcp){$sRcp .= ', <' . $sAddr;}
            else{$sRcp = 'To: <' . $sAddr;}
            $sRcp .= '>';
            if(strlen($sRcp) >= $this->iChuck){
              $aMimeMes[] = $sRcp . $this->CRLF;
              $sRcp = "\t";
            }
          }
          if($sRcp && strlen($sRcp) > 3){$aMimeMes[] = $sRcp . $this->CRLF;}
        }
        else{$bRet = false;}
        $aAddr = $this->cEvent->aCc();
        if(count($aAddr) > 0){
          $sRcp = false;
          foreach($aAddr as $sAddr){
            if($sRcp){$sRcp .= ', <' . $sAddr;}
            else{$sRcp = 'Cc: <' . $sAddr;}
            $sRcp .= '>';
            if(strlen($sRcp) >= $this->iChuck){
              $aMimeMes[] = $sRcp . $this->CRLF;
              $sRcp = "\t";
            }
          }
          if($sRcp && strlen($sRcp) > 3){$aMimeMes[] = $sRcp . $this->CRLF;}
        }
        $aAddr = $this->cEvent->aBcc();
        if(count($aAddr) > 0){
          $sRcp = false;
          foreach($aAddr as $sAddr){
            if($sRcp){$sRcp .= ', <' . $sAddr;}
            else{$sRcp = 'Bcc: <' . $sAddr;}
            $sRcp .= '>';
            if(strlen($sRcp) >= $this->iChuck){
              $aMimeMes[] = $sRcp . $this->CRLF;
              $sRcp = "\t";
            }
          }
          if($sRcp && strlen($sRcp) > 3){$aMimeMes[] = $sRcp . $this->CRLF;}
        }
      }
      else{$bRet = false;}
      
      if($sSbj = $this->cEvent->sSubject()){
        $sBase64Sbj = base64_encode($sSbj);
        $aMimeMes[] = 'Subject: =?utf-8?b?'
        . substr($sBase64Sbj, 0, $this->iChuck) . '?=' . $this->CRLF;
        for($i = $this->iChuck; $i < strlen($sBase64Sbj); $i += $this->iChuck){
          $aMimeMes[] =  "\t" . '=?utf-8?b?'
          . substr($sBase64Sbj, $i, $this->iChuck) . '?=' . $this->CRLF;
        }
      }
      $aMimeMes[] = 'MIME-Version: 1.0' . $this->CRLF;
      $aMimeMes[] = 'X-Mailer: WTS v ' . WTS_DB_SCHEME . $this->CRLF;

      $sTextBody = $this->cEvent->mUnserializeBody($cDb, true);
      $sHtmlBody = $this->cEvent->mUnserializeBody($cDb);
      $aAttachs = $aRelated = $bMixed = false;
      if($this->cEvent->iUnserializeAttach($cDb) > 0){
        $aAttachs = $this->cEvent->aAttach();
        $bMixed   = true;
        
        foreach($aAttachs as $key => &$a){
          if($a[FN_S_CID]){
            if($sHtmlBody){
              $aRelated[] = $a;
              unset($aAttachs[$key]);
            }//иначе сложим как простое вложение
            else{$a[FN_S_CID] = false;}
          }
        }
      }

      if($bMixed
      && $aRelated
      && count($aAttachs) === 0){
        $bMixed = false;//все картинки в теле
      }
      
      if($bMixed){
        $aMimeMes[] = 'Content-Type: multipart/mixed;' . $this->CRLF;
        $aMimeMes[] = "\t" . 'boundary="' . $this->sPart(1) . '"' . $this->CRLF . $this->CRLF;
        $aMimeMes[] = $this->sBeginPart(1) . $this->CRLF;
      }
      //тело
      if($sTextBody && $sHtmlBody){//alternative
        $aMimeMes[] = 'Content-Type: multipart/alternative;' . $this->CRLF;
        $aMimeMes[] =  "\t" . 'boundary="' . $this->sPart(2) . '"' . $this->CRLF . $this->CRLF;
        $aMimeMes[] = $this->sBeginPart(2) . $this->CRLF;
        //text
        $this->AddTextPart($sTextBody, $aMimeMes);
        $aMimeMes[] = $this->sBeginPart(2) . $this->CRLF;
        //html
        if($aRelated){
          $aMimeMes[] = 'Content-Type: multipart/related;' . $this->CRLF;
          $aMimeMes[] = "\t" . 'boundary="' . $this->sPart(3) . '"' . $this->CRLF . $this->CRLF;
          $aMimeMes[] = $this->sBeginPart(3) . $this->CRLF;
          $this->AddHtmlPart($sHtmlBody, $aMimeMes);
          foreach($aRelated as &$a){
            $aMimeMes[] = $this->sBeginPart(3) . $this->CRLF;
            if(!$this->bAddAttachPart($cDb, $a, $aMimeMes)){$bRet = false;}
          }
          $aMimeMes[] = $this->sEndPart(3) . $this->CRLF;
        }
        else{$this->AddHtmlPart($sHtmlBody, $aMimeMes);}
        $aMimeMes[] = $this->sEndPart(2) . $this->CRLF;
      }
      else{//добавить либо текст, либо html
        if($sHtmlBody){
          if($aRelated){
            $aMimeMes[] = 'Content-Type: multipart/related;' . $this->CRLF;
            $aMimeMes[] = "\t" . 'boundary="' . $this->sPart(3) . '"' . $this->CRLF . $this->CRLF;
            $aMimeMes[] = $this->sBeginPart(3) . $this->CRLF;
            $this->AddHtmlPart($sHtmlBody, $aMimeMes);
            foreach($aRelated as &$a){
              $aMimeMes[] = $this->sBeginPart(3) . $this->CRLF;
              if(!$this->bAddAttachPart($cDb, $a, $aMimeMes)){$bRet = false;}
            }
            $aMimeMes[] = $this->sEndPart(3) . $this->CRLF;
          }
          else{$this->AddHtmlPart($sHtmlBody, $aMimeMes);}
        }
        else{
          if(!$sTextBody){$sTextBody = WTS_EVENT_NOTEXT;}
          $this->AddTextPart($sTextBody, $aMimeMes);
        }
      }
      
      if($bMixed){
        foreach($aAttachs as &$a){//есть тело, добавим вложения
          $aMimeMes[] = $this->sBeginPart(1) . $this->CRLF;
          if(!$this->bAddAttachPart($cDb, $a, $aMimeMes)){$bRet = false;}
        }
        $aMimeMes[] = $this->sEndPart(1) . $this->CRLF;
      }
      //print_r($aMimeMes);
      //return false;
      return $bRet;
    }
  }

  /*-- SMTP --***********************************************************/
  class cSmtp{
    //as prototype https://github.com/Synchro/PHPMailer
    protected $CRLF;
    public function __construct(){
      $this->CRLF = "\r\n";
    }
    
    protected function sReadAnswer(&$cSmtpStream){
      $sAnswer = '';
      while($s = fgets($cSmtpStream, 515)){//why 515?
        $sAnswer .= $s;
        // if 4th character is a space, we are done reading, break the loop
        if(substr($s, 3, 1) == ' '){break;}
      }
      return $sAnswer;
    }
    
    protected function bHelo(&$cSmtpStream){
      $bSendHello = false;
      
      fputs($cSmtpStream, 'EHLO ' . php_uname('n') . $this->CRLF);
      $sAnswer = $this->sReadAnswer($cSmtpStream);
      $iCode   = substr($sAnswer, 0, 3);

      if($iCode == 250){$bSendHello = true;}
      else{
        fputs($cSmtpStream, 'HELO ' . php_uname('n') . $this->CRLF);
        $sAnswer = $this->sReadAnswer($cSmtpStream);
        $iCode   = substr($sAnswer, 0, 3);

        if($iCode == 250){$bSendHello = true;}
      }
      return $bSendHello;
    }
    
    protected function bAuthenticate(&$cSmtpStream, $sUser, $sPassw){

      fputs($cSmtpStream, 'AUTH LOGIN' . $this->CRLF);
      $sAnswer = $this->sReadAnswer($cSmtpStream);
      $iCode   = substr($sAnswer, 0, 3);
      if($iCode != 334){
        return false;
      }
      // Send encoded username
      fputs($cSmtpStream, base64_encode($sUser) . $this->CRLF);
      $sAnswer = $this->sReadAnswer($cSmtpStream);
      $iCode   = substr($sAnswer, 0, 3);
      if($iCode != 334){
        return false;
      }
      // Send encoded password
      fputs($cSmtpStream, base64_encode($sPassw) . $this->CRLF);
      $sAnswer = $this->sReadAnswer($cSmtpStream);
      $iCode   = substr($sAnswer, 0, 3);
      if($iCode != 235){
        return false;
      }
      return true;
    }
    
    protected function bQuit(&$cSmtpStream){
      //set bye;
      fputs($cSmtpStream, 'QUIT' . $this->CRLF);
      $sAnswer = $this->sReadAnswer($cSmtpStream);
      $iCode   = substr($sAnswer, 0, 3);
      if($iCode != 221){
        return false;
      }
      return true;
    }
    
    protected function bMail(&$cSmtpStream, $sFrom){
      fputs($cSmtpStream, 'MAIL FROM:<' . $sFrom . '>' . $this->CRLF);
      $sAnswer = $this->sReadAnswer($cSmtpStream);
      $iCode   = substr($sAnswer, 0, 3);
      if($iCode != 250){
        return false;
      }
      return true;
    }
    
    protected function bRecipient(&$cSmtpStream, $sTo){
      fputs($cSmtpStream, 'RCPT TO:<' . $sTo . '>' . $this->CRLF);
      $sAnswer = $this->sReadAnswer($cSmtpStream);
      $iCode   = substr($sAnswer, 0, 3);
      if($iCode != 250 && $iCode != 251){
        return false;
      }
      return true;
    }
    
    protected function bData(&$cSmtpStream, &$aData){//aData - array of strings that has max lenght 1000 symbols
      fputs($cSmtpStream, 'DATA' . $this->CRLF);

      $sAnswer = $this->sReadAnswer($cSmtpStream);
      $iCode   = substr($sAnswer, 0, 3);
      if($iCode != 354){
        return false;
      }
      foreach($aData as &$sData){
        fputs($cSmtpStream, $sData);
      }
      // message data has been sent
      fputs($cSmtpStream, $this->CRLF . '.' . $this->CRLF);
      $sAnswer = $this->sReadAnswer($cSmtpStream);
      $iCode = substr($sAnswer, 0, 3);
      if($iCode != 250){
        return false;
      }
      return true;
    }
    
    protected function bReset(&$cSmtpStream){
      fputs($cSmtpStream, "RSET" . $this->CRLF);
      $sAnswer = $this->sReadAnswer($cSmtpStream);
      $iCode   = substr($sAnswer, 0, 3);
      if($iCode != 250){
        return false;
      }
      return true;
    }
    
    //20 emails for one pass 
    public function Send(cDb &$cDb, cQueue &$cQueue){
      $aEvents = false;
      
      cEvent::ClearFilters();
      cEvent::bAddFilterQueue($cQueue->iID());
      cEvent::bAddFilterType(cEvent::OUT_EMAIL);
      cEvent::bAddFilterProcessed(false);
      if(defined('WTS_EVENT_DELAY') && (int)WTS_EVENT_DELAY > 0){
        cEvent::bAddFilterEnded(WTS_EVENT_DELAY);
      }
      
      if(cEvent::iCount($cDb) > 0
      && ($iRows = cEvent::iUnserialize($cDb, 1, 20, $aEvents)) > 0){
        //open SMTP
        $iError = false;
        $sError = false;
        
        $sConn = false;
        if($cQueue->iEncryption(cQueue::SMTP)){
          $sConn  = ($cQueue->iEncryption(cQueue::SMTP) == cQueue::ENC_SSL ? 'ssl://' : 'tls://');
          $sConn .= $cQueue->sHost(cQueue::SMTP);
        }
        else{$sConn = $cQueue->sHost(cQueue::SMTP);}

        if($cSmtpStream = fsockopen($sConn, $cQueue->iPort(cQueue::SMTP), $iError, $sError)){
          //get any announcement
          $sAnswer = $this->sReadAnswer($cSmtpStream);

          if($this->bHelo($cSmtpStream)
          && $this->bAuthenticate($cSmtpStream, $cQueue->sLogin_(cQueue::SMTP), $cQueue->sPasswd(cQueue::SMTP))){
            $iCount = 0;
            foreach ($aEvents as $key => &$cEvent){
              //create Email
              $aMimeMes = false;
              //create mime from event
              $cMimeMsg = new cMimeMsg($this->CRLF, $cEvent);
              if(!$cMimeMsg->bComposeMsg($cDb, $aMimeMes)){
                $cLog = new cLog(cLog::ERR);
                $cLog->Value('Can not compose event ' . $cEvent->iID()
                . ' for send.');
                $cLog->bSerialize($cDb);
                unset($cLog);
                continue;
              }
              unset($cMimeMsg);

              //start send
              if(!$this->bMail($cSmtpStream, $cEvent->sFrom())){
                $this->bReset($cSmtpStream);
                unset($aMimeMes);
                unset($aEvents[$key]);
                continue;
              }
              
              //add Recipients
              $aAddr = $cEvent->aTo();
              if(count($aAddr) > 0){
                foreach($aAddr as $sAddr){
                  if(!$this->bRecipient($cSmtpStream, $sAddr)){
                    $this->bReset($cSmtpStream);
                    unset($aMimeMes);
                    unset($aEvents[$key]);
                    continue;
                  }
                }
              }
              $aAddr = $cEvent->aCc();
              if(count($aAddr) > 0){
                foreach($aAddr as $sAddr){
                  if(!$this->bRecipient($cSmtpStream, $sAddr)){
                    $this->bReset($cSmtpStream);
                    unset($aMimeMes);
                    unset($aEvents[$key]);
                    continue;
                  }
                }
              }
              $aAddr = $cEvent->aBcc();
              if(count($aAddr) > 0){
                foreach($aAddr as $sAddr){
                  if(!$this->bRecipient($cSmtpStream, $sAddr)){
                    $this->bReset($cSmtpStream);
                    unset($aMimeMes);
                    unset($aEvents[$key]);
                    continue;
                  }
                }
              }

              //send
              if(!$this->bData($cSmtpStream, $aMimeMes)){
                $this->bReset($cSmtpStream);
                unset($aMimeMes);
                unset($aEvents[$key]);
                continue;
              }
              //set as sent or delete if no chain
              if($cEvent->iChain() > 0){
                $cEvent->Processed();
                $cEvent->bSerialize($cDb);
              }
              else{$cEvent->bDelete($cDb);}
              $iCount++;
              unset($aMimeMes);
              unset($aEvents[$key]);
              
              //if(!$this->bReset($cSmtpStream)){break;}
              //проверим как самочуствие у соединения
              $sSockStatus = socket_get_status($cSmtpStream);
              if($sSockStatus['eof']){break;}
            }
            
            $this->bQuit($cSmtpStream);
            //write to log
            if($iCount !== $iRows){
              $cLog = new cLog(cLog::WARN);
              $cLog->Value($iCount . ' of ' . $iRows . ' emails sent to smtp server: '
              . $cQueue->sHost(cQueue::SMTP) . ' for user: ' . $cQueue->sLogin(cQueue::SMTP));
              $cLog->bSerialize($cDb);
              unset($cLog);
            }
            else{
              if(defined('WTS_DEBUG_LOG')){
                $cLog = new cLog();
                $cLog->Value($iCount . ' emails sent to smtp mail server: '
                . $cQueue->sHost(cQueue::SMTP) . ' for user: ' . $cQueue->sLogin(cQueue::SMTP));
                $cLog->bSerialize($cDb);
                unset($cLog);
              }
            }
          }
          else{
            $cLog = new cLog(cLog::ERR);
            $cLog->Value('Authentication failed to smtp mail server: '
            . $cQueue->sHost(cQueue::SMTP) . ' as user: ' . $cQueue->sLogin(cQueue::SMTP));
            $cLog->bSerialize($cDb);
          }
          //close SMTP
          fclose($cSmtpStream);
        }
        else{
          $cLog = new cLog(cLog::ERR);
          if($iError && $sError){
            $cLog->Value('SMTP Socket error number: \'' . $iError
            . '\' error message: \'' . $sError . '\'');
            $cLog->bSerialize($cDb);
          }
          $cLog->Value('Connection failed to smtp mail server: '
          . $cQueue->sHost(cQueue::SMTP) . ' as user: ' . $cQueue->sLogin(cQueue::SMTP));
          $cLog->bSerialize($cDb);
        }
      }
    }
  }

  try{
    $cDb = new cDb(WTS_DB_NAME, WTS_DB_USER, WTS_DB_PASS, WTS_DB_TABLE_PREFIX);
    
    $aQueues = false;
    cQueue::bAddFilterDisabled(false);

    if(cQueue::iUnserialize($cDb, $aQueues) > 0){
      foreach($aQueues as $key => &$cQueue){
        
        $cImap = new cImap();
        $cImap->Load($cDb, $cQueue);
        unset($cImap);
        $cSmtp = new cSmtp();
        $cSmtp->Send($cDb, $cQueue);
        unset($cSmtp);
        unset($aQueues[$key]);
      }
    }
    unset($cDb);
  }
  catch (\Exception $e){
    echo 'Exception ' . $e->GetCode() . ': ' . $e->GetMessage() . cString::EOL . 'In file: ' . basename($e->GetFile()) . cString::EOL . 'line: ' . $e->GetLine() . cString::EOL;
    exit;
  }

}
?>
