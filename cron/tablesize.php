<?php
/*
 * tablesize.php (part of WTS) - data table switcher 
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{

  require_once('../site/include/defs.php');
  require_once('../site/include/core/utils/db.php');
  require_once('../site/include/core/scheme.php');
  require_once('../site/include/core/event.php');
  require_once('../site/include/core/contract.php');
  require_once('../site/include/core/log.php');

  try{
    $cDb = new cDb(WTS_DB_NAME, WTS_DB_USER, WTS_DB_PASS, WTS_DB_TABLE_PREFIX);
    
    if($cScheme = cScheme::mUnserialize($cDb)){
      
      $b = false;
      $s = date(WTS_DT_BACKUP_FMT, strtotime('now'));
      $q = 'select data_length from information_schema.tables where table_schema = \''
      . WTS_DB_NAME . '\' and table_name like \'';
      //event body
      if($sTable = $cScheme->mValue('EBT')){
        $iSize = (int)$cDb->sQueryRes($q . $sTable . '\'');
        if($iSize > WTS_DB_MAX_TBL_SIZE){
          
          $b = true;
          $cScheme->Value('EBT', $cDb->sTablePrefix() . 'evt_b_' . $s);
          
          $cObj = new cEvent(0);
          $cObj->bInstallBody($cDb, $cScheme->mValue('EBT'));
          unset($cObj);
          
          if(defined('WTS_DEBUG_LOG')){
            $cLog = new cLog(cLog::MSG);
            $cLog->Value('Cron job: create new bodies table for events');
            $cLog->bSerialize($cDb);
            unset($cLog);
          }
          
        }
      }
      //event attach
      if($sTable = $cScheme->mValue('EAT')){
        $iSize = (int)$cDb->sQueryRes($q . $sTable . '\'');
        if($iSize > WTS_DB_MAX_TBL_SIZE){
          
          $b = true;
          $cScheme->Value('EAT', $cDb->sTablePrefix() . 'evt_a_' . $s);
          
          $cObj = new cEvent(0);
          $cObj->bInstallAttach($cDb, $cScheme->mValue('EAT'));
          unset($cObj);
          
          if(defined('WTS_DEBUG_LOG')){
            $cLog = new cLog(cLog::MSG);
            $cLog->Value('Cron job: create new attachments table for events');
            $cLog->bSerialize($cDb);
            unset($cLog);
          }
          
        }
      }
      //contr attach
      if($sTable = $cScheme->mValue('CAT')){
        $iSize = (int)$cDb->sQueryRes($q . $sTable . '\'');
        if($iSize > WTS_DB_MAX_TBL_SIZE){
          
          $b = true;
          $cScheme->Value('CAT', $cDb->sTablePrefix() . 'ctr_a_' . $s);
          
          $cObj = new cContract();
          $cObj->bInstallAttach($cDb, $cScheme->mValue('CAT'));
          unset($cObj);
          
          if(defined('WTS_DEBUG_LOG')){
            $cLog = new cLog(cLog::MSG);
            $cLog->Value('Cron job: create new attachments table for contracts');
            $cLog->bSerialize($cDb);
            unset($cLog);
          }

        }
      }

      if($b === true){$cScheme->bSerialize($cDb);}
      unset($cScheme);
    }
    unset($cDb);
  }
  catch (\Exception $e){
    echo 'Exception ' . $e->GetCode() . ': ' . $e->GetMessage() . cString::EOL . 'In file: ' . basename($e->GetFile()) . cString::EOL . 'line: ' . $e->GetLine() . cString::EOL;
    exit;
  }

}
?>
