<?php
/*
 * rebuild-previews.php (part of WTS) - script for recreate text preview
 * for all events and chains - use if you change text preview size
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('../site/include/defs.php');
  require_once('../site/include/core/utils/db.php');
  require_once('../site/include/core/utils/string.php');
  require_once('../site/include/core/event.php');
  require_once('../site/include/core/chain.php');

  
  try{
    
    $cDb = new cDb(WTS_DB_NAME, WTS_DB_USER, WTS_DB_PASS, WTS_DB_TABLE_PREFIX);
    
    $EventTbl = $cDb->sTablePrefix() . cEvent::TABLE;
    $ChainTbl = $cDb->sTablePrefix() . cChain::TABLE;
    
    //rebuild events
    $iEventCount = (int)$cDb->sQueryRes('select count(*) from ' . $EventTbl);
    if($iEventCount > 0){
      echo 'found ' . $iEventCount . ' events' . cString::EOL;
      echo 'collect events ids ... ';
      
      $aEventIDs = false;
    
      $cDb->QueryRes('select ' . FN_I_ID . ' from ' . $EventTbl);
      if($cDb->iRowCount() > 0){
        while($aRow = $cDb->aRow()){$aEventIDs[] = $aRow[0];}
        $cDb->FreeResult();
      }
      echo 'OK' . cString::EOL;
      
      if($aEventIDs){
        foreach($aEventIDs as $key => $iEventID){
          $sBodyTable = $cDb->sQueryRes('select ' . FN_S_BT . ' from ' . $EventTbl . ' where ' . FN_I_ID . '=' . $iEventID);
          
          if($sBodyTable){
            echo 'rebuild event ' . ($key + 1) . ' of ' . $iEventCount
            . ' from table \'' . $sBodyTable . '\' id: ' . $iEventID . ' ... ';
            
            $sHtml = $cDb->sQueryRes('select ' . FN_S_VAL . ' from ' . $sBodyTable . ' where ' . FN_I_PRT . '=' . $iEventID);
            if($sHtml){
              $cEvent = new cEvent(0, 0);
              $cEvent->AddBody($sHtml);
              $sHtml  = $cEvent->mBody(false);
              
              $cDb->Query('update ' . $sBodyTable . ' set ' . FN_S_VAL . '=\'' . $cDb->sShield($sHtml) . '\' where ' . FN_I_PRT . '=' . $iEventID);
              
              $sPlain = $cEvent->mBody(true);
              
              $cDb->Query('update ' . $sBodyTable . ' set ' . FN_S_PLN . '=\'' . $cDb->sShield($sPlain) . '\' where ' . FN_I_PRT . '=' . $iEventID);
              
              $cEvent->GenPreview(WTS_ENC, WTS_EVENT_PREVIEW_LENGHT);
              $sPreview = $cEvent->sPreview();
              if($sPreview){
                $cDb->Query('update ' . $EventTbl . ' set ' . FN_S_PRV . '=\'' . $cDb->sShield($sPreview) . '\' where ' . FN_I_ID . '=' . $iEventID);
                unset($sPreview);
              }
              else{
                echo '-> No preview ... ';
                $cDb->Query('update ' . $EventTbl . ' set ' . FN_S_PRV . '=null where ' . FN_I_ID . '=' . $iEventID);
              }
              unset($sHtml);
              unset($sPlain);
            }
            else{
              echo '-> No text in message ... ';
              $cDb->Query('update ' . $sBodyTable . ' set ' . FN_S_PLN . '=null where ' . FN_I_PRT . '=' . $iEventID);
              $cDb->Query('update ' . $EventTbl . ' set ' . FN_S_PRV . '=null where ' . FN_I_ID . '=' . $iEventID);
            }
            echo 'OK' . cString::EOL;
          }
          
        }
        unset($aEventIDs);
      }

    }
    
    //rebuild chains
    $iChainCount = (int)$cDb->sQueryRes('select count(*) from ' . $ChainTbl);
    
    if($iChainCount > 0){
      echo 'found ' . $iChainCount . ' chains' . cString::EOL;
      echo 'collect chains ids ... ';
      
      $aChainIDs = false;
    
      $cDb->QueryRes('select ' . FN_I_ID . ' from ' . $ChainTbl);
      if($cDb->iRowCount() > 0){
        while($aRow = $cDb->aRow()){$aChainIDs[] = $aRow[0];}
        $cDb->FreeResult();
      }
      echo 'OK' . cString::EOL;
      
      if($aChainIDs){
        foreach($aChainIDs as $key => $iChainID){
          echo 'rebuild chain ' . ($key + 1) . ' of ' . $iChainCount . ' id: ' . $iChainID . ' ... ';

          $sPreview = $cDb->sQueryRes('select ' . FN_S_PRV . ' from ' . $EventTbl . ' where ' . FN_I_CHN . '=' . $iChainID . ' order by ' . FN_I_ID . ' limit 1');
          
          if($sPreview){
            $cDb->Query('update ' . $ChainTbl . ' set ' . FN_S_PRV . '=\'' . $cDb->sShield($sPreview) . '\' where ' . FN_I_ID . '=' . $iChainID);
            unset($sPreview);
          }
          else{
            echo '-> No preview ... ';
            $cDb->Query('update ' . $ChainTbl . ' set ' . FN_S_PRV . '=null where ' . FN_I_ID . '=' . $iChainID);
          }

          echo 'OK' . cString::EOL;

        }
        unset($aChainIDs);
      }
    }
    
    
    unset($cDb);
  }
  catch (\Exception $e){
    echo 'Exception ' . $e->GetCode() . ': ' . $e->GetMessage() . cString::EOL . 'In file: ' . basename($e->GetFile()) . cString::EOL . 'line: ' . $e->GetLine() . cString::EOL;
    exit;
  }
  
}
?>
