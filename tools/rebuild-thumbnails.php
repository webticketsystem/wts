<?php
/*
 * rebuild-previews.php (part of WTS) - script for recreate thumbnails
 * for event attachments for all events - use if you change thumbnail size
 *
 * !!! set memory_limit > 512M for php if proccess was stoped 
 *
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('../site/include/defs.php');
  require_once('../site/include/core/utils/db.php');
  require_once('../site/include/core/utils/string.php');
  require_once('../site/include/core/event.php');
  require_once('../site/include/core/utils/image.php');
  

  try{
    
    $cDb = new cDb(WTS_DB_NAME, WTS_DB_USER, WTS_DB_PASS, WTS_DB_TABLE_PREFIX);
    
    $EventTbl = $cDb->sTablePrefix() . cEvent::TABLE;
    
    //rebuild events
    $iEventCount = (int)$cDb->sQueryRes('select count(*) from ' . $EventTbl);
    if($iEventCount > 0){
      echo 'found ' . $iEventCount . ' events' . cString::EOL;
      echo 'collect events ids ... ';
      
      $aEventIDs = false;
    
      $cDb->QueryRes('select ' . FN_I_ID . ' from ' . $EventTbl);
      if($cDb->iRowCount() > 0){
        while($aRow = $cDb->aRow()){$aEventIDs[] = $aRow[0];}
        $cDb->FreeResult();
      }
      echo 'OK' . cString::EOL;
      
      if($aEventIDs){
        foreach($aEventIDs as $key => $iEventID){
          $sAttachTable = $cDb->sQueryRes('select ' . FN_S_AT. ' from ' . $EventTbl . ' where ' . FN_I_ID . '=' . $iEventID);
          
           
          if($sAttachTable){

            $aAttachs = false;
            $cDb->QueryRes('select ' . FN_I_ID . ', ' . FN_S_NM . ', ' . FN_S_VAL . ' from ' . $sAttachTable . ' where ' . FN_I_PRT . '=' . $iEventID);
            
            if($cDb->iRowCount() > 0){
              while($aRow = $cDb->aRow()){
                $aAttachs[(int)$aRow[0]][FN_S_NM]  = $aRow[1];
                $aAttachs[(int)$aRow[0]][FN_S_VAL] = $aRow[2];
              }
              $cDb->FreeResult();
            }
            
            if($aAttachs !== false){

              foreach($aAttachs as $key2 => &$aAttach){
                
                echo 'make thumbnail for file \'' . $aAttach[FN_S_NM] . '\'' . cString::EOL
                . 'id: ' . $key2 . ' from table \'' . $sAttachTable . '\' ... ';
                
                $sThumb = cImage::mCreateThumb($aAttach[FN_S_NM], WTS_EVENT_THUMB_SIDE, $aAttach[FN_S_VAL]);
                if($sThumb !== false){
                  $cDb->Query('update ' . $sAttachTable . ' set ' . FN_S_PRV . '=\'' . $cDb->sShield($sThumb) . '\' where ' . FN_I_ID . '=' . $key2);
                  unset($sThumb);
                  echo 'OK' . cString::EOL . cString::EOL;
                }
                else{
                  echo 'FAIL' . cString::EOL . cString::EOL;
                }
                
                unset($aAttach[FN_S_NM]);
                unset($aAttach[FN_S_VAL]);
                //unset($aAttach);
              }
            }//if($aAttachs !== false)
          }
        }
        unset($aEventIDs);
      }

    }
    
    unset($cDb);
  }
  catch (\Exception $e){
    echo 'Exception ' . $e->GetCode() . ': ' . $e->GetMessage() . cString::EOL . 'In file: ' . basename($e->GetFile()) . cString::EOL . 'line: ' . $e->GetLine() . cString::EOL;
    exit;
  }
  
  
}
?>
