<?php
/*
 * index.php (part of WTS) - run rest service
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * version you can get from header:
 * Content-Type:application/wtsrest.v2+json
 * 
 * nginx settings
 * 
 * location /wts/rest{
 *   rewrite ^/wts/rest/(.*)$ /wts/rest/index.php?q=$1? last; 
 *   alias /opt/wts/rest;
 *   location ~ \.php$ {
 *     fastcgi_param  DOCUMENT_ROOT /opt/wts/rest;
 *     fastcgi_pass   unix:/tmp/php-fpm.sock;
 *     fastcgi_param  HTTPS on;
 *     fastcgi_param  SCRIPT_FILENAME $request_filename;
 *     include        fastcgi_params;
 *   }
 * }
 * 
 * Для авторизации необходим HTTP заголовок + JSON POST {"login":"agent@mail.com"}
 * 
 * Content-Type:application/json
 * 
 * Если логин найден в системе в ответ Вы получите UID в JSON {"uid":"uid123-AbcDef-etc"}
 * для работы с REST API необходимо 2 HTTP заголовка
 * 
 * Content-Type:application/json
 * Authorization:login="agent@mail.com", hash="bd5ec8b69903924cdde52b435beae45312070def"
 * 
 * Где hash = sha1(UID . hash('sha256', 'YourPassword', false))
 * 
 * Метод   | URL                       | Доступ | Действие
 * ---------------------------------------------------------------------
 * POST    | /rest/auth                | All    | Возвращает uid_ssn если пользователь найден
 * ANY     | /rest/logout              | All    | no coments
 * ANY     | /rest/whoami              | All    | получить инфо о себе, продлить сессию...
 * 
 * GET     | /rest/events/types        | All    | Возвращает коллекцию типов событий (email, phone...)
 * GET     | /rest/events/unhandled    | Agent  | Возвращает id всех неприкреплённых событий (for dashboard)
 * GET     | /rest/events/2            | All    | Возвращает событие по его ключу (primary key)
 * POST    | /rest/events              | Agent  | Добавляет новое событие в цепочке
 * PUT     | /rest/events/2            | Agent  | Обновляет событие по его ключу (primary key)
 * DELETE  | /rest/events/2            | Agent  | Удаляет событие по его ключу (primary key)
 * 
 * GET     | /rest/tags                | All    | Возвращает коллекцию тегов
 * 
 * GET     | /rest/queues/list         | Agent  | Возвращает коллекцию очередей
 * GET     | /rest/queues/2/regexps    | Agent  | Возвращает коллекцию регулярных выражений для очереди
 * GET     | /rest/queues/2/templates  | Agent  | Возвращает коллекцию шаблонов событий для очереди
 * GET     | /rest/queues/replaces     | Agent  | Возвращает коллекцию констант в теле шаблонов для замены
 * GET     | /rest/queues/2/agents     | Agent  | Возвращает коллекцию агентов для очереди
 * 
 * GET     | /rest/chains/search/Astro | All    | Производит поиск цепочек содержащих "Astro"
 * GET     | /rest/chains/updated/600  | All    | Возвращает id цепочек, обновленных за последние 10 минут
 * GET     | /rest/chains/2            | All    | Возвращает цепочку по её ключу (primary key)
 * POST    | /rest/chains              | Agent  | Добавляет новую цепочку на основе события
 * PUT     | /rest/chains/2            | Agent  | Обновляет цепочку по её ключу (primary key)
 * 
 * GET     | /rest/groups/attrs        | Agent  | Возвращает коллекцию атрибутов групп
 * GET     | /rest/groups/search/Astro | Agent  | Производит поиск групп содержащих "Astro"
 * GET     | /rest/groups/updated/600  | Agent  | Возвращает id групп, обновленных за последние 10 минут
 * GET     | /rest/groups/2            | Agent  | Возвращает группу по её ключу (primary key)
 * POST    | /rest/groups              | Agent  | Добавляет новую группу {"name":"Astro"}
 * PUT     | /rest/groups/2            | Agent  | Обновляет группу по её ключу (primary key)
 * 
 * GET     | /rest/conts/attrs         | Agent  | Возвращает коллекцию атрибутов контактов
 * GET     | /rest/conts/search/Astro  | Agent  | Производит поиск контактов содержащих "Astro"
 * GET     | /rest/conts/updated/600   | Agent  | Возвращает id контактов, обновленных за последние 10 минут
 * GET     | /rest/conts/2             | Agent  | Возвращает контакт по его ключу (primary key)
 * POST    | /rest/conts               | Agent  | Добавляет новый контакт {"login":"valid@email.com"}
 * PUT     | /rest/conts/2             | Agent  | Обновляет контакт по его ключу (primary key)
 * 
 * GET     | /rest/contrs/search/Astro | Agent  | Производит поиск контрактов содержащих "Astro"
 * GET     | /rest/contrs/updated/600  | Agent  | Возвращает id контрактов, обновленных за последние 10 минут
 * GET     | /rest/contrs/2            | Agent  | Возвращает контракт по его ключу (primary key)
 * POST    | /rest/contrs              | Agent  | Добавляет новый контракт
 * PUT     | /rest/contrs/2            | Agent  | Обновляет контракт по его ключу
 * DELETE  | /rest/contrs/2            | Agent  | Удаляет контракт по его ключу (primary key)
 * 
 * GET     | /rest/prods/attrs         | Agent  | Возвращает коллекцию атрибутов продуктов
 * GET     | /rest/prods/cats          | Agent  | Возвращает коллекцию категорий продуктов
 * GET     | /rest/prods/search/Astro  | Agent  | Производит поиск продуктов содержащих "Astro"
 * GET     | /rest/prods/updated/600   | Agent  | Возвращает id продуктов, обновленных за последние 10 минут
 * GET     | /rest/prods/2             | Agent  | Возвращает продукт по его ключу (primary key)
 * POST    | /rest/prods               | Agent  | Добавляет новый(е) продукт(ы)
 * PUT     | /rest/prods/2             | Agent  | Обновляет продукт по его ключу (primary key)
 * 
 */
 
namespace wts{

  require_once('rest.php');

  try{
    header('Content-Type:application/wtsrest.v2+json');
    
    if($_SERVER['CONTENT_TYPE'] == 'application/json'){
    
      $cDb = new cDb(WTS_DB_NAME, WTS_DB_USER, WTS_DB_PASS, WTS_DB_TABLE_PREFIX);

      //разберём заголовок /rest/first/second/десять
      //!!! заголовок не должен заканчиваться на "/"
      $aRestRequest = false;
      if(isset($_GET['q']) && strlen($_GET['q']) > 0 && $_GET['q'][strlen($_GET['q'])-1] != '/'){
        $aRestRequest = array_map('trim', explode('/', $_GET['q']));
      }
      
      //вытащим json запрос, если есть...
      $aBodyRequest = false;
      switch($_SERVER['REQUEST_METHOD']){
        case 'POST':
        case 'PUT':
          $s = file_get_contents('php://input');
          if(strlen($s) > 0){$aBodyRequest = (array)json_decode($s);}
        break;
        default: break;
      }
      
      //проверим сессию
      $cCont = false;
      if(isset($_SERVER['HTTP_AUTHORIZATION'])){
        $_SERVER['HTTP_AUTHORIZATION'] = str_replace(';', ',', $_SERVER['HTTP_AUTHORIZATION']);
        $aParams = explode(',', $_SERVER['HTTP_AUTHORIZATION']);
        $aAuthData = array();
        foreach($aParams as $sPair) { 
          $aItem = array_map('trim', explode('=', $sPair));
          if(count($aItem) == 2) {
            $aAuthData[$aItem[0]] = urldecode(trim($aItem[1],'"')); 
          } 
        }
        if(isset($aAuthData['login'])
        && isset($aAuthData['hash'])){
          $cCont = cCont::mCheckSsn($cDb, $aAuthData['login'], $aAuthData['hash'], WTS_SESSION_TIMEOUT);
        }
      }
      
      if(isset($aRestRequest[0])){
        switch($aRestRequest[0]){
          case 'auth':
            if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($aBodyRequest['login'])){
              if(($sUID = cCont::mBeginSsn($cDb, $aBodyRequest['login']))){
                header(WTS_HTTP_200);
                unset($aBodyRequest);
                $aBodyRequest['key'] = $sUID;
                echo json_encode($aBodyRequest);
              }
              else{ 
                sleep(3);
                header(WTS_HTTP_400);
              }
            }
            else{header(WTS_HTTP_400);}
          break;
          case 'logout':
            if($cCont){
              $cCont->CloseSsn($cDb);
              header(WTS_HTTP_200);
              echo json_encode(array('Bye'));
            }
            else{header(WTS_HTTP_401);}
          break;
          case 'whoami':
            unset($aBodyRequest);
            if($cCont){
              $aBodyRequest['login'] = $cCont->sLogin();
              switch($cCont->iType()){
                case cCont::AGENT:
                  $aBodyRequest['api_level'] = 'agent';
                  break;
                case cCont::ADMIN:
                  $aBodyRequest['api_level'] = 'admin';
                  break;
                default:
                  $aBodyRequest['api_level'] = 'customer';
                  break;
              }
            }
            else{
              $aBodyRequest['login']    = 'unknown';
              $aBodyRequest['api_level'] = 'unknown';
            }
            header(WTS_HTTP_200);
            echo json_encode($aBodyRequest);
          break;
          case 'events':
            $cRest = new cRestEvents($cDb, $cCont);
            header($cRest->sParse($aRestRequest, $aBodyRequest));
            $cRest->sResponse();
            unset($cRest);
          break;
          case 'tags':
            if($cCont){
              $cRest = new cRestTags($cDb, $cCont);
              header($cRest->sParse($aRestRequest, $aBodyRequest));
              $cRest->sResponse();
              unset($cRest);
            }
            else header(WTS_HTTP_401);
          break;
          case 'queues':
            if($cCont && $cCont->iType() !== cCont::CSTMR){
              $cRest = new cRestQueues($cDb, $cCont);
              header($cRest->sParse($aRestRequest, $aBodyRequest));
              $cRest->sResponse();
              unset($cRest);
            }
            else header(WTS_HTTP_401);
          break;
          case 'chains':
            $cRest = new cRestChains($cDb, $cCont);
            header($cRest->sParse($aRestRequest, $aBodyRequest));
            $cRest->sResponse();
            unset($cRest);
          break;
          case 'groups':
            if($cCont && $cCont->iType() !== cCont::CSTMR){
              $cRest = new cRestGroups($cDb, $cCont);
              header($cRest->sParse($aRestRequest, $aBodyRequest));
              $cRest->sResponse();
              unset($cRest);
            }
            else header(WTS_HTTP_401);
          break;
          case 'conts':
            if($cCont && $cCont->iType() !== cCont::CSTMR){
              $cRest = new cRestConts($cDb, $cCont);
              header($cRest->sParse($aRestRequest, $aBodyRequest));
              $cRest->sResponse();
              unset($cRest);
            }
            else header(WTS_HTTP_401);
          break;
          case 'contrs':
            if($cCont && $cCont->iType() !== cCont::CSTMR){
              $cRest = new cRestContrs($cDb, $cCont);
              header($cRest->sParse($aRestRequest, $aBodyRequest));
              $cRest->sResponse();
              unset($cRest);
            }
            else header(WTS_HTTP_401);
          break;
          case 'prods':
            if($cCont && $cCont->iType() !== cCont::CSTMR){
              $cRest = new cRestProds($cDb, $cCont);
              header($cRest->sParse($aRestRequest, $aBodyRequest));
              $cRest->sResponse();
              unset($cRest);
            }
            else header(WTS_HTTP_401);
          break;
          default:
            header(WTS_HTTP_400);
          break;
        }
      }//no rest params
      else{header(WTS_HTTP_400);}

      if($cCont){unset($cCont);}
      unset($cDb);
    }//ругнёмся на несоответствие формату
    else{header(WTS_HTTP_400);}
  }
  catch (\Exception $e){
    echo 'Exception ' . $e->GetCode() . ': ' . $e->GetMessage() . cString::EOL . 'In file: ' . basename($e->GetFile()) . cString::EOL . 'line: ' . $e->GetLine() . cString::EOL;
    exit;
  }
  
}
?>
