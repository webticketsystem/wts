<?php
/*
 * rest.php (part of WTS) - rest service response classes
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
namespace wts{

  require_once('../site/include/core/utils/db.php');
  require_once('../site/include/defs.php');
  require_once('../site/include/core/log.php');
  require_once('../site/include/core/cont.php');
  require_once('../site/include/core/event.php');
  require_once('../site/include/core/chain.php');
  require_once('../site/include/core/queue.php');
  require_once('../site/include/core/group.php');
  require_once('../site/include/core/tag.php');
  require_once('../site/include/core/response.php');
  require_once('../site/include/core/regexp.php');
  require_once('../site/include/core/salutation.php');
  require_once('../site/include/core/signature.php');
  require_once('../site/include/core/contract.php');
  require_once('../site/include/core/product.php');


  abstract class cRest{
    protected $cDb;
    protected $cCont;
    protected $aResponse;
    
    public function __construct(cDb &$cDb, cCont &$cCont){
      $this->cDb       = $cDb;
      $this->cCont     = $cCont;
      $this->aResponse = array();
    }
    abstract public function sParse(&$aRestRequest, &$aBodyRequest);

    public function sResponse(){
      if(count($this->aResponse) > 0){
        echo json_encode($this->aResponse);
      }
    }
  }

  /**
   * events
   */
  class cRestEvents extends cRest{
    public function sParse(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;    
      switch($_SERVER['REQUEST_METHOD']){
        case 'GET':    $sRet = $this->sGet($aRestRequest, $aBodyRequest);
        break;
        case 'POST':   $sRet = $this->sPost($aRestRequest, $aBodyRequest);
        break;
        case 'PUT':    $sRet = $this->sPut($aRestRequest, $aBodyRequest);
        break;
        case 'DELETE': $sRet = $this->sDelete($aRestRequest, $aBodyRequest);
        break;
        default: break;
      }
      return $sRet;
    }
    
    protected function sGet(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;
      if(isset($aRestRequest[1])){
        switch($aRestRequest[1]){
          case 'types':
            $this->aResponse[cEvent::IN_EMAIL]  = cEvent::sType(cEvent::IN_EMAIL);
            $this->aResponse[cEvent::OUT_EMAIL] = cEvent::sType(cEvent::OUT_EMAIL);
            $this->aResponse[cEvent::IN_CALL]   = cEvent::sType(cEvent::IN_CALL);
            $this->aResponse[cEvent::OUT_CALL]  = cEvent::sType(cEvent::OUT_CALL);
            $this->aResponse[cEvent::MEETING]   = cEvent::sType(cEvent::MEETING);
            $sRet = WTS_HTTP_200;
          break;
          case 'unhandled':
            if($this->cCont->iType() !== cCont::CSTMR){
              cEvent::AddFilterChain(0);//not attached to chain
              cEvent::bAddFilterProcessed(false);

              if($this->cCont->iType() === cCont::AGENT){
                cEvent::bAddFilterQueue($this->cCont->iQueue());
              }
              
              if($aIDs = cEvent::mIDs($this->cDb)){
                $this->aResponse = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
          default://try get event
            $cObj = cEvent::mUnserialize($this->cDb, $aRestRequest[1]);
            if($cObj
            && $this->cCont->iType() === cCont::CSTMR){
              if(!$cObj->iChain()){unset($cObj);}
              else{
                if(!($cChain = cChain::mUnserialize($this->cDb, $cObj->iChain()))
                || $cChain->iCont() !== $this->cCont->iID()){
                  unset($cObj);
                  $cObj = false;
                }
              }
            }
            if($cObj){
              if($cObj->sFrom()){
                $this->aResponse['from'] = $cObj->sFrom();
              }
              if($cEvent->iUnserializeAddr($this->cDb) > 0){
                $aAddr = $cEvent->aTo();
                if(count($aAddr) > 0){$this->aResponse['to'] = $aAddr;}
                $aAddr = $cEvent->aCc();
                if(count($aAddr) > 0){$this->aResponse['cc'] = $aAddr;}
                $aAddr = $cEvent->aBcc();
                if(count($aAddr) > 0){$this->aResponse['bcc'] = $aAddr;}
              }
              if($cObj->sSubject()){
                $this->aResponse['subject'] = $cObj->sSubject();
              }
              $this->aResponse[($cObj->iType() == cEvent::OUT_EMAIL ? 'sent' : 'seen')] = ($cObj->bProcessed() ? 'true' : 'false');
              $this->aResponse['dt_begin'] = $cObj->iDtBegin();
              $this->aResponse['dt_end']   = $cObj->iDtEnd();
              
              if($sBody = $cObj->mUnserializeBody($this->cDb)){
                $this->aResponse['body'] = $sBody;
              }
              if($cObj->iUnserializeAttach($this->cDb) > 0){
                $aAttachs = $cObj->aAttach();
                foreach($aAttachs as $a){
                  $this->aResponse['attachments'][] = array('name' => $a[FN_S_NM]
                                                          , 'cid'  => $a[FN_S_CID]
                                                          , 'file' => $cObj->mUnserializeAttach($this->cDb, $a[FN_I_ID]),);
                }
              }
              $sRet = WTS_HTTP_200;
            }
          break;
        }
      }
      return $sRet;
    }
    //создаём событие только в цепочке
    protected function sPost(&$aRestRequest, &$aBodyRequest){
      if($this->cCont->iQueue() > 1
      && ($cQueue = cQueue::mUnserialize($this->cDb, $this->cCont->iQueue()))
      && isset($aBodyRequest['chain_id'])
      && ($cChain = cChain::mUnserialize($this->cDb, $aBodyRequest['chain_id']))
      && $cChain->iUpdater() == $this->cCont->iID()
      && isset($aBodyRequest['type_id'])
      && ($aBodyRequest['type_id']  == cEvent::OUT_EMAIL
        || $aBodyRequest['type_id'] == cEvent::IN_CALL
        || $aBodyRequest['type_id'] == cEvent::OUT_CALL
        || $aBodyRequest['type_id'] == cEvent::MEETING)){
        $cObj = new cEvent($aBodyRequest['type_id'], $cQueue->iID());
        if(isset($aBodyRequest['subject'])){$cObj->Subject($aBodyRequest['subject']);}
        $cObj->From($cQueue->sLogin());
        unset($cQueue);
        if(isset($aBodyRequest['dt_begin'])){$cObj->DtBegin($aBodyRequest['dt_begin']);}
        if(isset($aBodyRequest['dt_end'])){$cObj->DtEnd($aBodyRequest['dt_end']);}
        else{$cObj->DtEnd(strtotime('now'));}
        $cObj->Chain($cChain->iID());

        if(isset($aBodyRequest['body'])
        && strlen($aBodyRequest['body']) > 0){
          $cObj->AddBody($aBodyRequest['body']);
          $cObj->GenPreview(WTS_ENC, WTS_EVENT_PREVIEW_LENGHT);
        }
        if(isset($aBodyRequest['attachments'])
        && count($aBodyRequest['attachments']) > 0){
          foreach($aBodyRequest['attachments'] as &$a){
            if(isset($a['name']) && strlen($a['name']) > 0
            && isset($a['file']) && strlen($a['file']) > 0){
              $cObj->AddAttach($a['file'], $a['name'], (isset($a['cid']) ? $a['cid'] : null));
            }
          }
        }
        if($cObj->bSerialize($this->cDb, WTS_EVENT_THUMB_SIDE, $this->cCont->iID())){
          //откроем цепочку
          if($cChain->bClosed()){
            $cChain->Open();
            $cChain->bSerialize($this->cDb);
          }
          return WTS_HTTP_202;
        }
      }
      return WTS_HTTP_400;
    }
    //прикрепить/открепить/пометить прочтённым
    protected function sPut(&$aRestRequest, &$aBodyRequest){
      if($this->cCont->iQueue()
      && isset($aRestRequest[1])
      && ($cObj = cEvent::mUnserialize($this->cDb, (int)$aRestRequest[1]))){
        if(isset($aBodyRequest['chain_id'])){
          if((int)$aBodyRequest['chain_id'] > 0){//attach
            if(!$cObj->iChain()
            && $cObj->iType() !== cEvent::OUT_EMAIL
            && ($cChain = cChain::mUnserialize($this->cDb, $aBodyRequest['chain_id']))){
              $cObj->Chain($cChain->iID());
              
              //откроем цепочку
              if($cChain->bClosed()){
                $cChain->Open();
                $cChain->bSerialize($this->cDb);
              }

              unset($cChain);
            }
          }
          else{//detach
            if($cObj->iChain()
            && $cObj->iType() !== cEvent::OUT_EMAIL
            && ($cChain = cChain::mUnserialize($this->cDb, $aBodyRequest['chain_id']))
            && ($this->cCont->iType() === cCont::ADMIN
              || ($this->cCont->iID() === $cChain->iUpdater()
              && $cCurChain->bClosed() === false))){
              $cObj->Chain(0);

              $cLog = new cLog(cLog::WARN);
              $cLog->Value('REST: Detach Event ' . $cObj->iID()
              . ' from Chain ' . $cChain->iID() . ' by ' . $this->cCont->sLogin());
              $cLog->bSerialize($this->cDb);
              unset($cLog);

            }
          }
        }
        else{//mark as seen
          if(isset($aBodyRequest['seen'])
          && $aBodyRequest['seen'] == 'true'
          && $cObj->iChain()
          && $cObj->iType() !== cEvent::OUT_EMAIL
          && !$cObj->bSendOrReaded()
          && ($cChain = cChain::cUnserialize($this->cDb, $aBodyRequest['chain_id']))
          && !$cChain->bClosed()
          && $cChain->iUpdater() == $this->cCont->iID()){
            $cObj->SendOrRead();
          }
        }
        $cObj->bSerialize($this->cDb);
        unset($cObj);
        return WTS_HTTP_200;
      }
      return WTS_HTTP_400;
    }
    
    protected function sDelete(&$aRestRequest, &$aBodyRequest){
      if($this->cCont->iQueue()
      && isset($aRestRequest[1])
      && ($cObj = cEvent::mUnserialize($this->cDb, $aRestRequest[1]))){
        if($cObj->iChain()){//тут удаляем только исходящее и неотправленные
          if(($cChain = cChain::mUnserialize($this->cDb, $cObj->iChain()))
          && ($this->cCont->iType() === cCont::ADMIN
            || ($this->cCont->iID() === $cChain->iUpdater()
            && $cChain->bClosed() === false))
          && $cObj->iType() === cEvent::OUT_EMAIL
          && !$cObj->bProcessed()){
            $cLog = new cLog();
            $cLog->Value('REST: Delete Event ' . $cObj->iID()
            . ' from Chain ' . $cChain->iID() . ' by ' . $this->cCont->sLogin());
            $cLog->bSerialize($this->cDb);
            unset($cLog);
            $cObj->bDelete($this->cDb);
            unset($cObj);
            $cObj = false;
          }
        }
        else{//тут из дашборда
          if(!$cObj->bProcessed()){
            $cObj->bDelete($this->cDb);
            unset($cObj);
            $cObj = false;
          }
        }
        if(!$cObj){return WTS_HTTP_204;}
      }
      return WTS_HTTP_400;
    }
  }

  /**
   * events
   */
  class cRestTags extends cRest{
    public function sParse(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;    
      switch($_SERVER['REQUEST_METHOD']){
        case 'GET': $sRet = $this->sGet($aRestRequest, $aBodyRequest);
        default: break;
      }
      return $sRet;
    }
    
    protected function sGet(&$aRestRequest, &$aBodyRequest){
      $aTags = false;
      if(cTag::iUnserialize($this->cDb, $aTags)){
        foreach($aTags as &$c){
          $this->aResponse[$c->iID()] = array('name'  => $c->sName()
                                            , 'slug'  => $c->sSlug()
                                            , 'color' => $c->sColor(),);
        }
      }
      return WTS_HTTP_200;
    }
  }

  /**
   * queues
   */
  class cRestQueues extends cRest{
    public function sParse(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;    
      switch($_SERVER['REQUEST_METHOD']){
        case 'GET': $sRet = $this->sGet($aRestRequest, $aBodyRequest);
          break;
        default: break;
      }
      return $sRet;
    }
    
    protected function sGet(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;
      $aQueues = false;
      if(isset($aRestRequest[1])){
        switch($aRestRequest[1]){
          case 'list':
            if(cQueue::iUnserialize($this->cDb, $aQueues) > 0){
              foreach($aQueues as &$c){
                if($c->iID() == 1){continue;}
                $this->aResponse[$c->iID()] = array('name'     => $c->sName()
                                                  , 'login'    => $c->sLogin()
                                                  , 'disabled' => ($c->bDisabled() ? 'true' : 'false'),);
                
              }
            }
            $sRet = WTS_HTTP_200;
          break;
          case 'replaces':
            $this->aResponse['site_title']    = cResponse::RPL_EXP['SITE_TITLE'];
            $this->aResponse['agent_name']    = cResponse::RPL_EXP['AGENT_NAME'];
            $this->aResponse['current_chain'] = cResponse::RPL_EXP['CUR_CHAIN'];
            $this->aResponse['queue_email']   = cResponse::RPL_EXP['QUEUE_EMAIL'];
            $this->aResponse['queue_name']    = cResponse::RPL_EXP['QUEUE_NAME'];
            $this->aResponse['customer_name'] = cResponse::RPL_EXP['CSRMR_NAME'];
            $this->aResponse['forward_body']  = cResponse::RPL_EXP['FWD_MESSAGE'];
            $sRet = WTS_HTTP_200;
          break;
          default:
            if(isset($aRestRequest[2])
            && (int)$aRestRequest[1] > 1
            && ($cQueue = cQueue::mUnserialize($this->cDb, (int)$aRestRequest[1]))){
              switch($aRestRequest[2]){
                case 'tags':
                  $aTags = false;
                  if(cTag::iUnserialize($this->cDb, $aTags)){
                    foreach($aTags as &$c){
                      $this->aResponse[$c->iID()] = array('name'  => $c->sName()
                                                        , 'slug'  => $c->sSlug()
                                                        , 'color' => $c->sColor(),);
                    }
                  }
                  $sRet = WTS_HTTP_200;
                break;
                case 'regexps':
                  $aRegExps = false;
                  if(cRegExp::iUnserialize($this->cDb, $cQueue->iID(), $aRegExps)){
                    foreach($aRegExps as &$c){
                      $this->aResponse[$c->iID()] = array('name'  => $c->sName()
                                                        , 'value' => $c->sValue(),);
                    }
                  }
                  $sRet = WTS_HTTP_200;
                break;
                case 'templates':
                  $aResponsesID = false;
                  $cQueue->iUnserializeResps($this->cDb, $aResponsesID);
                  if($aResponsesID !== false){
                    if(isset($aResponsesID[cQueue::EML])
                    && ($cResponse = cResponse::mUnserialize($this->cDb, $aResponsesID[cQueue::EML]))){
                      $this->aResponse['email'] = $cResponse->mBuild($this->cDb);
                      unset($cResponse);
                    }
                    if(isset($aResponsesID[cQueue::PCL])
                    && ($cResponse = cResponse::mUnserialize($this->cDb, $aResponsesID[cQueue::PCL]))){
                      $this->aResponse['call'] = $cResponse->mBuild($this->cDb);
                      unset($cResponse);
                    }
                    if(isset($aResponsesID[cQueue::MTG])
                    && ($cResponse = cResponse::mUnserialize($this->cDb, $aResponsesID[cQueue::MTG]))){
                      $this->aResponse['meeting'] = $cResponse->mBuild($this->cDb);
                      unset($cResponse);
                    }
                    if(isset($aResponsesID[cQueue::FWD])
                    && ($cResponse = cResponse::mUnserialize($this->cDb, $aResponsesID[cQueue::FWD]))){
                      $this->aResponse['forward'] = $cResponse->mBuild($this->cDb);
                      unset($cResponse);
                    }
                  }
                  $sRet = WTS_HTTP_200;
                break;
                case 'agents':
                  $aAgents = false;
                  cCont::bAddFilterQueue($cQueue->iID());
                  if(($iRows = cCont::iCount($this->cDb)) > 0
                  && cCont::iUnserialize($this->cDb, 1, $iRows, $aAgents) > 0){
                    foreach($aAgents as &$c){
                      $this->aResponse[$c->iID()] = array('name' => $c->sName()
                        , 'login'    => $c->sLogin()
                        , 'disabled' => ($c->bDisabled() ? 'true' : 'false'),);
                    }
                  }
                  $sRet = WTS_HTTP_200;
                break;
              }//switch
            }
          break;
        }
      }
      return $sRet;
    }
  }

  /**
   * chains
   */
  class cRestChains extends cRest{
    public function sParse(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;    
      switch($_SERVER['REQUEST_METHOD']){
        case 'GET':  $sRet = $this->sGet($aRestRequest, $aBodyRequest);
        break;
        case 'POST': $sRet = $this->sPost($aRestRequest, $aBodyRequest);
        break;
        case 'PUT':  $sRet = $this->sPut($aRestRequest, $aBodyRequest);
        break;
        default: break;
      }
      return $sRet;
    }
    
    protected function sGet(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;
      if(isset($aRestRequest[1])){
        switch($aRestRequest[1]){
          case 'search':
            if(isset($aRestRequest[2])){
              if($this->cCont->iType() === cCont::CSTMR){
                cChain::bAddFilterCont($this->cCont->iID());
              }
              cChain::bAddFilterPartValue($this->cDb, $aRestRequest[2]);
              if($aIDs = cChain::mIDs($this->cDb)){
                $this->aResponse = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
          case 'updated':
            if(isset($aRestRequest[2])){
              if($this->cCont->iType() === cCont::CSTMR){
                cChain::bAddFilterCont($this->cCont->iID());
              }
              cChain::bAddFilterUpdated($aRestRequest[2]);
              if($aIDs = cChain::mIDs($this->cDb)){
                $this->aResponse = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
          default://try get chain
            $cObj = cChain::mUnserialize($this->cDb, $aRestRequest[1]);
            if($this->cCont->iType() === cCont::CSTMR
            && $this->cCont->iID() != $cObj->iCont()){
              unset($cObj);
              $cObj = false;
            }
            if($cObj){

              $this->aResponse['subject'] = $cObj->sSubject();

              $this->aResponse['preview'] = $cObj->sPreview();

              $this->aResponse['closed'] = ($cObj->bClosed() ? 'true' : 'false');
              if($this->cCont->iType() !== cCont::CSTMR){
                $this->aResponse['queue_id'] = $cObj->iQueue();
                
                $aTagViews = false;
                $aChains = array($cObj);
                if(cChain::iUnserializeTags($this->cDb, $aChains, $aTagViews) > 0){
                  $aSlugs = false;
                  foreach($aTagViews as $key => &$cTagView){
                    $aSlugs[] = $cTagView->sSlug();
                    unset($aTagViews[$key]);
                  }
                  $this->aResponse['slugs'] = $aSlugs;
                }

                $this->aResponse['cont_id'] = $cObj->iCont();
               
                $this->aResponse['group_id'] = $cObj->iGroup();

                $this->aResponse['creator_id'] = $cObj->iCreator();
                $this->aResponse['responsible_id'] = $cObj->iUpdater();
              }
              $this->aResponse['dt_creation'] = $cObj->iDtCreated();
              $this->aResponse['dt_modify']   = $cObj->iDtUpdated();
              
              cEvent::AddFilterChain($cObj->iID());
              if($aIDs = cEvent::mIDs($this->cDb)){
                $this->aResponse['event_ids'] = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
        }
      }
      return $sRet;
    }
    
    protected function sPost(&$aRestRequest, &$aBodyRequest){
      if($this->cCont->iQueue()
      && isset($aBodyRequest['event_id'])
      && ($cEvent = cEvent::mUnserialize($this->cDb, $aBodyRequest['event_id']))
      && !$cEvent->iChain()
      && $cEvent->iType() !== cEvent::OUT_EMAIL
      && isset($aBodyRequest['resp_id'])
      && (int)$aBodyRequest['resp_id'] > 1
      && ($cResp = cCont::mUnserialize($this->cDb, (int)$aBodyRequest['resp_id']))
      && $cResp->iQueue() > 1){
        $cObj = new cChain();
        
        //creator
        if($cResp->iID() !== $this->cCont->iID()){
          $cObj->Creator($this->cCont->iID());
        }
        //responsible & queue
        $cObj->Updater($cResp);
        
        if(isset($aBodyRequest['sbj'])
        && strlen($aBodyRequest['sbj']) > 0){
          $cObj->Subject($aBodyRequest['sbj']);
        }
        if(isset($aBodyRequest['cont_id'])
        && ($cCont = cCont::mUnserialize($this->cDb, (int)$aBodyRequest['cont_id']))){
          $cObj->Cont($cCont);
        }
        
        if($cEvent->sPreview()){
          $cObj->Preview($cEvent->sPreview());
        }
        
        if($cObj->bSerialize($this->cDb)){
          
          if(isset($aBodyRequest['slugs'])
          && count($aBodyRequest['slugs']) > 0){
            $cObj->bSerializeTag($this->cDb, $aBodyRequest['slugs'], false);
          }
          
          $cEvent->Chain($cObj->iID());
          //создаем для себя? - уже читали!
          if($cResp->iID() === $this->cCont->iID()){$cEvent->Processed();}
          $cEvent->bSerialize($this->cDb);
          
          $cLog = new cLog();
          $cLog->Value('REST: Chain ' . $cObj->iID() . ' created by ' . $this->cCont->sLogin());
          $cLog->bSerialize($this->cDb);
          unset($cLog);
          
          return WTS_HTTP_202;
        }
      }
      return WTS_HTTP_400;
    }
    
    protected function sPut(&$aRestRequest, &$aBodyRequest){
      if($this->cCont->iQueue()
      && isset($aRestRequest[1])
      && ($cObj = cChain::mUnserialize($this->cDb, (int)$aRestRequest[1]))
      && ($this->cCont->iType() === cCont::ADMIN
        || ($this->cCont->iID() === $cObj->iUpdater()
        && $cObj->bClosed() === false))){
        if(isset($aBodyRequest['sbj'])){
          $cObj->Subject($aBodyRequest['sbj']);
        }
        if(isset($aBodyRequest['cont_id'])
        && ($cCont = cCont::mUnserialize($this->cDb, (int)$aBodyRequest['cont_id']))){
          $cObj->Cont($cCont);
        }

        if(isset($aBodyRequest['resp_id'])
        && ($cResp = cCont::cUnserialize($this->cDb, (int)$aBodyRequest['resp_id']))){
          $cObj->Updater($cResp);
        }
        
        if(isset($aBodyRequest['close'])
        && $aBodyRequest['close'] === 'true'){
          $cObj->Close();
        }
        
        if($cObj->bSerialize($this->cDb, $this->cCont->iType() === cCont::AGENT)){
          $cLog = new cLog();
          $cLog->Value('REST: Chain ' . $cObj->iID() . ' updated by ' . $this->cCont->sLogin());
          $cLog->bSerialize($this->cDb);
          unset($cLog);
        }
        
        if(!isset($aBodyRequest['slugs'])){$aBodyRequest['slugs'] = array();}
        if($cObj->bSerializeTag($this->cDb, $aBodyRequest['slugs'], $this->cCont->iType() === cCont::AGENT)){
          $cLog = new cLog();
          $cLog->Value('REST: Tags for Chain ' . $cObj->iID() . ' updated by ' . $this->cCont->sLogin());
          $cLog->bSerialize($this->cDb);
          unset($cLog);
        }
        
        unset($cObj);
        return WTS_HTTP_200;
      }
      return WTS_HTTP_400;
    }
  }

  /**
   * groups
   */
  class cRestGroups extends cRest{
    public function sParse(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;    
      switch($_SERVER['REQUEST_METHOD']){
        case 'GET':  $sRet = $this->sGet($aRestRequest, $aBodyRequest);
        break;
        case 'POST': $sRet = $this->sPost($aRestRequest, $aBodyRequest);
        break;
        case 'PUT':  $sRet = $this->sPut($aRestRequest, $aBodyRequest);
        break;
        default: break;
      }
      return $sRet;
    }
    
    protected function sGet(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;
      if(isset($aRestRequest[1])){
        switch($aRestRequest[1]){
          case 'attrs'://return types of attributes
            $aAttrTypes = false;
            if($iRows = cGroupAttrType::iUnserialize($this->cDb, $aAttrTypes)){
              foreach($aAttrTypes as &$c){
                $this->aResponse[$c->iID()] = array('name'     => $c->sName()
                                                  , 'multiple' => ($c->bMultiple() ? 'true' : 'false')
                                                  , 'color'    => $c->sColor(),);
              }
              unset($aAttrTypes);
            }
            $sRet = WTS_HTTP_200;
          break;
          case 'search':
            if(isset($aRestRequest[2])){
              cGroup::bAddFilterPartValue($this->cDb, $aRestRequest[2]);
              if($aIDs = cGroup::mIDs($this->cDb)){
                $this->aResponse = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
          case 'updated':
            if(isset($aRestRequest[2])){
              cGroup::bAddFilterUpdated($aRestRequest[2]);
              if($aIDs = cGroup::mIDs($this->cDb)){
                $this->aResponse = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
          default://try get group
            $cObj = cGroup::mUnserialize($this->cDb, (int)$aRestRequest[1]);
            if($cObj){
              $this->aResponse['name']        = $cObj->sName();
              $this->aResponse['creator_id']  = $cObj->iCreator();
              $this->aResponse['dt_creation'] = $cObj->iDtCreated();
              $this->aResponse['modifier_id'] = $cObj->iUpdater();
              $this->aResponse['dt_modify']   = $cObj->iDtUpdated();
              $aAttrView = false;
              $cObj->iUnserializeAttr($this->cDb, $aAttrView);
              if($aAttrView){
                foreach($aAttrView as &$c){
                  $this->aResponse['attrs'][] = array('name'    => $c->sName()
                                                    , 'color'   => $c->sColor()
                                                    , 'id'      => $c->iID()
                                                    , 'type_id' => $c->iType()
                                                    , 'value'   => $c->sValue(),);
                }
                unset($aAttrView);
              }
              $sRet = WTS_HTTP_200;
            }
          break;
        }
      }
      return $sRet;
    }
    
    protected function sPost(&$aRestRequest, &$aBodyRequest){
      if(isset($aBodyRequest['name'])){
        $cObj = new cGroup();
        $cObj->Name($aBodyRequest['name']);
        $cObj->Creator($this->cCont->iID());
        if($cObj->bSerialize($this->cDb)){
          $cLog = new cLog();
          $cLog->Value('REST: Group ' . $cObj->sName() . ' created by ' . $this->cCont->sLogin());
          $cLog->bSerialize($this->cDb);
          unset($cLog);
          return WTS_HTTP_202;
        }
      }
      return WTS_HTTP_400;
    }
    
    protected function sPut(&$aRestRequest, &$aBodyRequest){
      if(isset($aRestRequest[1])
      && ($cObj = cGroup::mUnserialize($this->cDb, (int)$aRestRequest[1]))){
        
        $cObj->Updater($this->cCont->iID());
        
        if(isset($aBodyRequest['name'])){
          $sOldName = $cObj->sName();
          $cObj->Name($aBodyRequest['name']);
        
          if($cObj->bSerialize($this->cDb)){
            $cLog = new cLog();
            $cLog->Value('REST: Group ' . $sOldName . ' renamed to '
            . $cObj->sName() . ' by ' . $this->cCont->sLogin());
            $cLog->bSerialize($this->cDb);
            unset($cLog);
          }
        }
        
        $aAttrView = false;
        if(isset($aBodyRequest['attrs']) && count($aBodyRequest['attrs']) > 0){
          foreach($aBodyRequest['attrs'] as $a){
            if(isset($a['id'])
            && isset($a['type_id'])
            && isset($a['value'])
            && strlen($a['value']) > 0){
              $aAttrView[] = new cAttrView($a['id'], $a['type_id'], $a['value']);
            }
          }
        }
        if($cObj->bSerializeAttr($this->cDb, $aAttrView)){
          $cLog = new cLog();
          $cLog->Value('REST: Attributes for Group ' . $cObj->sName()
          . ' updated by ' . $this->cCont->sLogin());
          $cLog->bSerialize($this->cDb);
          unset($cLog);
        }
        unset($cObj);
        return WTS_HTTP_200;
      }
      return WTS_HTTP_400;
    }
  }

  /**
   * contacts
   */
  class cRestConts extends cRest{
    public function sParse(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;
      switch($_SERVER['REQUEST_METHOD']){
        case 'GET':  $sRet = $this->sGet($aRestRequest, $aBodyRequest);
        break;
        case 'POST': $sRet = $this->sPost($aRestRequest, $aBodyRequest);
        break;
        case 'PUT':  $sRet = $this->sPut($aRestRequest, $aBodyRequest);
        break;
        default: break;
      }
      return $sRet;
    }
    
    protected function sGet(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;
      if(isset($aRestRequest[1])){
        switch($aRestRequest[1]){
          case 'attrs'://return types of attributes
            $aAttrTypes = false;
            if($iRows = cContAttrType::iUnserialize($this->cDb, $aAttrTypes)){
              foreach($aAttrTypes as &$c){
                $this->aResponse[$c->iID()] = array('name'     => $c->sName()
                                                  , 'multiple' => ($c->bMultiple() ? 'true' : 'false')
                                                  , 'color'    => $c->sColor()
                                                  , 'in_name'  => ($c->bInName() ? 'true' : 'false'),);
              }
              unset($aAttrTypes);
            }
            $sRet = WTS_HTTP_200;
          break;
          case 'search':
            if(isset($aRestRequest[2])){
              cCont::bAddFilterPartValue($this->cDb, $aRestRequest[2]);
              if($aIDs = cCont::mIDs($this->cDb)){
                $this->aResponse = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
          case 'updated':
            if(isset($aRestRequest[2])){
              cCont::bAddFilterUpdated($aRestRequest[2]);
              if($aIDs = cCont::mIDs($this->cDb)){
                $this->aResponse = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
          default://try get contact
            $cObj = cCont::mUnserialize($this->cDb, (int)$aRestRequest[1]);
            if($cObj){
              $this->aResponse['login']       = $cObj->sLogin();

              $this->aResponse['group_id']    = $cObj->iGroup();
              $this->aResponse['queue_id']    = $cObj->iQueue();

              $this->aResponse['disabled']    = ($cObj->bDisabled() ? 'true' : 'false');
              $this->aResponse['creator_id']  = $cObj->iCreator();
              $this->aResponse['dt_creation'] = $cObj->iDtCreated();
              $this->aResponse['modifier_id'] = $cObj->iUpdater();
              $this->aResponse['dt_modify']   = $cObj->iDtUpdated();
              $aAttrView = false;
              $cObj->iUnserializeAttr($this->cDb, $aAttrView);
              if($aAttrView){
                foreach($aAttrView as &$c){
                  $this->aResponse['attrs'][] = array('name'    => $c->sName()
                                                    , 'color'   => $c->sColor()
                                                    , 'id'      => $c->iID()
                                                    , 'type_id' => $c->iType()
                                                    , 'value'   => $c->sValue(),);
                }
                unset($aAttrView);
              }
              $sRet = WTS_HTTP_200;
            }
          break;
        }
      }
      return $sRet;
    }
    //можно потом добавить сохранение атрибутов и группы
    protected function sPost(&$aRestRequest, &$aBodyRequest){
      if(isset($aBodyRequest['login'])){
        $cObj = new cCont();
        $cObj->Login($aBodyRequest['login']);
        $cObj->Creator($this->cCont->iID());
        
        if(isset($aBodyRequest['group_id'])
        && ($cGroup = cGroup::mUnserialize($this->cDb, (int)$aBodyRequest['group_id']))){
          $cObj->Group($cGroup->iID());
          unset($cGroup);
        }
        
        if(isset($aBodyRequest['queue_id'])
        && $this->cCont->iType() === cCont::ADMIN //только админ может назначать очередь
        && ($cQueue = cQueue::mUnserialize($this->cDb, (int)$aBodyRequest['queue_id']))){
          $cObj->Queue($cQueue->iID());
          unset($cQueue);
        }

        if($cObj->bSerialize($this->cDb)){
          $cLog = new cLog();
          $cLog->Value('REST: Contact ' . $cObj->sLogin() . ' created by ' . $this->cCont->sLogin());
          $cLog->bSerialize($this->cDb);
          unset($cLog);
          return WTS_HTTP_202;
        }
      }
      return WTS_HTTP_400;
    }
    
    protected function sPut(&$aRestRequest, &$aBodyRequest){
      if(isset($aRestRequest[1])
      && ($cObj = cCont::cUnserialize($this->cDb, (int)$aRestRequest[1]))
      && ($cObj->iType() === cCont::CSTMR || $this->cCont->iType() === cCont::ADMIN)){
        
        $cObj->Updater($this->cCont->iID());
        
        if(isset($aBodyRequest['login'])){$cObj->Login($aBodyRequest['login']);}
        if(isset($aBodyRequest['group_id'])){
          if($cGroup = cGroup::mUnserialize($this->cDb, (int)$aBodyRequest['group_id'])){
            $cObj->Group($cGroup->iID());
            unset($cGroup);
          }
          else{$cObj->Group(0);}
          
        }
        if(isset($aBodyRequest['disabled'])){
          if($aBodyRequest['disabled'] === 'true'){$cObj->Disable();}
          else{$cObj->Enable();}
        }
        
        if($cObj->bSerialize($this->cDb)){
          $cLog = new cLog();
          $cLog->Value('REST: Contact ' . $cObj->sLogin() . ' updated by ' . $this->cCont->sLogin());
          $cLog->bSerialize($this->cDb);
          unset($cLog);
        }
        
        $aAttrView = false;
        if(isset($aBodyRequest['attrs']) && count($aBodyRequest['attrs']) > 0){
          foreach($aBodyRequest['attrs'] as $a){
            if(isset($a['id'])
            && isset($a['type_id'])
            && isset($a['value'])
            && strlen($a['value']) > 0){
              $aAttrView[] = new cAttrView($a['id'], $a['type_id'], $a['value']);
            }
          }
        }
        if($cObj->bSerializeAttr($this->cDb, $aAttrView)){
          $cLog = new cLog();
          $cLog->Value('REST: Attributes for Contact ' . $cObj->sLogin()
          . ' updated by ' . $this->cCont->sLogin());
          $cLog->bSerialize($this->cDb);
          unset($cLog);
        }
        unset($cObj);
        return WTS_HTTP_200;
      }
      return WTS_HTTP_400;
    }
  }

  /**
   * contrs
   */
  class cRestContrs extends cRest{
    public function sParse(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;    
      switch($_SERVER['REQUEST_METHOD']){
        case 'GET':    $sRet = $this->sGet($aRestRequest, $aBodyRequest);
        break;
        case 'POST':   $sRet = $this->sPost($aRestRequest, $aBodyRequest);
        break;
        case 'PUT':    $sRet = $this->sPut($aRestRequest, $aBodyRequest);
        break;
        case 'DELETE': $sRet = $this->sDelete($aRestRequest, $aBodyRequest);
        break;
        default: break;
      }
      return $sRet;
    }
    
    protected function sGet(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;
      if(isset($aRestRequest[1])){
        switch($aRestRequest[1]){
          case 'search':
            if(isset($aRestRequest[2])){
              cContract::bAddFilterPartValue($this->cDb, $aRestRequest[2]);
              if($aIDs = cContract::mIDs($this->cDb)){
                $this->aResponse = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
          case 'updated':
            if(isset($aRestRequest[2])){
              cContract::bAddFilterUpdated($aRestRequest[2]);
              if($aIDs = cContract::mIDs($this->cDb)){
                $this->aResponse = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
          default://try get contract
            if($cObj = cContract::mUnserialize($this->cDb, $aRestRequest[1])){

              $this->aResponse['seller_type'] = ($cObj->iSellerType() === cContract::CONTACT ? 'cont' : 'group');
              $this->aResponse['seller_id']   = $cObj->iSeller();
             
              $this->aResponse['buyer_type']  = ($cObj->iBuyerType() === cContract::CONTACT ? 'cont' : 'group');
              $this->aResponse['buyer_id']    = $cObj->iBuyer();
              
              $this->aResponse['subject']     = $cObj->sSubject();
              $this->aResponse['cost']        = $cObj->fCost();

              $this->aResponse['dt_begin']    = $cObj->iDtBegin();
              $this->aResponse['dt_end']      = $cObj->iDtEnd();
              
              $this->aResponse['creator_id']  = $cObj->iCreator();
              $this->aResponse['dt_creation'] = $cObj->iDtCreated();
              $this->aResponse['modifier_id'] = $cObj->iUpdater();
              $this->aResponse['dt_modify']   = $cObj->iDtUpdated();
              
              $aTagViews = false;
              $aContracts = array($cObj);
              if(cContract::iUnserializeTags($this->cDb, $aContracts, $aTagViews) > 0){
                $aSlugs = false;
                foreach($aTagViews as $key => &$cTagView){
                  $aSlugs[] = $cTagView->sSlug();
                  unset($aTagViews[$key]);
                }
                $this->aResponse['slugs'] = $aSlugs;
              }
              
              if($cObj->iUnserializeAttach($this->cDb) > 0){
                $aAttachs = $cObj->aAttach();
                foreach($aAttachs as $a){
                  $this->aResponse['attachments'][] = array('name' => $a[FN_S_NM]
                                                          , 'cid'  => $a[FN_S_CID]
                                                          , 'file' => $cObj->mUnserializeAttach($this->cDb, $a[FN_I_ID]),);
                }
              }

              cProduct::bAddFilterContract($this->cDb, $cObj->iID());
              if($aIDs = cProduct::mIDs($this->cDb)){
                $this->aResponse['prod_ids'] = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
        }
      }
      return $sRet;
    }
    
    protected function sPost(&$aRestRequest, &$aBodyRequest){
      if($this->cCont->iQueue()
      && isset($aBodyRequest['seller_type'])
      && ($aBodyRequest['seller_type'] == 'cont' || $aBodyRequest['seller_type'] == 'group')
      && isset($aBodyRequest['seller_id'])
      && (int)$aBodyRequest['seller_id'] > 1
      && isset($aBodyRequest['buyer_type'])
      && ($aBodyRequest['buyer_type'] == 'cont' || $aBodyRequest['buyer_type'] == 'group')
      && isset($aBodyRequest['buyer_id'])
      && (int)$aBodyRequest['buyer_id'] > 1){
        
        $cObj = new cContract();
        
        //creator
        $cObj->Creator($this->cCont->iID());
        
        if($aBodyRequest['seller_type'] == 'cont'){
          if($cSeller = cCont::mUnserialize($this->cDb, (int)$aBodyRequest['seller_id'])){
            $cObj->Seller($cSeller->iID(), cContract::CONTACT);
            unset($cSeller);
          }
        }
        else{
          if($cSeller = cGroup::mUnserialize($this->cDb, (int)$aBodyRequest['seller_id'])){
            $cObj->Seller($cSeller->iID(), cContract::GROUP);
            unset($cSeller);
          }
        }
        
        if($aBodyRequest['buyer_type'] == 'cont'){
          if($cBuyer = cCont::mUnserialize($this->cDb, (int)$aBodyRequest['buyer_id'])){
            $cObj->Buyer($cBuyer->iID(), cContract::CONTACT);
            unset($cBuyer);
          }
        }
        else{
          if($cBuyer = cGroup::mUnserialize($this->cDb, (int)$aBodyRequest['buyer_id'])){
            $cObj->Buyer($cBuyer->iID(), cContract::GROUP);
            unset($cBuyer);
          }
        }
        
        if(isset($aBodyRequest['sbj'])
        && strlen($aBodyRequest['sbj']) > 0){
          $cObj->Subject($aBodyRequest['sbj']);
        }
        
        if(isset($aBodyRequest['cost'])){
          $cObj->Cost($aBodyRequest['cost']);
        }
        
        //dates
        if(isset($aBodyRequest['dt_begin'])){$cObj->DtBegin($aBodyRequest['dt_begin']);}
        else{$cObj->DtBegin(strtotime('now'));}
        if(isset($aBodyRequest['dt_end'])){$cObj->DtEnd($aBodyRequest['dt_end']);}
        else{$cObj->DtEnd(strtotime('now'));}
        
        if(isset($aBodyRequest['attachments'])
        && count($aBodyRequest['attachments']) > 0){
          foreach($aBodyRequest['attachments'] as &$a){
            if(isset($a['name']) && strlen($a['name']) > 0
            && isset($a['file']) && strlen($a['file']) > 0){
              $sCid  = cString::sRand(12) . '-' . WTS_DB_NAME
                . '-' . date(WTS_DT_BACKUP_FMT, strtotime('now'))
                . '-' . $this->cCont->sLogin();
              $cObj->AddAttach($a['file'], $a['name'], $sCid);
            }
          }
        }
        
        if($cObj->bSerialize($this->cDb)){
          
          if(isset($aBodyRequest['slugs'])
          && count($aBodyRequest['slugs']) > 0){
            $cObj->bSerializeTag($this->cDb, $aBodyRequest['slugs'], false);
          }

          $cLog = new cLog();
          $cLog->Value('REST: Contract ' . $cObj->iID() . ' created by ' . $this->cCont->sLogin());
          $cLog->bSerialize($this->cDb);
          unset($cLog);
          
          return WTS_HTTP_202;
        }
      }
      return WTS_HTTP_400;
    }
    
    protected function sPut(&$aRestRequest, &$aBodyRequest){
      if($this->cCont->iQueue()
      && isset($aRestRequest[1])
      && ($cObj = cContract::mUnserialize($this->cDb, (int)$aRestRequest[1]))){
        
        $cObj->Updater($this->cCont->iID());
        
        if(isset($aBodyRequest['sbj'])){
          $cObj->Subject($aBodyRequest['sbj']);
        }
        
        if(isset($aBodyRequest['cost'])){
          $cObj->Cost($aBodyRequest['cost']);
        }
        
        if(isset($aBodyRequest['attachments'])
        && count($aBodyRequest['attachments']) > 0){
          $cObj->iUnserializeAttach($this->cDb);
          foreach($aBodyRequest['attachments'] as &$a){
            if(isset($a['name']) && strlen($a['name']) > 0
            && isset($a['file']) && strlen($a['file']) > 0){
              $sCid  = cString::sRand(12) . '-' . WTS_DB_NAME
                . '-' . date(WTS_DT_BACKUP_FMT, strtotime('now'))
                . '-' . $this->cCont->sLogin();
              $cObj->AddAttach($a['file'], $a['name'], $sCid);
            }
          }
        }
        
        if($cObj->bSerialize($this->cDb, WTS_EVENT_THUMB_SIDE)){
          $cLog = new cLog();
          $cLog->Value('REST: Contract ' . $cObj->iID() . ' updated by ' . $this->cCont->sLogin());
          $cLog->bSerialize($this->cDb);
          unset($cLog);
        }
        
        //tags        
        if(!isset($aBodyRequest['slugs'])){$aBodyRequest['slugs'] = array();}
        if($cObj->bSerializeTag($this->cDb, $aBodyRequest['slugs'])){
          $cLog = new cLog();
          $cLog->Value('REST: Tags for Contract ' . $cObj->iID() . ' updated by ' . $this->cCont->sLogin());
          $cLog->bSerialize($this->cDb);
          unset($cLog);
        }
        
        unset($cObj);
        return WTS_HTTP_200;
      }
      return WTS_HTTP_400;
    }
    
    protected function sDelete(&$aRestRequest, &$aBodyRequest){
      if($this->cCont->iQueue()
      && isset($aRestRequest[1])
      && ($cObj = cContract::mUnserialize($this->cDb, $aRestRequest[1]))
      && cProduct::bAddFilterContract($this->cDb, $cObj->iID())
      && cProduct::iCount($this->cDb) === 0){

        $cLog = new cLog();
        $cLog->Value('REST: Delete empty Contract ' . $cObj->iID() . ' by ' . $this->cCont->sLogin());
        $cLog->bSerialize($this->cDb);
        unset($cLog);
        $cObj->bDelete($this->cDb);
        unset($cObj);
        $cObj = false;

        return WTS_HTTP_204;
      }
      return WTS_HTTP_400;
    }
  }

  /**
   * prods
   */
  class cRestProds extends cRest{
    public function sParse(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;    
      switch($_SERVER['REQUEST_METHOD']){
        case 'GET':  $sRet = $this->sGet($aRestRequest, $aBodyRequest);
        break;
        case 'POST': $sRet = $this->sPost($aRestRequest, $aBodyRequest);
        break;
        case 'PUT':  $sRet = $this->sPut($aRestRequest, $aBodyRequest);
        break;
        default: break;
      }
      return $sRet;
    }
    
    protected function sGet(&$aRestRequest, &$aBodyRequest){
      $sRet = WTS_HTTP_400;
      if(isset($aRestRequest[1])){
        switch($aRestRequest[1]){
          case 'attrs'://return types of attributes
            $aAttrTypes = false;
            if($iRows = cProdAttrType::iUnserialize($this->cDb, $aAttrTypes)){
              foreach($aAttrTypes as &$c){
                $this->aResponse[$c->iID()] = array('name'     => $c->sName()
                                                  , 'multiple' => ($c->bMultiple() ? 'true' : 'false')
                                                  , 'color'    => $c->sColor(),);
              }
              unset($aAttrTypes);
            }
            $sRet = WTS_HTTP_200;
          break;
          case 'cats'://return types of attributes
          
            $aCategories = false;
            if($sCurSearch === WTS_PARAM_ACT_CONTR
            && cCategory::iUnserialize($this->cDb, $aCategories) > 0){

              foreach($aCategories as &$c){
                $this->aResponse[$c->iID()] = array('parent_id' => $c->iParent()
                                                  , 'level'     => $c->iLevel()
                                                  , 'name'      => $c->sName()
                                                  , 'node'      => ($c->bIsNode() ? 'true' : 'false')
                                                  , 'color'     => $c->sColor(),);
              }
              unset($aAttrTypes);
            }
            $sRet = WTS_HTTP_200;
          break;
          case 'search':
            if(isset($aRestRequest[2])){
              cProduct::bAddFilterPartValue($this->cDb, $aRestRequest[2]);
              if($aIDs = cProduct::mIDs($this->cDb)){
                $this->aResponse = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
          case 'updated':
            if(isset($aRestRequest[2])){
              cProduct::bAddFilterUpdated($aRestRequest[2]);
              if($aIDs = cProduct::mIDs($this->cDb)){
                $this->aResponse = $aIDs;
              }
              $sRet = WTS_HTTP_200;
            }
          break;
          default://try get product
            $cObj = cProduct::mUnserialize($this->cDb, (int)$aRestRequest[1]);
            if($cObj){
              $this->aResponse['category_id']    = $cObj->iCategory();
              
              if($cObj->iHolderType() === cProduct::CONTACT){
                $this->aResponse['holder_type'] = 'cont';
              }
              elseif($cObj->iHolderType() === cProduct::GROUP){
                $this->aResponse['holder_type'] = 'group';
              }
              elseif($cObj->iHolderType() === cProduct::PRODUCT){
                $this->aResponse['holder_type'] = 'prod';
              }
              $this->aResponse['holder_id']   = $cObj->iHolder();
              
              $this->aResponse['dt_begin']    = $cObj->iDtBegin();
              $this->aResponse['dt_end']      = $cObj->iDtEnd();
              
              $this->aResponse['creator_id']  = $cObj->iCreator();
              $this->aResponse['dt_creation'] = $cObj->iDtCreated();
              $this->aResponse['modifier_id'] = $cObj->iUpdater();
              $this->aResponse['dt_modify']   = $cObj->iDtUpdated();
              $aAttrView = false;
              $cObj->iUnserializeAttr($this->cDb, $aAttrView);
              if($aAttrView){
                foreach($aAttrView as &$c){
                  $this->aResponse['attrs'][] = array('name'    => $c->sName()
                                                    , 'color'   => $c->sColor()
                                                    , 'id'      => $c->iID()
                                                    , 'type_id' => $c->iType()
                                                    , 'value'   => $c->sValue(),);
                }
                unset($aAttrView);
              }
              $sRet = WTS_HTTP_200;
            }
          break;
        }
      }
      return $sRet;
    }
    
    protected function sPost(&$aRestRequest, &$aBodyRequest){
      if(isset($aBodyRequest['category_id'])
      && ($cCategory = cCategory::mUnserialize($this->cDb, $aBodyRequest['category_id']))
      && $cCategory->bIsNode() === false
      && isset($aBodyRequest['contract_id'])
      && ($cContract = cContract::mUnserialize($this->cDb, $aBodyRequest['contract_id']))){
        
        $iCount = 1;
        if(isset($aBodyRequest['count'])
        && (int)$aBodyRequest['count'] > 1){
          $iCount = (int)$aBodyRequest['count'];
        }
        
        for($i = 0; $i < $iCount; $i++){
          $cObj = new cProduct($cCategory->iID());
          $cObj->Creator($this->cCont->iID());
          $cObj->Holder($cContract->iBuyer(), $cContract->iBuyerType());
          //dates
          $cObj->DtBegin($cContract->iDtBegin());
          $cObj->DtEnd($cContract->iDtEnd());
          
          if($cObj->bSerialize($this->cDb)){
            if(!$cContract->bAttach($this->cDb, $cObj)){
              $cObj->bDelete(cDb &$cDb);
              unset($cObj);
              $cObj = false;
              break;
            }
          }
          else{
            unset($cObj);
            $cObj = false;
            break;
          }
        }
        return WTS_HTTP_202;
      }
      return WTS_HTTP_400;
    }
    
    protected function sPut(&$aRestRequest, &$aBodyRequest){
      if(isset($aRestRequest[1])
      && ($cObj = cProduct::mUnserialize($this->cDb, (int)$aRestRequest[1]))){
        
        $cObj->Updater($this->cCont->iID());
        
        $aAttrView = false;
        if(isset($aBodyRequest['attrs']) && count($aBodyRequest['attrs']) > 0){
          foreach($aBodyRequest['attrs'] as $a){
            if(isset($a['id'])
            && isset($a['type_id'])
            && isset($a['value'])
            && strlen($a['value']) > 0){
              $aAttrView[] = new cAttrView($a['id'], $a['type_id'], $a['value']);
            }
          }
        }
        if($cObj->bSerializeAttr($this->cDb, $aAttrView)){
          $cLog = new cLog();
          $cLog->Value('REST: Attributes for Product ' . $cObj->iID()
          . ' updated by ' . $this->cCont->sLogin());
          $cLog->bSerialize($this->cDb);
          unset($cLog);
        }
        unset($cObj);
        return WTS_HTTP_200;
      }
      return WTS_HTTP_400;
    }

  }

}
?>
