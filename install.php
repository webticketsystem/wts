<?php
/*
 * install.php (part of WTS) - installation script
 * 
 * Copyright 2014-2017 wts support group <webticketsystem@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
namespace wts{
  
  require_once('site/include/defs.php');
  require_once('site/include/core/utils/db.php');
  require_once('site/include/core/scheme.php');
  require_once('site/include/core/log.php');
  require_once('site/include/core/msg.php');
  require_once('site/include/core/upload.php');
  require_once('site/include/core/report.php');
  
  require_once('site/include/core/tag.php');
  require_once('site/include/core/regexp.php');
  
  require_once('site/include/core/signature.php');
  require_once('site/include/core/response.php');
  
  require_once('site/include/core/event.php');
  require_once('site/include/core/chain.php');
  require_once('site/include/core/queue.php');
  
  require_once('site/include/core/contract.php');

  require_once('site/include/core/product.php');
  
  require_once('site/include/core/cont.php');
  require_once('site/include/core/group.php');

  $aMap['-t']['lkey'] = '--installation-type';
  $aMap['-t']['sum']  = 'values: complete OR scheme-only OR services-only';
  $aMap['-t']['val']  = null;

  $aMap['-u']['lkey'] = '--os-user-name';
  $aMap['-u']['sum']  = 'system user for WTS'
  . cString::EOL . '(must be represented as existing OS user)';
  $aMap['-u']['val']  = null;
  
  $aMap['-E']['lkey'] = '--admin-email';
  $aMap['-E']['sum']  = 'WTS admin login and email address for admin\'s queue'
  . cString::EOL . '(must be represented as existing email address)';
  $aMap['-E']['val']  = null;

  $aMap['-P']['lkey'] = '--admin-passwd';
  $aMap['-P']['sum']  = 'password for WTS admin'
  . cString::EOL . 'minimum lenght ' . WTS_PASSWD_LENGHT .  ' symbols';
  $aMap['-P']['val']  = null;
  
  $aMap['-e']['lkey'] = '--work-email';
  $aMap['-e']['sum']  = 'email address for work queue'
  . cString::EOL . '(must not match with admin login)';
  $aMap['-e']['val']  = null;

  $aMap['-p']['lkey'] = '--db-root-passwd';
  $aMap['-p']['sum']  = 'password for mysql root user';
  $aMap['-p']['val']  = null;

  $aMap['-n']['lkey'] = '--db-name';
  $aMap['-n']['sum']  = 'name for mysql database';
  $aMap['-n']['val']  = null;

  $aMap['-d']['lkey'] = '--dump-path';
  $aMap['-d']['sum']  = 'path to mysqldump'
  . cString::EOL . '(for example - \'/usr/bin\')';
  $aMap['-d']['val']  = null;

  $aMap['-a']['lkey'] = '--archive-path'; //backup
  $aMap['-a']['sum']  = 'path to backup files'
  . cString::EOL . '(this directory must be created and be empty)';
  $aMap['-a']['val']  = null;

  $aMap['-r']['lkey'] = '--www-root-path';
  $aMap['-r']['sum']  = 'path to web server root directory'
  . cString::EOL . '(for example - \'/usr/share/nginx/html\')';
  $aMap['-r']['val']  = null;

  $aMap['-N']['lkey'] = '--site-name';
  $aMap['-N']['sum']  = 'name for new site'
  . cString::EOL . '(for example - \'Bob\\\'s Cabinet\')';
  $aMap['-N']['val']  = null;
  
  $iType = false;

  if($argc > 1){ 
    for ($i = 1; $i < $argc; $i++){
      $sKey = $argv[$i];
      $sValue = null;
      if(isset($argv[$i + 1])){
        $i++;
        $sValue = $argv[$i];
      }
      else{
        if($sKey == '--help'){
          echo 'Usage: php -f install.php -- {[PARAMETER] [VALUE]}...' . cString::EOL;
          echo 'WTS installation script.'. cString::EOL;
          echo '* Should be executed with local administrator permissions for complete setup' . cString::EOL;
          echo '* Sequence of parameters must be observed' . cString::EOL;
          echo '* If [VALUE] is not given it\'s asked from the tty.' . cString::EOL . cString::EOL;
          echo 'Parameters:' . cString::EOL;
          foreach($aMap as $key => &$a){
            $s = str_replace(cString::EOL, cString::EOL . '                             ', $a['sum']);
            echo '  ' . $key . ', ' . str_pad($a['lkey'], 23) . $s . cString::EOL;
          }
          echo '      ' . str_pad('--help', 23) . 'without [VALUE] display this help and exit' . cString::EOL . cString::EOL;
          echo 'Report install.php bugs to webticketsystem@gmail.com' . cString::EOL;
          exit;
        }
        
        break;
      }

      foreach($aMap as $key => &$a){
        if($key == $sKey || $a['lkey'] == $sKey){
          switch($key){
            case '-t':
              if(strlen($sValue) > 1
              && ($sValue == 'complete' || $sValue == 'scheme-only' || $sValue == 'services-only')){
                //for scheme setup need precreated conf.php
                if($sValue == 'scheme-only'){
                  if(defined('WTS_DB_NAME')
                  && defined('WTS_DB_USER')
                  && cString::bIsEmail(WTS_DB_USER)
                  && defined('WTS_DB_PASS')){
                    $a['val'] = $sValue;
                    $iType = 20;
                  }
                }
                else{
                  $a['val'] = $sValue;
                  $iType = ($sValue == 'complete' ? 10 : 30);
                }
              }
            break;
            case '-a':
              if(strlen($sValue) > 1
              && file_exists($sValue)
              && isset($aMap['-u']['val'])){
                $userinfo = false;
                if(function_exists('posix_getpwuid')){
                  $userinfo = posix_getpwuid(fileowner($sValue));
                }
                else{$userinfo = array('name' => getenv('USERNAME'));}
                
                if($userinfo['name'] == $aMap['-u']['val']){
                  $a['val'] = $sValue;
                }
              }
            break;
            case '-r':
              if(file_exists($sValue)){$a['val'] = $sValue;}
            break;
            case '-d':
              $sDump = $sValue . '/mysqldump';
              if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){$sDump .= '.exe';}
              if(file_exists($sDump)){$a['val'] = $sValue;}
            break;
            case '-e':
            case '-E':
              if(cString::bIsEmail($sValue)){$a['val'] = $sValue;}
            break;
            case '-P':
              if(mb_strlen($sValue, WTS_ENC) >= WTS_PASSWD_LENGHT){
                $a['val']  = $sValue;
              }
            break;
            default:
              $a['val'] = $sValue;
            break;
          }
        }
      }
    }
  }

  echo 'install.php: WTS installation script' . cString::EOL;
  echo 'Try \'php -f install.php -- --help\' for more information.' . cString::EOL. cString::EOL;

  while(!$aMap['-t']['val']){
    echo 'Enter ' . $aMap['-t']['sum'] . ': ';
    $aMap['-t']['val'] = trim(fgets(STDIN));
    
    switch($aMap['-t']['val']){
      case 'complete':
        $iType = 10;
      break;
      case 'scheme-only':
        if(defined('WTS_DB_NAME') && defined('WTS_DB_USER') && cString::bIsEmail(WTS_DB_USER) && defined('WTS_DB_PASS')){
          $iType = 20;
          $aMap['-n']['val'] = WTS_DB_NAME;
          $aMap['-E']['val'] = WTS_DB_USER;
          $aMap['-P']['val'] = WTS_DB_PASS;
        }
        else{
          echo 'conf.php require for use scheme-only type.' . cString::EOL;
        }
      break;
      case 'services-only':
        $iType = 30;
      break;
      default: break;
    }
    
    if($iType === false){
      echo 'Bad type. Please Try again.' . cString::EOL;
      $aMap['-t']['val'] = null;
    }
  }
  
  if($iType === 10 || $iType === 30){
    while(!$aMap['-u']['val']){
      echo 'Enter ' . $aMap['-u']['sum'] . ': ';
      $aMap['-u']['val'] = trim(fgets(STDIN));
    }
  }
  
  if($iType === 10){
    while(!$aMap['-E']['val']){
      echo 'Enter ' . $aMap['-E']['sum'] . ': ';
      $aMap['-E']['val'] = trim(fgets(STDIN));
      if(!cString::bIsEmail($aMap['-E']['val'])){
        echo 'Bad e-mail address. Please Try again.' . cString::EOL;
        $aMap['-E']['val'] = null;
      }
    }

    while(!$aMap['-P']['val']){
      echo 'Enter ' . $aMap['-P']['sum'] . ': ';
      $aMap['-P']['val'] = trim(fgets(STDIN));
      if(mb_strlen($aMap['-P']['val'], WTS_ENC) < WTS_PASSWD_LENGHT){
        echo 'Password minimum lenght ' . WTS_PASSWD_LENGHT .  ' symbols. Please Try again.' . cString::EOL;
        $aMap['-P']['val'] = null;
      }
    }
  }
    
  if($iType === 10 || $iType === 20){
    while(!$aMap['-e']['val']){
      echo 'Enter ' . $aMap['-e']['sum'] . ': ';
      $aMap['-e']['val'] = trim(fgets(STDIN));
      if(!cString::bIsEmail($aMap['-e']['val'])){
        echo 'Bad e-mail address. Please Try again.' . cString::EOL;
        $aMap['-e']['val'] = null;
      }
    }
  }

  if($iType === 10){
    while(!$aMap['-p']['val']){
      echo 'Enter ' . $aMap['-p']['sum'] . ': ';
      $aMap['-p']['val'] = trim(fgets(STDIN));
    }

    while(!$aMap['-n']['val']){
      echo 'Enter ' . $aMap['-n']['sum'] . ': ';
      $aMap['-n']['val'] = trim(fgets(STDIN));
    }
  }

  if($iType === 10 || $iType === 30){
    while(!$aMap['-d']['val']){
      echo 'Enter ' . $aMap['-d']['sum'] . ': ';
      $aMap['-d']['val'] = trim(fgets(STDIN));
      $sValue = $aMap['-d']['val'] . '/mysqldump';
      if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){$sValue .= '.exe';}
      if(!file_exists($sValue)){
        echo 'Bad mysqldump path. Please Try again.' . cString::EOL;
        $aMap['-d']['val'] = null;
      }
    }
  
    while(!$aMap['-a']['val']){
      echo 'Enter ' . $aMap['-a']['sum'] . ': ';
      $aMap['-a']['val'] = trim(fgets(STDIN));
      if(strlen($aMap['-a']['val']) < 2 || !file_exists($aMap['-a']['val'])){
        echo 'Bad path. Please Try again.' . cString::EOL;
        $aMap['-a']['val'] = null;
      }
      else{
        $userinfo = false;
        if(function_exists('posix_getpwuid')){
          $userinfo = posix_getpwuid(fileowner($aMap['-a']['val']));
        }
        else{
          //so far do not test owner for backup path on Windows
          $userinfo = array('name' => getenv('USERNAME'));
        }
        if($userinfo['name'] != $aMap['-u']['val']){
          echo 'User \'' . $aMap['-u']['val'] . '\' must be owner for this directory. Please Try again.' . cString::EOL;
          $aMap['-a']['val'] = null;
        }
      }
    }
  }

  if($iType === 10){
    while(!$aMap['-r']['val']){
      echo 'Enter ' . $aMap['-r']['sum'] . ': ';
      $aMap['-r']['val'] = trim(fgets(STDIN));
      if(!file_exists($aMap['-r']['val'])){
        echo 'Bad path. Please Try again.' . cString::EOL;
        $aMap['-r']['val'] = null;
      }
    }

    while(!$aMap['-N']['val']){
      echo 'Enter ' . $aMap['-N']['sum'] . ': ';
      $aMap['-N']['val'] = trim(fgets(STDIN));
    }
  }

  $сontinue = false;

  while($сontinue === false){
    echo 'Setup will use next parameters:'. cString::EOL;
    foreach($aMap as &$a){
      if($a['val']){echo str_pad($a['lkey'], 23) . $a['val'] . cString::EOL;}
    }
    echo 'Continue [yes/no]: ';
    $сontinue = strtolower(trim(fgets(STDIN)));
    if($сontinue == 'y' || $сontinue == 'yes'){
      echo cString::EOL . 'Prepare setup...OK' . cString::EOL;
    }
    else{
      echo cString::EOL . 'Exit...OK' . cString::EOL;
      exit;
    }
  }

  try{

    echo 'Check PHP Settings...';
    
    if(version_compare(PHP_VERSION, WTS_PHP_VERSION_MIN, '<')){
      echo cString::EOL . 'Requires PHP version ' . WTS_PHP_VERSION_MIN . ' or higher. Your version:' . PHP_VERSION . cString::EOL;
      exit;
    }
    else{
      echo cString::EOL . 'PHP version ' . PHP_VERSION . ' ';
    }
    
    if(WTS_MAX_PKG_SIZE > cString::iStringToBytes(ini_get('memory_limit'))){
      throw new \Exception('Please setup memory_limit more or equal WTS_MAX_PKG_SIZE in php.ini (see in ...<WTSROOT>/site/include/defs.php)', 5000);
    }
    if(WTS_MAX_PKG_SIZE > cString::iStringToBytes(ini_get('post_max_size'))){
      throw new \Exception('Please setup post_max_size more or equal WTS_MAX_PKG_SIZE in php.ini (see in ...<WTSROOT>/site/include/defs.php)', 5001);
    }
    if(WTS_MAX_PKG_SIZE > cString::iStringToBytes(ini_get('upload_max_filesize'))){
      throw new \Exception('Please setup upload_max_filesize more or equal WTS_MAX_PKG_SIZE in php.ini (see in ...<WTSROOT>/site/include/defs.php)', 5002);
    }

    echo 'OK' . cString::EOL;

    if($iType === 10){
    /**
     * Files
     */
      echo 'Manage Files...';

      $sDir = dirname(__FILE__) . '/site';
      
      if(!file_exists($aMap['-r']['val'] . '/' . $aMap['-n']['val'])){
        if(!@symlink($sDir, $aMap['-r']['val'] . '/' . $aMap['-n']['val'])){
          throw new \Exception('Couldn\'t create symlink. This script should be executed with local administrator permissions.', 5003);
        }
      }
      
      $sDir .= '/img';
      copy($sDir . '/default/logo.ico', $sDir . '/logo.ico');
      chown($sDir . '/logo.ico', $aMap['-u']['val']);
      copy($sDir . '/default/logo.png', $sDir . '/logo.png');
      chown($sDir . '/logo.png', $aMap['-u']['val']);
      copy($sDir . '/default/bg.png', $sDir . '/bg.png');
      chown($sDir . '/bg.png', $aMap['-u']['val']);

      
      $sFile = dirname(__FILE__) . '/site/include/conf.php';

      $cStream = fopen($sFile, 'w');
      if($cStream !== false){
        fwrite($cStream, '<?php' . cString::EOL);
        fwrite($cStream, 'namespace wts;' . cString::EOL . cString::EOL);
        fwrite($cStream, 'define(\'WTS_TITLE\',             \'' . $aMap['-N']['val'] . '\');' . cString::EOL);
        fwrite($cStream, 'define(\'WTS_DB_NAME\',           \'' . $aMap['-n']['val'] . '\');' . cString::EOL);
        fwrite($cStream, 'define(\'WTS_DB_USER\',           \'' . strtolower($aMap['-E']['val']) . '\');' . cString::EOL);
        fwrite($cStream, 'define(\'WTS_DB_PASS\',           \'' . $aMap['-P']['val'] . '\');' . cString::EOL);
        fwrite($cStream, cString::EOL . '?>' . cString::EOL);
        fclose($cStream);
        chown($sFile, $aMap['-u']['val']);
      }
  
      echo 'OK' . cString::EOL;
    }
    
    
    if($iType === 10 || $iType === 20){
    /**
     * Database
     */
      if($iType === 10){
        echo 'Check Database Settings...';
      
        $cDb = new cDb('', 'root', $aMap['-p']['val'], WTS_DB_TABLE_PREFIX);
        
        $s = $cDb->sQueryRes('show variables like \'max_allowed_packet\'');
        if(!isset($s) || cString::iStringToBytes($s) < WTS_MAX_PKG_SIZE){
          throw new \Exception('Please setup max_allowed_packet more or equal WTS_MAX_PKG_SIZE for MySQL (see in ...<WTSROOT>/site/include/defs.php)', 6008);
        }
      
        echo 'OK' . cString::EOL . 'Create Database...';
    
        $cDb->Query('drop database if exists ' . $aMap['-n']['val']);
        $cDb->Query('create database ' . $aMap['-n']['val'] . ' character set ' . WTS_DB_CHARSET . ' collate ' . WTS_DB_COLLATE);
        $cDb->Query('use ' . $aMap['-n']['val']);
        
        $aName = explode('@', $aMap['-E']['val']);
        $aName[0] = strtolower($aName[0]);
        $cDb->Query('grant all privileges on ' . $aMap['-n']['val'] . '.* to \'' . $aName[0] . '\'@\'localhost\' identified by \'' . $aMap['-P']['val'] . '\' with grant option');
        $cDb->Query('flush privileges');
        
        unset($cDb);
        
        echo 'OK' . cString::EOL;
      }
      
      echo 'Create Database Scheme...';
    
      $cDb = new cDb($aMap['-n']['val'], $aMap['-E']['val'], $aMap['-P']['val'], WTS_DB_TABLE_PREFIX);
      
      $cScheme = new cScheme();
      $cScheme->Install($cDb);
      
      $cScheme->Value('VER', WTS_DB_SCHEME);
      
      $s = date(WTS_DT_BACKUP_FMT, strtotime('now'));
      
      $cScheme->Value('EBT', $cDb->sTablePrefix() . 'evt_b_' . $s);
      $cScheme->Value('EAT', $cDb->sTablePrefix() . 'evt_a_' . $s);
      $cScheme->Value('CAT', $cDb->sTablePrefix() . 'ctr_a_' . $s);
      $cScheme->bSerialize($cDb);

      $cObj = new cLog();
      $cObj->Install($cDb);
      unset($cObj);
      $cObj = new cMsg();
      $cObj->Install($cDb);
      unset($cObj);
      $cObj = new cUpload();
      $cObj->Install($cDb);
      unset($cObj);
      
      $cObj = new cReport();
      $cObj->Install($cDb);
      unset($cObj);
      //несколько отчётов для примера
      $cObj = new cReport(cCont::CSTMR);
      $cObj->Name('My chains last 3 months');
      $cObj->Value('select ' . FN_I_ID . ' as \'Chain ID\', date_format('
      . FN_DT . ', \'%d %b %T\') as \'Date & Time\', '
      . FN_S_SBJ . ' as Subject from ' . $cDb->sTablePrefix() . 'chains where '
      . FN_I_CNT . '=' . cReport::RPL_EXP['CONT_ID'] . ' and '
      . FN_DT . ' > date_add(now(), interval -3 month) order by ' . FN_DT . ' desc');

      $cObj->bSerialize($cDb);
      unset($cObj);
      $cObj = new cReport(cCont::AGENT);
      $cObj->Name('My owned chains last 3 months');
      $cObj->Value('select ' . FN_I_ID . ' as \'Chain ID\', date_format('
      . FN_DT . ', \'%d %b %T\') as \'Date & Time\', '
      . FN_S_SBJ . ' as Subject from ' . $cDb->sTablePrefix() . 'chains where '
      . FN_I_OWN . '=' . cReport::RPL_EXP['CONT_ID'] . ' and '
      . FN_DT . ' > date_add(now(), interval -3 month) order by ' . FN_DT . ' desc');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cReport(cCont::AGENT);
      $cObj->Name('Events count by Quarters');
      $cObj->Value('select concat(year(' . FN_DT_B . '), \' Q\', quarter('
      . FN_DT_B . ')) as Date, count(*) as Count from ' . $cDb->sTablePrefix() . 'events where '
      . FN_I_QUE . '=' . cReport::RPL_EXP['QUEUE_ID'] . ' group by year('
      . FN_DT_B . '), quarter(' . FN_DT_B . ')');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cReport(cCont::AGENT);
      $cObj->Name('Chains count by Quarters');
      $cObj->Value('select concat(year(' . FN_DT . '), \' Q\', quarter('
      . FN_DT . ')) as Date, count(*) as Count from ' . $cDb->sTablePrefix() . 'chains where '
      . FN_I_QUE . '=' . cReport::RPL_EXP['QUEUE_ID'] . ' group by year('
      . FN_DT . '), quarter(' . FN_DT . ')');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cReport(cCont::AGENT);
      $cObj->Name('Chains count by Tags by Quarters');
      $cObj->Value('select concat(year(' . FN_DT . '), \' Q\', quarter('
      . FN_DT . ')) as Date, ' . $cDb->sTablePrefix() . 'tags.' . FN_S_NM
      . ' as Tag, count(*) as Count from ' . $cDb->sTablePrefix() . 'chains left join '
      . $cDb->sTablePrefix() . 'chains_tags on (' . $cDb->sTablePrefix() . 'chains.'
      . FN_I_ID . '=' . $cDb->sTablePrefix() . 'chains_tags.' . FN_I_PRT . ') left join '
      . $cDb->sTablePrefix() . 'tags on (' . $cDb->sTablePrefix() . 'chains_tags.'
      . FN_I_TAG . '=' . $cDb->sTablePrefix() . 'tags.' . FN_I_ID . ') where '
      . FN_I_QUE . '=' . cReport::RPL_EXP['QUEUE_ID'] . ' group by year('
      . FN_DT . '), quarter(' . FN_DT . '), ' . $cDb->sTablePrefix() . 'tags.' . FN_S_NM);
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cReport(cCont::AGENT);
      $cObj->Name('New Contacts count by Quarters');
      $cObj->Value('select concat(year(' . FN_DT . '), \' Q\', quarter('
      . FN_DT . ')) as Date, count(*) as Count from ' . $cDb->sTablePrefix() . 'conts group by year('
      . FN_DT . '), quarter(' . FN_DT . ')');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cReport(cCont::AGENT);
      $cObj->Name('New Groups count by Quarters');
      $cObj->Value('select concat(year(' . FN_DT . '), \' Q\', quarter('
      . FN_DT . ')) as Date, count(*) as Count from ' . $cDb->sTablePrefix() . 'groups group by year('
      . FN_DT . '), quarter(' . FN_DT . ')');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cReport(cCont::AGENT);
      $cObj->Name('Contracts count by Quarters');
      $cObj->Value('select concat(year(' . FN_DT . '), \' Q\', quarter(' . FN_DT
      . ')) as Date, count(*) as Count, format(sum(' . FN_I_CST . '/100.0),2) as Cost from '
      . $cDb->sTablePrefix() . 'contracts group by year(' . FN_DT . '), quarter(' . FN_DT . ')');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cReport(cCont::AGENT);
      $cObj->Name('Contracts count by Tags by Quarters');
      $cObj->Value('select concat(year(' . FN_DT . '), \' Q\', quarter('
      . FN_DT . ')) as Date, ' . $cDb->sTablePrefix() . 'tags.' . FN_S_NM
      . ' as Tag, count(*) as Count, format(sum(' . FN_I_CST . '/100.0),2) as Cost from '
      . $cDb->sTablePrefix() . 'contracts left join '
      . $cDb->sTablePrefix() . 'contracts_tags on (' . $cDb->sTablePrefix() . 'contracts.'
      . FN_I_ID . '=' . $cDb->sTablePrefix() . 'contracts_tags.' . FN_I_PRT . ') left join '
      . $cDb->sTablePrefix() . 'tags on (' . $cDb->sTablePrefix() . 'contracts_tags.'
      . FN_I_TAG . '=' . $cDb->sTablePrefix() . 'tags.' . FN_I_ID . ') group by year('
      . FN_DT . '), quarter(' . FN_DT . '), ' . $cDb->sTablePrefix() . 'tags.' . FN_S_NM);
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      //цепочки - очереди
      $cObj = new cTag();
      $cObj->Install($cDb);
      
      $cObj->Name('Inspection');
      $cObj->Slug('inspection');
      $cObj->Color('cdeeff');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cTag();
      $cObj->Name('Service');
      $cObj->Slug('service');
      $cObj->Color('fffacd');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cTag();
      $cObj->Name('Repairs');
      $cObj->Slug('repairs');
      $cObj->Color('daffcd');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cTag();
      $cObj->Name('Regular');
      $cObj->Slug('regular');
      $cObj->Color('ffffff');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cTag();
      $cObj->Name('Critical');
      $cObj->Slug('critical');
      $cObj->Color('ffcdcd');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cRegExp(2);
      $cObj->Install($cDb);
      $cObj->Name('Your tickets...');
      $cObj->Value('<br>
<div>Your tickets are ready for you to print.</div>
<br>');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cRegExp(2);
      $cObj->Name('Your application...');
      $cObj->Value('<br>
<div>Thank you for your application, which we have accepted.</div>
<br>');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cSalutation();
      $cObj->Install($cDb);
      
      $cObj->Name('Standard Salutation');
      $cObj->Value('<div>Hello ' . cResponse::RPL_EXP['CSRMR_NAME'] . ',</div>
<br>');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cSignature();
      $cObj->Install($cDb);
      $cObj->Name('Standard Signature');
      $cObj->Value('<br>
<div>Organisation</div>
<div>Address</div>
<div>City Zip-Code Country</div>
<div>☎ +7 777 777 77 77</div>
<div>✉ ' . cResponse::RPL_EXP['QUEUE_EMAIL'] . '</div>
<div>☛ http://www.' . gethostname() . '</div>');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cResponse();//1
      $cObj->Install($cDb);
      
      $cObj->Salutation(1);
      $cObj->Signature(1);
      $cObj->Name('Restore Password');
      $cObj->Value('<br>
<div><hr noshade size="1"></div>
<div>New password for website ' . cResponse::RPL_EXP['SITE_TITLE'] . ': <b>' . cResponse::RPL_EXP['NEW_PASSWD'] . '</b></div>
<div><hr noshade size="1"></div>
<br>
<div><font color="red">If you did not request a password restore, please notify us by e-mail.</font></div>
<br>
<div>Best Regards, Site Administrator.</div>
<br>');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cResponse();//2
      $cObj->Salutation(1);
      $cObj->Signature(1);
      $cObj->Name('Standard Email');
      $cObj->Value('<br>
<div>For appeal, on the same issue, please do not change the subject line.</div>
<div><hr noshade size="1"></div>
<div>Number your chain: #' . cResponse::RPL_EXP['CUR_CHAIN'] . '</div>
<div><hr noshade size="1"></div>
<br>
<div>You wrote:</div>
<div style="border-left: 3px solid blue; padding-left: 5px; overflow: hidden;"><br>' . cResponse::RPL_EXP['FWD_MESSAGE'] . '<br><br></div>
<br>
<div>Best Regards, ' . cResponse::RPL_EXP['AGENT_NAME'] . '.</div>
<br>');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cResponse();//3
      $cObj->Name('Standard Phone Call');
      $cObj->Value('<br>
<div><hr noshade size="1"></div>
<div>Companion:</div>
<div><hr noshade size="1"></div>
<br>
<div>Issues discussed at the dialogue:</div>
<br>
<div>Results:</div>
<br>
<div>Created by ' . cResponse::RPL_EXP['AGENT_NAME'] . '.</div>
<br>');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cResponse();//4
      $cObj->Name('Standard Meeting');
      $cObj->Value('<br>
<div><hr noshade size="1"></div>
<div>Participants:</div>
<div><hr noshade size="1"></div>
<br>
<div>Issues discussed at the meeting:</div>
<br>
<div>Results:</div>
<br>
<div>Created by ' . cResponse::RPL_EXP['AGENT_NAME'] . '.</div>
<br>');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cResponse();//5
      $cObj->Salutation(1);
      $cObj->Signature(1);
      $cObj->Name('Standard Forward');
      $cObj->Value('<br>
<div>We forwarded this message to You, so we hope that You will be able to answer it better.</b></div>
<br>
<div>--- Begin forwarded message ---</div>
<div style="border-left: 3px solid blue; padding-left: 5px; overflow: hidden;"><br>' . cResponse::RPL_EXP['FWD_MESSAGE'] . '<br><br></div>
<div>--- End forwarded message ---</div>
<br>
<div>Best Regards, ' . cResponse::RPL_EXP['AGENT_NAME'] . '.</div>
<br>');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cEvent(0, 0);
      $cObj->Install($cDb, $cScheme->mValue('EBT'), $cScheme->mValue('EAT'));
     
      unset($cObj);
      
      $cObj = new cChain();
      $cObj->Install($cDb);
      unset($cObj);
      
      $cObj = new cQueue();
      $cObj->Install($cDb);

      //административная очередь
      $cObj->Login($aMap['-E']['val']);
      $cObj->Name(_('Admin\'s Queue'));
      $cObj->Passwd($aMap['-P']['val']);
      $cObj->bSerialize($cDb);
      $cObj->bSerializeResp($cDb, cQueue::PWD, 1);
      unset($cObj);
      
      if($aMap['-E']['val'] === $aMap['-e']['val']){
        $aMap['-e']['val'] = 'copy.' . $aMap['-e']['val'];
      }
      //рабочая очередь
      $cObj = new cQueue();
      $cObj->Login($aMap['-e']['val']);
      $cObj->bSerialize($cDb);
      $cObj->bSerializeResp($cDb, cQueue::EML, 2);
      $cObj->bSerializeResp($cDb, cQueue::PCL, 3);
      $cObj->bSerializeResp($cDb, cQueue::MTG, 4);
      $cObj->bSerializeResp($cDb, cQueue::FWD, 5);
      unset($cObj);
      
      //продукты - контракты
      $cObj = new cContract();
      $cObj->Install($cDb, $cScheme->mValue('CAT'));
      unset($cScheme);
      unset($cObj);
      
      $cObj = new cCategory(true);
      $cObj->Install($cDb);
      $cObj->Name('Bikes');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cCategory(true);
      $cObj->Name('Accessories');
      $cObj->Color('cdeeff');
      $cObj->Parent(1);
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cCategory(true);
      $cObj->Name('Parts');
      $cObj->Color('fffacd');
      $cObj->Parent(1);
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cCategory();
      $cObj->Name('Mountain Bike');
      $cObj->Color('daffcd');
      $cObj->Parent(1);
      $cObj->bSerialize($cDb);
      unset($cObj);
      $cObj = new cCategory();
      $cObj->Name('Classic Bike');
      $cObj->Color('ffcdcd');
      $cObj->Parent(1);
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cCategory();
      $cObj->Name('Flashlight');
      $cObj->Parent(2);
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cCategory();
      $cObj->Name('Chain');
      $cObj->Parent(3);
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cProdAttrType();
      $cObj->Install($cDb);
      $cObj->Name('Brand Name');
      $cObj->bSerialize($cDb);
      unset($cObj);
      $cObj = new cProdAttrType();
      $cObj->Name('Model');
      $cObj->bSerialize($cDb);
      unset($cObj);
      $cObj = new cProdAttrType();
      $cObj->Name('Serial Number');
      $cObj->bSerialize($cDb);
      unset($cObj);
      
      $cObj = new cProduct(0);
      $cObj->Install($cDb);
      unset($cObj);
      
      //контакты - группы
      $cObj = new cGroupAttrType();
      $cObj->Install($cDb);
      unset($cObj);

      $cObj = new cContAttrType();
      $cObj->Install($cDb);
      $cObj->InName(true);
      $cObj->Name('First Name');
      $cObj->bSerialize($cDb);
      unset($cObj);
      $cObj = new cContAttrType();
      $cObj->InName(true);
      $cObj->Name('Middle Name');
      $cObj->bSerialize($cDb);
      unset($cObj);
      $cObj = new cContAttrType();
      $cObj->InName(true);
      $cObj->Name('Last Name');
      $cObj->bSerialize($cDb);
      unset($cObj);
      $cObj = new cContAttrType(true);
      $cObj->Name('Phone');
      $cObj->bSerialize($cDb);
      unset($cObj);
      $cObj = new cContAttrType(true);
      $cObj->Name('Email');
      $cObj->bSerialize($cDb);
      unset($cObj);

      $cObj = new cGroup();
      $cObj->Install($cDb);
      unset($cObj);
      
      $cObj = new cCont();
      $cObj->Install($cDb);
      //сам администратор
      $cObj->Login($aMap['-E']['val']);
      $cObj->Creator(1);
      if($cObj->bSerialize($cDb)){
        if($sUID = cCont::mBeginSsn($cDb, $cObj->sLogin())){
          cCont::bPasswd($cDb, $sUID, $aMap['-P']['val']);
        }
      }
      //name for admin
      $aAttrView[] = new cAttrView(0, 1, 'Admin');
      $cObj->Updater(1);
      $cObj->bSerializeAttr($cDb, $aAttrView);
      unset($cObj);

      $cObj = new cLog();
      $cObj->Value('Database Scheme ' . WTS_DB_SCHEME . ' successfully installed');
      $cObj->bSerialize($cDb);
      unset($cObj);

      unset($cDb);

      echo 'OK' . cString::EOL;
    }
    
    if($iType === 10 || $iType === 30){
      echo 'Create Services ';
    /**
     * Services
     */
      if(strtoupper(substr(PHP_OS, 0, 3)) === 'WIN'){
        echo '(Schtasks)...';
        //раз в 15 минут [c - добавить  /st 08:00 /et 20:00]
        
        shell_exec('schtasks /delete /tn "WTS Get and Send e-mails for '
        . $aMap['-u']['val'] . '" /f');
        
        shell_exec('schtasks /create /sc minute /mo 15 /tn "WTS Get and Send e-mails for '
        . $aMap['-u']['val'] . '" /tr "cmd.exe /c cd ' . dirname(__FILE__)
        . '/cron && php.exe -f getsendmail.php" /ru "System"');
        //раз в 5 минут
        
        shell_exec('schtasks /delete /tn "WTS Remove old locks for '
        . $aMap['-u']['val'] . '" /f');
        
        shell_exec('schtasks /create /sc minute /mo 5 /tn "WTS Remove old locks for '
        . $aMap['-u']['val'] . '" /tr "cmd.exe /c cd ' . dirname(__FILE__)
        . '/cron && php.exe -f deloldlock.php" /ru "System"');
        //раз в 3 часа с отсрочкой в 10 минут
        
        shell_exec('schtasks /delete /tn "WTS Remove old sessions for '
        . $aMap['-u']['val'] . '" /f');
        
        shell_exec('schtasks /create /sc hourly /mo 3 /st 00:10 /tn "WTS Remove old sessions for '
        . $aMap['-u']['val'] . '" /tr "cmd.exe /c cd ' . dirname(__FILE__)
        . '/cron && php.exe -f deloldssn.php" /ru "System"');
        //раз в неделю в воскресенье в 3:00
        
        shell_exec('schtasks /delete /tn "WTS Remove old uploads for '
        . $aMap['-u']['val'] . '" /f');
        
        shell_exec('schtasks /create /sc weekly /d sun /st 03:00 /tn "WTS Remove old uploads for '
        . $aMap['-u']['val'] . '" /tr "cmd.exe /c cd ' . dirname(__FILE__)
        . '/cron && php.exe -f deluploads.php" /ru "System"');
        //раз в неделю в воскресенье в 4:00 - по умолчанию, только раз в неделю!!!
        
        shell_exec('schtasks /delete /tn "WTS Create backup for '
        . $aMap['-u']['val'] . '" /f');
        
        shell_exec('schtasks /create /sc weekly /d sun /st 04:00 /tn "WTS Create backup for '
        . $aMap['-u']['val'] . '" /tr "cmd.exe /c cd ' . dirname(__FILE__)
        . '/cron && php.exe -f backup.php -- -d \'' .$aMap['-d']['val'] . '\' -a \'' . $aMap['-a']['val'] .'\'" /ru "System"');
        //раз в месяц 1-го числа (/d 1 -by default) в 3:30
        
        shell_exec('schtasks /delete /tn "WTS Change tables for '
        . $aMap['-u']['val'] . '" /f');
        
        shell_exec('schtasks /create /sc monthly /st 03:30 /tn "WTS Change tables for '
        . $aMap['-u']['val'] . '" /tr "cmd.exe /c cd ' . dirname(__FILE__)
        . '/cron && php.exe -f tablesize.php" /ru "System"');
        
        echo 'OK' . cString::EOL . cString::EOL;
      }
      else{
        echo '(Cron Jobs)...';
        
        $sCurrentUser = shell_exec('whoami');

        if($sCurrentUser == $aMap['-u']['val']){
          shell_exec('crontab -r > /dev/null 2>&1');
        }
        else{
          shell_exec('crontab -u ' . $aMap['-u']['val'] . ' -r > /dev/null 2>&1');
        }
        $cStream = fopen(dirname(__FILE__) . '/cronjobs', 'w');
        if($cStream !== false){
          fwrite($cStream, 'SHELL=/bin/bash' . cString::EOL);
          fwrite($cStream, 'MAILTO=""' . cString::EOL);
          fwrite($cStream, cString::EOL . '#>>>wts jobs<<<' . cString::EOL . cString::EOL);
          
          //раз в 15 минут (c 8 утра до 8 вечера? - */15    8-20       *...)
          fwrite($cStream, '*/15    *       *       *       *       cd ' . dirname(__FILE__)
          . '/cron && php -f getsendmail.php > /dev/null 2>&1' . cString::EOL);
          //раз в 5 минут
          fwrite($cStream, '*/5     *       *       *       *       cd ' . dirname(__FILE__)
          . '/cron && php -f deloldlock.php > /dev/null 2>&1' . cString::EOL);
          //раз в 3 часа с отсрочкой в 10 минут
          fwrite($cStream, '10     */3      *       *       *       cd ' . dirname(__FILE__)
          . '/cron && php -f deloldssn.php > /dev/null 2>&1' . cString::EOL);
          //каждое воскресенье в 3:00
          fwrite($cStream, '0       3       *       *       7       cd ' . dirname(__FILE__)
          . '/cron && php -f deluploads.php > /dev/null 2>&1' . cString::EOL);
          //каждое воскресенье в 4:00 - по умолчанию, только раз в неделю!!!
          fwrite($cStream, '0       4       *       *       7       cd ' . dirname(__FILE__)
          . '/cron && php -f backup.php -- -d ' . $aMap['-d']['val'] . ' -a ' . $aMap['-a']['val']
          . ' > /dev/null 2>&1' . cString::EOL);
          //в 3:30 каждый месяц 1-го числа
          fwrite($cStream, '30    3       1       *       *       cd ' . dirname(__FILE__) .
          '/cron && php -f tablesize.php > /dev/null 2>&1' . cString::EOL);

          fwrite($cStream, cString::EOL . '#>>>wts jobs<<<' . cString::EOL);
          fclose($cStream);
          
          if($sCurrentUser == $aMap['-u']['val']){
            shell_exec('crontab ' . dirname(__FILE__) . '/cronjobs');
          }
          else{
            shell_exec('crontab -u ' . $aMap['-u']['val']  . ' ' . dirname(__FILE__) . '/cronjobs');
          }
          
          shell_exec('rm -f ' . dirname(__FILE__) . '/cronjobs');
          
          echo 'OK' . cString::EOL . cString::EOL;
        }
      }
    }
    
    if($iType === 10){
    /**
     * Summary
     */
        
      $sURL = 'http://' . gethostname() . '/' . $aMap['-n']['val'] . '/';
      $aHeaders = @get_headers($sURL);
      if(!$aHeaders || $aHeaders[0] === WTS_HTTP_404){
        $sURL = false;
      }
      if($sURL === false){
        $sURL = 'https://' . gethostname() . '/' . $aMap['-n']['val'] . '/';
        $aHeaders = @get_headers($sURL);
        if(!$aHeaders || $aHeaders[0] === WTS_HTTP_404){
          $sURL = false;
        }
      }

      if($sURL !== false){
        echo 'Goto ' . $sURL . cString::EOL;
        echo 'Set up work queue.'. cString::EOL;
        echo 'Add agent.'. cString::EOL;
        echo 'Attach agent to work queue.'. cString::EOL;
        echo 'Done.'. cString::EOL . cString::EOL;
        
        echo 'You can change images' . cString::EOL;
        echo dirname(__FILE__) . '/site/img/logo.png' . cString::EOL;
        echo dirname(__FILE__) . '/site/img/bg.png' . cString::EOL;
        echo dirname(__FILE__) . '/site/img/logo.ico' . cString::EOL;
        echo 'for customise your site.' . cString::EOL . cString::EOL;
      }
      else{
        echo 'Error: Setup can not resolve URL your web server.' . cString::EOL;
        echo '* Check network settings and try Setup again.' . cString::EOL . cString::EOL;
      }
    }
  }
  catch (\Exception $e){
    echo 'Exception ' . $e->GetCode() . ': ' . $e->GetMessage() . cString::EOL . 'In file: ' . basename($e->GetFile()) . cString::EOL . 'line: ' . $e->GetLine() . cString::EOL;
    exit;
  }
  
}
?>
